package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.*
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.inboxmessage.CordialInboxMessageListener
import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.feature.notification.CordialNotificationProcessService
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.Page
import com.cordial.network.request.PageRequest
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import com.cordial.util.JsonUtils.toMutableListOfStrings
import com.cordial.util.TimeUtils
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*
import java.util.concurrent.CountDownLatch

internal class InboxTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var cordialInboxMessageApi: CordialInboxMessageApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    private val accountKey = "qc-all-channels"
    private val channelKey = "push"
    private val host = "https://events-stream-svc.stg.cordialdev.com/"

    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testUrl = "https://www.testurl.test/test1/"
    private var testMcIDs = listOf("testMcID1", "testMcID2", "testMcID3")

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        mockNetworkStateListener.isNetworkAvailable = true
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        cordialInboxMessageApi = CordialInboxMessageApi()
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_inbox_is_fetched_then_inbox_messages_are_caching() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    mockReturnInbox(request, responseHandler, onResponseListener)
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                val inboxMessagesMcIDs = pageInboxMessages.content.map { it.mcID }
                val fetchInboxMessageUseCase =
                    CordialApiConfiguration.getInstance().injection.fetchInboxMessageInjection().fetchInboxMessageUseCase
                fetchInboxMessageUseCase.getCachedInboxMessages { cachedInboxMessages ->
                    val cachedInboxMessagesMcIDs = cachedInboxMessages.map { it.mcID }
                    Assert.assertTrue(
                        "Cached inbox messages don't equal fetched inbox messages",
                        inboxMessagesMcIDs.size == cachedInboxMessagesMcIDs.size &&
                                inboxMessagesMcIDs.containsAll(cachedInboxMessagesMcIDs) &&
                                cachedInboxMessagesMcIDs.containsAll(inboxMessagesMcIDs)
                    )
                    countDownLatch.countDown()
                }
            }, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetched_then_inbox_message_from_cache_returns() {
        val countDownLatch = CountDownLatch(1)
        var isInboxFetched = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        isInboxFetched = true
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else {
                        val response = getInboxMessageContent()
                        Log.d(C.LOG_TAG, "${request.url} : $response")
                        Assert.assertEquals("Inbox message didn't cache", isInboxFetched, false)
                        Assert.assertEquals("Urls don't match", testUrl, request.url)
                        countDownLatch.countDown()
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                isInboxFetched = false
                val inboxMessage = pageInboxMessages.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {},
                    onFailure = {})

            }, {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetched_then_inbox_message_content_is_caching() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else {
                        mockInboxMessageContent(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                val inboxMessage = pageInboxMessages.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = { content ->
                        val fetchInboxMessageContentUseCase = CordialApiConfiguration.getInstance()
                            .injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
                        fetchInboxMessageContentUseCase.getCachedInboxMessageContent(inboxMessage.mcID) { cachedContent ->
                            Assert.assertEquals(
                                "Inbox message content didn't cache",
                                content,
                                cachedContent
                            )
                            countDownLatch.countDown()
                        }
                    },
                    onFailure = {})

            }, {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetching_and_inbox_message_url_is_expired_then_inbox_message_is_updating() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener, isUrlExpired = true)
                    } else if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL)) {
                        countDownLatch.countDown()
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val inboxMessage = page.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {},
                    onFailure = {})
            },
            onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetched_and_access_denied_error_is_returned_then_inbox_message_is_updating_and_content_is_fetching() {
        val countDownLatch = CountDownLatch(1)
        var mcID: String? = null
        var isAccessDenied = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        } else if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL)) {
                            mcID?.let {
                                mockInboxMessage(request, responseHandler, onResponseListener, it)
                            }
                        }
                    } else {
                        if (isAccessDenied) {
                            mockInboxMessageContent(request, responseHandler, onResponseListener)
                        } else {
                            isAccessDenied = true
                            MockResponse.mockAccessDeniedError(request, responseHandler, onResponseListener)
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val inboxMessage = page.content[0]
                mcID = inboxMessage.mcID
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {
                        countDownLatch.countDown()
                    },
                    onFailure = {})
            },
            onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetched_and_error_is_returned_then_error_is_returning_on_failure_callback() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender =
            {
                object : RequestSender {
                    override fun send(
                        request: SDKRequest,
                        responseHandler: ResponseHandler,
                        onResponseListener: OnResponseListener
                    ) {
                        if (request.isCordial) {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        } else {
                            MockResponse.mockInternalServerError(request, responseHandler, onResponseListener)
                        }
                    }
                }
            }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val inboxMessage = page.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {
                    },
                    onFailure = {
                        Assert.assertEquals(
                            "Fetch inbox message content error is invalid",
                            MockResponse.INTERNAL_SERVER_ERROR,
                            it
                        )
                        countDownLatch.countDown()
                    })
            },
            onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_mc_ids_marking_as_read_unread_and_one_is_invalid_then_sdk_deletes_invalid_mc_ids_and_sends_new_request() {
        val countDownLatch = CountDownLatch(1)
        val mcIDsFailed: MutableList<String> = mutableListOf()
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL)) {
                        request.jsonBody?.let { jsonBody ->
                            Log.d(C.LOG_TAG, "${request.url} : $jsonBody")
                            val json = JSONObject(jsonBody)
                            val readIdsJsonArray = JSONArray(json.getString(C.MARK_AS_READ_IDS))
                            val readIDs = readIdsJsonArray.toMutableListOfStrings()
                            val unreadIdsJsonArray = JSONArray(json.getString(C.MARK_AS_UNREAD_IDS))
                            val unreadIDs = unreadIdsJsonArray.toMutableListOfStrings()
                            if (mcIDsFailed.isNotEmpty()) {
                                Assert.assertTrue(
                                    "Failed read mcIDs were not removed from the payload",
                                    !readIDs.contains(mcIDsFailed[0])
                                )
                                Assert.assertTrue(
                                    "Failed unread mcIDs were not removed from the payload",
                                    !unreadIDs.contains(mcIDsFailed[1])
                                )
                                SendingCacheState.updatingInboxMessageReadStatus.set(false)
                                countDownLatch.countDown()
                            } else {
                                mcIDsFailed.add(readIDs[0])
                                mcIDsFailed.add(unreadIDs[0])
                                val response = mockUpdateInboxMessageReadStatusError(mcIDsFailed)
                                MockResponse.mock422Error(request, response, responseHandler, onResponseListener)
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val mcIDs = page.content.map { it.mcID }
                mockNetworkStateListener.isNetworkAvailable = false
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[0]))
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[2]))
                cordialInboxMessageApi.markInboxMessagesUnread(listOf(mcIDs[1]))
                Delay(timeMillis = 200L).doAfterDelay {
                    mockNetworkStateListener.isNetworkAvailable = true
                }
            },
            onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_no_network_and_non_unique_mark_as_read_or_unread_mcIDs_were_cached_then_payload_contains_only_unique_mcIDs() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL)) {
                        checkUpdateInboxMessageReadStatus(request, countDownLatch)
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                mockNetworkStateListener.isNetworkAvailable = false
                val mcIDs = page.content.map { it.mcID }
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[0]))
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[1]))
                cordialInboxMessageApi.markInboxMessagesUnread(listOf(mcIDs[1]))
                cordialInboxMessageApi.markInboxMessagesUnread(listOf(mcIDs[2]))
                Delay(timeMillis = 200L).doAfterDelay {
                    mockNetworkStateListener.isNetworkAvailable = true
                }
            },
            onFailure = {})
        countDownLatch.await()
    }

    private fun checkUpdateInboxMessageReadStatus(
        request: SDKRequest,
        countDownLatch: CountDownLatch
    ) {
        request.jsonBody?.let { jsonBody ->
            Log.d(C.LOG_TAG, "${request.url} : $jsonBody")
            val json = JSONObject(jsonBody)
            val readIdsJsonArray = JSONArray(json.getString(C.MARK_AS_READ_IDS))
            val readIDs = readIdsJsonArray.toMutableListOfStrings()
            val unreadIdsArray = JSONArray(json.getString(C.MARK_AS_UNREAD_IDS))
            val unreadIDs = unreadIdsArray.toMutableListOfStrings()
            val expectedReadIDs = listOf(testMcIDs[0])
            Assert.assertTrue(
                "Wrong read mcIDs content",
                readIDs.size == expectedReadIDs.size &&
                        readIDs.containsAll(expectedReadIDs) &&
                        expectedReadIDs.containsAll(readIDs)
            )
            val expectedUnreadIDs = listOf(testMcIDs[1], testMcIDs[2])
            Assert.assertTrue(
                "Wrong unread mcIDs content",
                unreadIDs.size == expectedUnreadIDs.size &&
                        unreadIDs.containsAll(expectedUnreadIDs) &&
                        expectedUnreadIDs.containsAll(unreadIDs)
            )
            countDownLatch.countDown()
        }
    }

    @Test
    fun given_sdk_initialized_when_push_with_inbox_message_was_received_then_inbox_message_listener_was_triggered() {
        val countDownLatch = CountDownLatch(1)
        val newMcID = testMcIDs[0]
        CordialApiConfiguration.getInstance().inboxMessageListener =
            object : CordialInboxMessageListener {
                override fun newInboxMessageDelivered(mcID: String) {
                    Assert.assertEquals(
                        "New inbox message mcID doesn't match the expected McID",
                        newMcID,
                        mcID
                    )
                    countDownLatch.countDown()
                }
            }
        val remoteMessage = mockPushNotificationWithInboxMessage(newMcID)
        CordialNotificationProcessService().processMessage(context, remoteMessage)
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_then_inbox_message_is_deleting_and_no_network_connection_then_delete_request_is_caching() {
        val countDownLatch = CountDownLatch(1)
        var mcIDToDelete: String? = null
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.DELETE_INBOX_MESSAGE_URL)) {
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)

                        mcIDToDelete?.let {
                            Log.d(C.LOG_TAG, request.url)
                            Assert.assertTrue(
                                "Delete inbox message request doesn't contain the expected mcID",
                                request.url.contains(it)
                            )
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                mockNetworkStateListener.isNetworkAvailable = false
                mcIDToDelete = page.content[0].mcID
                cordialInboxMessageApi.deleteInboxMessage(page.content[0].mcID)
                Delay(timeMillis = 200L).doAfterDelay {
                    mockNetworkStateListener.isNetworkAvailable = true
                }
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_then_inbox_message_was_deleted_then_inbox_message_metadata_and_content_is_deleting() {
        val countDownLatch = CountDownLatch(1)
        var mcIDToDelete: String? = null
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.DELETE_INBOX_MESSAGE_URL)) {
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        Delay(timeMillis = 200L, isIOThread = true).doAfterDelay {
                            mcIDToDelete?.let {
                                val fetchInboxMessageUseCase = CordialApiConfiguration.getInstance()
                                    .injection.fetchInboxMessageInjection().fetchInboxMessageUseCase
                                fetchInboxMessageUseCase.getCachedInboxMessage(it) { inboxMessage ->
                                    Assert.assertEquals(
                                        "Inbox message wasn't deleted from the cache",
                                        null,
                                        inboxMessage
                                    )
                                    val fetchInboxMessageContentUseCase = CordialApiConfiguration.getInstance()
                                        .injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
                                    fetchInboxMessageContentUseCase.getCachedInboxMessageContent(it) { content ->
                                        Assert.assertEquals(
                                            "Inbox message content wasn't deleted from the cache",
                                            null,
                                            content
                                        )
                                        countDownLatch.countDown()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                mcIDToDelete = page.content[0].mcID
                cordialInboxMessageApi.deleteInboxMessage(page.content[0].mcID)
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_inbox_was_fetched_when_inbox_message_content_is_fetched_then_inbox_message_read_event_is_sending() {
        val countDownLatch = CountDownLatch(1)
        val testIndex = 0

        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        } else if (request.url.contains(CordialApiEndpoints.Url.SEND_EVENT)) {
                            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                            request.jsonBody?.let { json ->
                                val jsonObject = JSONArray(json).optJSONObject(testIndex)
                                val eventName = jsonObject.optString(C.EVENT)
                                Assert.assertEquals(
                                    "Event names don't match",
                                    C.EVENT_NAME_INBOX_MESSAGE_READ,
                                    eventName
                                )
                                val mcID = jsonObject.optString(C.MC_ID)
                                Assert.assertEquals(
                                    "McIDs don't match",
                                    testMcIDs[testIndex],
                                    mcID
                                )
                                countDownLatch.countDown()
                            }

                        }
                    } else {
                        mockInboxMessageContent(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                val inboxMessage = pageInboxMessages.content[testIndex]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {
                        cordialInboxMessageApi.sendInboxMessageReadEvent(inboxMessage.mcID)
                    },
                    onFailure = {})
            }, {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_message_content_is_caching_when_max_cache_size_is_reached_then_old_content_cache_is_removing_and_new_content_is_caching() {
        val countDownLatch = CountDownLatch(1)
        CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheSize = 70
        CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheableMessageSize = 70
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else {
                        mockInboxMessageContent(request, responseHandler, onResponseListener)
                    }
                }
            }
        }

        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                val inboxMessage = pageInboxMessages.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {
                        val secondInboxMessage = pageInboxMessages.content[1]
                        cordialInboxMessageApi.fetchInboxMessageContent(
                            secondInboxMessage.mcID,
                            onSuccess = {
                                val fetchInboxMessageContentUseCase = CordialApiConfiguration.getInstance()
                                    .injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
                                fetchInboxMessageContentUseCase.getInboxMessageContentsMetadata { metadataList ->
                                    if (metadataList == null) {
                                        return@getInboxMessageContentsMetadata
                                    }
                                    Assert.assertEquals(
                                        "Old content cache was not deleted",
                                        1,
                                        metadataList.size
                                    )
                                    Assert.assertEquals(
                                        "Cached content's mcID is not as expected",
                                        secondInboxMessage.mcID,
                                        metadataList[0].mcID
                                    )
                                    countDownLatch.countDown()
                                }
                            },
                            onFailure = {})
                    },
                    onFailure = {})

            }, {})
        countDownLatch.await()
    }

    @Test
    fun given_inbox_message_content_is_caching_when_content_cache_size_is_bigger_than_max_cacheable_message_size_then_content_is_not_caching() {
        val countDownLatch = CountDownLatch(1)
        CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheableMessageSize = 10
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    } else {
                        mockInboxMessageContent(request, responseHandler, onResponseListener)
                    }
                }
            }
        }

        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = { pageInboxMessages ->
                val inboxMessage = pageInboxMessages.content[0]
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {
                        val fetchInboxMessageContentUseCase = CordialApiConfiguration.getInstance()
                            .injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
                        fetchInboxMessageContentUseCase.getCachedInboxMessageContent(inboxMessage.mcID) { cachedContent ->
                            Assert.assertEquals(
                                "Exceeded max cacheable message size inbox message content was cached",
                                null,
                                cachedContent
                            )
                            countDownLatch.countDown()
                        }
                    },
                    onFailure = {})

            }, {})
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_inbox_with_filters_is_fetching_then_url_is_correct() {
        val countDownLatch = CountDownLatch(1)
        val filterParams =
            InboxFilterParams(isRead = false, dateBefore = Date(), dateAfter = Date())
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                        request.queryParams?.let { params ->
                            val isRead = params[C.FILTER_IS_READ].toBoolean()
                            Assert.assertEquals(
                                "Filter isRead param is incorrect",
                                filterParams.isRead,
                                isRead
                            )
                            filterParams.dateBefore?.let {
                                val dateBefore = params[C.FILTER_BEFORE_DATE]
                                Assert.assertEquals(
                                    "Filter dateBefore param is incorrect",
                                    TimeUtils.getTimestamp(it, isMillis = true),
                                    dateBefore
                                )
                            }
                            filterParams.dateAfter?.let {
                                val dateAfter = params[C.FILTER_AFTER_DATE]
                                Assert.assertEquals(
                                    "Filter dateAfter param is incorrect",
                                    TimeUtils.getTimestamp(it, isMillis = true),
                                    dateAfter
                                )
                            }
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, filterParams,
            onSuccess = {}, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_custom_message_hub_host_is_set_when_inbox_is_fetching_then_message_hub_host_is_changed() {
        val countDownLatch = CountDownLatch(1)
        val customMessageHubHost = "https://custom-message-hub-host.com"
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    val url = request.url
                    Log.d(C.LOG_TAG, "${request.methodOfRequest} $url : ${request.jsonBody}")
                    Assert.assertTrue("Message hub host was not changed", url.contains(customMessageHubHost))
                    countDownLatch.countDown()
                }
            }
        }
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, host, customMessageHubHost)
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = {}, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_custom_message_hub_host_is_set_when_new_custom_message_hub_inbox_is_setting_to_null_then_inbox_is_fetching_and_message_hub_host_is_set_by_default() {
        val countDownLatch = CountDownLatch(1)
        val customMessageHubHost = "https://custom-m-hub-host.com"
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, host, customMessageHubHost)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    val url = request.url
                    Log.d(C.LOG_TAG, "${request.methodOfRequest} $url : ${request.jsonBody}")
                    Assert.assertTrue("Custom message hub host was not removed", url.contains(C.MESSAGE_HUB))
                    countDownLatch.countDown()
                }
            }
        }
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, host)
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null,
            onSuccess = {}, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_mcID_of_inbox_message_when_inbox_message_is_fetching_then_inbox_message_is_fetched() {
        val countDownLatch = CountDownLatch(1)
        val mcID = testMcIDs[1]
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial && request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL)) {
                        mockInboxMessage(request, responseHandler, onResponseListener, mcID)
                    }
                }

            }
        }
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { message ->
            Assert.assertEquals("Inbox message's mcID is invalid", mcID, message.mcID)
            countDownLatch.countDown()
        }, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_guest_contact_logged_in_when_inbox_message_is_fetching_then_inbox_message_url_for_guest_contact_is_correct() {
        val countDownLatch = CountDownLatch(1)
        val mcID = testMcIDs[1]
        testCordialApi.savePrimaryKeyAndLoginState(context, null)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial && request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL)) {
                        val config = CordialApiConfiguration.getInstance()
                        val guestContact = "${config.getChannelKey()}:${cordialApi.getFirebaseToken()}"
                        Assert.assertTrue(
                            "Inbox message url for guest contact is invalid",
                            request.url.contains(guestContact)
                        )
                        mockInboxMessage(request, responseHandler, onResponseListener, mcID)
                    }
                }

            }
        }
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { message ->
            Assert.assertEquals("Inbox message's mcID is invalid", mcID, message.mcID)
            countDownLatch.countDown()
        }, onFailure = {})
        countDownLatch.await()
    }


    @Test
    fun given_guest_contact_logged_in_when_inbox_is_fetching_then_inbox_url_for_guest_contact_is_correct() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.savePrimaryKeyAndLoginState(context, null)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial && request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        val config = CordialApiConfiguration.getInstance()
                        val guestContact = "${config.getChannelKey()}:${cordialApi.getFirebaseToken()}"
                        Assert.assertTrue(
                            "Inbox url for guest contact is invalid",
                            request.url.contains(guestContact)
                        )
                        countDownLatch.countDown()
                    }
                }

            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, onSuccess = {}, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_delete_inbox_message_is_sending_and_caching_then_jwt_token_is_updating_and_delete_inbox_message_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var mcIDToDelete: String? = null
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.DELETE_INBOX_MESSAGE_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                            mcIDToDelete?.let {
                                Log.d(C.LOG_TAG, request.url)
                                Assert.assertTrue(
                                    "Delete inbox message request doesn't contain the expected mcID",
                                    request.url.contains(it)
                                )
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                testCordialApi.clearJwtToken(context)
                mcIDToDelete = page.content[0].mcID
                cordialInboxMessageApi.deleteInboxMessage(page.content[0].mcID)
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_delete_inbox_message_is_sending_when_jwt_token_is_expired_then_delete_inbox_message_is_caching_then_jwt_token_is_updating_and_delete_inbox_message_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        var mcIDToDelete: String? = null
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.DELETE_INBOX_MESSAGE_URL) -> {
                            if (isJwtTokenExpired) {
                                isJwtTokenExpired = false
                                MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            } else {
                                MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                                mcIDToDelete?.let {
                                    Log.d(C.LOG_TAG, request.url)
                                    Assert.assertTrue(
                                        "Delete inbox message request doesn't contain the expected mcID",
                                        request.url.contains(it)
                                    )
                                    countDownLatch.countDown()
                                }
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                mcIDToDelete = page.content[0].mcID
                cordialInboxMessageApi.deleteInboxMessage(page.content[0].mcID)
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_delete_inbox_message_is_sending_and_caching_then_contact_is_logging_in_and_delete_inbox_message_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var mcIDToDelete: String? = null
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.DELETE_INBOX_MESSAGE_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                            mcIDToDelete?.let {
                                Log.d(C.LOG_TAG, request.url)
                                Assert.assertTrue(
                                    "Delete inbox message request doesn't contain the expected mcID",
                                    request.url.contains(it)
                                )
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                testCordialApi.removePrimaryKeyAndLoginState(context)
                mcIDToDelete = page.content[0].mcID
                cordialInboxMessageApi.deleteInboxMessage(page.content[0].mcID)
                Delay(timeMillis = 100L).doAfterDelay {
                    cordialApi.setContact(testContact)
                }
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_update_inbox_message_read_status_is_sending_and_caching_then_jwt_token_is_updating_and_update_inbox_message_read_status_is_sent() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL) -> {
                            checkMarkInboxMessageAsRead(request, countDownLatch, responseHandler, onResponseListener)
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                testCordialApi.clearJwtToken(context)
                val mcIDs = page.content.map { it.mcID }
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[0]))
            },
            onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_update_inbox_message_read_status_is_sending_when_jwt_token_is_expired_then_update_inbox_message_read_status_is_caching_then_jwt_token_is_updating_and_update_inbox_message_read_status_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL) -> {
                            if (isJwtTokenExpired) {
                                isJwtTokenExpired = false
                                MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            } else {
                                checkMarkInboxMessageAsRead(
                                    request,
                                    countDownLatch,
                                    responseHandler,
                                    onResponseListener
                                )
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val mcIDs = page.content.map { it.mcID }
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[0]))
            },
            onFailure = {})
        countDownLatch.await()
    }


    @Test
    fun given_contact_is_not_logged_in_when_update_inbox_message_read_status_is_sending_and_caching_then_contact_is_logging_in_and_update_inbox_message_read_status_is_sent() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL) -> {
                            checkMarkInboxMessageAsRead(request, countDownLatch, responseHandler, onResponseListener)
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                testCordialApi.removePrimaryKeyAndLoginState(context)
                val mcIDs = page.content.map { it.mcID }
                cordialInboxMessageApi.markInboxMessagesRead(listOf(mcIDs[0]))
                Delay(timeMillis = 100L).doAfterDelay {
                    cordialApi.setContact(testContact)
                }
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    private fun checkMarkInboxMessageAsRead(
        request: SDKRequest,
        countDownLatch: CountDownLatch,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        request.jsonBody?.let { jsonBody ->
            Log.d(C.LOG_TAG, "${request.url} : $jsonBody")
            val json = JSONObject(jsonBody)
            val readIdsJsonArray = JSONArray(json.getString(C.MARK_AS_READ_IDS))
            val readIDs = readIdsJsonArray.toMutableListOfStrings()
            val expectedReadIDs = listOf(testMcIDs[0])
            Assert.assertTrue(
                "Wrong read mcIDs content",
                readIDs.size == expectedReadIDs.size &&
                        readIDs.containsAll(expectedReadIDs) &&
                        expectedReadIDs.containsAll(readIDs)
            )
            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
            countDownLatch.countDown()
        }
    }

    @Test
    fun given_no_jwt_token_when_inbox_is_fetching_then_jwt_token_is_updating_and_inbox_is_fetched() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            mockReturnInbox(request, responseHandler, onResponseListener)
                        }
                    }
                }
            }
        }
        testCordialApi.clearJwtToken(context)
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                checkFetchInbox(page, countDownLatch)
            },
            onFailure = {}
        )
        countDownLatch.await()
    }


    @Test
    fun given_inbox_is_fetching_when_jwt_token_is_expired_then_jwt_token_is_updating_and_inbox_is_fetched() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL) -> {
                            if (isJwtTokenExpired) {
                                isJwtTokenExpired = false
                                MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            } else {
                                mockReturnInbox(request, responseHandler, onResponseListener)
                            }
                        }
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = { page ->
                checkFetchInbox(page, countDownLatch)
            },
            onFailure = {}
        )
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_inbox_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.removePrimaryKeyAndLoginState(context)
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = {
            },
            onFailure = { error ->
                val expectedError = C.FETCH_INBOX_CONTACT_IS_NOT_REGISTERED_ERROR
                Assert.assertEquals(
                    "Expected error is not match actual one",
                    expectedError,
                    error
                )
                countDownLatch.countDown()
            }
        )
        countDownLatch.await()
    }


    @Test
    fun given_no_network_connection_when_inbox_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            inboxFilterParams = null,
            onSuccess = {
            },
            onFailure = { error ->
                val expectedError = C.NO_NETWORK_ERROR
                Assert.assertEquals(
                    "Expected error is not match actual one",
                    expectedError,
                    error
                )
                countDownLatch.countDown()
            }
        )
        countDownLatch.await()
    }

    private fun checkFetchInbox(
        page: Page<InboxMessage>,
        countDownLatch: CountDownLatch
    ) {
        val inboxMessagesMcIDs = page.content.map { it.mcID }
        Assert.assertTrue(
            "Fetch inbox messages error",
            inboxMessagesMcIDs.size == testMcIDs.size &&
                    inboxMessagesMcIDs.containsAll(testMcIDs) &&
                    testMcIDs.containsAll(inboxMessagesMcIDs)
        )
        countDownLatch.countDown()
    }


    @Test
    fun given_no_jwt_token_when_inbox_message_is_fetching_then_jwt_token_is_updating_and_inbox_message_is_fetched() {
        val countDownLatch = CountDownLatch(1)
        val mcID = testMcIDs[1]
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.isCordial && request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL) -> {
                            mockInboxMessage(request, responseHandler, onResponseListener, mcID)
                        }
                    }
                }
            }
        }
        testCordialApi.clearJwtToken(context)
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { message ->
            Assert.assertEquals("Inbox message's mcID is invalid", mcID, message.mcID)
            countDownLatch.countDown()
        }, onFailure = {})
        countDownLatch.await()
    }


    @Test
    fun given_inbox_message_is_fetching_when_jwt_token_is_expired_then_jwt_token_is_updating_and_inbox_message_is_fetched() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        val mcID = testMcIDs[1]
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    when {
                        request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL) -> {
                            MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                        }
                        request.isCordial && request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGE_URL) -> {
                            if (isJwtTokenExpired) {
                                isJwtTokenExpired = false
                                MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            } else {
                                mockInboxMessage(request, responseHandler, onResponseListener, mcID)
                            }
                        }
                    }
                }
            }
        }
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { message ->
            Assert.assertEquals("Inbox message's mcID is invalid", mcID, message.mcID)
            countDownLatch.countDown()
        }, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_inbox_message_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.removePrimaryKeyAndLoginState(context)
        val mcID = testMcIDs[1]
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { },
            onFailure = { error ->
                val expectedError = C.FETCH_INBOX_MESSAGE_CONTACT_IS_NOT_REGISTERED_ERROR
                Assert.assertEquals(
                    "Expected error is not match actual one",
                    expectedError,
                    error
                )
                countDownLatch.countDown()
            }
        )
        countDownLatch.await()
    }

    @Test
    fun given_no_network_connection_when_inbox_message_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        val mcID = testMcIDs[1]
        cordialInboxMessageApi.fetchInboxMessage(mcID, onSuccess = { },
            onFailure = { error ->
                val expectedError = C.NO_NETWORK_ERROR
                Assert.assertEquals(
                    "Expected error is not match actual one",
                    expectedError,
                    error
                )
                countDownLatch.countDown()
            }
        )
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_inbox_message_content_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(pageRequest, null, onSuccess = { page ->
            val inboxMessage = page.content[0]
            testCordialApi.removePrimaryKeyAndLoginState(context)
            cordialInboxMessageApi.fetchInboxMessageContent(
                inboxMessage.mcID,
                onSuccess = {},
                onFailure = { error ->
                    val expectedError = C.FETCH_INBOX_MESSAGE_CONTENT_CONTACT_IS_NOT_REGISTERED_ERROR
                    Assert.assertEquals(
                        "Expected error is not match actual one",
                        expectedError,
                        error
                    )
                    countDownLatch.countDown()
                })
        }, onFailure = {})
        countDownLatch.await()
    }

    @Test
    fun given_no_network_connection_when_inbox_message_content_is_fetching_then_on_failure_callback_is_called() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.GET_INBOX_MESSAGES_URL)) {
                        mockReturnInbox(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val pageRequest = PageRequest(page = 1, size = 10)
        cordialInboxMessageApi.fetchInboxMessages(
            pageRequest,
            null,
            onSuccess = { page ->
                val inboxMessage = page.content[0]
                mockNetworkStateListener.isNetworkAvailable = false
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessage.mcID,
                    onSuccess = {},
                    onFailure = { error ->
                        val expectedError = C.NO_NETWORK_ERROR
                        Assert.assertEquals(
                            "Expected error is not match actual one",
                            expectedError,
                            error
                        )
                        countDownLatch.countDown()
                    })
            }, onFailure = {}
        )
        countDownLatch.await()
    }

    private fun mockReturnInbox(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener,
        isUrlExpired: Boolean = false
    ) {
        val response = getInbox(isUrlExpired)
        MockResponse.mock200Response(request, response, responseHandler, onResponseListener)
    }

    private fun mockInboxMessage(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener,
        mcID: String
    ) {
        val response = getInboxMessage(mcID)
        MockResponse.mock200Response(request, response, responseHandler, onResponseListener)
    }

    private fun mockInboxMessageContent(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val response = getInboxMessageContent()
        MockResponse.mock200Response(request, response, responseHandler, onResponseListener)
    }

    private fun getInbox(isUrlExpired: Boolean = false): String {
        val urlExpireAt =
            if (isUrlExpired) "2020-12-03T05:12:05.000Z" else "2120-12-03T05:12:05.000Z"
        return "{\"success\":true,\"total\":2,\"perPage\":10,\"currentPage\":1,\"lastPage\":1, \"messages\":[" +
                "{\"_id\":\"${testMcIDs[0]}\",\"title\":\"none\",\"read\":false,\"downloaded\":false,\"sentAt\":\"2020-12-02T17:12:02.000Z\",\"url\":\"$testUrl\",\"urlExpireAt\":\"$urlExpireAt\"}," +
                "{\"_id\":\"${testMcIDs[1]}\",\"title\":\"none\",\"read\":false,\"downloaded\":true,\"sentAt\":\"2020-12-02T17:10:54.000Z\",\"url\":\"https://www.testurl.test/test2/\",\"urlExpireAt\":\"$urlExpireAt\"}," +
                "{\"_id\":\"${testMcIDs[2]}\",\"title\":\"none\",\"read\":false,\"downloaded\":true,\"sentAt\":\"2020-12-02T17:10:54.000Z\",\"url\":\"https://www.testurl.test/test3/\",\"urlExpireAt\":\"$urlExpireAt\"}]}"
    }

    private fun getInboxMessage(mcID: String): String {
        return "{\"message\":" +
                "{\"_id\":\"$mcID\",\"title\":\"none\",\"read\":false,\"downloaded\":false,\"sentAt\":\"2020-12-02T17:12:02.000Z\",\"url\":\"$testUrl\",\"urlExpireAt\":\"2120-12-03T05:12:05.000Z\"}}"
    }

    private fun getInboxMessageContent(): String {
        return "{\"html\":\"test\",\"customKeyValuePairs\":[]}"
    }

    private fun mockUpdateInboxMessageReadStatusError(invalidMcIDs: MutableList<String>): String {
        return "{\"success\":false,\"error\":{\"code\":422,\"message\":\"The given data was invalid.\"," +
                "\"errors\":{\"markAsReadIds.0\":[\"Unable to decrypt a mcID value from ${invalidMcIDs[0]}.\"]," +
                "\"markAsUnReadIds.0\":[\"Unable to decrypt a mcID value from ${invalidMcIDs[1]}.\"]}}}"
    }

    private fun mockPushNotificationWithInboxMessage(mcID: String): RemoteMessage {
        val remoteMessageBuilder = RemoteMessage.Builder("test@gcm.googleapis.com")
        val data = mutableMapOf(
            "mcID" to mcID,
        )
        data["system"] =
            "{\"inbox\":{\"expirationTime\":\"2121-01-11T11:45:33Z\"}}\"}"
        remoteMessageBuilder.setData(data)
        return remoteMessageBuilder.build()
    }
}