package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import org.json.JSONArray
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class MessageAttributionTests {
    lateinit var context: Context
    lateinit var testCordialApi: TestCordialApi
    lateinit var cordialApi: CordialApi
    lateinit var hostSettingsChangedDownLatch: CountDownLatch
    private val accountKey = "qc-all-channels"
    private val channelKey = "push"
    private val host = "https://events-stream-svc.stg.cordialdev.com/"
    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testAnotherContact = "test2@example.com"
    private val testMcID = "testMcID"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        initializeSdk()
        cordialApi.setMcID(testMcID)
        val countDownLatch = CountDownLatch(1)
        val sendEventUseCase = CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_unset_contact_then_message_attribution_cleared() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    when {
                        request.url.contains(CordialApiEndpoints.Url.CONTACT_LOGOUT_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)

                            testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
                            cordialApi.sendEvent("testEventName1", null)
                            cordialApi.sendEvent("testEventName2", null)
                            cordialApi.sendEvent("testEventName3", null)
                        }
                        request.url.contains(CordialApiEndpoints.Url.SEND_EVENT) -> {
                            request.jsonBody?.let { json ->
                                val jsonArray = JSONArray(json)
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject = jsonArray.optJSONObject(i)
                                    val mcID = jsonObject.optString("mcID")
                                    Assert.assertEquals(
                                        "mcID of custom event didn't cleared",
                                        "",
                                        mcID
                                    )
                                }
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        cordialApi.unsetContact()
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_set_another_contact_then_message_attribution_cleared() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    when {
                        request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)

                            cordialApi.sendEvent("testEventName1", null)
                            cordialApi.sendEvent("testEventName2", null)
                            cordialApi.sendEvent("testEventName3", null)
                        }
                        request.url.contains(CordialApiEndpoints.Url.SEND_EVENT) -> {
                            request.jsonBody?.let { json ->
                                val jsonArray = JSONArray(json)
                                for (i in 0 until jsonArray.length()) {
                                    val jsonObject = jsonArray.optJSONObject(i)
                                    val mcID = jsonObject.optString("mcID")
                                    Assert.assertEquals(
                                        "mcID of custom event didn't cleared",
                                        "",
                                        mcID
                                    )
                                }
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        cordialApi.setContact(testAnotherContact)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_account_key_changed_then_message_attribution_cleared() {
        hostSettingsChangedDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender =
            { HostSettingsChangedMockRequestSender() }

        val accountKey = "test"
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, host)
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        hostSettingsChangedDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_channel_key_changed_then_message_attribution_cleared() {
        hostSettingsChangedDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender =
            { HostSettingsChangedMockRequestSender() }

        val channelKey = "test"
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, host)
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        hostSettingsChangedDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_base_url_changed_then_message_attribution_cleared() {
        hostSettingsChangedDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender =
            { HostSettingsChangedMockRequestSender() }

        val baseUrl = "test"
        CordialApiConfiguration.getInstance().initialize(context, accountKey, channelKey, baseUrl)
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        hostSettingsChangedDownLatch.await()
    }

    inner class HostSettingsChangedMockRequestSender : RequestSender {

        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")

            request.jsonBody?.let { json ->
                val jsonArray = JSONArray(json)
                for (i in 0 until jsonArray.length()) {
                    val jsonObject = jsonArray.optJSONObject(i)
                    val mcID = jsonObject.optString("mcID")
                    Assert.assertEquals(
                        "mcID of custom event didn't cleared",
                        "",
                        mcID
                    )
                }
                hostSettingsChangedDownLatch.countDown()
            }
        }
    }
}