package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.mock.MockNotificationStatusRequestor
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.upsertcontact.model.NotificationStatus
import com.cordial.feature.upsertcontact.model.attributes.*
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import com.cordial.util.JsonUtils
import com.cordial.util.TimeUtils
import com.cordial.util.toMutableList
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.*
import java.util.concurrent.CountDownLatch

internal class UpsertContactTests {
    lateinit var context: Context
    lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    lateinit var notificationStatusChangedCountDownLatch: CountDownLatch

    private var testContact = "test@example.com"
    private var anotherTestContact = "test2@example.com"
    private var testCustomEvent = "testCustomEvent"
    private val testAttrs = mapOf(
        "city" to StringValue("Kyiv"),
        "age" to NumericValue(22),
        "weight" to NumericValue(68.9),
        "bool" to BooleanValue(true),
        "favorite-colors" to ArrayValue(arrayOf("red", "green", "blue")),
        "birthdate" to DateValue(Date()),
        "geo" to getGeoValue()
    )
    private val testToken = "test token21"
    private var isNotificationsEnabled = false

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.skipAppInstallEvent(context)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        CordialApiConfiguration.getInstance().initializeContext(context)
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.eventsBulkSize = 1
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_set_contact_is_called_then_json_has_all_fields() {
        testCordialApi.createFirebaseToken(context, testToken)
        val countDownLatch = CountDownLatch(1)
        var sendMethodCallCounter = 0
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    sendMethodCallCounter++
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).getJSONObject(0)
                        val primaryKey = jsonObject.optString("primaryKey")
                        Assert.assertEquals(
                            "Set contact test failed: primary key is invalid",
                            testContact,
                            primaryKey
                        )
                        Assert.assertEquals(
                            "Set contact test failed: deviceId is not presented in the JSON",
                            true,
                            jsonObject.has("deviceId")
                        )
                        Assert.assertEquals(
                            "Set contact test failed: status is not presented in the JSON",
                            true,
                            jsonObject.has("status")
                        )
                        Assert.assertEquals(
                            "Push notification token is not present in upsert contact JSON",
                            testToken,
                            jsonObject.optString("token")
                        )
                        Assert.assertEquals(
                            "Contact was sent more than once",
                            sendMethodCallCounter,
                            1
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        initializeSdk()
        testCordialApi.clearFirebaseToken(context)
        cordialApi.setContact(testContact)
        testCordialApi.createFirebaseToken(context, testToken)
        cordialApi.setContact(testContact)
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_registered_when_upsert_contact_is_called_then_json_has_attributes() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        Assert.assertEquals(
                            "Push notification token is not present in upsert contact JSON",
                            testToken,
                            jsonObject.optString("token")
                        )
                        checkContactAttributes(jsonObject, request, responseHandler, onResponseListener, countDownLatch)
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        initializeSdk()
        testCordialApi.savePrimaryKey(context, testContact)
        cordialApi.upsertContact(testAttrs)
        countDownLatch.await()
    }

    @Test
    fun given_contact_is_registered_and_no_network_connection_when_upsert_contact_is_cached_then_network_available_upsert_is_sending_and_json_has_attributes() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        Assert.assertEquals(
                            "Push notification token is not present in upsert contact JSON",
                            testToken,
                            jsonObject.optString("token")
                        )
                        checkContactAttributes(jsonObject, request, responseHandler, onResponseListener, countDownLatch)
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        initializeSdk()
        testCordialApi.savePrimaryKey(context, testContact)
        mockNetworkStateListener.isNetworkAvailable = false
        cordialApi.upsertContact(testAttrs)
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        countDownLatch.await()
    }

    @Test
    fun given_no_network_connection_when_set_contact_and_send_event_are_cached_then_network_available_and_upsert_is_sending_and_event_is_sending_too() {
        val countDownLatch = CountDownLatch(1)
        val testEvent = "testEvent"
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        when {
                            request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                                MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                            }
                            request.url.contains(CordialApiEndpoints.Url.SEND_EVENT) -> {
                                val jsonObject = JSONArray(json).optJSONObject(0)
                                val event = jsonObject.optString("event")
                                Assert.assertEquals("Event name is invalid", testEvent, event)
                                MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        initializeSdk()
        mockNetworkStateListener.isNetworkAvailable = false
        cordialApi.upsertContact(testAttrs)
        cordialApi.sendEvent(testEvent, properties = null)
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        countDownLatch.await()
    }

    private fun checkContactAttributes(
        jsonObject: JSONObject,
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener,
        countDownLatch: CountDownLatch
    ) {
        if (jsonObject.has("attributes")) {
            val jsonAttrs = jsonObject.getJSONObject("attributes")

            val stringValue = jsonAttrs.optString("city")
            val numericValue = jsonAttrs.optDouble("age")
            val doubleValue = jsonAttrs.optDouble("weight")
            val booleanValue = jsonAttrs.optBoolean("bool")
            val arrayValue =
                (jsonAttrs.optJSONArray("favorite-colors")
                    ?: JSONArray()).toMutableList()
                    .toTypedArray()
            val dateValue = jsonAttrs.optString("birthdate")
            val geoValue =
                JsonUtils.getGeoAttributeValueFromJson(jsonAttrs.optJSONObject("geo"))
            val geoValueArr = arrayOf(
                geoValue.streetAddress,
                geoValue.streetAddress2,
                geoValue.postalCode,
                geoValue.country,
                geoValue.state,
                geoValue.city,
                geoValue.timezone
            )

            val testStringValue = (testAttrs["city"] as StringValue).value
            val testNumericValue = (testAttrs["age"] as NumericValue).value
            val testDoubleNumericValue = (testAttrs["weight"] as NumericValue).value
            val testBooleanValue = (testAttrs["bool"] as BooleanValue).value
            val testArrayValue = (testAttrs["favorite-colors"] as ArrayValue).value
            val testDateValue =
                TimeUtils.getTimestamp((testAttrs["birthdate"] as DateValue).date)
            val testGeoValue = testAttrs["geo"] as GeoValue
            val testGeoValueArr = arrayOf(
                testGeoValue.streetAddress,
                testGeoValue.streetAddress2,
                testGeoValue.postalCode,
                testGeoValue.country,
                testGeoValue.state,
                testGeoValue.city,
                testGeoValue.timezone
            )

            Assert.assertEquals(
                "Upsert contact attributes string value error",
                testStringValue,
                stringValue
            )
            Assert.assertEquals(
                "Upsert contact attributes numeric value error",
                testNumericValue,
                numericValue,
                0.1
            )
            Assert.assertEquals(
                "Upsert contact attributes double numeric value error",
                testDoubleNumericValue,
                doubleValue,
                0.1
            )
            Assert.assertEquals(
                "Upsert contact attributes boolean value error",
                testBooleanValue,
                booleanValue
            )
            Assert.assertArrayEquals(
                "Upsert contact attributes array value error",
                testArrayValue,
                arrayValue
            )
            Assert.assertEquals(
                "Upsert contact attributes date value error",
                testDateValue,
                dateValue
            )
            Assert.assertArrayEquals(
                "Upsert contact attributes geo value error",
                testGeoValueArr,
                geoValueArr
            )
            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
            countDownLatch.countDown()
        }
    }

    @Test
    fun given_contact_registered_when_notification_status_changed_to_opt_in_then_set_contact_is_sent() {
        notificationStatusChangedCountDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { NotificationStatusChangedMockRequestSender() }
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.savePrimaryKey(context, testContact)
        initializeSdk()

        isNotificationsEnabled = true
        testCordialApi.setNotificationStatus(context, !isNotificationsEnabled)
        DependencyConfiguration.getInstance().notificationStatusRequester =
            MockNotificationStatusRequestor(isNotificationsEnabled)
        CordialApiConfiguration.getInstance().onAppForegrounded()

        notificationStatusChangedCountDownLatch.await()
    }

    @Test
    fun given_contact_is_registered_when_notification_status_changed_to_opt_out_then_set_contact_is_sent() {
        notificationStatusChangedCountDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { NotificationStatusChangedMockRequestSender() }
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.savePrimaryKey(context, testContact)
        initializeSdk()

        isNotificationsEnabled = false
        testCordialApi.setNotificationStatus(context, !isNotificationsEnabled)
        DependencyConfiguration.getInstance().notificationStatusRequester =
            MockNotificationStatusRequestor(isNotificationsEnabled)
        CordialApiConfiguration.getInstance().onAppForegrounded()

        notificationStatusChangedCountDownLatch.await()
    }

    @Test
    fun given_contact_is_registered_and_events_are_cached_when_another_contact_is_logged_in_then_cache_is_cleared() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).getJSONObject(0)
                        val primaryKey = jsonObject.optString("primaryKey")
                        Assert.assertEquals(
                            "Set contact test failed: primary key is invalid",
                            anotherTestContact,
                            primaryKey
                        )
                        Assert.assertEquals(
                            "Push notification token is not present in upsert contact JSON",
                            testToken,
                            jsonObject.optString("token")
                        )
                        val previousPrimaryKey = testCordialApi.getPreviousPrimaryKey(context)
                        Assert.assertTrue(
                            "Previous primary key is not cleared",
                            previousPrimaryKey.isEmpty()
                        )
                        val sendEventUseCase =
                            CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
                        sendEventUseCase.getAllCachedEvents { events ->
                            Assert.assertTrue(
                                "Events cache is not dropped",
                                events.isEmpty()
                            )
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        initializeSdk()
        CordialApiConfiguration.getInstance().eventsBulkSize = 2

        cordialApi.sendEvent(testCustomEvent, null)
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.setContact(anotherTestContact)
        }
        countDownLatch.await()
    }

    @Test
    fun given_events_are_cached_when_contact_is_logged_in_then_cache_is_sent() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    when {
                        request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                            request.jsonBody?.let { json ->
                                val jsonObject = JSONArray(json).getJSONObject(0)
                                val primaryKey = jsonObject.optString("primaryKey")
                                Assert.assertEquals(
                                    "Set contact test failed: primary key is invalid",
                                    testContact,
                                    primaryKey
                                )
                                Assert.assertEquals(
                                    "Push notification token is not present in upsert contact JSON",
                                    testToken,
                                    jsonObject.optString("token")
                                )
                                MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                            }
                        }
                        else -> {
                            request.jsonBody?.let { json ->
                                val jsonObject = JSONArray(json).getJSONObject(0)
                                val eventName = jsonObject.optString("event")
                                Assert.assertEquals(
                                    "Set contact test failed: event name is invalid",
                                    testCustomEvent,
                                    eventName
                                )
                                SendingCacheState.sendingEvents.set(false)
                                countDownLatch.countDown()
                            }
                        }
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        initializeSdk()

        cordialApi.sendEvent(testCustomEvent, null)
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.setContact(testContact)
        }
        countDownLatch.await()
    }

    @Test
    fun given_last_set_contact_was_24_hours_ago_when_app_goes_to_the_foreground_then_contact_self_healing_is_sending() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.savePrimaryKey(context, testContact)
        val lastSetContactTimestamp = "2021-07-18T14:35:22Z"
        testCordialApi.saveSetContactTimestamp(context, lastSetContactTimestamp)
        initializeSdk()
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).getJSONObject(0)
                        val primaryKey = jsonObject.optString("primaryKey")
                        Assert.assertEquals(
                            "Set contact test failed: primary key is invalid",
                            testContact,
                            primaryKey
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().onAppForegrounded()
        countDownLatch.await()
    }

    private fun getGeoValue(): GeoValue {
        val geoValue = GeoValue()
        geoValue.streetAddress = "Velyka Vasylkivska 2"
        geoValue.streetAddress2 = "Ivana Kudri 3"
        geoValue.postalCode = "12323"
        geoValue.country = "Ukraine"
        geoValue.state = "Kyivska oblast"
        geoValue.city = "Kyiv"
        geoValue.timezone = "GMT+2"
        return geoValue
    }

    inner class NotificationStatusChangedMockRequestSender : RequestSender {
        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
            when {
                request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL) -> {
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        val isOptInStatus =
                            jsonObject.optString("status") == NotificationStatus.OPT_IN.status
                        Assert.assertEquals(
                            "Notification status is invalid",
                            isNotificationsEnabled,
                            isOptInStatus
                        )
                        Assert.assertEquals(
                            "Push notification token is not present in upsert contact JSON",
                            testToken,
                            jsonObject.optString("token")
                        )
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                    }
                }
                request.url.contains(CordialApiEndpoints.Url.SEND_EVENT) -> {
                    SendingCacheState.sendingEvents.set(false)
                    notificationStatusChangedCountDownLatch.countDown()
                }
            }
        }
    }

    @Test
    fun given_upsert_contact_sending_when_jwt_token_is_expired_then_contact_is_caching_then_jwt_token_is_updating_and_upsert_contact_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL)) {
                        if (isJwtTokenExpired) {
                            MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            isJwtTokenExpired = false
                        } else {
                            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                            request.jsonBody?.let { json ->
                                val jsonObject = JSONArray(json).optJSONObject(0)
                                Assert.assertEquals(
                                    "Push notification token is not present in upsert contact JSON",
                                    testToken,
                                    jsonObject.optString("token")
                                )
                                checkContactAttributes(
                                    jsonObject,
                                    request,
                                    responseHandler,
                                    onResponseListener,
                                    countDownLatch
                                )
                            }
                        }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        testCordialApi.createFirebaseToken(context, testToken)
        initializeSdk()
        testCordialApi.savePrimaryKey(context, testContact)
        cordialApi.upsertContact(testAttrs)
        countDownLatch.await()
    }
}