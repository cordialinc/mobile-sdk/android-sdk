package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.Delay
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class UnsetContactTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    private val testToken = "test token21"
    private var testContact = "test@example.com"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_unset_contact_with_primary_key_then_contact_is_unregister() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONObject(json)
                        val primaryKey = jsonObject.optString("primaryKey")
                        Assert.assertEquals(
                            "Primary key of unset contact is not present in the json or invalid",
                            testContact,
                            primaryKey
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        cordialApi.unsetContact()
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_unset_guest_contact_then_contact_is_unregister() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONObject(json)
                        val primaryKey = jsonObject.optString("primaryKey")
                        Assert.assertEquals(
                            "Primary key of unset contact is present in the json",
                            "",
                            primaryKey
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        testCordialApi.saveLoginState(context)
        cordialApi.unsetContact()
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_unset_contact_with_primary_key_is_cached_then_contact_is_unregister() {
        val countDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    checkUnsetContact(request, responseHandler, onResponseListener, countDownLatch)
                }
            }
        }
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        cordialApi.unsetContact()
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        countDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_unset_contact_is_sending_and_caching_then_jwt_token_is_updating_and_unset_contact_is_sent() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.clearJwtToken(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.CONTACT_LOGOUT_URL)) {
                        checkUnsetContact(request, responseHandler, onResponseListener, countDownLatch)
                    }
                }
            }
        }
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        cordialApi.unsetContact()
        countDownLatch.await()
    }

    @Test
    fun given_unset_contact_is_sending_when_jwt_token_is_expired_then_unset_contact_is_caching_then_jwt_token_is_updating_and_unset_contact_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACT_LOGOUT_URL)) {
                        if (isJwtTokenExpired) {
                            MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            isJwtTokenExpired = false
                        } else {
                            checkUnsetContact(request, responseHandler, onResponseListener, countDownLatch)
                        }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        cordialApi.unsetContact()
        countDownLatch.await()
    }

    private fun checkUnsetContact(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener,
        countDownLatch: CountDownLatch
    ) {
        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
        request.jsonBody?.let { json ->
            val jsonObject = JSONObject(json)
            val primaryKey = jsonObject.optString("primaryKey")
            Assert.assertEquals(
                "Primary key of unset contact is not present in the json or invalid",
                testContact,
                primaryKey
            )
            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
            countDownLatch.countDown()
        }
    }
}