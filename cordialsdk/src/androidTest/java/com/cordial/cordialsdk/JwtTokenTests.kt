package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import org.json.JSONArray
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class JwtTokenTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    lateinit var countDownLatch: CountDownLatch

    private var testContact = "testJwt@example.com"
    private val testToken = "test token21"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.clearJwtToken(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey)
        cordialApi = CordialApi()
    }

    @After
    fun clearCache() {
        val countDownLatch = CountDownLatch(1)
        val upsertContactUseCase =
            CordialApiConfiguration.getInstance().injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.clearUpsertContactCache {
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_set_contact_is_called_and_jwt_is_absent_then_token_is_received_and_request_is_repeated() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL)) {
                        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                        request.jsonBody?.let { json ->
                            val jsonObject = JSONArray(json).getJSONObject(0)
                            val primaryKey = jsonObject.optString("primaryKey")
                            Assert.assertEquals(
                                "Set contact test failed: primary key is invalid",
                                testContact,
                                primaryKey
                            )
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        }
                        countDownLatch.countDown()
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    }
                }

            }
        }
        cordialApi.setContact(testContact)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_set_contact_is_called_and_jwt_is_expired_then_token_is_received_and_request_is_repeated() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL)) {
                        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                        request.jsonBody?.let { json ->
                            val jsonObject = JSONArray(json).getJSONObject(0)
                            val primaryKey = jsonObject.optString("primaryKey")
                            Assert.assertEquals(
                                "Set contact test failed: primary key is invalid",
                                testContact,
                                primaryKey
                            )
                            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(
                            request,
                            responseHandler,
                            onResponseListener,
                            isJwtTokenAbsent = true
                        )
                        Delay(timeMillis = 100L).doAfterDelay {
                            val token = testCordialApi.getJwtToken(context)
                            Assert.assertTrue("Jwt token was received", token.isEmpty())
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        cordialApi.setContact(testContact)
        countDownLatch.await()
    }
}