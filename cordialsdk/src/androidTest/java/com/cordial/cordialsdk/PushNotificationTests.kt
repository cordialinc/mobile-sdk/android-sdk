package com.cordial.cordialsdk

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.deeplink.model.CordialDeepLinkOpenListener
import com.cordial.feature.deeplink.model.DeepLinkAction
import com.cordial.feature.notification.CordialNotificationProcessService
import com.cordial.feature.notification.CordialPushNotificationListener
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.notification.receiver.NotificationClickedReceiver
import com.cordial.feature.notification.receiver.NotificationDismissedReceiver
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.decode
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class PushNotificationTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    lateinit var pushNotificationHandlerReceivedEventDownLatch: CountDownLatch
    lateinit var pushNotificationHandlerClickedEventDownLatch: CountDownLatch
    lateinit var pushNotificationHandlerDismissedEventDownLatch: CountDownLatch
    lateinit var pushNotificationHandlerTokenReceivedEventDownLatch: CountDownLatch
    private val testToken = "test token21"
    private val testNewToken = "test token201"
    private var testContact = "test@example.com"
    private var testCustomEvent = "testCustomEvent"
    private val testMcID = "testMcID"
    private val testDeepLinkUrl = "https://tjs.cordialdev.com/prep-tj1.html"
    private val testDeepLinkFallbackUrl = "https://tjs.cordialdev.com/prep-tj2.html"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        context.applicationInfo.icon = 2131623936
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val sendEventUseCase = CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_received_then_event_with_mcID_sended() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).getJSONObject(0)
                        val eventName = jsonObject.optString("event")
                        Assert.assertTrue(
                            "Push notification delivered event isn't present in the json." +
                                    "Was $eventName",
                            listOf(
                                C.EVENT_NAME_PUSH_NOTIFICATION_DELIVERED_FOREGROUND,
                                C.EVENT_NAME_PUSH_NOTIFICATION_DELIVERED_BACKGROUND
                            ).contains(eventName)
                        )
                        val mcID = jsonObject.optString("mcID")
                        Assert.assertEquals(
                            "McID is not present in the json or invalid",
                            testMcID,
                            mcID
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 1

        val remoteMessage = createRemoteMessage(isDeepLink = false)
        CordialNotificationProcessService().processMessage(context, remoteMessage)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_clicked_then_mc_id_saved_and_event_sended() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonArray = JSONArray(json)
                        val jsonObjects = mutableListOf<JSONObject>()
                        for (i in 0 until jsonArray.length()) {
                            jsonObjects.add(jsonArray.getJSONObject(i))
                        }
                        val eventNames: List<String> = jsonObjects.map { jsonObject ->
                            jsonObject.optString("event")
                        }
                        Assert.assertTrue(
                            "Push notification tap event isn't present in the json",
                            eventNames.contains(C.EVENT_NAME_PUSH_NOTIFICATION_APP_OPEN_VIA_TAP)
                        )
                        val eventPosition =
                            eventNames.indexOf(C.EVENT_NAME_PUSH_NOTIFICATION_APP_OPEN_VIA_TAP)
                        val mcID = jsonObjects[eventPosition].optString("mcID")
                        Assert.assertEquals(
                            "McID is not present in the json or invalid",
                            testMcID,
                            mcID
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 2

        val remoteMessage = createRemoteMessage(isDeepLink = true)
        val intent = Intent(context, NotificationClickedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationClickedReceiver().onReceive(context, intent)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_dismissed_then_event_sended() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).getJSONObject(0)
                        val eventName = jsonObject.optString("event")
                        Assert.assertEquals(
                            "Dismiss event name is not present in the json or invalid",
                            C.EVENT_NAME_PUSH_NOTIFICATION_DISMISSED,
                            eventName
                        )
                        val mcID = jsonObject.optString("mcID")
                        Assert.assertEquals(
                            "McID is not present in the json or invalid",
                            testMcID,
                            mcID
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 1

        val remoteMessage = createRemoteMessage(isDeepLink = false)
        val intent = Intent(context, NotificationDismissedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationDismissedReceiver().onReceive(context, intent)
        countDownLatch.await()
    }


    @Test
    fun given_sdk_initialized_when_push_notification_clicked_then_mc_id_saved() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonArray = JSONArray(json)
                        val jsonObjects = mutableListOf<JSONObject>()
                        for (i in 0 until jsonArray.length()) {
                            jsonObjects.add(jsonArray.getJSONObject(i))
                        }
                        val eventNames: List<String> = jsonObjects.map { jsonObject ->
                            jsonObject.optString("event")
                        }
                        Assert.assertTrue(
                            "Custom event isn't present in the json",
                            eventNames.contains(testCustomEvent)
                        )
                        val eventPosition =
                            eventNames.indexOf(testCustomEvent)
                        val mcID = jsonObjects[eventPosition].optString("mcID")
                        Assert.assertEquals(
                            "McID is not present in the json or invalid",
                            testMcID,
                            mcID
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 3

        val remoteMessage = createRemoteMessage(isDeepLink = true)
        val intent = Intent(context, NotificationClickedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationClickedReceiver().onReceive(context, intent)
        cordialApi.sendEvent(testCustomEvent, null)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_clicked_then_deep_link_opened() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    val url = uri?.decode()
                    val fallBackUrl = fallBackUri?.decode()
                    Assert.assertEquals(
                        "Deep link url is invalid",
                        testDeepLinkUrl,
                        url
                    )
                    Assert.assertEquals(
                        "Deep link fallback url is invalid",
                        testDeepLinkFallbackUrl,
                        fallBackUrl
                    )
                    countDownLatch.countDown()
                }
            }

        val remoteMessage = createRemoteMessage(isDeepLink = true)
        val intent = Intent(context, NotificationClickedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationClickedReceiver().onReceive(context, intent)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_received_then_push_notification_handler_method_is_triggered() {
        pushNotificationHandlerReceivedEventDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { PushNotificationHandlerMockRequestSender() }
        CordialApiConfiguration.getInstance().pushNotificationListener = TestPushNotificationHandler()

        val remoteMessage = createRemoteMessage(isDeepLink = false)
        CordialNotificationProcessService().processMessage(context, remoteMessage)
        pushNotificationHandlerReceivedEventDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_clicked_then_push_notification_handler_method_is_triggered() {
        pushNotificationHandlerClickedEventDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { PushNotificationHandlerMockRequestSender() }
        CordialApiConfiguration.getInstance().pushNotificationListener = TestPushNotificationHandler()

        val remoteMessage = createRemoteMessage(isDeepLink = false)
        val intent = Intent(context, NotificationClickedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationClickedReceiver().onReceive(context, intent)
        pushNotificationHandlerClickedEventDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_push_notification_dismissed_then_push_notification_handler_method_is_triggered() {
        pushNotificationHandlerDismissedEventDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { PushNotificationHandlerMockRequestSender() }
        CordialApiConfiguration.getInstance().pushNotificationListener = TestPushNotificationHandler()

        val remoteMessage = createRemoteMessage(isDeepLink = false)
        val intent = Intent(context, NotificationDismissedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        NotificationDismissedReceiver().onReceive(context, intent)
        pushNotificationHandlerDismissedEventDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_firebase_token_received_then_push_notification_handler_method_is_triggered() {
        pushNotificationHandlerTokenReceivedEventDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { PushNotificationHandlerMockRequestSender() }
        CordialApiConfiguration.getInstance().pushNotificationListener = TestPushNotificationHandler()

        CordialNotificationProcessService().processNewToken(context, testNewToken)
        pushNotificationHandlerTokenReceivedEventDownLatch.await()
    }

    private fun createRemoteMessage(isDeepLink: Boolean): RemoteMessage {
        val remoteMessageBuilder = RemoteMessage.Builder("test@gcm.googleapis.com")
        val data = mutableMapOf(
            "body" to "body",
            "mcID" to testMcID,
            "subtitle" to "subtitle",
            "imageURL" to "https://s.ill.in.ua/i/news/350x252/419/419035.jpg",
            "title" to "title"
        )
        if (isDeepLink) {
            data["deepLink"] =
                "{\"fallbackUrl\": \"$testDeepLinkFallbackUrl\",\"url\": \"$testDeepLinkUrl\"}"
        }
        remoteMessageBuilder.setData(data)
        return remoteMessageBuilder.build()
    }

    inner class PushNotificationHandlerMockRequestSender : RequestSender {

        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
        }
    }

    inner class TestPushNotificationHandler : CordialPushNotificationListener {
        override fun onNotificationReceived(
            remoteMessage: RemoteMessage,
            isReceivedInForeground: Boolean
        ) {
            if (::pushNotificationHandlerReceivedEventDownLatch.isInitialized) {
                val mcID = remoteMessage.data["mcID"]
                Assert.assertEquals(
                    "McID is not present in the json or invalid",
                    testMcID,
                    mcID
                )
                pushNotificationHandlerReceivedEventDownLatch.countDown()
            }
        }

        override fun onNotificationClicked(remoteMessage: RemoteMessage) {
            if (::pushNotificationHandlerClickedEventDownLatch.isInitialized) {
                val mcID = remoteMessage.data["mcID"]
                Assert.assertEquals(
                    "McID is not present in the json or invalid",
                    testMcID,
                    mcID
                )
                pushNotificationHandlerClickedEventDownLatch.countDown()
            }
        }

        override fun onNotificationDismissed(remoteMessage: RemoteMessage) {
            if (::pushNotificationHandlerDismissedEventDownLatch.isInitialized) {
                val mcID = remoteMessage.data["mcID"]
                Assert.assertEquals(
                    "McID is not present in the json or invalid",
                    testMcID,
                    mcID
                )
                pushNotificationHandlerDismissedEventDownLatch.countDown()
            }
        }

        override fun onFcmTokenReceived(token: String) {
            if (::pushNotificationHandlerTokenReceivedEventDownLatch.isInitialized
                && pushNotificationHandlerTokenReceivedEventDownLatch.count > 0
            ) {
                Assert.assertEquals(
                    "Push notification token is invalid",
                    testNewToken,
                    token
                )
                pushNotificationHandlerTokenReceivedEventDownLatch.countDown()
            }
        }
    }
}
