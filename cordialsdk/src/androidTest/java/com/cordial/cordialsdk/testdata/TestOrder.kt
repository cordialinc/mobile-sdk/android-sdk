package com.cordial.cordialsdk.testdata

import com.cordial.feature.sendcontactorder.model.Address
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.upsertcontactcart.model.CartItem
import java.util.*

internal class TestOrder(var orderId: String) {
    var status: String = "testStatus"
    var storeId: String = "testStoreId"
    var customerId: String = "testCustomerId"
    var shippingAddress: Address = Address(
        "testShippingAddressName",
        "testShippingAddress",
        "testShippingCity",
        "testShippingState",
        "23822",
        "testShippingCountry"
    )
    var billingAddress: Address = Address(
        "testBillingAddressName",
        "testBillingAddress",
        "testBillingCity",
        "testBillingState",
        "23412",
        "testBillingCountry"
    )

    val testCartItem = TestCartItem()
    var items: List<CartItem> = listOf(getCartItem())
    var tax: Double = 80.0
    var shippingAndHandling: Double = 100.5
    var properties: Map<String, PropertyValue>? =
        mapOf("viaDeepLink" to PropertyValue.BooleanProperty(true))
    var purchaseDate = Date()

    private fun getCartItem(): CartItem {
        return CartItem(
            testCartItem.productID,
            testCartItem.name,
            testCartItem.sku,
            testCartItem.category,
            testCartItem.qty
        )
            .withUrl(testCartItem.url)
            .withItemDescription(testCartItem.itemDescription)
            .withItemPrice(testCartItem.itemPrice)
            .withSalePrice(testCartItem.salePrice)
            .withAttr(testCartItem.attr)
            .withImages(testCartItem.images)
            .withProperties(testCartItem.properties)
            .withTimestamp(testCartItem.timestamp)
    }
}