package com.cordial.cordialsdk.testdata

import com.cordial.feature.sendevent.model.property.PropertyValue
import java.util.*

internal class TestCartItem {
    var productID = "218761"
    var name = "testName"
    var sku = "testSku"
    var category = "testCategory"
    var url = "testUrl"
    var itemDescription = "testItemDescription"
    var qty = 1
    var itemPrice = 10.0
    var salePrice = 13.0
    var attr = mapOf("catalogID" to "2323")
    var images = listOf("one", "two")
    var properties = mapOf("catalogName" to PropertyValue.StringProperty("Mens"))
    val timestamp = Date()
}