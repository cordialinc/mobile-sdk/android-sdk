package com.cordial.cordialsdk.testdata

import com.cordial.api.C
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponse
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.URL

internal object MockResponse {
    const val INTERNAL_SERVER_ERROR = "500 Internal Server Error"
    const val TEST_JWT_TOKEN = "testJWTToken"

    fun mockSuccessJwtTokenResponse(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener,
        isJwtTokenAbsent: Boolean = false
    ) {
        val token = if (isJwtTokenAbsent) "" else ",\"token\":\"$TEST_JWT_TOKEN\""
        val response = "{\"success\":true $token}"
        mock200Response(request, response, responseHandler, onResponseListener)
    }

    fun mockErrorJwtResponse(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 401,
            url = URL(request.url),
            body = null,
            error = C.JWT_TOKEN_EXPIRED,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockSuccessCartResponse(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val response = "{\"success\":true}"
        mock200Response(request, response, responseHandler, onResponseListener)
    }

    fun mockSuccessResponse(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val response = "{\"success\":true}"
        mock200Response(request, response, responseHandler, onResponseListener)
    }

    fun mock200Response(
        request: SDKRequest,
        response: String,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 200,
            url = URL(request.url),
            body = response,
            error = null,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockEventErrorResponse(
        request: SDKRequest,
        response: String,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 422,
            url = URL(request.url),
            body = null,
            error = response,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockProvidedTokenExpiredError(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 400,
            url = URL(request.url),
            body = null,
            error = C.PROVIDED_TOKEN_EXPIRED,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockTimestampsFileIsNotFoundError(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val error =
            "{\"success\":false,\"message\":{\"code\":404,\"errorCode\":\"no_timestamps_file\",\"message\":\"Timestamps file has not been found\",\"errors\":[]}}"
        val sdkResponse = SDKResponse(
            responseCode = 404,
            url = URL(request.url),
            body = null,
            error = error,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockAccessDeniedError(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 403,
            url = URL(request.url),
            body = null,
            error = C.ACCESS_DENIED,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockInternalServerError(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 500,
            url = URL(request.url),
            body = null,
            error = INTERNAL_SERVER_ERROR,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mock422Error(
        request: SDKRequest,
        response: String,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 422,
            url = URL(request.url),
            body = null,
            error = response,
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    fun mockLinkRedirect302Response(
        request: SDKRequest,
        deepLink: String?,
        mcID: String,
        isTestMessage: Boolean = false,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 302,
            url = URL(request.url),
            body = "",
            error = null,
            headers = getHeaders(deepLink, mcID, isTestMessage),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }

    private fun getHeaders(
        deepLink: String?,
        mcID: String,
        isTestMessage: Boolean
    ): MutableMap<String, MutableList<String>> {
        val headers: MutableMap<String, MutableList<String>> = mutableMapOf()
        if (deepLink != null)
            headers[C.LOCATION] = mutableListOf(deepLink)
        headers[C.MC_ID_HEADER] = mutableListOf(mcID)
        val testMcIDHeader = if (isTestMessage) "1" else "0"
        headers[C.TEST_MC_ID_HEADER] = mutableListOf(testMcIDHeader)
        return headers
    }

    fun mockLinkRedirect404Response(
        request: SDKRequest,
        deepLink: String,
        mcID: String,
        isTestMessage: Boolean = false,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 404,
            url = URL(request.url),
            body = null,
            error = "404 error",
            headers = getHeaders(deepLink, mcID, isTestMessage),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }
    fun mock404Response(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val sdkResponse = SDKResponse(
            responseCode = 404,
            url = URL(request.url),
            body = null,
            error = "404 error",
            headers = mapOf(),
            methodOfRequest = request.methodOfRequest
        )
        val scope = CoroutineScope(Dispatchers.IO)
        scope.launch {
            responseHandler.onHandleResponse(sdkResponse, onResponseListener)
        }
    }
}