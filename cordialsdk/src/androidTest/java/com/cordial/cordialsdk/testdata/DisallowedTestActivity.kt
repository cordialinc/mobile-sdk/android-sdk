package com.cordial.cordialsdk.testdata

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cordial.cordialsdk.databinding.ActivityTestBinding
import com.cordial.util.ActivityUtils

internal class DisallowedTestActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityTestBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ActivityUtils.setStatusBarTransparent(this)
    }
}