package com.cordial.cordialsdk

import android.content.Context
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.feature.upsertcontact.model.NotificationStatus
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.TimeUtils

internal class TestCordialApi {

    fun createJwtToken(context: Context, testJwtToken: String = MockResponse.TEST_JWT_TOKEN) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.JWT_TOKEN, testJwtToken)
    }

    fun clearJwtToken(context: Context) {
        val preferences = Preferences(context)
        preferences.remove(PreferenceKeys.JWT_TOKEN)
    }

    fun getJwtToken(context: Context): String {
        val preferences = Preferences(context)
        return preferences.getString(PreferenceKeys.JWT_TOKEN, "")
    }

    fun clearPreferences(context: Context) {
        val preferences = Preferences(context)
        preferences.clear()
    }

    fun createFirebaseToken(context: Context, token: String) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.FIREBASE_TOKEN, token)
    }

    fun clearFirebaseToken(context: Context) {
        val preferences = Preferences(context)
        preferences.remove(PreferenceKeys.FIREBASE_TOKEN)
    }

    fun savePrimaryKey(context: Context, primaryKey: String) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.PRIMARY_KEY, primaryKey)
    }

    fun removePrimaryKeyAndLoginState(context: Context) {
        val preferences = Preferences(context)
        preferences.remove(PreferenceKeys.PRIMARY_KEY)
        preferences.remove(PreferenceKeys.IS_CONTACT_SET)
        preferences.remove(PreferenceKeys.IS_LOGGED_IN)
    }

    fun saveLoginState(context: Context) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.IS_CONTACT_SET, true)
        preferences.put(PreferenceKeys.IS_LOGGED_IN, true)
    }

    fun savePrimaryKeyAndLoginState(context: Context, primaryKey: String?) {
        val preferences = Preferences(context)
        if (primaryKey != null) {
            preferences.put(PreferenceKeys.PRIMARY_KEY, primaryKey)
        } else {
            preferences.remove(PreferenceKeys.PRIMARY_KEY)
        }
        preferences.put(PreferenceKeys.IS_CONTACT_SET, true)
        preferences.put(PreferenceKeys.IS_LOGGED_IN, true)
    }

    fun skipAppInstallEvent(context: Context) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.WAS_LAUNCHED_BEFORE, true)
    }

    fun removeWasLaunchedBeforeFlag(context: Context) {
        val preferences = Preferences(context)
        preferences.remove(PreferenceKeys.WAS_LAUNCHED_BEFORE)
    }

    fun setNotificationStatus(context: Context, areNotificationsEnabled: Boolean) {
        val preferences = Preferences(context)
        preferences.put(
            PreferenceKeys.NOTIFICATION_STATUS,
            NotificationStatus.findKey(areNotificationsEnabled).status
        )
    }

    fun getPreviousPrimaryKey(context: Context): String {
        val preferences = Preferences(context)
        return preferences.getString(PreferenceKeys.PREVIOUS_PRIMARY_KEY, "")
    }

    fun saveSetContactTimestamp(context: Context, setContactTimestamp: String) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.LAST_SET_CONTACT_TIMESTAMP, setContactTimestamp)
    }

    fun saveMcIDAndMcTapTime(context: Context, mcID: String) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.MC_ID, mcID)
        preferences.put(PreferenceKeys.MC_TAP_TIME, TimeUtils.getTimestamp())
    }

    fun getLastInAppTimestamp(context: Context): Long {
        val preferences = Preferences(context)
        return preferences.getLong(PreferenceKeys.LAST_IN_APP_TIMESTAMP, 0L)
    }

    fun saveLastInAppTimestamp(context: Context, lastInAppTimestamp: Long) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.LAST_IN_APP_TIMESTAMP, lastInAppTimestamp)
    }

    fun saveTimestampUrl(context: Context, timestampsUrl: String, timestampsURLValidDate: String) {
        val preferences = Preferences(context)
        preferences.put(PreferenceKeys.TIMESTAMPS_URL, timestampsUrl)
        preferences.put(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT, timestampsURLValidDate)
    }
}