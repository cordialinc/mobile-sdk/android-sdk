package com.cordial.cordialsdk

import android.content.Context
import android.net.Uri
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.deeplink.CordialDeepLinkProcessService
import com.cordial.feature.deeplink.model.CordialDeepLinkOpenListener
import com.cordial.feature.deeplink.model.DeepLinkAction
import com.cordial.feature.deeplink.model.browser.Browser
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class EmailLinkRedirectTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testMcID = "testMcID"
    val smsLink = "https://s.cordial.com/3udMLK"
    val deepLink = "https://tjs.cordialdev.com/prep-tj1.html"
    val emailLink =
        "https://events-handling-svc.cordial-core.cp-7796-x-mcid-header.cordialdev.com/c2/38:6006bf1a284df9648311e62a:ot:6006b9a3a245b30683ed77e0:1/11e5568b?jwtH=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9&jwtP=eyJpYXQiOjE2MTEwNTQ5MTksImNkIjoiLmNvcmRpYWwtY29yZS5jcC03Nzk2LXgtbWNpZC1oZWFkZXIuY29yZGlhbGRldi5jb20iLCJjZSI6ODY0MDAsInRrIjoicWMtYWxsLWNoYW5uZWxzIiwibXRsSUQiOiI2MDA2YmY0NjJjMGI5YTVmODk2MTY0MDMiLCJsaW5rVXJsIjoiaHR0cHM6XC9cL3Rqcy5jb3JkaWFsZGV2LmNvbVwvcHJlcC10ajEuaHRtbD91dG1fc291cmNlPWVtYWlsJnV0bV9jYW1wYWlnbj0xMjM0JnV0bV9tZWRpdW09c29tZVZhbHVlTmUlM0Z3JnV0bV9pbnZhbGlkPXF3JTIzZnIlMjZrc24lMkFkJTI1LSUyOC0lMjktJTNEJTJCJUNCJTg2JTI0JTQwJTVCJTVEJTdCJTdEJTJGJnV0bV9zcGFjZT1xd2VyK3R5In0&jwtS=0ysz7P5tu8h5AAgUngjJEAAA4CXqsD8n0ZaAuwN3MPM"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        val messageAttributionManager = CordialApiConfiguration.getInstance().injection.messageAttributionManager
        messageAttributionManager.clearMessageAttributes()
    }

    @After
    fun afterSendingEvents() {
        val sendEventUseCase = CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.clearCache()
        SendingCacheState.sendingEvents.set(false)
        CordialApiConfiguration.getInstance().deepLinkListener = null
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        config.vanityDomains =
            listOf(
                "events-handling-svc.cordial-core.cp-7796-x-mcid-header.cordialdev.com",
                "s.cordial.com"
            )
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_email_link_is_clicked_then_email_link_redirects_to_deep_link_and_deep_link_opens() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect302Response(
                        request,
                        deepLink,
                        testMcID,
                        isTestMessage = false,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", deepLink, uri.toString())
                    countDownLatch.countDown()
                }

            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_email_link_is_clicked_then_email_link_redirects_to_deep_link_and_mc_id_is_saved() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect302Response(
                        request,
                        deepLink,
                        testMcID,
                        isTestMessage = false,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", deepLink, uri.toString())
                    val mcID = cordialApi.getMcID()
                    Assert.assertEquals("McID is not saved", testMcID, mcID)
                    countDownLatch.countDown()
                }

            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_email_link_from_test_message_is_clicked_then_email_link_redirects_to_deep_link_and_mc_id_is_not_saved() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect302Response(
                        request,
                        deepLink,
                        testMcID,
                        isTestMessage = true,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", deepLink, uri.toString())
                    val mcID = cordialApi.getMcID()
                    Assert.assertEquals("McID is saved", null, mcID)
                    countDownLatch.countDown()
                }

            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_email_link_is_clicked_but_does_not_contain_in_vanity_domains_then_email_link_as_deep_link_opens() {
        val countDownLatch = CountDownLatch(1)
        CordialApiConfiguration.getInstance().vanityDomains = listOf()
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", emailLink, uri.toString())
                    countDownLatch.countDown()
                }
            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_email_link_is_clicked_then_email_link_returns_error_and_email_link_as_deep_link_opens() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect404Response(
                        request,
                        deepLink,
                        testMcID,
                        isTestMessage = false,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", emailLink, uri.toString())
                    countDownLatch.countDown()
                }
            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_sms_link_is_clicked_then_redirects_to_email_link_and_later_to_deep_link() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url == smsLink) {
                        MockResponse.mockLinkRedirect302Response(
                            request,
                            emailLink,
                            testMcID,
                            isTestMessage = false,
                            responseHandler,
                            onResponseListener
                        )
                    } else if (request.url == emailLink) {
                        MockResponse.mockLinkRedirect302Response(
                            request,
                            deepLink,
                            testMcID,
                            isTestMessage = false,
                            responseHandler,
                            onResponseListener
                        )
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", deepLink, uri.toString())
                    countDownLatch.countDown()
                }
            }
        cordialApi.openDeepLink(smsLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_sms_link_is_clicked_then_sms_link_returns_error_and_sms_link_as_deep_link_opens() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url == smsLink) {
                        MockResponse.mockLinkRedirect404Response(
                            request,
                            emailLink,
                            testMcID,
                            isTestMessage = false,
                            responseHandler,
                            onResponseListener
                        )
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", smsLink, uri.toString())
                    countDownLatch.countDown()
                }
            }
        cordialApi.openDeepLink(smsLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_sms_link_is_clicked_then_sms_link_redirects_to_email_link_later_email_link_returns_error_and_email_link_as_deep_link_opens() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url == smsLink) {
                        MockResponse.mockLinkRedirect302Response(
                            request,
                            emailLink,
                            testMcID,
                            isTestMessage = false,
                            responseHandler,
                            onResponseListener
                        )
                    } else if (request.url == emailLink) {
                        MockResponse.mockLinkRedirect404Response(
                            request,
                            deepLink,
                            testMcID,
                            isTestMessage = false,
                            responseHandler,
                            onResponseListener
                        )
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", emailLink, uri.toString())
                    countDownLatch.countDown()
                }
            }
        cordialApi.openDeepLink(smsLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_sms_link_is_clicked_and_sms_link_redirects_more_than_three_times_then_last_link_opens_as_deep_links() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect302Response(
                        request,
                        smsLink,
                        testMcID,
                        isTestMessage = false,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link url is invalid", smsLink, uri.toString())
                    countDownLatch.countDown()
                }
            }

        cordialApi.openDeepLink(smsLink)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_link_with_webonly_fragment_is_clicked_then_browser_opens_and_events_are_not_sent() {
        val countDownLatch = CountDownLatch(1)
        val webOnlyUri = Uri.parse("$deepLink#webonly")

        DependencyConfiguration.getInstance().browser = {
            object : Browser {
                override fun openLink(uri: Uri) {
                    val sendEventUseCase =
                        CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
                    sendEventUseCase.getEventsCount { eventsCount ->
                        Assert.assertTrue(
                            "Events cache is not dropped",
                            eventsCount == 0L
                        )
                        countDownLatch.countDown()
                    }
                    Assert.assertEquals("Deep link web only url is invalid", webOnlyUri, uri)
                    countDownLatch.countDown()
                }
            }
        }
        CordialDeepLinkProcessService().checkOnRedirectAndWebOnly(webOnlyUri)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_and_new_webonly_fragment_is_defined_when_link_with_webonly_fragment_is_clicked_then_browser_opens_and_events_are_not_sent() {
        val countDownLatch = CountDownLatch(1)
        val webOnlyUri = Uri.parse("$deepLink#browsable")

        DependencyConfiguration.getInstance().browser = {
            object : Browser {
                override fun openLink(uri: Uri) {
                    val sendEventUseCase =
                        CordialApiConfiguration.getInstance().injection.sendEventInjection().sendEventUseCase
                    sendEventUseCase.getEventsCount { eventsCount ->
                        Assert.assertTrue(
                            "Events cache is not dropped",
                            eventsCount == 0L
                        )
                        countDownLatch.countDown()
                    }
                    Assert.assertEquals("Deep link web only url is invalid", webOnlyUri, uri)
                    countDownLatch.countDown()
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkWebOnlyFragments = listOf("webonly", "browsable")
        CordialDeepLinkProcessService().checkOnRedirectAndWebOnly(webOnlyUri)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_link_with_webonly_fragment_and_invalid_symbols_and_fragment_is_clicked_then_deep_link_opens_without_browser() {
        val countDownLatch = CountDownLatch(1)
        val deepLinkWithInvalidSymbols =
            "https://tjs.cordialdev.com/prep-tj1.html?utm_source=email&utm_campaign=1234&utm_medium=someValueNe?w&utm_invalid=qw#fr%26ksn*d%-(-)-=+%CB%86\$@[]{}/&utm_space=qwer%20ty"
        val deepLinkUri = Uri.parse("$deepLinkWithInvalidSymbols#webonly")

        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Deep link web only url is invalid", deepLinkUri, uri)
                    countDownLatch.countDown()
                }
            }
        CordialDeepLinkProcessService().checkOnRedirectAndWebOnly(deepLinkUri)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_email_link_is_clicked_then_redirect_url_is_null_and_email_link_opens() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    MockResponse.mockLinkRedirect302Response(
                        request,
                        null,
                        testMcID,
                        isTestMessage = false,
                        responseHandler,
                        onResponseListener
                    )
                }
            }
        }
        CordialApiConfiguration.getInstance().deepLinkListener =
            object : CordialDeepLinkOpenListener {
                override fun appOpenViaDeepLink(
                    context: Context?,
                    uri: Uri?,
                    fallBackUri: Uri?,
                    onComplete: ((action: DeepLinkAction) -> Unit)?
                ) {
                    Assert.assertEquals("Email link url is invalid", emailLink, uri.toString())
                    countDownLatch.countDown()
                }

            }
        cordialApi.openDeepLink(emailLink)
        countDownLatch.await()
    }
}