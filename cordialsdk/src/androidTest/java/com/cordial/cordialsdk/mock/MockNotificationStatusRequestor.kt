package com.cordial.cordialsdk.mock

import android.content.Context
import com.cordial.feature.notification.OnNotificationStatusListener

internal class MockNotificationStatusRequestor(private var areNotificationsEnabled: Boolean) :
    OnNotificationStatusListener {
    override fun areNotificationsEnabled(context: Context): Boolean {
        return areNotificationsEnabled
    }
}