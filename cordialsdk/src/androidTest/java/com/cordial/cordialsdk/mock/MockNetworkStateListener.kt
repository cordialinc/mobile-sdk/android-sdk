package com.cordial.cordialsdk.mock

import android.content.Context
import com.cordial.network.NetworkState
import com.cordial.network.OnConnectivityListener

internal class MockNetworkStateListener : NetworkState {
    var isNetworkAvailable = true
        set(value) {
            field = value
            onConnectivityListener?.onConnectionChanged(value)
        }

    private var onConnectivityListener: OnConnectivityListener? = null

    override fun register(context: Context, onConnectivityListener: OnConnectivityListener) {
        this.onConnectivityListener = onConnectivityListener
    }

    override fun unregister(context: Context, onConnectivityListener: OnConnectivityListener) {
        this.onConnectivityListener = null
    }

    override fun isNetworkAvailable(context: Context): Boolean {
        return isNetworkAvailable
    }
}