package com.cordial.cordialsdk

import android.content.Context
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import com.cordial.util.TimeUtils
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class TimestampsTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    lateinit var cordialApi: CordialApi

    private var testContact = "test@example.com"
    private val testToken = "test token21"
    private val testTimestampsURL = "https://cordialdev-message-hub.s3.us-west-2.amazonaws.com"
    private val testTimestampsURLValidDate = "2121-03-23T00:13:41.000Z"
    private val testTimestampsURLExpiredDate = "2021-03-23T00:13:41.000Z"
    private val testLastInAppTimestamp = "2121-03-23T00:13:41.000Z"
    private val testOldLastInAppTimestamp = "2021-03-23T00:13:41.000Z"

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.eventsBulkSize = 2
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_no_timestamps_data_when_timestamps_are_updating_then_in_app_messages_are_fetching() {
        val countDownLatch = CountDownLatch(1)
        val config = CordialApiConfiguration.getInstance()
        var inAppMessagesAreFetching = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        if (inAppMessagesAreFetching) {
                            Assert.assertTrue(
                                "Invalid request. Expected in-app messages url, but was ${request.url}",
                                request.url.contains(CordialApiEndpoints.Url.IN_APP_MESSAGES_URL)
                            )
                            countDownLatch.countDown()
                        } else {
                            MockResponse.mock200Response(
                                request,
                                getTimestampsUrl(),
                                responseHandler,
                                onResponseListener
                            )
                            inAppMessagesAreFetching = true
                        }
                    } else {
                        MockResponse.mock200Response(
                            request,
                            getTimestamps(isNewInApp = true),
                            responseHandler,
                            onResponseListener
                        )
                    }

                }
            }
        }
        config.onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_url_when_timestamps_are_updating_then_timestamps_data_is_fetching() {
        val countDownLatch = CountDownLatch(1)

        val config = CordialApiConfiguration.getInstance()
        testCordialApi.saveTimestampUrl(context, testTimestampsURL, testTimestampsURLValidDate)
        val lastInAppTimestamp = TimeUtils.getTime(testOldLastInAppTimestamp, isMillis = true)
        testCordialApi.saveLastInAppTimestamp(context, lastInAppTimestamp)

        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        Assert.assertTrue(
                            "Invalid request. Expected in-app messages url, but was ${request.url}",
                            request.url.contains(CordialApiEndpoints.Url.IN_APP_MESSAGES_URL)
                        )
                        countDownLatch.countDown()
                    } else {
                        MockResponse.mock200Response(
                            request,
                            getTimestamps(isNewInApp = true),
                            responseHandler,
                            onResponseListener
                        )
                    }

                }
            }
        }
        config.onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_are_fetching_when_timestamps_url_is_expired_then_new_timestamps_url_is_updating() {
        val countDownLatch = CountDownLatch(1)

        val config = CordialApiConfiguration.getInstance()
        testCordialApi.saveTimestampUrl(context, testTimestampsURL, testTimestampsURLExpiredDate)

        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        val cordialApiEndpoints = config.injection.cordialApiEndpoints
                        val url = cordialApiEndpoints.getTimestampsUrl()
                        Assert.assertEquals("The timestamps url is invalid", url, request.url)
                        countDownLatch.countDown()
                    }
                }
            }
        }
        config.onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_are_updating_and_timestamps_url_is_valid_when_timestamps_url_returns_400_error_then_new_timestamps_url_is_fetching_and_later_timestamps_are_fetching() {
        val countDownLatch = CountDownLatch(1)

        val config = CordialApiConfiguration.getInstance()
        testCordialApi.saveTimestampUrl(context, testTimestampsURL, testTimestampsURLValidDate)

        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        val cordialApiEndpoints = config.injection.cordialApiEndpoints
                        val url = cordialApiEndpoints.getTimestampsUrl()
                        Assert.assertEquals("The timestamps url is invalid", url, request.url)
                        countDownLatch.countDown()
                    } else {
                        MockResponse.mockProvidedTokenExpiredError(request, responseHandler, onResponseListener)
                    }

                }
            }
        }
        config.onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_url_when_timestamps_are_fetching_and_there_is_old_inapp_timestamp_then_inapp_is_not_fetching() {
        val countDownLatch = CountDownLatch(1)
        var isInAppMessageFetched = false

        val config = CordialApiConfiguration.getInstance()
        testCordialApi.saveTimestampUrl(context, testTimestampsURL, testTimestampsURLValidDate)
        val lastInAppTimestamp = TimeUtils.getTime(testLastInAppTimestamp, isMillis = true)
        testCordialApi.saveLastInAppTimestamp(context, lastInAppTimestamp)

        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial) {
                        isInAppMessageFetched = request.url.contains(CordialApiEndpoints.Url.IN_APP_MESSAGES_URL)
                    } else {
                        MockResponse.mock200Response(
                            request,
                            getTimestamps(isNewInApp = true),
                            responseHandler,
                            onResponseListener
                        )
                        Delay(timeMillis = 300L).doAfterDelay {
                            Assert.assertTrue("In-app message was fetched", !isInAppMessageFetched)
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        config.onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_guest_contact_logged_in_when_timestamp_url_is_fetching_then_url_of_timestamp_for_guest_contact_is_correct() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.savePrimaryKeyAndLoginState(context, null)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.isCordial && request.url.contains(CordialApiEndpoints.Url.TIMESTAMPS_URL)) {
                        val config = CordialApiConfiguration.getInstance()
                        val guestContact = "${config.getChannelKey()}:${cordialApi.getFirebaseToken()}"
                        Assert.assertTrue(
                            "Timestamp url for guest contact is invalid",
                            request.url.contains(guestContact)
                        )
                        countDownLatch.countDown()
                    }
                }

            }
        }
        CordialApiConfiguration.getInstance().onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamp_url_is_fetching_when_jwt_token_is_empty_then_jwt_token_is_retrieving_and_timestamp_url_is_fetching() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.clearJwtToken(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.TIMESTAMPS_URL)) {
                        MockResponse.mock200Response(
                            request,
                            getTimestampsUrl(),
                            responseHandler,
                            onResponseListener
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_url_is_fetching_when_jwt_token_is_expired_then_jwt_token_is_updating_and_timestamp_url_is_fetching() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.TIMESTAMPS_URL)) {
                        if (isJwtTokenExpired) {
                            isJwtTokenExpired = false
                            MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                        } else {
                            MockResponse.mock200Response(
                                request,
                                getTimestampsUrl(),
                                responseHandler,
                                onResponseListener
                            )
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().onAppForegrounded()
        countDownLatch.await()
    }

    @Test
    fun given_timestamps_are_fetching_when_no_timestamps_file_error_is_received_then_timestamps_are_not_fetched() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.TIMESTAMPS_URL)) {
                        MockResponse.mockTimestampsFileIsNotFoundError(request, responseHandler, onResponseListener)
                        Delay(timeMillis = 100L).doAfterDelay {
                            val lastInAppTimestamp = testCordialApi.getLastInAppTimestamp(context)
                            Assert.assertTrue(
                                "Last in-app timestamp is not equals zero",
                                lastInAppTimestamp == 0L
                            )
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().onAppForegrounded()
        countDownLatch.await()
    }

    private fun getTimestampsUrl(isExpired: Boolean = false): String {
        val urlExpireAt = if (isExpired) testTimestampsURLExpiredDate else testTimestampsURLValidDate
        return "{\"success\":true, \"url\":\"$testTimestampsURL\",\"urlExpireAt\":\"$urlExpireAt\"}"
    }

    private fun getTimestamps(isNewInApp: Boolean): String {
        return if (isNewInApp) "{\"inApp\":\"$testLastInAppTimestamp\"}" else "{\"inApp\":\"$testOldLastInAppTimestamp\"}"
    }

}