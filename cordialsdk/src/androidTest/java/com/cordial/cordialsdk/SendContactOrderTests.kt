package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.cordialsdk.testdata.TestOrder
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.sendcontactorder.model.Order
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.Delay
import com.cordial.util.JsonUtils
import com.cordial.util.TimeUtils
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class SendContactOrderTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    private lateinit var sendContactOrderDownLatch: CountDownLatch
    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testOrder = TestOrder(orderId = "10223")
    private var testOrder2 = TestOrder(orderId = "10224")

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val sendContactOrderUseCase =
            CordialApiConfiguration.getInstance().injection.sendContactOrderInjection().sendContactOrderUseCase
        sendContactOrderUseCase.clearCache {
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_send_contact_order_was_called_then_order_is_sending() {
        sendContactOrderDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { SendContactOrderMockRequestSender() }

        cordialApi.sendContactOrder(getOrder(testOrder))
        sendContactOrderDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_contact_order_was_cashed_then_order_is_sending() {
        sendContactOrderDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = { SendContactOrderMockRequestSender() }

        testCordialApi.saveMcIDAndMcTapTime(context, mcID = "testMcID")
        cordialApi.sendContactOrder(getOrder(testOrder))
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        sendContactOrderDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_two_contact_orders_were_cashed_then_orders_are_sending() {
        sendContactOrderDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = { SendContactOrderMockRequestSender() }

        cordialApi.sendContactOrder(getOrder(testOrder))
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.sendContactOrder(getOrder(testOrder2))
        }
        Delay(timeMillis = 200L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        sendContactOrderDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_order_is_sending_and_caching_then_jwt_token_is_updating_and_order_is_sent() {
        sendContactOrderDownLatch = CountDownLatch(1)
        testCordialApi.clearJwtToken(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.SEND_CONTACT_ORDER)) {
                        checkOrder(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        cordialApi.sendContactOrder(getOrder(testOrder))
        sendContactOrderDownLatch.await()
    }

    @Test
    fun given_order_is_sending_when_jwt_token_is_expired_then_order_is_caching_then_jwt_token_is_updating_and_order_is_sent() {
        sendContactOrderDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SEND_CONTACT_ORDER)) {
                        if (isJwtTokenExpired) {
                            MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                            isJwtTokenExpired = false
                        } else {
                            checkOrder(request, responseHandler, onResponseListener)
                        }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        cordialApi.sendContactOrder(getOrder(testOrder))
        sendContactOrderDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_upsert_contact_cart_is_sending_and_caching_then_contact_is_logging_in_and_cart_is_sent() {
        sendContactOrderDownLatch = CountDownLatch(1)
        testCordialApi.removePrimaryKeyAndLoginState(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL)) {
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.SEND_CONTACT_ORDER)) {
                        checkOrder(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        cordialApi.sendContactOrder(getOrder(testOrder))
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.setContact(testContact)
        }
        sendContactOrderDownLatch.await()
    }

    private fun getOrder(testOrder: TestOrder): Order {
        return Order(
            testOrder.orderId,
            testOrder.status,
            testOrder.storeId,
            testOrder.customerId,
            testOrder.shippingAddress,
            testOrder.billingAddress,
            testOrder.items
        ).withTax(
            testOrder.tax
        ).withShippingAndHandling(
            testOrder.shippingAndHandling
        ).withProperties(
            testOrder.properties
        ).withPurchaseDate(testOrder.purchaseDate)
    }

    internal inner class SendContactOrderMockRequestSender : RequestSender {
        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            checkOrder(request, responseHandler, onResponseListener)
        }
    }

    private fun checkOrder(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")

        request.jsonBody?.let { json ->
            val jsonArray = JSONArray(json)
            val jsonObject = jsonArray.optJSONObject(0)
            validateOrder(jsonObject, testOrder)
            if (jsonArray.length() > 1) {
                val jsonObject2 = jsonArray.optJSONObject(1)
                validateOrder(jsonObject2, testOrder2)
            }
            MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
            sendContactOrderDownLatch.countDown()
        }
    }

    private fun validateOrder(
        jsonObject: JSONObject,
        testOrder: TestOrder
    ) {
        val primaryKey = jsonObject.optString(C.PRIMARY_KEY)
        Assert.assertEquals(
            "Primary key is not present in the json or invalid",
            testContact,
            primaryKey
        )
        val order = jsonObject.optJSONObject(C.ORDER)
        order?.let {
            val orderID = order.optString(C.ORDER_ID)
            Assert.assertEquals(
                "Order id is not present in the json or invalid",
                testOrder.orderId,
                orderID
            )
            val status = order.optString(C.STATUS)
            Assert.assertEquals(
                "Order status is not present in the json or invalid",
                testOrder.status,
                status
            )
            val storeId = order.optString(C.STORE_ID)
            Assert.assertEquals(
                "Order storeId is not present in the json or invalid",
                testOrder.storeId,
                storeId
            )
            val customerId = order.optString(C.CUSTOMER_ID)
            Assert.assertEquals(
                "Order customerId is not present in the json or invalid",
                testOrder.customerId,
                customerId
            )
            val shippingAddressObject = order.optJSONObject(C.SHIPPING_ADDRESS)
            shippingAddressObject?.let {
                val shippingName = shippingAddressObject.optString(C.NAME)
                val shippingAddress = shippingAddressObject.optString(C.ADDRESS)
                val shippingCity = shippingAddressObject.optString(C.CITY)
                val shippingState = shippingAddressObject.optString(C.STATE)
                val shippingPostalCode = shippingAddressObject.optString(C.POSTAL_CODE)
                val shippingAddressOptions = arrayOf(
                    shippingName, shippingAddress, shippingCity,
                    shippingState, shippingPostalCode
                )
                val testShippingAddressOptions = arrayOf(
                    testOrder.shippingAddress.name,
                    testOrder.shippingAddress.address,
                    testOrder.shippingAddress.city,
                    testOrder.shippingAddress.state,
                    testOrder.shippingAddress.postalCode
                )
                Assert.assertArrayEquals(
                    "Order shipping address validation error",
                    testShippingAddressOptions,
                    shippingAddressOptions
                )
            }
            val billingAddressObject = order.optJSONObject(C.BILLING_ADDRESS)
            billingAddressObject?.let {
                val billingName = billingAddressObject.optString(C.NAME)
                val billingAddress = billingAddressObject.optString(C.ADDRESS)
                val billingCity = billingAddressObject.optString(C.CITY)
                val billingState = billingAddressObject.optString(C.STATE)
                val billingPostalCode = billingAddressObject.optString(C.POSTAL_CODE)

                val billingAddressOptions = arrayOf(
                    billingName, billingAddress, billingCity,
                    billingState, billingPostalCode
                )
                val testBillingAddressOptions = arrayOf(
                    testOrder.billingAddress.name,
                    testOrder.billingAddress.address,
                    testOrder.billingAddress.city,
                    testOrder.billingAddress.state,
                    testOrder.billingAddress.postalCode
                )
                Assert.assertArrayEquals(
                    "Order billing address validation error",
                    testBillingAddressOptions,
                    billingAddressOptions
                )
            }
            val orderCartItems = order.optJSONArray(C.CART_ITEMS)
            orderCartItems?.let {
                val cartItem = orderCartItems.optJSONObject(0)
                UpsertContactCartTests.validateCartItem(cartItem, testOrder.testCartItem)
            }

            val tax = order.optDouble(C.TAX)
            testOrder.tax.let {
                Assert.assertEquals(
                    "Order tax is not present in json or invalid",
                    it,
                    tax,
                    0.0001
                )
            }
            val shippingAndHandling = order.optDouble(C.SHIPPING_AND_HANDLING)
            testOrder.shippingAndHandling.let {
                Assert.assertEquals(
                    "Order shippingAndHandling is not present in the json or invalid",
                    it,
                    shippingAndHandling,
                    0.0001
                )
            }
            val propertiesJsonObject = order.optString(C.PROPERTIES)
            val properties = JsonUtils.getPropertyMapFromJson(propertiesJsonObject)
            testOrder.properties?.let { propertyList ->
                Assert.assertEquals(
                    "Properties are not present in json or invalid",
                    propertyList,
                    properties
                )
            }
            val purchaseDate = order.optString(C.PURCHASE_DATE)
            val testPurchaseDate = TimeUtils.getTimestamp(testOrder.purchaseDate)
            Assert.assertEquals(
                "Properties are not present in json or invalid",
                testPurchaseDate,
                purchaseDate
            )
        }
    }
}