package com.cordial.cordialsdk

import android.content.Context
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.cordialsdk.testdata.TestCartItem
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import com.cordial.util.JsonUtils
import com.cordial.util.TimeUtils
import com.cordial.util.toMutableList
import org.json.JSONArray
import org.json.JSONObject
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class UpsertContactCartTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener
    lateinit var upsertContactCartDownLatch: CountDownLatch
    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testCartItem = TestCartItem()

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            SendingCacheState.sendingUpsertContactCart.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_upsert_contact_cart_was_called_then_cart_is_sending() {
        upsertContactCartDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { UpsertContactCartMockRequestSender() }

        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        upsertContactCartDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_upsert_empty_contact_cart_was_called_then_cart_is_sending() {
        upsertContactCartDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = { UpsertEmptyContactCartMockRequestSender() }

        val cartItems = listOf<CartItem>()
        cordialApi.upsertContactCart(cartItems)
        upsertContactCartDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_upsert_contact_cart_was_cached_then_cart_is_sending() {
        upsertContactCartDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = { UpsertContactCartMockRequestSender() }

        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        upsertContactCartDownLatch.await()
    }


    @Test
    fun given_sdk_initialized_when_two_upsert_contact_cart_were_cached_then_last_cart_is_sending() {
        upsertContactCartDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = { UpsertEmptyContactCartMockRequestSender() }

        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        val emptyCartItems = listOf<CartItem>()
        cordialApi.upsertContactCart(emptyCartItems)
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        upsertContactCartDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_upsert_contact_cart_is_sending_and_caching_then_jwt_token_is_updating_and_cart_is_sent() {
        upsertContactCartDownLatch = CountDownLatch(1)
        testCordialApi.clearJwtToken(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.UPSERT_CONTACT_CART)) {
                        checkContactCart(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        upsertContactCartDownLatch.await()
    }

    @Test
    fun given_upsert_contact_cart_is_sending_when_jwt_token_is_expired_then_cart_is_caching_then_jwt_token_is_updating_and_cart_is_sent() {
        upsertContactCartDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                   if (request.url.contains(CordialApiEndpoints.Url.UPSERT_CONTACT_CART)) {
                       if (isJwtTokenExpired) {
                           isJwtTokenExpired = false
                           MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                       } else {
                           checkContactCart(request, responseHandler, onResponseListener)
                       }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                       MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                   }
                }
            }
        }
        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        upsertContactCartDownLatch.await()
    }

    @Test
    fun given_contact_is_not_logged_in_when_upsert_contact_cart_is_sending_and_caching_then_contact_is_logging_in_and_cart_is_sent() {
        upsertContactCartDownLatch = CountDownLatch(1)
        testCordialApi.removePrimaryKeyAndLoginState(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.CONTACTS_URL)) {
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.UPSERT_CONTACT_CART)) {
                        checkContactCart(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        val cartItems = getCartItems()
        cordialApi.upsertContactCart(cartItems)
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.setContact(testContact)
        }
        upsertContactCartDownLatch.await()
    }

    private fun getCartItems(): List<CartItem> {
        val cartItems = mutableListOf<CartItem>()
        val cartItem = CartItem(
            testCartItem.productID,
            testCartItem.name,
            testCartItem.sku,
            testCartItem.category,
            testCartItem.qty
        )
            .withUrl(testCartItem.url)
            .withItemDescription(testCartItem.itemDescription)
            .withItemPrice(testCartItem.itemPrice)
            .withSalePrice(testCartItem.salePrice)
            .withAttr(testCartItem.attr)
            .withImages(testCartItem.images)
            .withProperties(testCartItem.properties)
            .withTimestamp(testCartItem.timestamp)
        cartItems.add(cartItem)
        return cartItems
    }

    inner class UpsertContactCartMockRequestSender : RequestSender {
        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            checkContactCart(request, responseHandler, onResponseListener)
        }
    }

    private fun checkContactCart(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
        request.jsonBody?.let { json ->
            val jsonArray = JSONArray(json)
            val jsonObject = jsonArray.getJSONObject(0)
            val contact = jsonObject.optString(C.PRIMARY_KEY)
            Assert.assertEquals(
                "Primary key is not present in json or invalid",
                testContact,
                contact
            )

            val cartItems = jsonObject.getJSONArray(C.CART_ITEMS)
            val cartItem = cartItems.getJSONObject(0)

            validateCartItem(cartItem, testCartItem)
            MockResponse.mockSuccessCartResponse(request, responseHandler, onResponseListener)
            upsertContactCartDownLatch.countDown()
        }
    }

    companion object {
        fun validateCartItem(cartItem: JSONObject, testCartItem: TestCartItem) {
            val productID = cartItem.getString(C.PRODUCT_ID)
            Assert.assertEquals(
                "Product ID is not present in json or invalid",
                testCartItem.productID,
                productID
            )
            val name = cartItem.getString(C.NAME)
            Assert.assertEquals(
                "Name is not present in json or invalid",
                testCartItem.name,
                name
            )
            val sku = cartItem.getString(C.SKU)
            Assert.assertEquals(
                "Sku is not present in json or invalid",
                testCartItem.sku,
                sku
            )
            val category = cartItem.getString(C.CATEGORY)
            Assert.assertEquals(
                "Category is not present in json or invalid",
                testCartItem.category,
                category
            )
            val url = cartItem.getString(C.URL)
            Assert.assertEquals(
                "Url is not present in json or invalid",
                testCartItem.url,
                url
            )
            val itemDescription = cartItem.getString(C.DESCRIPTION)
            Assert.assertEquals(
                "Item description is not present in json or invalid",
                testCartItem.itemDescription,
                itemDescription
            )
            val qty = cartItem.getInt(C.QTY)
            Assert.assertEquals(
                "Qty is not present in json or invalid",
                testCartItem.qty,
                qty
            )
            val itemPrice = cartItem.getDouble(C.ITEM_PRICE)
            Assert.assertEquals(
                "Item price is not present in json or invalid",
                testCartItem.itemPrice,
                itemPrice,
                0.0001
            )
            val salePrice = cartItem.getDouble(C.SALE_PRICE)
            Assert.assertEquals(
                "Sale price is not present in json or invalid",
                testCartItem.salePrice,
                salePrice,
                0.0001
            )
            val attr = JsonUtils.getMapFromJson(cartItem.optJSONObject(C.ATTR)?.toString())
            Assert.assertEquals(
                "Attr are not present in json or invalid",
                testCartItem.attr,
                attr
            )
            val images = (cartItem.optJSONArray(C.IMAGES) ?: JSONArray()).toMutableList().toTypedArray()
            Assert.assertArrayEquals(
                "Images are not present in json or invalid",
                testCartItem.images.toTypedArray(),
                images
            )
            val properties =
                JsonUtils.getPropertyMapFromJson(cartItem.optJSONObject(C.PROPERTIES)?.toString())
            Assert.assertEquals(
                "Properties are not present in json or invalid",
                testCartItem.properties,
                properties
            )
            val timestamp = cartItem.optString(C.TIMESTAMP)
            val testTimestamp = TimeUtils.getTimestamp(testCartItem.timestamp)
            Assert.assertEquals(
                "Timestamp is not present in json or invalid",
                testTimestamp,
                timestamp
            )
        }
    }

    inner class UpsertEmptyContactCartMockRequestSender : RequestSender {
        override fun send(
            request: SDKRequest,
            responseHandler: ResponseHandler,
            onResponseListener: OnResponseListener
        ) {
            Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")

            request.jsonBody?.let { json ->
                val jsonArray = JSONArray(json)
                val jsonObject = jsonArray.optJSONObject(0)
                val contact = jsonObject.optString(C.PRIMARY_KEY)
                Assert.assertEquals(
                    "Primary key is not present in json or invalid",
                    testContact,
                    contact
                )

                val cartItems = jsonObject.optJSONArray(C.CART_ITEMS)
                cartItems?.let {
                    val cartItemsSize = cartItems.length()
                    Assert.assertEquals(
                        "Cart isn't empty",
                        0,
                        cartItemsSize
                    )
                    MockResponse.mockSuccessCartResponse(request, responseHandler, onResponseListener)
                    upsertContactCartDownLatch.countDown()
                }
            }
        }
    }


}