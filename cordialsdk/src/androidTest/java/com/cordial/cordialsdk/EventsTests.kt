package com.cordial.cordialsdk

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.cordialsdk.mock.MockNetworkStateListener
import com.cordial.cordialsdk.testdata.MockResponse
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.lifecycle.AppLifecycleHandler
import com.cordial.lifecycle.devicelock.DeviceLockStatusRequester
import com.cordial.lifecycle.devicelock.OnDeviceLockStatus
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.Delay
import com.cordial.util.JsonUtils
import org.json.JSONArray
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.CountDownLatch

internal class EventsTests {
    lateinit var context: Context
    private lateinit var testCordialApi: TestCordialApi
    private lateinit var cordialApi: CordialApi
    private lateinit var mockNetworkStateListener: MockNetworkStateListener

    private val testToken = "test token21"
    private var testContact = "test@example.com"
    private var testProperties = mapOf(
        "firstName" to PropertyValue.StringProperty("Test"),
        "surname" to PropertyValue.StringProperty("Guest"),
        "age" to PropertyValue.NumericProperty(24),
        "maritalStatus" to PropertyValue.BooleanProperty(false),
        "languages" to PropertyValue.JSONArrayProperty(JSONArray("[\"english\", \"spanish\"]"))
    )
    private var testLongitude = 44.44444
    private var testLatitude = 55.55555
    private val eventsBulkSize = 3

    @Before
    fun setup() {
        context = InstrumentationRegistry.getInstrumentation().context
        testCordialApi = TestCordialApi()
        testCordialApi.clearPreferences(context)
        testCordialApi.createJwtToken(context)
        testCordialApi.createFirebaseToken(context, testToken)
        testCordialApi.skipAppInstallEvent(context)
        testCordialApi.savePrimaryKeyAndLoginState(context, testContact)
        mockNetworkStateListener = MockNetworkStateListener()
        DependencyConfiguration.getInstance().networkState = mockNetworkStateListener
        initializeSdk()
        val countDownLatch = CountDownLatch(1)
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.clearCache {
            SendingCacheState.sendingEvents.set(false)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    private fun initializeSdk() {
        AppLifecycleHandler.appInForeground = true
        val accountKey = "qc-all-channels"
        val channelKey = "push"
        val host = "https://events-stream-svc.stg.cordialdev.com/"
        val config = CordialApiConfiguration.getInstance()
        config.pushesConfiguration = PushesConfiguration.SDK
        config.eventsBulkSize = eventsBulkSize
        config.eventsBulkUploadInterval = 3
        config.initialize(context, accountKey, channelKey, host)
        cordialApi = CordialApi()
    }

    @Test
    fun given_sdk_initialized_when_bulk_size_reached_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    checkEventBulkSize(request, countDownLatch, eventsBulkSize)
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = eventsBulkSize
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_bulk_interval_is_triggered_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    checkEventBulkSize(request, countDownLatch, eventsBulkSize - 1)
                }
            }
        }
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_and_no_network_when_network_connection_is_available_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    checkEventBulkSize(request, countDownLatch, eventsBulkSize)
                }
            }
        }
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        Delay(timeMillis = 100L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_events_qty_is_reached_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        val eventsQty = 3
        mockNetworkStateListener.isNetworkAvailable = false
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val eventsRequestSize = JSONArray(json).length()
                        Assert.assertEquals(
                            "Events qty is invalid",
                            eventsQty,
                            eventsRequestSize
                        )
                        CordialApiConfiguration.getInstance().setQty(100)
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().setQty(eventsQty)

        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        cordialApi.sendEvent("testEventName4", null)
        cordialApi.sendEvent("testEventName5", null)
        Delay(timeMillis = 200L).doAfterDelay {
            mockNetworkStateListener.isNetworkAvailable = true
        }
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_app_is_closed_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val eventsRequestSize = JSONArray(json).length()
                        Assert.assertTrue(
                            "Events bulk size after app close event is invalid. " +
                                    "Expected greater than 1, but was $eventsRequestSize",
                            eventsRequestSize >= 1
                        )
                        DependencyConfiguration.getInstance().deviceLockStatusRequester = DeviceLockStatusRequester()
                        countDownLatch.countDown()
                    }
                }
            }
        }
        DependencyConfiguration.getInstance().deviceLockStatusRequester = object : OnDeviceLockStatus {
            override fun isDeviceLocked(): Boolean {
                return true
            }
        }
        cordialApi.sendEvent("testEventName1", null)
        val intent = Intent(Intent.ACTION_SCREEN_OFF)
        CordialApiConfiguration.getInstance().deviceLockListener?.onReceive(context, intent)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_properties_of_system_events_are_initialized_then_events_with_properties_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        val properties =
                            JsonUtils.getPropertyMapFromJson(jsonObject.optString("properties"))
                        Assert.assertEquals(
                            "Properties of system events are not present in the json or invalid",
                            testProperties,
                            properties
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 1
        CordialApiConfiguration.getInstance().setSystemEventProperties(testProperties)
        cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_APP_OPEN)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_longitude_latitude_are_initialized_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        val longitude = jsonObject.optDouble("lon")
                        val latitude = jsonObject.optDouble("lat")
                        Assert.assertEquals(
                            "Event's longitude is not present in the json or invalid",
                            testLongitude,
                            longitude,
                            0.00001
                        )
                        Assert.assertEquals(
                            "Event's latitude is not present in the json or invalid",
                            testLatitude,
                            latitude,
                            0.00001
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 1
        cordialApi.setLongLat(testLongitude, testLatitude)
        cordialApi.sendEvent("testEventName1", null)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_custom_events_contain_invalid_event_then_events_are_sending_without_failed_event() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonArray = JSONArray(json)
                        val failedEventsIndexes = mutableListOf<Int>()
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject = jsonArray.optJSONObject(i)
                            val eventName = jsonObject.optString("event")
                            if (eventName == "invalid") {
                                failedEventsIndexes.add(i)
                            }
                        }
                        if (failedEventsIndexes.isNotEmpty()) {
                            var error = "{\"success\":false,\"error\":{\"code\":422,\"message\":" +
                                    "\"The given data was invalid.\",\"errors\":" +
                                    "{"
                            failedEventsIndexes.forEach { index ->
                                error += "\"$index.event\":[\"The $index.event field is required.\"]"
                                if (failedEventsIndexes.indexOf(index) != failedEventsIndexes.lastIndex) {
                                    error += ","
                                }
                            }
                            error += "}}}"
                            MockResponse.mockEventErrorResponse(request, error, responseHandler, onResponseListener)
                            if (failedEventsIndexes.size == jsonArray.length()) {
                                countDownLatch.countDown()
                            }
                        } else {
                            countDownLatch.countDown()
                        }
                    }
                }
            }
        }
        cordialApi.sendEvent("valid", null)
        cordialApi.sendEvent("invalid", null)
        cordialApi.sendEvent("valid", null)
        cordialApi.sendEvent("invalid", null)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_flush_events_is_triggered_then_events_are_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    checkEventBulkSize(request, countDownLatch, eventsBulkSize - 1)
                }
            }
        }
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        Delay(timeMillis = 100L).doAfterDelay {
            cordialApi.flushEvents()
        }
        countDownLatch.await()
    }

    @Test
    fun given_system_properties_are_initialized_when_new_empty_system_properties_are_setting_then_event_with_empty_system_properties_is_sent() {
        val countDownLatch = CountDownLatch(1)
        CordialApiConfiguration.getInstance().setSystemEventProperties(testProperties)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonObject = JSONArray(json).optJSONObject(0)
                        val properties =
                            JsonUtils.getPropertyMapFromJson(jsonObject.optString("properties"))
                        Assert.assertEquals(
                            "Properties of system events are not empty",
                            mapOf<String, PropertyValue>(),
                            properties
                        )
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 1
        CordialApiConfiguration.getInstance().setSystemEventProperties(mapOf())
        cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_APP_OPEN)
        countDownLatch.await()
    }

    @Test
    fun given_sdk_initialized_when_event_with_crdl_prefix_is_trying_to_send_then_event_is_not_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val jsonArray = JSONArray(json)
                        for (i in 0 until jsonArray.length()) {
                            val jsonObject = jsonArray.optJSONObject(i)
                            val eventName = jsonObject.optString("event")
                            Assert.assertEquals(
                                "Event name is invalid",
                                "test_event_name_${i + 2}",
                                eventName
                            )
                        }
                        countDownLatch.countDown()
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = 2
        cordialApi.sendEvent("crdl_test_event_name_1", null)
        cordialApi.sendEvent("test_event_name_2", null)
        cordialApi.sendEvent("test_event_name_3", null)
        countDownLatch.await()
    }

    @Test
    fun given_no_jwt_token_when_upsert_contact_cart_is_sending_and_caching_then_jwt_token_is_updating_and_cart_is_sent() {
        val countDownLatch = CountDownLatch(1)
        testCordialApi.clearJwtToken(context)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    } else if (request.url.contains(CordialApiEndpoints.Url.SEND_EVENT)) {
                        checkEventBulkSize(request, countDownLatch, eventsBulkSize)
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = eventsBulkSize
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        countDownLatch.await()
    }

    @Test
    fun given_upsert_contact_cart_is_sending_when_jwt_token_is_expired_then_cart_is_caching_then_jwt_token_is_updating_and_cart_is_sent() {
        val countDownLatch = CountDownLatch(1)
        var isJwtTokenExpired = true
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    if (request.url.contains(CordialApiEndpoints.Url.SEND_EVENT)) {
                        if (isJwtTokenExpired) {
                            isJwtTokenExpired = false
                            MockResponse.mockErrorJwtResponse(request, responseHandler, onResponseListener)
                        } else {
                            checkEventBulkSize(request, countDownLatch, eventsBulkSize)
                        }
                    } else if (request.url.contains(CordialApiEndpoints.Url.SDK_SECURITY_URL)) {
                        MockResponse.mockSuccessJwtTokenResponse(request, responseHandler, onResponseListener)
                    }
                }
            }
        }
        CordialApiConfiguration.getInstance().eventsBulkSize = eventsBulkSize
        cordialApi.sendEvent("testEventName1", null)
        cordialApi.sendEvent("testEventName2", null)
        cordialApi.sendEvent("testEventName3", null)
        countDownLatch.await()
    }

    @Test
    fun given_app_install_when_contact_is_registered_and_events_are_sending_then_app_install_event_is_sending() {
        val countDownLatch = CountDownLatch(1)
        DependencyConfiguration.getInstance().requestSender = {
            object : RequestSender {
                override fun send(
                    request: SDKRequest,
                    responseHandler: ResponseHandler,
                    onResponseListener: OnResponseListener
                ) {
                    Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
                    request.jsonBody?.let { json ->
                        val eventName = JSONArray(json).optJSONObject(0).optString("event")
                        Assert.assertEquals(
                            "App install event is not present in the json",
                            C.EVENT_NAME_APP_INSTALL,
                            eventName
                        )
                        MockResponse.mockSuccessResponse(request, responseHandler, onResponseListener)
                        countDownLatch.countDown()
                    }
                }
            }
        }
        testCordialApi.removeWasLaunchedBeforeFlag(context)
        initializeSdk()
        CordialApiConfiguration.getInstance().eventsBulkSize = 1
        countDownLatch.await()
    }

    private fun checkEventBulkSize(
        request: SDKRequest,
        countDownLatch: CountDownLatch,
        eventsBulkSize: Int
    ) {
        Log.d(C.LOG_TAG, "${request.url} : ${request.jsonBody}")
        request.jsonBody?.let { json ->
            val eventsRequestSize = JSONArray(json).length()
            Assert.assertEquals(
                "Events bulk size is invalid",
                eventsBulkSize,
                eventsRequestSize
            )
            countDownLatch.countDown()
        }
    }
}