package com.cordial.storage.db.dao.event

import com.cordial.feature.sendevent.model.EventRequest

internal interface EventDao {
    fun insert(eventRequest: EventRequest, onFinishSaveListener: (() -> Unit)?)
    fun clear(onEventCacheClearListener: (() -> Unit)? = null)
    fun deleteEvents(events: List<EventRequest>, onDeleteCompleteListener: () -> Unit)
    fun getEventsCount(onEventsCount: (count: Long) -> Unit)
    fun getAllEvents(onCachedEventsListener: (cachedEvents: List<EventRequest>) -> Unit)
    fun updateEventLimit(newQty: Int)
}