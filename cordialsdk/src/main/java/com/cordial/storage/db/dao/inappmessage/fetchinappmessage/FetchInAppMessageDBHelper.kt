package com.cordial.storage.db.dao.inappmessage.fetchinappmessage

import android.content.ContentValues
import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class FetchInAppMessageDBHelper : BaseDBHelper(), FetchInAppMessageDao {

    override fun insert(inAppMessageProperties: InAppMessageProperties, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.MC_ID, inAppMessageProperties.mcID)
                values.put(CordialSdkDBHelper.IN_APP_MESSAGE_TYPE, inAppMessageProperties.type.type)
                values.put(
                    CordialSdkDBHelper.EXPIRATION_TIME,
                    inAppMessageProperties.expirationTime
                )
                values.put(CordialSdkDBHelper.TIME_IN_MILLIS, inAppMessageProperties.timestamp)
                values.put(CordialSdkDBHelper.URL, inAppMessageProperties.url)
                values.put(CordialSdkDBHelper.URL_EXPIRE_AT, inAppMessageProperties.urlExpireAt)
                db.writableDatabase.insert(
                    CordialSdkDBHelper.TABLE_FETCH_IN_APP_MESSAGE,
                    null,
                    values
                )
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_FETCH_IN_APP_MESSAGE, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun getInAppMessageProperties(onInAppMessagePropertiesListener: ((inAppMessageProperties: InAppMessageProperties?, id: Int) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_FETCH_IN_APP_MESSAGE} ORDER BY ${CordialSdkDBHelper.TIME_IN_MILLIS} DESC LIMIT 1"
                val cursor = db.readableDatabase.rawQuery(query, null)
                var id = -1
                var inAppMessageProperties: InAppMessageProperties? = null
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    val messageTypeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.IN_APP_MESSAGE_TYPE)
                    val expirationTimeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.EXPIRATION_TIME)
                    val timestampIndex = cursor.getColumnIndex(CordialSdkDBHelper.TIME_IN_MILLIS)
                    val urlIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL)
                    val urlExpireAtIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL_EXPIRE_AT)
                    do {
                        id = cursor.getInt(idIndex)
                        inAppMessageProperties =
                            InAppMessageProperties(
                                cursor.getString(mcIDIndex),
                                InAppMessageType.findKey(
                                    cursor.getString(
                                        messageTypeIndex
                                    )
                                ),
                                cursor.getString(expirationTimeIndex),
                                cursor.getLong(timestampIndex),
                                cursor.getString(urlIndex),
                                cursor.getString(urlExpireAtIndex)
                            )

                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onInAppMessagePropertiesListener?.invoke(inAppMessageProperties, id)
                }
                cursor.close()
            }
        }
    }

    override fun deleteMcID(id: Int, onSuccessDeleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_FETCH_IN_APP_MESSAGE,
                    CordialSdkDBHelper.ID + "=" + id,
                    null
                )
                withContext(Dispatchers.Main) {
                    onSuccessDeleteListener?.invoke()
                }
            }
        }
    }
}