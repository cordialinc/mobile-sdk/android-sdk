package com.cordial.storage.db.dao.inboxmessage.updateinboxmessagereadstatus

import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest

internal interface UpdateInboxMessageReadStatusDao {
    fun insert(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onSaveRequestListener: (() -> Unit)?
    )

    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun deleteUpdateInboxMessageReadStatusRequests(requestsIds: List<Int>, onCompleteListener: (() -> Unit)?)
    fun getUpdateInboxMessageReadStatusRequests(
        isAnonymousContact: Boolean,
        onCachedUpdateInboxReadStatusListener: (requests: List<UpdateInboxMessageReadStatusRequest>) -> Unit
    )
}