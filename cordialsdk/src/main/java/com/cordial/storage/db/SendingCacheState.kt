package com.cordial.storage.db

import java.util.concurrent.atomic.AtomicBoolean

internal class SendingCacheState {
    companion object {
        var sendingUpsertContact: AtomicBoolean = AtomicBoolean(false)
        var sendingContactLogout: AtomicBoolean = AtomicBoolean(false)
        var sendingEvents: AtomicBoolean = AtomicBoolean(false)
        var sendingContactOrder: AtomicBoolean = AtomicBoolean(false)
        var sendingUpsertContactCart: AtomicBoolean = AtomicBoolean(false)
        var gettingInAppMessage: AtomicBoolean = AtomicBoolean(false)
        var updatingInboxMessageReadStatus: AtomicBoolean = AtomicBoolean(false)
        var deletingInboxMessage: AtomicBoolean = AtomicBoolean(false)
        var isJWTCurrentlyFetching = AtomicBoolean(false)
    }
}