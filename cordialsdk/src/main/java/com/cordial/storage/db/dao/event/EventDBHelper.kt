package com.cordial.storage.db.dao.event

import android.content.ContentValues
import android.database.DatabaseUtils
import com.cordial.feature.sendevent.model.EventRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import com.cordial.util.JsonUtils
import com.cordial.util.nullable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class EventDBHelper : BaseDBHelper(), EventDao {

    override fun insert(eventRequest: EventRequest, onFinishSaveListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.TIMESTAMP, eventRequest.timestamp)
                values.put(CordialSdkDBHelper.EVENT_NAME, eventRequest.eventName)
                eventRequest.longitude?.let {
                    values.put(CordialSdkDBHelper.LONGITUDE, it)
                }
                eventRequest.latitude?.let {
                    values.put(CordialSdkDBHelper.LATITUDE, it)
                }
                eventRequest.properties?.let {
                    values.put(
                        CordialSdkDBHelper.PROPERTIES,
                        JsonUtils.getJsonFromPropertyValueMap(it).toString()
                    )
                }
                eventRequest.mcID?.let {
                    values.put(CordialSdkDBHelper.MC_ID, it)
                }
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_CUSTOM_EVENTS, null, values)
                onFinishSaveListener?.invoke()
            }
        }
    }

    override fun clear(onEventCacheClearListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_CUSTOM_EVENTS, null, null)
            withContext(Dispatchers.Main) {
                onEventCacheClearListener?.invoke()
            }
        }
    }

    override fun deleteEvents(events: List<EventRequest>, onDeleteCompleteListener: () -> Unit) {
        doAsyncDbCall {
            events.forEach { event ->
                if (event.id != -1) {
                    dataBase?.writableDatabase?.delete(
                        CordialSdkDBHelper.TABLE_CUSTOM_EVENTS,
                        CordialSdkDBHelper.ID + "=" + event.id,
                        null
                    )
                }
            }
            onDeleteCompleteListener.invoke()
        }
    }

    override fun getEventsCount(onEventsCount: (count: Long) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val count = DatabaseUtils.queryNumEntries(
                    db.readableDatabase,
                    CordialSdkDBHelper.TABLE_CUSTOM_EVENTS
                )
                withContext(Dispatchers.Main) {
                    onEventsCount.invoke(count)
                }
            }
        }
    }

    override fun getAllEvents(onCachedEventsListener: (cachedEvents: List<EventRequest>) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val cursor = db.readableDatabase.query(
                    CordialSdkDBHelper.TABLE_CUSTOM_EVENTS,
                    arrayOf(
                        CordialSdkDBHelper.ID,
                        CordialSdkDBHelper.TIMESTAMP,
                        CordialSdkDBHelper.EVENT_NAME,
                        CordialSdkDBHelper.LONGITUDE,
                        CordialSdkDBHelper.LATITUDE,
                        CordialSdkDBHelper.PROPERTIES,
                        CordialSdkDBHelper.MC_ID
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )
                val requests = mutableListOf<EventRequest>()
                var eventRequest: EventRequest
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)
                    val timestampIndex = cursor.getColumnIndex(CordialSdkDBHelper.TIMESTAMP)
                    val eventNameIndex = cursor.getColumnIndex(CordialSdkDBHelper.EVENT_NAME)
                    val longitudeIndex = cursor.getColumnIndex(CordialSdkDBHelper.LONGITUDE)
                    val latitudeIndex = cursor.getColumnIndex(CordialSdkDBHelper.LATITUDE)
                    val propertiesIndex = cursor.getColumnIndex(CordialSdkDBHelper.PROPERTIES)
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)

                    do {
                        eventRequest =
                            EventRequest(
                                "",
                                "",
                                cursor.getString(timestampIndex),
                                cursor.getString(eventNameIndex),
                                cursor.getDouble(longitudeIndex).nullable(),
                                cursor.getDouble(latitudeIndex).nullable(),
                                JsonUtils.getPropertyMapFromJson(
                                    cursor.getString(
                                        propertiesIndex
                                    )
                                ),
                                cursor.getString(mcIDIndex)
                            )
                        eventRequest.id = cursor.getInt(idIndex)
                        requests.add(eventRequest)
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onCachedEventsListener.invoke(requests)
                }
                cursor.close()
            }
        }
    }

    override fun updateEventLimit(newQty: Int) {
        doAsyncDbCall {
            dataBase?.updateEventLimit(newQty)
        }
    }
}