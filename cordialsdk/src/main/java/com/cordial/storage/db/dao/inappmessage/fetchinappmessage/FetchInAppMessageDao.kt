package com.cordial.storage.db.dao.inappmessage.fetchinappmessage

import com.cordial.feature.inappmessage.model.InAppMessageProperties

internal interface FetchInAppMessageDao {
    fun insert(inAppMessageProperties: InAppMessageProperties, onSaveRequestListener: (() -> Unit)?)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun getInAppMessageProperties(onInAppMessagePropertiesListener: ((inAppMessageProperties: InAppMessageProperties?, id: Int) -> Unit)?)
    fun deleteMcID(id: Int, onSuccessDeleteListener: (() -> Unit)?)
}