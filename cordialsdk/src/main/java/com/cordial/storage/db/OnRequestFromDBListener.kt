package com.cordial.storage.db

internal interface OnRequestFromDBListener {
    fun onSuccess()
    fun onFailure()
}