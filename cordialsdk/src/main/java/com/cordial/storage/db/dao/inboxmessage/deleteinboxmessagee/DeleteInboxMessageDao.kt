package com.cordial.storage.db.dao.inboxmessage.deleteinboxmessagee

import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest

internal interface DeleteInboxMessageDao {
    fun insert(deleteInboxMessageRequest: DeleteInboxMessageRequest, onSaveRequestListener: (() -> Unit)?)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun deleteDeleteInboxMessageRequest(requestId: Int, onSuccessDeleteListener: (() -> Unit)?)
    fun getDeleteInboxMessageRequest(
        onDeleteInboxMessageRequest: ((deleteInboxMessageRequest: DeleteInboxMessageRequest?, requestId: Int) -> Unit)?
    )
}