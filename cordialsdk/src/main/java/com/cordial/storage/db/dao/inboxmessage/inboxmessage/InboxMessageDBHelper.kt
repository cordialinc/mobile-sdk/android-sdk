package com.cordial.storage.db.dao.inboxmessage.inboxmessage

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class InboxMessageDBHelper : BaseDBHelper(), InboxMessageDao {

    override fun insert(inboxMessages: List<InboxMessage>, onSaveRequestListener: (() -> Unit)?) {
        inboxMessages.forEachIndexed { index, inboxMessage ->
            val isLastItem = index == (inboxMessages.size - 1)
            insert(inboxMessage, onSaveRequestListener, isLastItem)
        }
    }

    override fun insert(inboxMessage: InboxMessage, onSaveRequestListener: (() -> Unit)?, isLastItem: Boolean) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.MC_ID, inboxMessage.mcID)
                values.put(CordialSdkDBHelper.IS_READ, inboxMessage.isRead)
                values.put(CordialSdkDBHelper.SENT_AT, inboxMessage.sentAt)
                values.put(CordialSdkDBHelper.URL, inboxMessage.url)
                values.put(CordialSdkDBHelper.URL_EXPIRE_AT, inboxMessage.urlExpireAt)
                values.put(CordialSdkDBHelper.INBOX_PREVIEW_DATA, inboxMessage.previewData)
                db.writableDatabase.insertWithOnConflict(
                    CordialSdkDBHelper.TABLE_INBOX_MESSAGE,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_REPLACE
                )
                if (isLastItem) {
                    withContext(Dispatchers.Main) {
                        onSaveRequestListener?.invoke()
                    }
                }
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_INBOX_MESSAGE, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun deleteInboxMessage(mcID: String, onSuccessDeleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_INBOX_MESSAGE,
                    CordialSdkDBHelper.MC_ID + "='$mcID'",
                    null
                )
                withContext(Dispatchers.Main) {
                    onSuccessDeleteListener?.invoke()
                }
            }
        }
    }

    override fun getInboxMessage(mcID: String, onInboxMessageListener: ((inboxMessage: InboxMessage?) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_INBOX_MESSAGE} " +
                            "WHERE ${CordialSdkDBHelper.MC_ID} = '$mcID' LIMIT 1"

                val cursor = db.readableDatabase.rawQuery(query, null)
                var inboxMessage: InboxMessage? = null
                if (cursor.moveToFirst()) {
                    val isReadIndex = cursor.getColumnIndex(CordialSdkDBHelper.IS_READ)
                    val sentAtIndex = cursor.getColumnIndex(CordialSdkDBHelper.SENT_AT)
                    val urlIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL)
                    val urlExpireAtIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL_EXPIRE_AT)
                    val previewDataIndex = cursor.getColumnIndex(CordialSdkDBHelper.INBOX_PREVIEW_DATA)
                    do {
                        val isRead = cursor.getInt(isReadIndex) > 0
                        val sentAt = cursor.getString(sentAtIndex)
                        val url = cursor.getString(urlIndex)
                        val urlExpireAt = cursor.getString(urlExpireAtIndex)
                        val previewData = cursor.getString(previewDataIndex)
                        inboxMessage = InboxMessage(mcID, isRead, sentAt, previewData)
                        inboxMessage.url = url
                        inboxMessage.urlExpireAt = urlExpireAt
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onInboxMessageListener?.invoke(inboxMessage)
                }
            }
        }
    }

    override fun getInboxMessages(onInboxMessageListener: ((inboxMessages: List<InboxMessage>) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_INBOX_MESSAGE}"

                val cursor = db.readableDatabase.rawQuery(query, null)
                val inboxMessages = mutableListOf<InboxMessage>()
                if (cursor.moveToFirst()) {
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    val isReadIndex = cursor.getColumnIndex(CordialSdkDBHelper.IS_READ)
                    val sentAtIndex = cursor.getColumnIndex(CordialSdkDBHelper.SENT_AT)
                    val urlIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL)
                    val urlExpireAtIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL_EXPIRE_AT)
                    val previewDataIndex = cursor.getColumnIndex(CordialSdkDBHelper.INBOX_PREVIEW_DATA)
                    do {
                        val mcID = cursor.getString(mcIDIndex)
                        val isRead = cursor.getInt(isReadIndex) > 0
                        val sentAt = cursor.getString(sentAtIndex)
                        val url = cursor.getString(urlIndex)
                        val urlExpireAt = cursor.getString(urlExpireAtIndex)
                        val previewData = cursor.getString(previewDataIndex)
                        val inboxMessage = InboxMessage(mcID, isRead, sentAt, previewData)
                        inboxMessage.url = url
                        inboxMessage.urlExpireAt = urlExpireAt
                        inboxMessages.add(inboxMessage)
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onInboxMessageListener?.invoke(inboxMessages)
                }
            }
        }
    }
}