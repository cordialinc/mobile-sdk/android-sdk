package com.cordial.storage.db.dao.inappmessage.inappmessagetodelete

import android.content.ContentValues
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class InAppMessageToDeleteDBHelper : BaseDBHelper(), InAppMessageToDeleteDao {
    override fun insert(mcID: String) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.MC_ID, mcID)
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_IN_APP_MESSAGE_TO_DELETE, null, values)
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_IN_APP_MESSAGE_TO_DELETE, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun getInAppMessagesToDelete(onInAppMessagesMcIDListener: ((mcIDsToDelete: List<String>) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query = "SELECT * FROM ${CordialSdkDBHelper.TABLE_IN_APP_MESSAGE_TO_DELETE}"
                val cursor = db.readableDatabase.rawQuery(query, null)
                val mcIDsToDelete = mutableListOf<String>()
                if (cursor.moveToFirst()) {
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    do {
                        val mcID = cursor.getString(mcIDIndex)
                        mcIDsToDelete.add(mcID)
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onInAppMessagesMcIDListener?.invoke(mcIDsToDelete)
                }
                cursor.close()
            }
        }
    }

    override fun removeInAppMessageToDelete(mcID: String) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(
                CordialSdkDBHelper.TABLE_IN_APP_MESSAGE_TO_DELETE, "${CordialSdkDBHelper.MC_ID} =?",
                arrayOf(mcID)
            )
        }
    }
}