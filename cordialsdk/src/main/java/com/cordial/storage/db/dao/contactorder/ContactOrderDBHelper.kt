package com.cordial.storage.db.dao.contactorder

import android.content.ContentValues
import com.cordial.feature.sendcontactorder.model.Address
import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.feature.sendcontactorder.model.Order
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import com.cordial.storage.db.dao.contactcart.ContactCartDBUtils
import com.cordial.util.JsonUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class ContactOrderDBHelper : BaseDBHelper(), ContactOrderDao {

    override fun insert(contactOrderRequest: ContactOrderRequest, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                val order = contactOrderRequest.order
                contactOrderRequest.mcID?.let { mcID ->
                    values.put(CordialSdkDBHelper.MC_ID, mcID)
                }
                contactOrderRequest.mcTapTime?.let { mcTapTime ->
                    values.put(CordialSdkDBHelper.MC_TAP_TIME, mcTapTime)
                }
                values.put(CordialSdkDBHelper.ORDER_ID, order.orderId)
                values.put(CordialSdkDBHelper.STATUS, order.status)
                values.put(CordialSdkDBHelper.STORE_ID, order.storeId)
                values.put(CordialSdkDBHelper.CUSTOMER_ID, order.customerId)
                values.put(CordialSdkDBHelper.PURCHASE_DATE, order.getPurchaseDate())
                values.put(CordialSdkDBHelper.SHIPPING_NAME, order.shippingAddress.name)
                values.put(CordialSdkDBHelper.SHIPPING_ADDRESS, order.shippingAddress.address)
                values.put(CordialSdkDBHelper.SHIPPING_CITY, order.shippingAddress.city)
                values.put(CordialSdkDBHelper.SHIPPING_STATE, order.shippingAddress.state)
                values.put(CordialSdkDBHelper.SHIPPING_POSTAL_CODE, order.shippingAddress.postalCode)
                values.put(CordialSdkDBHelper.SHIPPING_COUNTRY, order.shippingAddress.country)
                values.put(CordialSdkDBHelper.BILLING_NAME, order.billingAddress.name)
                values.put(CordialSdkDBHelper.BILLING_ADDRESS, order.billingAddress.address)
                values.put(CordialSdkDBHelper.BILLING_CITY, order.billingAddress.city)
                values.put(CordialSdkDBHelper.BILLING_STATE, order.billingAddress.state)
                values.put(CordialSdkDBHelper.BILLING_POSTAL_CODE, order.billingAddress.postalCode)
                values.put(CordialSdkDBHelper.BILLING_COUNTRY, order.billingAddress.country)
                order.tax?.let {
                    values.put(CordialSdkDBHelper.TAX, it)
                }
                order.shippingAndHandling?.let {
                    values.put(CordialSdkDBHelper.SHIPPING_AND_HANDLING, it)
                }
                order.properties?.let {
                    values.put(CordialSdkDBHelper.PROPERTIES, JsonUtils.getJsonFromPropertyValueMap(it).toString())
                }
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_CONTACT_ORDER, null, values)

                var cartItemValues: ContentValues
                order.items.forEach { cartItem ->
                    cartItemValues = ContactCartDBUtils.getContactCartValues(cartItem)
                    cartItemValues.put(CordialSdkDBHelper.ORDER_ID, order.orderId)
                    db.writableDatabase.insert(
                        CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS,
                        null,
                        cartItemValues
                    )
                }
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(CordialSdkDBHelper.TABLE_CONTACT_ORDER, null, null)
                db.writableDatabase.delete(CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS, null, null)
                withContext(Dispatchers.Main) {
                    onCompleteListener?.invoke()
                }
            }
        }
    }

    override fun getAllContactOrders(onCashedContactOrdersListener: (contactOrders: List<ContactOrderRequest>) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT ${CordialSdkDBHelper.TABLE_CONTACT_ORDER}.${CordialSdkDBHelper.ID}, ${CordialSdkDBHelper.MC_ID}, ${CordialSdkDBHelper.MC_TAP_TIME}," +
                            "${CordialSdkDBHelper.TABLE_CONTACT_ORDER}.${CordialSdkDBHelper.ORDER_ID}, ${CordialSdkDBHelper.STATUS}, ${CordialSdkDBHelper.STORE_ID}," +
                            "${CordialSdkDBHelper.CUSTOMER_ID}, ${CordialSdkDBHelper.PURCHASE_DATE}, ${CordialSdkDBHelper.SHIPPING_NAME}," +
                            "${CordialSdkDBHelper.SHIPPING_ADDRESS}, ${CordialSdkDBHelper.SHIPPING_CITY}, ${CordialSdkDBHelper.SHIPPING_STATE}," +
                            "${CordialSdkDBHelper.SHIPPING_POSTAL_CODE}, ${CordialSdkDBHelper.SHIPPING_COUNTRY}, ${CordialSdkDBHelper.BILLING_NAME}," +
                            "${CordialSdkDBHelper.BILLING_ADDRESS}, ${CordialSdkDBHelper.BILLING_CITY}, ${CordialSdkDBHelper.BILLING_STATE}," +
                            "${CordialSdkDBHelper.BILLING_POSTAL_CODE}, ${CordialSdkDBHelper.BILLING_COUNTRY}, ${CordialSdkDBHelper.TAX}," +
                            "${CordialSdkDBHelper.SHIPPING_AND_HANDLING}, ${CordialSdkDBHelper.TABLE_CONTACT_ORDER}.${CordialSdkDBHelper.PROPERTIES} as orderProperties, ${CordialSdkDBHelper.PRODUCT_ID}," +
                            "${CordialSdkDBHelper.NAME}, ${CordialSdkDBHelper.SKU}, ${CordialSdkDBHelper.CATEGORY}," +
                            "${CordialSdkDBHelper.URL}, ${CordialSdkDBHelper.DESCRIPTION}, ${CordialSdkDBHelper.QTY}," +
                            "${CordialSdkDBHelper.ITEM_PRICE}, ${CordialSdkDBHelper.SALE_PRICE}, ${CordialSdkDBHelper.TIMESTAMP}," +
                            "${CordialSdkDBHelper.ATTR}, ${CordialSdkDBHelper.IMAGES}, ${CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS}.${CordialSdkDBHelper.PROPERTIES}," +
                            "${CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS}.${CordialSdkDBHelper.ORDER_ID} FROM ${CordialSdkDBHelper.TABLE_CONTACT_ORDER} INNER JOIN " +
                            "${CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS} " +
                            "ON ${CordialSdkDBHelper.TABLE_CONTACT_ORDER}.${CordialSdkDBHelper.ORDER_ID}" +
                            " = ${CordialSdkDBHelper.TABLE_CONTACT_ORDER_CART_ITEMS}.${CordialSdkDBHelper.ORDER_ID} " +
                            "ORDER BY ${CordialSdkDBHelper.TABLE_CONTACT_ORDER}.${CordialSdkDBHelper.ID} ASC"

                val cursor = db.readableDatabase.rawQuery(query, null)

                val contactOrderRequests = mutableListOf<ContactOrderRequest>()
                if (cursor.moveToFirst()) {
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    val mcTapTimeIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_TAP_TIME)
                    val orderIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.ORDER_ID)
                    val statusIndex = cursor.getColumnIndex(CordialSdkDBHelper.STATUS)
                    val storeIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.STORE_ID)
                    val customerIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.CUSTOMER_ID)
                    val purchaseDateIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.PURCHASE_DATE)
                    val shippingNameIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_NAME)
                    val shippingAddressIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_ADDRESS)
                    val shippingCityIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_CITY)
                    val shippingStateIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_STATE)
                    val shippingPostalCodeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_POSTAL_CODE)
                    val shippingCountryIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_COUNTRY)
                    val billingNameIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_NAME)
                    val billingAddressIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_ADDRESS)
                    val billingCityIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_CITY)
                    val billingStateIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_STATE)
                    val billingPostalCodeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_POSTAL_CODE)
                    val billingCountryIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.BILLING_COUNTRY)
                    val taxIndex = cursor.getColumnIndex(CordialSdkDBHelper.TAX)
                    val shippingAndHandlingIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SHIPPING_AND_HANDLING)
                    val propertiesIndex = cursor.getColumnIndex("orderProperties")

                    var contactOrderRequest: ContactOrderRequest
                    var isOrderExists = false
                    do {
                        contactOrderRequests.forEach { contactOrder ->
                            val order = contactOrder.order
                            if (order.orderId == cursor.getString(orderIdIndex)) {
                                val items = order.items.toMutableList()
                                items.add(ContactCartDBUtils.getCartItemFromCursor(cursor))
                                order.items = items
                                isOrderExists = true
                                return@forEach
                            } else {
                                isOrderExists = false
                            }
                        }
                        if (!isOrderExists) {
                            val order = Order(
                                cursor.getString(orderIdIndex),
                                cursor.getString(statusIndex),
                                cursor.getString(storeIdIndex),
                                cursor.getString(customerIdIndex),
                                Address(
                                    cursor.getString(shippingNameIndex),
                                    cursor.getString(shippingAddressIndex),
                                    cursor.getString(shippingCityIndex),
                                    cursor.getString(shippingStateIndex),
                                    cursor.getString(shippingPostalCodeIndex),
                                    cursor.getString(shippingCountryIndex)
                                ),
                                Address(
                                    cursor.getString(billingNameIndex),
                                    cursor.getString(billingAddressIndex),
                                    cursor.getString(billingCityIndex),
                                    cursor.getString(billingStateIndex),
                                    cursor.getString(billingPostalCodeIndex),
                                    cursor.getString(billingCountryIndex)
                                ),
                                mutableListOf(ContactCartDBUtils.getCartItemFromCursor(cursor))
                            )
                                .withTax(cursor.getDouble(taxIndex))
                                .withShippingAndHandling(cursor.getDouble(shippingAndHandlingIndex))
                                .withProperties(JsonUtils.getPropertyMapFromJson(cursor.getString(propertiesIndex)))
                            order.setPurchaseDate(cursor.getString(purchaseDateIndex))
                            contactOrderRequest = ContactOrderRequest(
                                "",
                                "",
                                cursor.getString(mcIDIndex),
                                cursor.getString(mcTapTimeIndex),
                                order
                            )
                            contactOrderRequests.add(contactOrderRequest)

                        }
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onCashedContactOrdersListener.invoke(contactOrderRequests)
                }
                cursor.close()
            }
        }
    }
}