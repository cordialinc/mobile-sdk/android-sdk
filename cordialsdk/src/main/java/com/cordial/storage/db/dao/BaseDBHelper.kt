package com.cordial.storage.db.dao

import com.cordial.api.CordialApiConfiguration
import com.cordial.network.SingleThreadDispatcher
import com.cordial.storage.db.CordialSdkDBHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

internal abstract class BaseDBHelper : CoroutineScope {
    private var job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = job + SingleThreadDispatcher.getInstance().dispatcher

    var dataBase: CordialSdkDBHelper? =
        CordialSdkDBHelper.getInstance(CordialApiConfiguration.getInstance().getContext())

    fun doAsyncDbCall(dbCall: suspend () -> Unit) {
        if (job.isCompleted) {
            job = SupervisorJob()
        }
        launch {
            dbCall()
        }
    }

}