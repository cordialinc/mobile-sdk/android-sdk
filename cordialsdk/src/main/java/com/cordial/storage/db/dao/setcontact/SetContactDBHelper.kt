package com.cordial.storage.db.dao.setcontact

import android.content.ContentValues
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.upsertcontact.model.NotificationStatus
import com.cordial.feature.upsertcontact.model.SubscribeStatus
import com.cordial.feature.upsertcontact.model.UpsertContactRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.JsonUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class SetContactDBHelper : BaseDBHelper(), SetContactDao {

    override fun insert(upsertContactRequest: UpsertContactRequest, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.PRIMARY_KEY, upsertContactRequest.primaryKey)
                upsertContactRequest.token?.let {
                    values.put(CordialSdkDBHelper.TOKEN, upsertContactRequest.token)
                }
                upsertContactRequest.attributes?.let {
                    values.put(CordialSdkDBHelper.ATTR, JsonUtils.getJsonFromAttributeValueMap(it).toString())
                }
                upsertContactRequest.subscribeStatus?.let {
                    values.put(CordialSdkDBHelper.SUBSCRIBE_STATUS, it.status)
                }
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_SET_CONTACT, null, values)
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onSendLogoutListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_SET_CONTACT, null, null)
            onSendLogoutListener?.invoke()
        }
    }

    override fun getSetContacts(onCachedSetContactListener: (upsertContactRequests: List<UpsertContactRequest>) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_SET_CONTACT} ORDER BY ${CordialSdkDBHelper.ID} ASC"
                val cursor = db.readableDatabase.rawQuery(query, null)
                val upsertContactRequests = mutableListOf<UpsertContactRequest>()
                if (cursor.moveToFirst()) {
                    val primaryKeyIndex = cursor.getColumnIndex(CordialSdkDBHelper.PRIMARY_KEY)
                    val tokenIndex = cursor.getColumnIndex(CordialSdkDBHelper.TOKEN)
                    val attrIndex = cursor.getColumnIndex(CordialSdkDBHelper.ATTR)
                    val subscribeStatusIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.SUBSCRIBE_STATUS)
                    val dbIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)

                    do {
                        val status = NotificationStatus.findKey(
                            Preferences(CordialApiConfiguration.getInstance().getContext()).getString(
                                PreferenceKeys.NOTIFICATION_STATUS
                            )
                        )
                        val subscribeStatus = if (cursor.getString(subscribeStatusIndex) != null)
                            SubscribeStatus.findKey(cursor.getString(subscribeStatusIndex)) else null
                        upsertContactRequests.add(
                            UpsertContactRequest(
                                cursor.getString(primaryKeyIndex),
                                cursor.getString(tokenIndex),
                                subscribeStatus,
                                status,
                                JsonUtils.getAttributeValueMapFromJson(cursor.getString(attrIndex)),
                                cursor.getInt(dbIdIndex)
                            )
                        )
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onCachedSetContactListener.invoke(upsertContactRequests)
                }
            }
        }
    }

    override fun deleteSetContacts(
        upsertContactRequests: List<UpsertContactRequest>,
        onDeleteCompleteListener: (() -> Unit)?
    ) {
        doAsyncDbCall {
            upsertContactRequests.forEach { upsertContactRequest ->
                dataBase?.writableDatabase?.delete(
                    CordialSdkDBHelper.TABLE_SET_CONTACT,
                    CordialSdkDBHelper.ID + "=" + upsertContactRequest.dbId,
                    null
                )
            }
            onDeleteCompleteListener?.invoke()
        }
    }
}