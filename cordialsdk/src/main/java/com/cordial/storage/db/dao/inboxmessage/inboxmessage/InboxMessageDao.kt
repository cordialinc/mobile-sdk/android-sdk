package com.cordial.storage.db.dao.inboxmessage.inboxmessage

import com.cordial.feature.inboxmessage.InboxMessage

internal interface InboxMessageDao {
    fun insert(inboxMessages: List<InboxMessage>, onSaveRequestListener: (() -> Unit)?)
    fun insert(inboxMessage: InboxMessage, onSaveRequestListener: (() -> Unit)? = null, isLastItem: Boolean = true)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun deleteInboxMessage(mcID: String, onSuccessDeleteListener: (() -> Unit)?)
    fun getInboxMessage(mcID: String, onInboxMessageListener: ((inboxMessage: InboxMessage?) -> Unit)?)
    fun getInboxMessages(onInboxMessageListener: ((inboxMessages: List<InboxMessage>) -> Unit)?)
}