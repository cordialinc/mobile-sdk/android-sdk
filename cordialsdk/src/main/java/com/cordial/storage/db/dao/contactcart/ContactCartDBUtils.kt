package com.cordial.storage.db.dao.contactcart

import android.content.ContentValues
import android.database.Cursor
import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.util.JsonUtils
import org.json.JSONArray
import org.json.JSONObject

internal object ContactCartDBUtils {
    fun getContactCartValues(cartItem: CartItem): ContentValues {
        val values = ContentValues()
        values.put(CordialSdkDBHelper.PRODUCT_ID, cartItem.productId)
        values.put(CordialSdkDBHelper.NAME, cartItem.name)
        values.put(CordialSdkDBHelper.SKU, cartItem.sku)
        values.put(CordialSdkDBHelper.CATEGORY, cartItem.category)
        values.put(CordialSdkDBHelper.QTY, cartItem.qty)
        cartItem.url?.let {
            values.put(CordialSdkDBHelper.URL, it)
        }
        cartItem.itemDescription?.let {
            values.put(CordialSdkDBHelper.DESCRIPTION, it)
        }
        cartItem.itemPrice?.let {
            values.put(CordialSdkDBHelper.ITEM_PRICE, it)
        }
        cartItem.salePrice?.let {
            values.put(CordialSdkDBHelper.SALE_PRICE, it)
        }
        values.put(CordialSdkDBHelper.TIMESTAMP, cartItem.getTimestamp())
        cartItem.attr?.let {
            values.put(CordialSdkDBHelper.ATTR, JSONObject(it).toString())
        }
        cartItem.images?.let {
            val json = JSONObject()
            json.put(CordialSdkDBHelper.IMAGES, JSONArray(it))
            values.put(CordialSdkDBHelper.IMAGES, json.toString())
        }
        cartItem.properties?.let {
            values.put(
                CordialSdkDBHelper.PROPERTIES,
                JsonUtils.getJsonFromPropertyValueMap(it).toString()
            )
        }
        return values
    }

    fun getCartItemFromCursor(cursor: Cursor): CartItem {
        val item: CartItem
        val productIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.PRODUCT_ID)
        val nameIndex = cursor.getColumnIndex(CordialSdkDBHelper.NAME)
        val skuIndex = cursor.getColumnIndex(CordialSdkDBHelper.SKU)
        val categoryIndex = cursor.getColumnIndex(CordialSdkDBHelper.CATEGORY)
        val urlIndex = cursor.getColumnIndex(CordialSdkDBHelper.URL)
        val descriptionIndex = cursor.getColumnIndex(CordialSdkDBHelper.DESCRIPTION)
        val qtyIndex = cursor.getColumnIndex(CordialSdkDBHelper.QTY)
        val itemPriceIndex = cursor.getColumnIndex(CordialSdkDBHelper.ITEM_PRICE)
        val salePriceIndex = cursor.getColumnIndex(CordialSdkDBHelper.SALE_PRICE)
        val timestampIndex = cursor.getColumnIndex(CordialSdkDBHelper.TIMESTAMP)
        val attrIndex = cursor.getColumnIndex(CordialSdkDBHelper.ATTR)
        val imagesIndex = cursor.getColumnIndex(CordialSdkDBHelper.IMAGES)
        val propertiesIndex = cursor.getColumnIndex(CordialSdkDBHelper.PROPERTIES)

        val category = if (cursor.isNull(categoryIndex)) "" else cursor.getString(categoryIndex)
        val qty = if (cursor.isNull(qtyIndex)) 1 else cursor.getInt(qtyIndex)
        item = CartItem(
            cursor.getString(productIdIndex),
            cursor.getString(nameIndex),
            cursor.getString(skuIndex),
            category,
            qty
        )
            .withUrl(cursor.getString(urlIndex))
            .withItemDescription(cursor.getString(descriptionIndex))
            .withItemPrice(cursor.getDouble(itemPriceIndex))
            .withSalePrice(cursor.getDouble(salePriceIndex))
            .withAttr(JsonUtils.getMapFromJson(cursor.getString(attrIndex)))
            .withImages(
                JsonUtils.getListFromJson(
                    cursor.getString(imagesIndex),
                    CordialSdkDBHelper.IMAGES
                )?.filterIsInstance<String>()
            )
            .withProperties(JsonUtils.getPropertyMapFromJson(cursor.getString(propertiesIndex)))
        item.setTimestamp(cursor.getString(timestampIndex))
        return item
    }
}