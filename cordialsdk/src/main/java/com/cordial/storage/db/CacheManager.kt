package com.cordial.storage.db

import com.cordial.api.CordialApiConfiguration
import com.cordial.dependency.injection.CordialInjection
import com.cordial.storage.preferences.PreferenceKeys

internal class CacheManager {

    fun sendCachedData() {
        val injection = CordialApiConfiguration.getInstance().injection
        val preferences = injection.preferences
        sendCachedUpsertContact(injection)
        if (preferences.getBoolean(PreferenceKeys.IS_LOGGED_IN)) {
            sendCachedEvents(injection)
            sendCachedUpsertContactCart(injection)
            sendCachedContactOrders(injection)
            fetchCachedInAppMessage(injection)
            sendUpdateInboxMessageReadStatusCache(injection)
            sendDeleteInboxMessageCache(injection)
        }
    }

    private fun sendCachedUpsertContact(injection: CordialInjection) {
        val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.sendCachedUpsertContacts {
            sendCachedUnsetContact(injection)
        }
    }

    private fun sendCachedUnsetContact(injection: CordialInjection) {
        val unsetContactUseCase = injection.unsetContactInjection().unsetContactUseCase
        unsetContactUseCase.sendCachedContactLogout()
    }

    private fun sendCachedEvents(injection: CordialInjection) {
        val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.sendEventBoxIfBulkReached()
    }

    private fun sendCachedUpsertContactCart(injection: CordialInjection) {
        val upsertContactCartUseCase = injection.upsertContactCartInjection().upsertContactCartUseCase
        upsertContactCartUseCase.sendCachedContactCart()
    }

    private fun sendCachedContactOrders(injection: CordialInjection) {
        val sendContactOrderUseCase = injection.sendContactOrderInjection().sendContactOrderUseCase
        sendContactOrderUseCase.sendOrderCachedData()
    }

    private fun fetchCachedInAppMessage(injection: CordialInjection) {
        val inAppMessageUseCase = injection.inAppMessageInjection().inAppMessageUseCase
        inAppMessageUseCase.fetchInAppMessageFromCache()
    }

    private fun sendUpdateInboxMessageReadStatusCache(injection: CordialInjection) {
        val updateInboxMessageReadStatusUseCase =
            injection.updateInboxMessageReadStatusInjection().updateInboxMessageReadStatusUseCase
        updateInboxMessageReadStatusUseCase.sendCachedUpdateInboxMessageReadStatus(true)
    }

    private fun sendDeleteInboxMessageCache(injection: CordialInjection) {
        val deleteInboxUseCase = injection.deleteInboxMessageInjection().deleteInboxMessageUseCase
        deleteInboxUseCase.sendCachedDeleteInboxMessage()
    }

    fun clearCache(onCompleteListener: (() -> Unit)? = null) {
        // Pass onCompleteListener to the last dao class to make sure that all cache is cleared
        val injection = CordialApiConfiguration.getInstance().injection
        clearEventCache(injection)
        clearContactOrderCache(injection)
        clearContactCartCache(injection)
        clearFetchInAppMessageCache(injection)
        clearInAppMessageCache(injection)
        clearUpdateInboxMessageReadStatusCache(injection)
        clearDeleteInboxMessageCache(injection)
        clearInboxMessageCache(injection)
        clearInboxMessageContentCache(injection, onCompleteListener)
    }

    private fun clearEventCache(injection: CordialInjection) {
        val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.clearCache()
    }

    private fun clearContactOrderCache(injection: CordialInjection) {
        val sendContactOrderUseCase = injection.sendContactOrderInjection().sendContactOrderUseCase
        sendContactOrderUseCase.clearCache()
    }

    private fun clearContactCartCache(injection: CordialInjection) {
        val upsertContactCartUseCase = injection.upsertContactCartInjection().upsertContactCartUseCase
        upsertContactCartUseCase.clearCache()
    }

    private fun clearFetchInAppMessageCache(injection: CordialInjection) {
        val inAppMessageUseCase = injection.inAppMessageInjection().inAppMessageUseCase
        inAppMessageUseCase.clearCache()
    }

    private fun clearInAppMessageCache(injection: CordialInjection) {
        val inAppMessageDataUseCase = injection.inAppMessageDataInjection().inAppMessageDataUseCase
        inAppMessageDataUseCase.clearCache()
    }

    private fun clearUpdateInboxMessageReadStatusCache(injection: CordialInjection) {
        val updateInboxMessageReadStatusUseCase =
            injection.updateInboxMessageReadStatusInjection().updateInboxMessageReadStatusUseCase
        updateInboxMessageReadStatusUseCase.clearCache()
    }

    private fun clearDeleteInboxMessageCache(injection: CordialInjection) {
        val deleteInboxUseCase = injection.deleteInboxMessageInjection().deleteInboxMessageUseCase
        deleteInboxUseCase.clearCache()
    }

    private fun clearInboxMessageCache(injection: CordialInjection) {
        val fetchInboxMessageUseCase = injection.fetchInboxMessageInjection().fetchInboxMessageUseCase
        fetchInboxMessageUseCase.clearCache()
    }

    private fun clearInboxMessageContentCache(injection: CordialInjection, onCompleteListener: (() -> Unit)? = null) {
        val fetchInboxMessageContentUseCase =
            injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
        fetchInboxMessageContentUseCase.clearCache(onCompleteListener)
    }
}