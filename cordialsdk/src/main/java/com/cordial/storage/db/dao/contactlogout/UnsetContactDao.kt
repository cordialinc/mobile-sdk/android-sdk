package com.cordial.storage.db.dao.contactlogout

import com.cordial.feature.unsetcontact.model.UnsetContactRequest

internal interface UnsetContactDao {
    fun insert(unsetContactRequest: UnsetContactRequest, onSaveRequestListener: (() -> Unit)?)
    fun clear(onLogoutCacheClearListener: (() -> Unit)? = null)
    fun getContactLogout(onCachedContactLogoutListener: (unsetContactRequest: UnsetContactRequest?) -> Unit)
}