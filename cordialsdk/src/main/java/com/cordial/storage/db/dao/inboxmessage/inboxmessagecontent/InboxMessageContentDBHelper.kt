package com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model.InboxMessageContentMetadata
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import com.cordial.util.TimeUtils
import com.cordial.util.getByteSize
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class InboxMessageContentDBHelper : BaseDBHelper(), InboxMessageContentDao {

    override fun insert(mcID: String, inboxMessageContent: String, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.MC_ID, mcID)
                values.put(CordialSdkDBHelper.INBOX_MESSAGE_CONTENT, inboxMessageContent)
                val timeInMillis = TimeUtils.getCurrentTime()
                values.put(CordialSdkDBHelper.TIME_IN_MILLIS, timeInMillis)
                val byteSize = inboxMessageContent.getByteSize()
                values.put(CordialSdkDBHelper.SIZE, byteSize)
                db.writableDatabase.insertWithOnConflict(
                    CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT,
                    null,
                    values,
                    SQLiteDatabase.CONFLICT_REPLACE
                )
                withContext(Dispatchers.Main) {
                    onSaveRequestListener?.invoke()
                }
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun deleteInboxMessageContent(mcID: String, onSuccessDeleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT,
                    CordialSdkDBHelper.MC_ID + "='$mcID'",
                    null
                )
                withContext(Dispatchers.Main) {
                    onSuccessDeleteListener?.invoke()
                }
            }
        }
    }

    override fun deleteInboxMessageContents(mcIDs: List<String>, onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT,
                    "CAST(" + CordialSdkDBHelper.MC_ID + " AS TEXT) IN (" + String(
                        CharArray(
                            mcIDs.size - 1
                        )
                    ).replace(
                        "\u0000",
                        "?, "
                    ) + "?)",
                    mcIDs.toTypedArray()
                )
                onCompleteListener?.invoke()
            }
        }
    }

    override fun getInboxMessageContentCacheSize(onCacheSizeListener: ((cacheSize: Int) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT SUM(${CordialSdkDBHelper.SIZE}) as ${CordialSdkDBHelper.CACHE_SIZE} " +
                            "FROM ${CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT}"

                val cursor = db.readableDatabase.rawQuery(query, null)
                var cacheSize = 0
                if (cursor.moveToFirst()) {
                    val cacheSizeIndex = cursor.getColumnIndex(CordialSdkDBHelper.CACHE_SIZE)
                    cacheSize = cursor.getInt(cacheSizeIndex)
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onCacheSizeListener?.invoke(cacheSize)
                }
            }
        }
    }

    override fun getInboxMessageContentsMetadata(
        onInboxMessageContentMetadataListener: ((metadataList: List<InboxMessageContentMetadata>?) -> Unit)?
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT ${CordialSdkDBHelper.MC_ID}, ${CordialSdkDBHelper.SIZE} " +
                            "FROM ${CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT} " +
                            "WHERE ${CordialSdkDBHelper.MC_ID} <> 'null'" +
                            "ORDER BY ${CordialSdkDBHelper.TIME_IN_MILLIS} ASC"

                val cursor = db.readableDatabase.rawQuery(query, null)
                var contents: MutableList<InboxMessageContentMetadata>? = null
                if (cursor.moveToFirst()) {
                    contents = mutableListOf()
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    val sizeIndex = cursor.getColumnIndex(CordialSdkDBHelper.SIZE)
                    do {
                        val mcID = cursor.getString(mcIDIndex)
                        val size = cursor.getInt(sizeIndex)
                        contents.add(InboxMessageContentMetadata(mcID, size))
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onInboxMessageContentMetadataListener?.invoke(contents)
                }
            }
        }
    }

    override fun getInboxMessageContent(
        mcID: String,
        onInboxMessageContentListener: ((inboxMessageContent: String?) -> Unit)?
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_INBOX_MESSAGE_CONTENT} " +
                            "WHERE ${CordialSdkDBHelper.MC_ID} = '$mcID' LIMIT 1"

                val cursor = db.readableDatabase.rawQuery(query, null)
                var inboxMessageContent: String? = null
                if (cursor.moveToFirst()) {
                    val contentIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.INBOX_MESSAGE_CONTENT)
                    do {
                        inboxMessageContent = cursor.getString(contentIndex)
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onInboxMessageContentListener?.invoke(inboxMessageContent)
                }
            }
        }
    }
}