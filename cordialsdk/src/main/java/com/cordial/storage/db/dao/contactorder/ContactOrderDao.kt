package com.cordial.storage.db.dao.contactorder

import com.cordial.feature.sendcontactorder.model.ContactOrderRequest

internal interface ContactOrderDao {
    fun insert(contactOrderRequest: ContactOrderRequest, onSaveRequestListener: (() -> Unit)?)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun getAllContactOrders(onCashedContactOrdersListener: (contactOrders: List<ContactOrderRequest>) -> Unit)
}