package com.cordial.storage.db.dao.notification.category

import com.cordial.feature.notification.model.NotificationCategory

interface NotificationCategoryDao {
    fun insert(categories: Iterable<NotificationCategory>, onCompleteListener: (() -> Unit)?)
    fun getNotificationCategories(onNotificationCategoriesListener: (categories: List<NotificationCategory>) -> Unit)
    fun deleteNotificationCategory(categoryId: String, onDeleteCategoryListener: (() -> Unit)? = null)
    fun clear(onCompleteListener: (() -> Unit)?)
}