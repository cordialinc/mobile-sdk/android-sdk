package com.cordial.storage.db.dao.contactcart

import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest

internal interface ContactCartDao {
    fun insert(contactCartRequest: UpsertContactCartRequest, onSaveRequestListener: (() -> Unit)?)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun getAllCartItems(onCashedCartItemsListener: (cartItems: List<CartItem>) -> Unit)
}