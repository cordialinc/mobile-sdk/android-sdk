package com.cordial.storage.db.dao.inappmessage.inappmessagedata

import com.cordial.feature.inappmessage.model.InAppMessageData

internal interface InAppMessageDao {
    fun insert(inAppMessageData: InAppMessageData)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun getLatestInAppMessage(onInAppMessageListener: ((inAppMessageData: InAppMessageData?) -> Unit)?)
    fun updateInAppMessageShownData(mcID: String, isInAppMessageShown: Boolean)
    fun deleteInAppIfExist(mcID: String)
}