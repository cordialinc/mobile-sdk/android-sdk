package com.cordial.storage.db.dao.inappmessage.inappmessagedata

import android.content.ContentValues
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageMargin
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class InAppMessageDBHelper : BaseDBHelper(), InAppMessageDao {

    override fun insert(inAppMessageData: InAppMessageData) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.MC_ID, inAppMessageData.mcID)
                values.put(CordialSdkDBHelper.IN_APP_MESSAGE_HMTL, inAppMessageData.html)
                values.put(CordialSdkDBHelper.IN_APP_MESSAGE_TYPE, inAppMessageData.type.type)
                values.put(CordialSdkDBHelper.START, inAppMessageData.margin.start)
                values.put(CordialSdkDBHelper.TOP, inAppMessageData.margin.top)
                values.put(CordialSdkDBHelper.END, inAppMessageData.margin.end)
                values.put(CordialSdkDBHelper.BOTTOM, inAppMessageData.margin.bottom)
                values.put(CordialSdkDBHelper.EXPIRATION_TIME, inAppMessageData.expirationTime)
                values.put(CordialSdkDBHelper.TIME_IN_MILLIS, inAppMessageData.timestamp)
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_IN_APP_MESSAGE, null, values)
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_IN_APP_MESSAGE, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun getLatestInAppMessage(onInAppMessageListener: ((inAppMessageData: InAppMessageData?) -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_IN_APP_MESSAGE} ORDER BY ${CordialSdkDBHelper.TIME_IN_MILLIS} DESC"
                val cursor = db.readableDatabase.rawQuery(query, null)
                var inAppMessageData: InAppMessageData? = null
                if (cursor.moveToFirst()) {
                    val mcIdIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)
                    val htmlIndex = cursor.getColumnIndex(CordialSdkDBHelper.IN_APP_MESSAGE_HMTL)
                    val messageTypeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.IN_APP_MESSAGE_TYPE)
                    val marginTopIndex = cursor.getColumnIndex(CordialSdkDBHelper.TOP)
                    val marginStartIndex = cursor.getColumnIndex(CordialSdkDBHelper.START)
                    val marginBottomIndex = cursor.getColumnIndex(CordialSdkDBHelper.BOTTOM)
                    val marginEndIndex = cursor.getColumnIndex(CordialSdkDBHelper.END)
                    val expirationTimeIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.EXPIRATION_TIME)
                    val timestampIndex = cursor.getColumnIndex(CordialSdkDBHelper.TIME_IN_MILLIS)
                    val isInAppMessageShownIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.IS_IN_APP_MESSAGE_SHOWN)
                    val inAppMessageMargin = InAppMessageMargin(
                        cursor.getInt(marginStartIndex),
                        cursor.getInt(marginTopIndex),
                        cursor.getInt(marginEndIndex),
                        cursor.getInt(marginBottomIndex)
                    )
                    inAppMessageData = InAppMessageData(
                        cursor.getString(mcIdIndex),
                        cursor.getString(htmlIndex),
                        InAppMessageType.findKey(
                            cursor.getString(
                                messageTypeIndex
                            )
                        ),
                        inAppMessageMargin,
                        cursor.getString(expirationTimeIndex),
                        cursor.getLong(timestampIndex)
                    )
                    val isInAppMessageShown = cursor.getInt(isInAppMessageShownIndex)
                    inAppMessageData.isInAppMessageShown = isInAppMessageShown == 1
                }
                withContext(Dispatchers.Main) {
                    onInAppMessageListener?.invoke(inAppMessageData)
                }
                cursor.close()
            }
        }
    }

    override fun updateInAppMessageShownData(mcID: String, isInAppMessageShown: Boolean) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val inAppMessageShown = if (isInAppMessageShown) 1 else 0
                val updateInAppMessageDataQuery =
                    "UPDATE ${CordialSdkDBHelper.TABLE_IN_APP_MESSAGE} " +
                            "SET ${CordialSdkDBHelper.IS_IN_APP_MESSAGE_SHOWN}=$inAppMessageShown " +
                            "WHERE ${CordialSdkDBHelper.MC_ID}='$mcID'"
                db.writableDatabase.execSQL(updateInAppMessageDataQuery)
            }
        }
    }

    override fun deleteInAppIfExist(mcID: String) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(
                CordialSdkDBHelper.TABLE_IN_APP_MESSAGE, "${CordialSdkDBHelper.MC_ID} =?",
                arrayOf(mcID)
            )
        }
    }
}