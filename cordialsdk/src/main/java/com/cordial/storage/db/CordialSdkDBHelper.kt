package com.cordial.storage.db

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.cordial.api.CordialApiConfiguration

internal class CordialSdkDBHelper(
    context: Context
) : SQLiteOpenHelper(
    context,
    DATABASE_NAME, null,
    DATABASE_VERSION
) {

    private var qty = CordialApiConfiguration.getInstance().getQty()

    private val CREATE_TABLE_CUSTOM_EVENTS =
        ("CREATE TABLE if not exists " + TABLE_CUSTOM_EVENTS + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$TIMESTAMP text," +
                "$EVENT_NAME text," +
                "$LONGITUDE double," +
                "$LATITUDE double," +
                "$PROPERTIES text," +
                "$MC_ID text" +
                ")")

    private val CREATE_TABLE_SET_CONTACT =
        ("CREATE TABLE if not exists " + TABLE_SET_CONTACT + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRIMARY_KEY text," +
                "$TOKEN text," +
                "$ATTR text," +
                "$SUBSCRIBE_STATUS text" +
                ")")

    private val CREATE_TABLE_CONTACT_LOGOUT =
        ("CREATE TABLE if not exists " + TABLE_CONTACT_LOGOUT + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRIMARY_KEY text" +
                ")")

    private val CREATE_TABLE_CART_ITEM =
        ("CREATE TABLE if not exists " + TABLE_CONTACT_CART_ITEMS + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRODUCT_ID text," +
                "$NAME text," +
                "$SKU text," +
                "$CATEGORY text," +
                "$URL text," +
                "$DESCRIPTION text," +
                "$QTY int," +
                "$ITEM_PRICE double," +
                "$SALE_PRICE double," +
                "$TIMESTAMP text," +
                "$ATTR text," +
                "$IMAGES text," +
                "$PROPERTIES text" +
                ")")

    private val CREATE_TABLE_CONTACT_ORDER =
        ("CREATE TABLE if not exists " + TABLE_CONTACT_ORDER + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$MC_ID text," +
                "$MC_TAP_TIME text," +
                "$ORDER_ID text," +
                "$STATUS text," +
                "$STORE_ID text," +
                "$CUSTOMER_ID text," +
                "$PURCHASE_DATE text," +
                "$SHIPPING_NAME text," +
                "$SHIPPING_ADDRESS text," +
                "$SHIPPING_CITY text," +
                "$SHIPPING_STATE text," +
                "$SHIPPING_POSTAL_CODE text," +
                "$SHIPPING_COUNTRY text," +
                "$BILLING_NAME text," +
                "$BILLING_ADDRESS text," +
                "$BILLING_CITY text," +
                "$BILLING_STATE text," +
                "$BILLING_POSTAL_CODE text," +
                "$BILLING_COUNTRY text," +
                "$TAX int," +
                "$SHIPPING_AND_HANDLING text," +
                "$PROPERTIES text" +
                ")")

    private val CREATE_TABLE_CONTACT_ORDER_CART_ITEMS =
        ("CREATE TABLE if not exists " + TABLE_CONTACT_ORDER_CART_ITEMS + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRODUCT_ID text," +
                "$NAME text," +
                "$SKU text," +
                "$CATEGORY text," +
                "$URL text," +
                "$DESCRIPTION text," +
                "$QTY int," +
                "$ITEM_PRICE double," +
                "$SALE_PRICE double," +
                "$TIMESTAMP text," +
                "$ATTR text," +
                "$IMAGES text," +
                "$PROPERTIES text," +
                "$ORDER_ID text" +
                ")")

    private fun createTriggerLimitOfCustomEvents(): String {
        return "CREATE TRIGGER $TRIGGER_CUSTOM_EVENTS_LIMIT INSERT ON $TABLE_CUSTOM_EVENTS " +
                "WHEN (select count(*) from $TABLE_CUSTOM_EVENTS) > ${qty - 1} BEGIN" +
                " DELETE FROM $TABLE_CUSTOM_EVENTS WHERE $TABLE_CUSTOM_EVENTS.$ID IN  " +
                "(SELECT $TABLE_CUSTOM_EVENTS.$ID FROM $TABLE_CUSTOM_EVENTS ORDER BY $TABLE_CUSTOM_EVENTS.$ID " +
                "limit (select count(*) -${qty - 1} from $TABLE_CUSTOM_EVENTS ));" +
                "END;"
    }

    private val DROP_TRIGGER_LIMIT_CUSTOM_EVENTS = "DROP TRIGGER $TRIGGER_CUSTOM_EVENTS_LIMIT"

    private val CREATE_TABLE_FETCH_IN_APP_MESSAGE =
        ("CREATE TABLE if not exists " + TABLE_FETCH_IN_APP_MESSAGE + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$MC_ID text," +
                "$IN_APP_MESSAGE_TYPE text," +
                "$EXPIRATION_TIME text," +
                "$TIME_IN_MILLIS int," +
                "$URL text," +
                "$URL_EXPIRE_AT text" +
                ")")

    private val CREATE_TABLE_IN_APP_MESSAGE =
        ("CREATE TABLE if not exists " + TABLE_IN_APP_MESSAGE + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$MC_ID text," +
                "$IN_APP_MESSAGE_HMTL text," +
                "$IN_APP_MESSAGE_TYPE text," +
                "$TOP int," +
                "$START int," +
                "$BOTTOM int," +
                "$END int," +
                "$EXPIRATION_TIME text," +
                "$TIME_IN_MILLIS int," +
                "$IS_IN_APP_MESSAGE_SHOWN int" +
                ")")

    private val CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS =
        ("CREATE TABLE if not exists " + TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRIMARY_KEY text," +
                "$MARK_AS_READ_IDS text," +
                "$MARK_AS_UNREAD_IDS text" +
                ")")

    private val CREATE_TABLE_DELETE_INBOX_MESSAGE =
        ("CREATE TABLE if not exists " + TABLE_DELETE_INBOX_MESSAGE + " (" +
                "$ID integer PRIMARY KEY autoincrement," +
                "$PRIMARY_KEY text," +
                "$MC_ID text" +
                ")")

    private val CREATE_TABLE_INBOX_MESSAGE =
        ("CREATE TABLE if not exists " + TABLE_INBOX_MESSAGE + " (" +
                "$MC_ID text PRIMARY KEY," +
                "$IS_READ boolean," +
                "$SENT_AT text," +
                "$URL text," +
                "$URL_EXPIRE_AT text," +
                "$INBOX_PREVIEW_DATA text" +
                ")")

    private val CREATE_TABLE_INBOX_MESSAGE_CONTENT =
        ("CREATE TABLE if not exists " + TABLE_INBOX_MESSAGE_CONTENT + " (" +
                "$MC_ID text PRIMARY KEY," +
                "$INBOX_MESSAGE_CONTENT text," +
                "$TIME_IN_MILLIS int," +
                "$SIZE int" +
                ")")

    private val CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE =
        ("CREATE TABLE if not exists " + TABLE_IN_APP_MESSAGE_TO_DELETE + " (" +
                "$MC_ID text PRIMARY KEY" +
                ")")

    private val CREATE_TABLE_NOTIFICATION_CATEGORIES =
        ("CREATE TABLE if not exists " + TABLE_NOTIFICATION_CATEGORY + " (" +
                "$ID text PRIMARY KEY," +
                "$NAME text," +
                "$DESCRIPTION text," +
                "$STATE boolean" +
                ")")

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_CUSTOM_EVENTS)
        db.execSQL(CREATE_TABLE_SET_CONTACT)
        db.execSQL(CREATE_TABLE_CONTACT_LOGOUT)
        db.execSQL(CREATE_TABLE_CART_ITEM)
        db.execSQL(CREATE_TABLE_CONTACT_ORDER)
        db.execSQL(CREATE_TABLE_CONTACT_ORDER_CART_ITEMS)
        db.execSQL(createTriggerLimitOfCustomEvents())
        db.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
        db.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
        db.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
        db.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
        db.execSQL(CREATE_TABLE_INBOX_MESSAGE)
        db.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
        db.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
        db.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
    }

    fun updateEventLimit(newQty: Int) {
        qty = newQty
        writableDatabase.execSQL(DROP_TRIGGER_LIMIT_CUSTOM_EVENTS)
        writableDatabase.execSQL(createTriggerLimitOfCustomEvents())
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        when (oldVersion) {
            1 -> {
                db?.execSQL(CREATE_TABLE_SET_CONTACT)
                db?.execSQL(CREATE_TABLE_CONTACT_LOGOUT)
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $MC_ID TEXT")
                db?.execSQL(CREATE_TABLE_CART_ITEM)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER_CART_ITEMS)
                db?.execSQL(createTriggerLimitOfCustomEvents())
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LONGITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LATITUDE DOUBLE")
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            2 -> {
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $MC_ID TEXT")
                db?.execSQL(CREATE_TABLE_CART_ITEM)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER_CART_ITEMS)
                db?.execSQL(createTriggerLimitOfCustomEvents())
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LONGITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LATITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $ATTR TEXT")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $SUBSCRIBE_STATUS TEXT")
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            3 -> {
                db?.execSQL(CREATE_TABLE_CART_ITEM)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER)
                db?.execSQL(CREATE_TABLE_CONTACT_ORDER_CART_ITEMS)
                db?.execSQL(createTriggerLimitOfCustomEvents())
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LONGITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LATITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $ATTR TEXT")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $SUBSCRIBE_STATUS TEXT")
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            4 -> {
                db?.execSQL(createTriggerLimitOfCustomEvents())
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LONGITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_CUSTOM_EVENTS ADD COLUMN $LATITUDE DOUBLE")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $ATTR TEXT")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $SUBSCRIBE_STATUS TEXT")
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            5 -> {
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $ATTR TEXT")
                db?.execSQL("ALTER TABLE $TABLE_SET_CONTACT ADD COLUMN $SUBSCRIBE_STATUS TEXT")
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            6 -> {
                db?.execSQL(CREATE_TABLE_FETCH_IN_APP_MESSAGE)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE)
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            7 -> {
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL("ALTER TABLE $TABLE_IN_APP_MESSAGE ADD COLUMN $IS_IN_APP_MESSAGE_SHOWN INT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL TEXT")
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL_EXPIRE_AT TEXT")
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            8 -> {
                if (!isFieldExist(db, TABLE_CONTACT_ORDER, MC_ID))
                    db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL("ALTER TABLE $TABLE_IN_APP_MESSAGE ADD COLUMN $IS_IN_APP_MESSAGE_SHOWN INT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL TEXT")
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL_EXPIRE_AT TEXT")
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            9 -> {
                if (!isFieldExist(db, TABLE_CONTACT_ORDER, MC_ID))
                    db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_ID TEXT")
                if (!isFieldExist(db, TABLE_CONTACT_ORDER, MC_TAP_TIME))
                    db?.execSQL("ALTER TABLE $TABLE_CONTACT_ORDER ADD COLUMN $MC_TAP_TIME TEXT")
                db?.execSQL("ALTER TABLE $TABLE_IN_APP_MESSAGE ADD COLUMN $IS_IN_APP_MESSAGE_SHOWN INT")
                db?.execSQL(CREATE_TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS)
                db?.execSQL(CREATE_TABLE_DELETE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE)
                db?.execSQL(CREATE_TABLE_INBOX_MESSAGE_CONTENT)
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL TEXT")
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL_EXPIRE_AT TEXT")
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            10 -> {
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL TEXT")
                db?.execSQL("ALTER TABLE $TABLE_FETCH_IN_APP_MESSAGE ADD COLUMN $URL_EXPIRE_AT TEXT")
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            11 -> {
                db?.execSQL(CREATE_TABLE_IN_APP_MESSAGE_TO_DELETE)
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
            12 -> {
                db?.execSQL(CREATE_TABLE_NOTIFICATION_CATEGORIES)
            }
        }
    }

    private fun isFieldExist(db: SQLiteDatabase?, tableName: String, fieldName: String): Boolean {
        var isExist = false
        db?.let {
            val cursor: Cursor = db.rawQuery("PRAGMA table_info($tableName)", null)
            cursor.moveToFirst()
            do {
                val currentColumn: String = cursor.getString(1)
                if (currentColumn == fieldName) {
                    isExist = true
                }
            } while (cursor.moveToNext())
            cursor.close()
        }
        return isExist
    }

    @Synchronized
    override fun getWritableDatabase(): SQLiteDatabase {
        return super.getWritableDatabase()
    }

    @Synchronized
    override fun getReadableDatabase(): SQLiteDatabase {
        return super.getReadableDatabase()
    }

    @Synchronized
    override fun close() {
        super.close()
    }

    companion object {
        private var INSTANCE: CordialSdkDBHelper? = null

        @Synchronized
        fun getInstance(context: Context): CordialSdkDBHelper {
            if (INSTANCE == null) {
                INSTANCE = CordialSdkDBHelper(context)
            }
            return INSTANCE as CordialSdkDBHelper
        }

        private const val DATABASE_VERSION = 13
        private const val DATABASE_NAME = "cordialSdkDB.db"
        const val TABLE_CUSTOM_EVENTS = "events"
        const val TABLE_SET_CONTACT = "setcontact"
        const val TABLE_CONTACT_LOGOUT = "contactlogout"
        const val TABLE_CONTACT_CART_ITEMS = "contactcartitems"
        const val TABLE_CONTACT_ORDER_CART_ITEMS = "contactordercartitems"
        const val TABLE_CONTACT_ORDER = "contactcartorder"
        const val TRIGGER_CUSTOM_EVENTS_LIMIT = "triggercustomeventslimit"
        const val TABLE_FETCH_IN_APP_MESSAGE = "fetchinappmessage"
        const val TABLE_IN_APP_MESSAGE = "inappmessage"
        const val TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS = "updateinboxmessagereadstatus"
        const val TABLE_DELETE_INBOX_MESSAGE = "deleteinboxmessage"
        const val TABLE_INBOX_MESSAGE = "inboxmessage"
        const val TABLE_INBOX_MESSAGE_CONTENT = "inboxmessagecontent"
        const val TABLE_IN_APP_MESSAGE_TO_DELETE = "inappmessagetodelete"
        const val TABLE_NOTIFICATION_CATEGORY = "notificationcategory"

        const val ID = "_id"
        const val TIMESTAMP = "TIMESTAMP"
        const val EVENT_NAME = "EVENT_NAME"
        const val PROPERTIES = "PROPERTIES"
        const val PRIMARY_KEY = "PRIMARY_KEY"
        const val TOKEN = "TOKEN"
        const val MC_ID = "MC_ID"
        const val MC_TAP_TIME = "MC_TAP_TIME"
        const val PRODUCT_ID = "PRODUCT_ID"
        const val NAME = "NAME"
        const val SKU = "SKU"
        const val CATEGORY = "CATEGORY"
        const val URL = "URL"
        const val DESCRIPTION = "DESCRIPTION"
        const val QTY = "QTY"
        const val ITEM_PRICE = "ITEM_PRICE"
        const val SALE_PRICE = "SALE_PRICE"
        const val ATTR = "ATTR"
        const val IMAGES = "IMAGES"
        const val ORDER_ID = "ORDER_ID"
        const val STATUS = "STATUS"
        const val STORE_ID = "STORE_ID"
        const val CUSTOMER_ID = "CUSTOMER_ID"
        const val PURCHASE_DATE = "PURCHASE_DATE"
        const val SHIPPING_NAME = "SHIPPING_NAME"
        const val SHIPPING_ADDRESS = "SHIPPING_ADDRESS"
        const val SHIPPING_CITY = "SHIPPING_CITY"
        const val SHIPPING_STATE = "SHIPPING_STATE"
        const val SHIPPING_POSTAL_CODE = "SHIPPING_POSTAL_CODE"
        const val SHIPPING_COUNTRY = "SHIPPING_COUNTRY"
        const val BILLING_NAME = "BILLING_NAME"
        const val BILLING_ADDRESS = "BILLING_ADDRESS"
        const val BILLING_CITY = "BILLING_CITY"
        const val BILLING_STATE = "BILLING_STATE"
        const val BILLING_POSTAL_CODE = "BILLING_POSTAL_CODE"
        const val BILLING_COUNTRY = "BILLING_COUNTRY"
        const val SHIPPING_AND_HANDLING = "SHIPPING_AND_HANDLING"
        const val TAX = "TAX"
        const val LONGITUDE = "LONGITUDE"
        const val LATITUDE = "LATITUDE"
        const val SUBSCRIBE_STATUS = "SUBSCRIBE_STATUS"
        const val IN_APP_MESSAGE_HMTL = "IN_APP_MESSAGE_HMTL"
        const val IN_APP_MESSAGE_TYPE = "IN_APP_MESSAGE_TYPE"
        const val TOP = "TOP"
        const val START = "START"
        const val BOTTOM = "BOTTOM"
        const val END = "END"
        const val EXPIRATION_TIME = "EXPIRATION_TIME"
        const val TIME_IN_MILLIS = "TIME_IN_MILLIS"
        const val IS_IN_APP_MESSAGE_SHOWN = "IS_IN_APP_MESSAGE_SHOWN"
        const val MARK_AS_READ_IDS = "MARK_AS_READ_IDS"
        const val MARK_AS_UNREAD_IDS = "MARK_AS_UNREAD_IDS"
        const val IS_READ = "IS_READ"
        const val SENT_AT = "SENT_AT"
        const val URL_EXPIRE_AT = "URL_EXPIRE_AT"
        const val INBOX_MESSAGE_CONTENT = "INBOX_MESSAGE_CONTENT"
        const val SIZE = "SIZE"
        const val CACHE_SIZE = "CACHE_SIZE"
        const val INBOX_PREVIEW_DATA = "INBOX_PREVIEW_DATA"
        const val STATE = "STATE"
    }
}