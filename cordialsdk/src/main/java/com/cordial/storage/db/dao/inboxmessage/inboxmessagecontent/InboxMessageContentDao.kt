package com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent

import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model.InboxMessageContentMetadata

internal interface InboxMessageContentDao {
    fun insert(mcID: String, inboxMessageContent: String, onSaveRequestListener: (() -> Unit)?)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun deleteInboxMessageContent(mcID: String, onSuccessDeleteListener: (() -> Unit)?)
    fun deleteInboxMessageContents(mcIDs: List<String>, onCompleteListener: (() -> Unit)?)
    fun getInboxMessageContentCacheSize(onCacheSizeListener: ((cacheSize: Int) -> Unit)?)
    fun getInboxMessageContentsMetadata(
        onInboxMessageContentMetadataListener: ((metadataList: List<InboxMessageContentMetadata>?) -> Unit)?
    )

    fun getInboxMessageContent(mcID: String, onInboxMessageContentListener: ((inboxMessageContent: String?) -> Unit)?)
}