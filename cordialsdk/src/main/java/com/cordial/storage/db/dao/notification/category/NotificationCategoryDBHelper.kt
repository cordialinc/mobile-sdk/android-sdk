package com.cordial.storage.db.dao.notification.category

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class NotificationCategoryDBHelper : BaseDBHelper(), NotificationCategoryDao {
    override fun insert(categories: Iterable<NotificationCategory>, onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                categories.forEach { category ->
                    val values = ContentValues()
                    values.put(CordialSdkDBHelper.ID, category.id)
                    values.put(CordialSdkDBHelper.NAME, category.name)
                    category.description?.let { description ->
                        values.put(CordialSdkDBHelper.DESCRIPTION, description)
                    }
                    values.put(CordialSdkDBHelper.STATE, category.state)
                    db.writableDatabase.insertWithOnConflict(
                        CordialSdkDBHelper.TABLE_NOTIFICATION_CATEGORY,
                        null,
                        values,
                        SQLiteDatabase.CONFLICT_REPLACE
                    )
                }
                withContext(Dispatchers.Main) {
                    onCompleteListener?.invoke()
                }
            }
        }
    }

    override fun getNotificationCategories(onNotificationCategoriesListener: (categories: List<NotificationCategory>) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val cursor = db.readableDatabase.query(
                    CordialSdkDBHelper.TABLE_NOTIFICATION_CATEGORY,
                    arrayOf(
                        CordialSdkDBHelper.ID,
                        CordialSdkDBHelper.NAME,
                        CordialSdkDBHelper.DESCRIPTION,
                        CordialSdkDBHelper.STATE
                    ),
                    null, null, null, null, null
                )
                val categories = mutableListOf<NotificationCategory>()
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)
                    val nameIndex = cursor.getColumnIndex(CordialSdkDBHelper.NAME)
                    val descriptionIndex = cursor.getColumnIndex(CordialSdkDBHelper.DESCRIPTION)
                    val stateIndex = cursor.getColumnIndex(CordialSdkDBHelper.STATE)
                    do {
                        val id = cursor.getString(idIndex)
                        val name = cursor.getString(nameIndex)
                        val description = cursor.getString(descriptionIndex)
                        val state = cursor.getInt(stateIndex) > 0
                        val category = NotificationCategory(id, name, state, description)
                        categories.add(category)
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onNotificationCategoriesListener.invoke(categories)
                }
            }
        }
    }

    override fun deleteNotificationCategory(
        categoryId: String,
        onDeleteCategoryListener: (() -> Unit)?
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_NOTIFICATION_CATEGORY,
                    CordialSdkDBHelper.ID + "=" + "'$categoryId'",
                    null
                )
                withContext(Dispatchers.Main) {
                    onDeleteCategoryListener?.invoke()
                }
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(CordialSdkDBHelper.TABLE_NOTIFICATION_CATEGORY, null, null)
                withContext(Dispatchers.Main) {
                    onCompleteListener?.invoke()
                }
            }
        }
    }
}