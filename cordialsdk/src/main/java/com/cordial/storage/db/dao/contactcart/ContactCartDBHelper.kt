package com.cordial.storage.db.dao.contactcart

import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class ContactCartDBHelper : BaseDBHelper(), ContactCartDao {
    override fun insert(contactCartRequest: UpsertContactCartRequest, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            for (i in contactCartRequest.cartItems.indices) {
                val cartItem = contactCartRequest.cartItems[i]
                dataBase?.writableDatabase?.insert(
                    CordialSdkDBHelper.TABLE_CONTACT_CART_ITEMS,
                    null,
                    ContactCartDBUtils.getContactCartValues(cartItem)
                )
                if (i == contactCartRequest.cartItems.size - 1) {
                    onSaveRequestListener?.invoke()
                }
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_CONTACT_CART_ITEMS, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun getAllCartItems(onCashedCartItemsListener: (cartItems: List<CartItem>) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val cursor = db.readableDatabase.query(
                    CordialSdkDBHelper.TABLE_CONTACT_CART_ITEMS,
                    arrayOf(
                        CordialSdkDBHelper.PRODUCT_ID,
                        CordialSdkDBHelper.NAME,
                        CordialSdkDBHelper.SKU,
                        CordialSdkDBHelper.CATEGORY,
                        CordialSdkDBHelper.URL,
                        CordialSdkDBHelper.DESCRIPTION,
                        CordialSdkDBHelper.QTY,
                        CordialSdkDBHelper.ITEM_PRICE,
                        CordialSdkDBHelper.SALE_PRICE,
                        CordialSdkDBHelper.TIMESTAMP,
                        CordialSdkDBHelper.ATTR,
                        CordialSdkDBHelper.IMAGES,
                        CordialSdkDBHelper.PROPERTIES
                    ),
                    null,
                    null,
                    null,
                    null,
                    null
                )
                val cartItems = mutableListOf<CartItem>()
                var cartItem: CartItem
                if (cursor.moveToFirst()) {
                    do {
                        cartItem =
                            ContactCartDBUtils.getCartItemFromCursor(
                                cursor
                            )
                        cartItems.add(cartItem)
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onCashedCartItemsListener.invoke(cartItems)
                }
                cursor.close()
            }
        }
    }
}