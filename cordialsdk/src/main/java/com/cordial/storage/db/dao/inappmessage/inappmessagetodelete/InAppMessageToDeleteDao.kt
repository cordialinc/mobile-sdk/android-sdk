package com.cordial.storage.db.dao.inappmessage.inappmessagetodelete

internal interface InAppMessageToDeleteDao {
    fun insert(mcID: String)
    fun clear(onCompleteListener: (() -> Unit)? = null)
    fun getInAppMessagesToDelete(onInAppMessagesMcIDListener: ((mcIDsToDelete: List<String>) -> Unit)? = null)
    fun removeInAppMessageToDelete(mcID: String)
}