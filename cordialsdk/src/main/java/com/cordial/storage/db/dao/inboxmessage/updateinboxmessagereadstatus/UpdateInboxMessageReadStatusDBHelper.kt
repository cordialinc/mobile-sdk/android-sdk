package com.cordial.storage.db.dao.inboxmessage.updateinboxmessagereadstatus

import android.content.ContentValues
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


internal class UpdateInboxMessageReadStatusDBHelper : BaseDBHelper(), UpdateInboxMessageReadStatusDao {

    override fun insert(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onSaveRequestListener: (() -> Unit)?
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(
                    CordialSdkDBHelper.PRIMARY_KEY,
                    updateInboxMessageReadStatusRequest.primaryKey
                )
                updateInboxMessageReadStatusRequest.readInboxMessagesIds?.let { readInboxMessagesIds ->
                    val markAsReadIds = readInboxMessagesIds.joinToString { it }
                    values.put(CordialSdkDBHelper.MARK_AS_READ_IDS, markAsReadIds)
                }
                updateInboxMessageReadStatusRequest.unreadInboxMessagesIds?.let { unreadInboxMessagesIds ->
                    val markAsUnreadIds = unreadInboxMessagesIds.joinToString { it }
                    values.put(CordialSdkDBHelper.MARK_AS_UNREAD_IDS, markAsUnreadIds)
                }
                db.writableDatabase.insert(
                    CordialSdkDBHelper.TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS,
                    null,
                    values
                )
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(CordialSdkDBHelper.TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS, null, null)
                withContext(Dispatchers.Main) {
                    onCompleteListener?.invoke()
                }
            }
        }
    }

    override fun deleteUpdateInboxMessageReadStatusRequests(requestsIds: List<Int>, onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS,
                    "CAST(" + CordialSdkDBHelper.ID + " AS TEXT) IN (" + String(
                        CharArray(
                            requestsIds.size - 1
                        )
                    ).replace(
                        "\u0000",
                        "?, "
                    ) + "?)",
                    requestsIds.map { it.toString() }.toTypedArray()
                )
                onCompleteListener?.invoke()
            }
        }
    }

    override fun getUpdateInboxMessageReadStatusRequests(
        isAnonymousContact: Boolean,
        onCachedUpdateInboxReadStatusListener: (requests: List<UpdateInboxMessageReadStatusRequest>) -> Unit
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query = if (isAnonymousContact) {
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS} " +
                            "WHERE ${CordialSdkDBHelper.PRIMARY_KEY} IS NULL OR ${CordialSdkDBHelper.PRIMARY_KEY} = ''"
                } else {
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_UPDATE_INBOX_MESSAGE_READ_STATUS} " +
                            "WHERE NOT ${CordialSdkDBHelper.PRIMARY_KEY} IS NULL AND ${CordialSdkDBHelper.PRIMARY_KEY} != ''"
                }
                val cursor = db.readableDatabase.rawQuery(query, null)

                val requests = mutableListOf<UpdateInboxMessageReadStatusRequest>()
                var updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)
                    val primaryKeyIndex = cursor.getColumnIndex(CordialSdkDBHelper.PRIMARY_KEY)
                    val markAsReadIdsIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.MARK_AS_READ_IDS)
                    val markAsUnreadIdsIndex =
                        cursor.getColumnIndex(CordialSdkDBHelper.MARK_AS_UNREAD_IDS)
                    do {
                        var primaryKey = ""
                        cursor.getString(primaryKeyIndex)?.let { pk ->
                            primaryKey = pk
                        }
                        var markAsReadIds: List<String>? = null
                        cursor.getString(markAsReadIdsIndex)?.let { markAsReadIdsString ->
                            markAsReadIds = markAsReadIdsString.split(',').map { it.trim() }
                        }
                        var markAsUnreadIds: List<String>? = null
                        cursor.getString(markAsUnreadIdsIndex)?.let { markAsUnreadIdsString ->
                            markAsUnreadIds = markAsUnreadIdsString.split(',').map { it.trim() }
                        }
                        updateInboxMessageReadStatusRequest =
                            UpdateInboxMessageReadStatusRequest(
                                primaryKey,
                                "",
                                markAsReadIds,
                                markAsUnreadIds
                            )
                        updateInboxMessageReadStatusRequest.id = cursor.getInt(idIndex)
                        requests.add(updateInboxMessageReadStatusRequest)
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onCachedUpdateInboxReadStatusListener.invoke(requests)
                }
                cursor.close()
            }
        }
    }
}