package com.cordial.storage.db.dao.setcontact

import com.cordial.feature.upsertcontact.model.UpsertContactRequest

internal interface SetContactDao {
    fun insert(
        upsertContactRequest: UpsertContactRequest,
        onSaveRequestListener: (() -> Unit)? = null
    )

    fun clear(onSendLogoutListener: (() -> Unit)? = null)

    fun getSetContacts(
        onCachedSetContactListener: (upsertContactRequests: List<UpsertContactRequest>) -> Unit
    )

    fun deleteSetContacts(
        upsertContactRequests: List<UpsertContactRequest>,
        onDeleteCompleteListener: (() -> Unit)? = null
    )
}