package com.cordial.storage.db.dao.contactlogout

import android.content.ContentValues
import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class UnsetContactDBHelper : BaseDBHelper(), UnsetContactDao {

    override fun insert(unsetContactRequest: UnsetContactRequest, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.PRIMARY_KEY, unsetContactRequest.primaryKey)
                db.writableDatabase.insert(CordialSdkDBHelper.TABLE_CONTACT_LOGOUT, null, values)
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onLogoutCacheClearListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_CONTACT_LOGOUT, null, null)
            withContext(Dispatchers.Main) {
                onLogoutCacheClearListener?.invoke()
            }
        }
    }

    override fun getContactLogout(onCachedContactLogoutListener: (unsetContactRequest: UnsetContactRequest?) -> Unit) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val cursor = db.readableDatabase.query(
                    CordialSdkDBHelper.TABLE_CONTACT_LOGOUT,
                    arrayOf(CordialSdkDBHelper.PRIMARY_KEY),
                    null,
                    null,
                    null,
                    null,
                    null
                )
                var unsetContactRequest: UnsetContactRequest? = null
                if (cursor.moveToFirst()) {
                    val primaryKeyIndex = cursor.getColumnIndex(CordialSdkDBHelper.PRIMARY_KEY)
                    do {
                        unsetContactRequest =
                            UnsetContactRequest(
                                "",
                                cursor.getString(primaryKeyIndex)
                            )
                    } while (cursor.moveToNext())
                }
                withContext(Dispatchers.Main) {
                    onCachedContactLogoutListener.invoke(unsetContactRequest)
                }
                cursor.close()
            }
        }
    }
}