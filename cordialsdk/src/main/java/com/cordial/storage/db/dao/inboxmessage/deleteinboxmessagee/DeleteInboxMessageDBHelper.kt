package com.cordial.storage.db.dao.inboxmessage.deleteinboxmessagee

import android.content.ContentValues
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.storage.db.CordialSdkDBHelper
import com.cordial.storage.db.dao.BaseDBHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class DeleteInboxMessageDBHelper : BaseDBHelper(), DeleteInboxMessageDao {

    override fun insert(deleteInboxMessageRequest: DeleteInboxMessageRequest, onSaveRequestListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val values = ContentValues()
                values.put(CordialSdkDBHelper.PRIMARY_KEY, deleteInboxMessageRequest.primaryKey)
                values.put(CordialSdkDBHelper.MC_ID, deleteInboxMessageRequest.mcID)
                db.writableDatabase.insert(
                    CordialSdkDBHelper.TABLE_DELETE_INBOX_MESSAGE,
                    null,
                    values
                )
                onSaveRequestListener?.invoke()
            }
        }
    }

    override fun clear(onCompleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.writableDatabase?.delete(CordialSdkDBHelper.TABLE_DELETE_INBOX_MESSAGE, null, null)
            withContext(Dispatchers.Main) {
                onCompleteListener?.invoke()
            }
        }
    }

    override fun deleteDeleteInboxMessageRequest(requestId: Int, onSuccessDeleteListener: (() -> Unit)?) {
        doAsyncDbCall {
            dataBase?.let { db ->
                db.writableDatabase.delete(
                    CordialSdkDBHelper.TABLE_DELETE_INBOX_MESSAGE,
                    CordialSdkDBHelper.ID + "=" + requestId,
                    null
                )
                withContext(Dispatchers.Main) {
                    onSuccessDeleteListener?.invoke()
                }
            }
        }
    }

    override fun getDeleteInboxMessageRequest(
        onDeleteInboxMessageRequest: ((deleteInboxMessageRequest: DeleteInboxMessageRequest?, requestId: Int) -> Unit)?
    ) {
        doAsyncDbCall {
            dataBase?.let { db ->
                val query =
                    "SELECT * FROM ${CordialSdkDBHelper.TABLE_DELETE_INBOX_MESSAGE} ORDER BY ${CordialSdkDBHelper.ID} ASC LIMIT 1"
                val cursor = db.readableDatabase.rawQuery(query, null)
                var requestId = -1
                var deleteInboxMessageRequest: DeleteInboxMessageRequest? = null
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(CordialSdkDBHelper.ID)
                    val primaryKeyIndex = cursor.getColumnIndex(CordialSdkDBHelper.PRIMARY_KEY)
                    val mcIDIndex = cursor.getColumnIndex(CordialSdkDBHelper.MC_ID)

                    do {
                        requestId = cursor.getInt(idIndex)
                        val primaryKey = cursor.getString(primaryKeyIndex)
                        val mcID = cursor.getString(mcIDIndex)
                        deleteInboxMessageRequest = DeleteInboxMessageRequest(primaryKey, "", mcID)
                    } while (cursor.moveToNext())
                }
                cursor.close()
                withContext(Dispatchers.Main) {
                    onDeleteInboxMessageRequest?.invoke(deleteInboxMessageRequest, requestId)
                }
            }
        }
    }
}