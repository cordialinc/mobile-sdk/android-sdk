package com.cordial.network

internal interface OnConnectivityListener {
    fun onConnectionChanged(isAvailable: Boolean)
}