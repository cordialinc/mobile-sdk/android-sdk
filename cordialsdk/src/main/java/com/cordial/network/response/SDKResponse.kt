package com.cordial.network.response

import com.cordial.network.request.RequestMethod
import java.net.URL

internal class SDKResponse(
    val responseCode: Int,
    val url: URL,
    val body: String?,
    val error: String?,
    val headers: Map<String, MutableList<String>>,
    val methodOfRequest: RequestMethod? = null
)