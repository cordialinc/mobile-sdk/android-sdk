package com.cordial.network.response

import com.cordial.api.C
import com.cordial.feature.log.CordialLoggerManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class S3SDKResponseHandler : ResponseHandler {
    override suspend fun onHandleResponse(sdkResponse: SDKResponse, onResponseListener: OnResponseListener) {
        with(sdkResponse) {
            when (responseCode) {
                200 -> {
                    body?.let { response ->
                        CordialLoggerManager.debug("${methodOfRequest?.key} $url : $response")
                        withContext(Dispatchers.Main) {
                            onResponseListener.onSuccess(response)
                        }
                    }
                }
                400 -> {
                    error?.let {
                        CordialLoggerManager.debug("$${methodOfRequest?.key} $url : $error")
                    }
                    withContext(Dispatchers.Main) {
                        onResponseListener.onLogicError(C.PROVIDED_TOKEN_EXPIRED)
                    }
                }
                403 -> {
                    error?.let {
                        CordialLoggerManager.debug("${methodOfRequest?.key} $url : $error")
                    }
                    withContext(Dispatchers.Main) {
                        onResponseListener.onLogicError(C.ACCESS_DENIED)
                    }
                }
                else -> {
                    error?.let {
                        CordialLoggerManager.debug("${methodOfRequest?.key} $url : $error")
                        withContext(Dispatchers.Main) {
                            onResponseListener.onLogicError(error)
                        }
                    }
                }
            }
        }
    }
}