package com.cordial.network.response

import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.feature.log.CordialLoggerManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class DeepLinkResponseHandler : ResponseHandler {

    override suspend fun onHandleResponse(sdkResponse: SDKResponse, onResponseListener: OnResponseListener) {
        with(sdkResponse) {
            when (responseCode) {
                302 -> {
                    val redirectDeepLink = getRedirectDeepLink(sdkResponse = this)
                    if (redirectDeepLink == null) {
                        val response = C.NO_LOCATION_HEADER_ERROR
                        withContext(Dispatchers.Main) {
                            onResponseListener.onLogicError(response)
                        }
                        return
                    }
                    saveMcID(sdkResponse = this)
                    CordialLoggerManager.debug("$url : $redirectDeepLink")
                    withContext(Dispatchers.Main) {
                        onResponseListener.onSuccess(redirectDeepLink)
                    }
                }
                else -> {
                    error?.let {
                        CordialLoggerManager.debug("$url : $error")
                        withContext(Dispatchers.Main) {
                            onResponseListener.onLogicError(error)
                        }
                    }
                }
            }
        }
    }

    private fun getRedirectDeepLink(sdkResponse: SDKResponse): String? {
        return sdkResponse.headers[C.LOCATION]?.get(0)
    }

    private fun saveMcID(sdkResponse: SDKResponse) {
        val mcID = sdkResponse.headers[C.MC_ID_HEADER]?.get(0)
        mcID?.let {
            if (!isTestMessage(sdkResponse))
                CordialApi().setMcID(it)
        }
    }

    private fun isTestMessage(sdkResponse: SDKResponse): Boolean {
        var isTestMessage = false
        val testMessage = sdkResponse.headers[C.TEST_MC_ID_HEADER]?.get(0)
        testMessage?.let {
            isTestMessage = it.toInt() == 1
        }
        return isTestMessage
    }
}