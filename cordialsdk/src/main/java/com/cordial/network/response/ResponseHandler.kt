package com.cordial.network.response

internal interface ResponseHandler {
    suspend fun onHandleResponse(
        sdkResponse: SDKResponse,
        onResponseListener: OnResponseListener
    )
}