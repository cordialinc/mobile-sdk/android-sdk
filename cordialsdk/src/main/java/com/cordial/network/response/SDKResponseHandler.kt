package com.cordial.network.response

import com.cordial.api.C
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class SDKResponseHandler(
    private val sdkSecurityUseCase: SDKSecurityUseCase? = null
): ResponseHandler {
    override suspend fun onHandleResponse(sdkResponse: SDKResponse, onResponseListener: OnResponseListener) {
        with(sdkResponse) {
            when (responseCode) {
                200 -> {
                    body?.let { response ->
                        CordialLoggerManager.debug("${methodOfRequest?.key} $url : $response")
                        withContext(Dispatchers.Main) {
                            onResponseListener.onSuccess(response)
                        }
                    }
                }
                401 -> {
                    withContext(Dispatchers.Main) {
                        onResponseListener.onSystemError(C.JWT_TOKEN_EXPIRED) {
                            sdkSecurityUseCase?.updateJWT()
                        }
                    }
                }
                else -> {
                    error?.let {
                        CordialLoggerManager.debug("${methodOfRequest?.key} $url : $error")
                        withContext(Dispatchers.Main) {
                            onResponseListener.onLogicError(error)
                        }
                    }
                }
            }
        }
    }
}