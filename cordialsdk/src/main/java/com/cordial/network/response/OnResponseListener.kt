package com.cordial.network.response

internal interface OnResponseListener {
    fun onSuccess(response: String)
    fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)? = null)
    fun onLogicError(response: String)
}