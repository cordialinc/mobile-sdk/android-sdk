package com.cordial.network.request

class PageRequest(
    var page: Int,
    var size: Int
) {
    fun next(): PageRequest {
        page++
        return this
    }

    fun previous(): PageRequest {
        if (page > 1)
            page--
        return this
    }
}