package com.cordial.network.request

import com.cordial.feature.log.CordialLoggerManager
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponse
import kotlinx.coroutines.*
import java.net.HttpURLConnection
import kotlin.coroutines.CoroutineContext

internal class SDKRequestSender : RequestSender, CoroutineScope {

    private var job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.IO

    override fun send(request: SDKRequest, responseHandler: ResponseHandler, onResponseListener: OnResponseListener) {
        launch {
            try {
                with(CordialRequestFactory().getURLRequest(request)) {
                    withContext(Dispatchers.IO) {
                        if (request.methodOfRequest == RequestMethod.POST) {
                            writeOutput(request)
                        }
                        handleResponse(request, responseHandler, onResponseListener)
                    }
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                    onResponseListener.onSystemError(e.toString(), null)
                }
            }
        }
    }

    private suspend fun HttpURLConnection.writeOutput(request: SDKRequest) = withContext(Dispatchers.IO) {
        request.jsonBody?.let { jsonBody ->
            CordialLoggerManager.debug("$requestMethod $url : ${request.jsonBody}")
            val bytes = jsonBody.toByteArray()
            outputStream.write(bytes, 0, bytes.size)
        }
    }

    private suspend fun HttpURLConnection.handleResponse(
        request: SDKRequest,
        responseHandler: ResponseHandler,
        onResponseListener: OnResponseListener
    ) {
        val body = if (responseCode == 200) inputStream?.bufferedReader()?.readText() else null
        val error = if (responseCode != 200) errorStream?.bufferedReader()?.readText() else null
        val sdkResponse = SDKResponse(responseCode, url, body, error, headerFields, request.methodOfRequest)
        responseHandler.onHandleResponse(sdkResponse, onResponseListener)
    }
}