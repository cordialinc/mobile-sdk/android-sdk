package com.cordial.network.request

import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal interface RequestSender {
    fun send(request: SDKRequest, responseHandler: ResponseHandler, onResponseListener: OnResponseListener)
}