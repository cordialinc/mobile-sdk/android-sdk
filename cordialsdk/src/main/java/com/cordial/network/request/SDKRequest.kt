package com.cordial.network.request

internal class SDKRequest(
    val jsonBody: String?,
    val url: String,
    val methodOfRequest: RequestMethod
) {
    var isCordial: Boolean = true
    var queryParams: Map<String, String>? = null
    var followRedirect: Boolean = true
}