package com.cordial.network.request

internal enum class RequestMethod(val key: String) {
    POST("POST"),
    GET("GET"),
    DELETE("DELETE"),
    DEFAULT("POST");

    companion object {
        fun findKey(key: String): RequestMethod {
            return values().firstOrNull { it.key == key } ?: DEFAULT
        }
    }
}