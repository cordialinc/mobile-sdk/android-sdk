package com.cordial.network.request

import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import java.net.HttpURLConnection
import java.net.URL

internal class CordialRequestFactory {

    fun getURLRequest(request: SDKRequest): HttpURLConnection {
        val url = getURL(request)
        val cordialApi = CordialApi()
        val config = CordialApiConfiguration.getInstance()
        return (url.openConnection() as HttpURLConnection).apply {
            requestMethod = request.methodOfRequest.key
            setRequestProperty("Content-Type", "application/json")
            setRequestProperty("Accept", "application/json")
            if (request.isCordial) {
                setRequestProperty("User-Agent", cordialApi.getUserAgent())
                setRequestProperty("Cordial-AccountKey", "" + config.getAccountKey())
                setRequestProperty("Cordial-ChannelKey", "" + config.getChannelKey())
                if (cordialApi.getCurrentJWT() != "") {
                    val authorizationBearerSchema = "Bearer ${cordialApi.getCurrentJWT()}"
                    setRequestProperty("Authorization", authorizationBearerSchema)
                }
//                CordialLoggerManager.debug("header: User-Agent = ${cordialApi.getUserAgent()}\n" +
//                    "AccountKey = ${config.getAccountKey()}\n" +
//                    "ChannelKey = ${config.getChannelKey()}\n" +
//                    "Token = ${cordialApi.getCurrentJWT()}")
            }
            instanceFollowRedirects = request.followRedirect
        }
    }

    private fun getURL(request: SDKRequest): URL {
        request.queryParams?.let { params ->
            var query = ""
            for (param in params) {
                query += "${param.key}=${param.value}&"
            }
            query = query.substring(0, query.length - 1)
            return URL(request.url + "/?" + query)
        }
        return URL(request.url)
    }
}