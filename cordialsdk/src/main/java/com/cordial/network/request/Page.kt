package com.cordial.network.request

class Page<T>(
    val content: List<T>,
    val current: Int,
    val last: Int,
    val size: Int,
    val total: Int
) {
    fun hasNext(): Boolean {
        return current < last
    }
}