package com.cordial.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

internal class NetworkStateListener : NetworkState {

    override fun register(context: Context, onConnectivityListener: OnConnectivityListener) {
        ConnectionStateMonitor(onConnectivityListener).enable(context)
    }

    override fun unregister(context: Context, onConnectivityListener: OnConnectivityListener) {
        ConnectionStateMonitor(onConnectivityListener).disable(context)
    }

    override fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        connectivityManager?.let { cm ->
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                val networkInfo = cm.activeNetworkInfo
                return networkInfo != null && networkInfo.isConnected
            } else {
                val network = cm.activeNetwork
                network?.let {
                    val networkCapabilities = cm.getNetworkCapabilities(network)
                    networkCapabilities?.let {
                        return (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
                                || networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
                    }
                }
            }
        }
        return false
    }
}