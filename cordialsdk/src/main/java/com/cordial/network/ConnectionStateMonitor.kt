package com.cordial.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import com.cordial.api.CordialApiConfiguration

internal class ConnectionStateMonitor(private val listener: OnConnectivityListener) :
    ConnectivityManager.NetworkCallback() {
    private val networkRequest: NetworkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .addTransportType(NetworkCapabilities.TRANSPORT_VPN).build()


    fun enable(context: Context) {
        val networkStateHandler = CordialApiConfiguration.getInstance().networkStateHandler
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            connectivityManager?.registerNetworkCallback(networkRequest, this)
            networkStateHandler?.isNetworkCallbackRegistered = true
        } catch (e: Exception) {
            networkStateHandler?.isNetworkCallbackRegistered = false
            e.printStackTrace()
        }
    }

    fun disable(context: Context) {
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            connectivityManager?.unregisterNetworkCallback(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAvailable(network: Network) {
        super.onAvailable(network)
        listener.onConnectionChanged(true)
    }

    override fun onLost(network: Network) {
        super.onLost(network)
        listener.onConnectionChanged(false)
    }
}