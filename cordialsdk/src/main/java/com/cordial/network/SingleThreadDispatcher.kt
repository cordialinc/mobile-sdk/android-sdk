package com.cordial.network

import kotlinx.coroutines.asCoroutineDispatcher
import java.util.concurrent.Executors

internal class SingleThreadDispatcher {
    val dispatcher = Executors.newSingleThreadExecutor().asCoroutineDispatcher()

    companion object {
        private var INSTANCE: SingleThreadDispatcher? = null

        fun getInstance(): SingleThreadDispatcher {
            if (INSTANCE == null) {
                INSTANCE = SingleThreadDispatcher()
            }
            return INSTANCE as SingleThreadDispatcher
        }
    }
}
