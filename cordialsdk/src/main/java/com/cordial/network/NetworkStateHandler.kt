package com.cordial.network

import android.content.Context
import com.cordial.api.CordialApiConfiguration
import com.cordial.dependency.DependencyConfiguration

internal class NetworkStateHandler(val context: Context) : OnConnectivityListener {
    var onConnectionWasAvailableBefore = false
    var isNetworkCallbackRegistered = false

    fun register() {
        DependencyConfiguration.getInstance().networkState.register(context, this)
    }

    fun unregister() {
        DependencyConfiguration.getInstance().networkState.unregister(context, this)
    }

    override fun onConnectionChanged(isAvailable: Boolean) {
        if (!onConnectionWasAvailableBefore && isAvailable) {
            sendCachedData()
        }
        onConnectionWasAvailableBefore = isAvailable
    }

    private fun sendCachedData() {
        val cacheManager = CordialApiConfiguration.getInstance().injection.cacheManager()
        cacheManager.sendCachedData()
    }

    fun isNetworkAvailable(): Boolean {
        return DependencyConfiguration.getInstance().networkState.isNetworkAvailable(context)
    }
}
