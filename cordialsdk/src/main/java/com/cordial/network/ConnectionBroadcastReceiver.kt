package com.cordial.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.cordial.api.CordialApiConfiguration

internal class ConnectionBroadcastReceiver(private val listener: OnConnectivityListener) :
    BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        listener.onConnectionChanged(
            CordialApiConfiguration.getInstance().networkStateHandler?.isNetworkAvailable() ?: false
        )
    }
}