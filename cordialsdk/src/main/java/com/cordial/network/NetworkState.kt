package com.cordial.network

import android.content.Context


internal interface NetworkState {
    fun register(context: Context, onConnectivityListener: OnConnectivityListener)
    fun unregister(context: Context, onConnectivityListener: OnConnectivityListener)
    fun isNetworkAvailable(context: Context): Boolean
}