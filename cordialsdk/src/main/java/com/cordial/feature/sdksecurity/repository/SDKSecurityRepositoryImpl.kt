package com.cordial.feature.sdksecurity.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class SDKSecurityRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : SDKSecurityRepository {

    override fun getJWTToken(onResponseListener: OnResponseListener) {
        val securityUrl = cordialApiEndpoints.getSdkSecurityUrl()
        val sdkRequest = SDKRequest(null, securityUrl, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }
}