package com.cordial.feature.sdksecurity.usecase

import com.cordial.api.CordialApi
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.repository.SDKSecurityRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.util.JsonUtils.optNullableString
import org.json.JSONObject


internal class SDKSecurityUseCaseImpl(private val sdkSecurityRepository: SDKSecurityRepository) : SDKSecurityUseCase,
    CordialCheck() {

    override fun updateJWT(onUpdateJwtListener: (() -> Unit)?) {
        checkIsJWTFetching()
        doAfterCheck {
            sdkSecurityRepository.getJWTToken(object : OnResponseListener {
                override fun onSuccess(response: String) {
                    SendingCacheState.isJWTCurrentlyFetching.set(false)
                    onTokenReceived(response, onUpdateJwtListener)
                }

                override fun onLogicError(response: String) {
                    handleError(error = "Failed to fetch the jwt due to the error: $response")
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleError(error = "Failed to fetch the jwt due to the error: $error")
                    onSaveRequestListener?.invoke()
                }
            })
        }
    }

    private fun checkIsJWTFetching() {
        val isJWTCurrentlyFetchingCheck = Check(
            checkFunction = { SendingCacheState.isJWTCurrentlyFetching.compareAndSet(false, true) },
            doOnSuccess = { isChecked = true }
        )
        isJWTCurrentlyFetchingCheck.execute()
    }

    private fun onTokenReceived(response: String, onUpdateJwtListener: (() -> Unit)?) {
        val token = JSONObject(response).optNullableString("token")
        if (token != null) {
            CordialApi().setCurrentJWT(token)
            onUpdateJwtListener?.invoke()
        } else {
            handleError(error = "Failed to fetch the jwt because there is no jwt in response")
        }
    }

    private fun handleError(error: String) {
        CordialLoggerManager.error(error)
        SendingCacheState.isJWTCurrentlyFetching.set(false)
    }
}