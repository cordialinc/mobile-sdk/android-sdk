package com.cordial.feature.sdksecurity.usecase

internal interface SDKSecurityUseCase {
    fun updateJWT(onUpdateJwtListener: (() -> Unit)? = null)
}