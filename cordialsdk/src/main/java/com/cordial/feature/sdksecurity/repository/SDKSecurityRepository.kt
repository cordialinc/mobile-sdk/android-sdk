package com.cordial.feature.sdksecurity.repository

import com.cordial.network.response.OnResponseListener

internal interface SDKSecurityRepository {
    fun getJWTToken(onResponseListener: OnResponseListener)
}