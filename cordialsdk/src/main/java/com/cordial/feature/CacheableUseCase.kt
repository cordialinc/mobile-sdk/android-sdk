package com.cordial.feature

interface CacheableUseCase {
    fun clearCache(onCompleteListener: (() -> Unit)? = null)
}