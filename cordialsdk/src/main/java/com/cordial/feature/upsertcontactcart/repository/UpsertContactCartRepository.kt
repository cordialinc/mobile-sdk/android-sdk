package com.cordial.feature.upsertcontactcart.repository

import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.network.response.OnResponseListener

internal interface UpsertContactCartRepository {
    fun upsertContactCart(upsertContactCartRequest: UpsertContactCartRequest, onResponseListener: OnResponseListener)
}