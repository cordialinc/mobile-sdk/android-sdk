package com.cordial.feature.upsertcontactcart.usecase

import com.cordial.api.C
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.feature.upsertcontactcart.repository.UpsertContactCartRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.contactcart.ContactCartDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences

internal class UpsertContactCartUseCaseImpl(
    private val upsertContactCartRepository: UpsertContactCartRepository,
    private val preferences: Preferences,
    private val contactCartDao: ContactCartDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : UpsertContactCartUseCase, CordialCheck() {

    override fun upsertContactCart(
        upsertContactCartRequest: UpsertContactCartRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        checkAuth(upsertContactCartRequest, onRequestFromDBListener)
        doAfterCheck {
            upsertContactCartRepository.upsertContactCart(
                upsertContactCartRequest,
                object : OnResponseListener {
                    override fun onSuccess(response: String) {
                        onRequestFromDBListener?.onSuccess()
                    }

                    override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                        handleSystemError(
                            onRequestFromDBListener,
                            upsertContactCartRequest,
                            reason = "Failed to upsert contact cart due to the error : $error, saving contact cart to the cache",
                            onSaveRequestListener
                        )
                    }

                    override fun onLogicError(response: String) {
                        onRequestFromDBListener?.onFailure()
                        CordialLoggerManager.error("Failed to upsert contact cart due to the error: $response")
                    }
                })
        }
    }

    private fun checkAuth(
        upsertContactCartRequest: UpsertContactCartRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    upsertContactCartRequest,
                    reason = "Failed to upsert contact cart because the contact is not registered, saving contact cart to the cache",
                    onSaveRequestListener = null
                )
            })
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    upsertContactCartRequest,
                    reason = "Failed to upsert contact cart because the jwt is absent, saving contact cart to the cache",
                    onSaveRequestListener = {
                        sdkSecurityUseCase.updateJWT()
                    })
            })
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    upsertContactCartRequest,
                    reason = "Failed to upsert contact cart because there is no network connection, saving contact cart to the cache",
                    onSaveRequestListener = null
                )
            })
        networkAvailableCheck.execute()
    }

    private fun handleSystemError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        upsertContactCartRequest: UpsertContactCartRequest,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        CordialLoggerManager.info(reason)
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            saveContactCartToDB(upsertContactCartRequest, onSaveRequestListener)
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    private fun saveContactCartToDB(
        upsertContactCartRequest: UpsertContactCartRequest,
        onSaveRequestListener: (() -> Unit)?
    ) {
        // contact cart table is clearing, because by logic we need to save only one last request
        contactCartDao?.clear {
            if (upsertContactCartRequest.cartItems.isEmpty()) {
                val emptyCartItem = CartItem(C.CORDIAL_EMPTY_CART_ITEM, name = "", sku = "", category = "", qty = 1)
                upsertContactCartRequest.cartItems = listOf(emptyCartItem)
            }
            contactCartDao.insert(upsertContactCartRequest, onSaveRequestListener)
        }
    }

    override fun sendCachedContactCart() {
        if (!SendingCacheState.sendingUpsertContactCart.compareAndSet(false, true))
            return
        contactCartDao?.getAllCartItems { cartItemList ->
            if (cartItemList.isEmpty()) {
                SendingCacheState.sendingUpsertContactCart.set(false)
                return@getAllCartItems
            }
            val contactCartRequest = getContactCartRequestFromCache(cartItemList)
            upsertCachedContactCart(contactCartRequest)
        }
    }

    private fun getContactCartRequestFromCache(cartItemList: List<CartItem>): UpsertContactCartRequest {
        var cartItems = cartItemList
        cartItemList.forEach {
            if (it.productId == C.CORDIAL_EMPTY_CART_ITEM) {
                cartItems = listOf()
                return@forEach
            }
        }
        val deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
        val primaryKey = preferences.getString(PreferenceKeys.PRIMARY_KEY)
        return UpsertContactCartRequest(deviceId, primaryKey, cartItems)
    }

    private fun upsertCachedContactCart(contactCartRequest: UpsertContactCartRequest) {
        upsertContactCart(contactCartRequest, object : OnRequestFromDBListener {
            override fun onSuccess() {
                contactCartDao?.clear()
                CordialLoggerManager.info("Contact cart sent, clearing contact cart cache")
                SendingCacheState.sendingUpsertContactCart.set(false)
            }

            override fun onFailure() {
                SendingCacheState.sendingUpsertContactCart.set(false)
            }
        })
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        contactCartDao?.clear(onCompleteListener)
    }
}