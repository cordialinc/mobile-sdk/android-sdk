package com.cordial.feature.upsertcontactcart.model

import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.util.TimeUtils
import java.util.*

data class CartItem(
    val productId: String,
    val name: String,
    val sku: String,
    var category: String,
    var qty: Int
) {
    @Deprecated(
        "Replaced by CartItem(String, String, String, String, Int)",
        level = DeprecationLevel.WARNING
    )
    constructor(
        productId: String,
        name: String,
        sku: String,
    ) : this(productId, name, sku, category = "", qty = 1)

    @Deprecated(
        "Replaced by CartItem(String, String, String, String, Int)",
        level = DeprecationLevel.WARNING
    )
    constructor(
        productId: String,
        name: String,
        sku: String,
        category: String?,
        url: String?,
        itemDescription: String?,
        qty: Int?,
        itemPrice: Double?,
        salePrice: Double?,
        attr: Map<String, String>?,
        images: List<String>?,
        properties: Map<String, String>?
    ) : this(productId, name, sku, category = category ?: "", qty = qty ?: 1) {
        this.url = url
        this.itemDescription = itemDescription
        this.itemPrice = itemPrice
        this.salePrice = salePrice
        this.attr = attr
        this.images = images
        val propertyValues = properties?.mapValues { PropertyValue.StringProperty(it.value) }
        this.properties = propertyValues
    }

    private var timestamp: String = TimeUtils.getTimestamp()
    var url: String? = null
    var itemDescription: String? = null
    var itemPrice: Double? = null
    var salePrice: Double? = null
    var attr: Map<String, String>? = null
    var images: List<String>? = null
    var properties: Map<String, PropertyValue>? = null

    fun setTimestamp(date: Date) {
        setTimestamp(TimeUtils.getTimestamp(date))
    }

    internal fun setTimestamp(timestamp: String) {
        this.timestamp = timestamp
    }

    fun getTimestamp(): String {
        return timestamp
    }

    fun withTimestamp(date: Date): CartItem {
        setTimestamp(date)
        return this
    }

    @Deprecated(
        "Property moved to the constructor",
        level = DeprecationLevel.WARNING
    )
    fun withCategory(category: String?): CartItem {
        if (category == null) this.category = ""
        else this.category = category
        return this
    }

    fun withUrl(url: String?): CartItem {
        this.url = url
        return this
    }

    fun withItemDescription(itemDescription: String?): CartItem {
        this.itemDescription = itemDescription
        return this
    }

    @Deprecated(
        "Property moved to the constructor",
        level = DeprecationLevel.WARNING
    )
    fun withQty(qty: Int?): CartItem {
        if (qty == null) this.qty = 1
        else this.qty = qty
        return this
    }

    fun withItemPrice(itemPrice: Double?): CartItem {
        this.itemPrice = itemPrice
        return this
    }

    fun withSalePrice(salePrice: Double?): CartItem {
        this.salePrice = salePrice
        return this
    }

    fun withAttr(attr: Map<String, String>?): CartItem {
        this.attr = attr
        return this
    }

    fun withImages(images: List<String>?): CartItem {
        this.images = images
        return this
    }

    fun withProperties(properties: Map<String, PropertyValue>?): CartItem {
        this.properties = properties
        return this
    }
}