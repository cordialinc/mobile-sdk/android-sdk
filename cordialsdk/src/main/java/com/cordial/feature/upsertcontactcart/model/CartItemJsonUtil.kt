package com.cordial.feature.upsertcontactcart.model

import com.cordial.api.C
import com.cordial.util.JsonUtils
import org.json.JSONArray
import org.json.JSONObject

object CartItemJsonUtil {

    fun getCartItemJsonObject(cartItem: CartItem): JSONObject {
        val jsonObject = JSONObject()
        jsonObject.put(C.PRODUCT_ID, cartItem.productId)
        jsonObject.put(C.NAME, cartItem.name)
        jsonObject.put(C.SKU, cartItem.sku)
        jsonObject.put(C.CATEGORY, cartItem.category)
        jsonObject.put(C.QTY, cartItem.qty)
        cartItem.url?.let {
            jsonObject.put(C.URL, cartItem.url)
        }
        cartItem.itemDescription?.let {
            jsonObject.put(C.DESCRIPTION, it)
        }
        cartItem.itemPrice?.let {
            jsonObject.put(C.ITEM_PRICE, it)
        }
        cartItem.salePrice?.let {
            jsonObject.put(C.SALE_PRICE, it)
        }
        jsonObject.put(C.TIMESTAMP, cartItem.getTimestamp())
        cartItem.attr?.let { attrList ->
            val attrs = JSONObject()
            for (attr in attrList) {
                attrs.put(attr.key, attr.value)
            }
            jsonObject.put(C.ATTR, attrs)
        }
        cartItem.images?.let { imageList ->
            val images = JSONArray()
            for (image in imageList) {
                images.put(image)
            }
            jsonObject.put(C.IMAGES, images)
        }
        cartItem.properties?.let { propertyList ->
            val properties = JsonUtils.getJsonFromPropertyValueMap(propertyList)
            jsonObject.put(C.PROPERTIES, properties)
        }
        return jsonObject
    }
}