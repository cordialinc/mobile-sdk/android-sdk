package com.cordial.feature.upsertcontactcart.model

class UpsertContactCartRequest(
    val deviceId: String,
    val primaryKey: String,
    var cartItems: List<CartItem>
)