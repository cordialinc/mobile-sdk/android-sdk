package com.cordial.feature.upsertcontactcart.repository

import com.cordial.api.C
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.upsertcontactcart.model.CartItemJsonUtil
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import org.json.JSONArray
import org.json.JSONObject

internal class UpsertContactCartRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : UpsertContactCartRepository {

    override fun upsertContactCart(
        upsertContactCartRequest: UpsertContactCartRequest,
        onResponseListener: OnResponseListener
    ) {
        val requestJson = getUpsertContactCartRequestJSON(mutableListOf(upsertContactCartRequest))
        val url = cordialApiEndpoints.getUpsertContactCartUrl()
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getUpsertContactCartRequestJSON(upsertContactCartRequests: List<UpsertContactCartRequest>): String {
        val jsonArray = JSONArray()
        upsertContactCartRequests.forEach { upsertContactCartRequest ->
            val param = JSONObject()
            val cartItems = JSONArray()
            param.put(C.DEVICE_ID, upsertContactCartRequest.deviceId)
            if (upsertContactCartRequest.primaryKey.isNotEmpty()) {
                param.put(C.PRIMARY_KEY, upsertContactCartRequest.primaryKey)
            }
            upsertContactCartRequest.cartItems.forEach { cartItem ->
                cartItems.put(CartItemJsonUtil.getCartItemJsonObject(cartItem))
            }
            param.put(C.CART_ITEMS, cartItems)

            jsonArray.put(param)
        }
        return jsonArray.toString()
    }
}