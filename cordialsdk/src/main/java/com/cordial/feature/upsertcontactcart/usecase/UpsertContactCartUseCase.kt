package com.cordial.feature.upsertcontactcart.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.storage.db.OnRequestFromDBListener

internal interface UpsertContactCartUseCase : CacheableUseCase {
    fun upsertContactCart(
        upsertContactCartRequest: UpsertContactCartRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    )

    fun sendCachedContactCart()
}