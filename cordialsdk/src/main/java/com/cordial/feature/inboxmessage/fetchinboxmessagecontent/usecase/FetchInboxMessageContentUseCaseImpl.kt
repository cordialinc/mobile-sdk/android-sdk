package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.usecase

import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model.InboxMessageContentMetadata
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.repository.FetchInboxMessageContentRepository
import com.cordial.feature.inboxmessage.getinboxmessage.usecase.FetchInboxMessageUseCase
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDao
import com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent.InboxMessageContentDao
import com.cordial.util.TimeUtils
import com.cordial.util.getByteSize

internal class FetchInboxMessageContentUseCaseImpl(
    private val fetchInboxMessageContentRepository: FetchInboxMessageContentRepository,
    private val inboxMessageDao: InboxMessageDao?,
    private val inboxMessageContentDao: InboxMessageContentDao?,
    private val fetchInboxMessageUseCase: FetchInboxMessageUseCase?
) : FetchInboxMessageContentUseCase, CordialCheck() {

    override fun checkCacheAndFetchInboxMessageContent(
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        getCachedInboxMessageContent(mcID) { inboxMessageContent ->
            if (inboxMessageContent != null) {
                onSuccess.invoke(inboxMessageContent)
                return@getCachedInboxMessageContent
            }
            getInboxMessageAndFetchInboxMessageContent(mcID, onSuccess, onFailure)
        }
    }

    private fun getInboxMessageAndFetchInboxMessageContent(
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        inboxMessageDao?.getInboxMessage(mcID) { cachedInboxMessage ->
            val isUrlValid = !TimeUtils.isDateExpired(cachedInboxMessage?.urlExpireAt, isMillis = true)
            val url = cachedInboxMessage?.url
            if (url != null && isUrlValid) {
                fetchInboxMessageContent(mcID, url, onSuccess) { error ->
                    handleCachedUrlError(error, mcID, onSuccess, onFailure)
                }
            } else {
                fetchInboxMessageAndInboxMessageContent(mcID, onSuccess, onFailure)
            }
        }
    }

    private fun handleCachedUrlError(
        error: String,
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        if (error == C.ACCESS_DENIED) {
            fetchInboxMessageAndInboxMessageContent(mcID, onSuccess, onFailure)
        } else {
            onFailure.invoke(error)
        }
    }

    private fun fetchInboxMessageAndInboxMessageContent(
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        fetchInboxMessageUseCase?.fetchInboxMessage(mcID, onSuccess = { inboxMessage ->
            inboxMessage.url?.let { url ->
                fetchInboxMessageContent(mcID, url, onSuccess, onFailure)
            }
        }, onFailure = { error ->
            onFailure(error)
        })
    }

    private fun fetchInboxMessageContent(
        mcID: String,
        inboxMessageUrl: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        checkAuth(onFailure)
        doAfterCheck {
            fetchInboxMessageContentRepository.fetchInboxMessageContent(inboxMessageUrl, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    checkCacheSizeAndSaveInboxMessageContentToCache(mcID, response) {
                        onSuccess(response)
                    }
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleSystemError(error, onFailure)
                }

                override fun onLogicError(response: String) {
                    handleLogicError(response, onFailure)
                }
            })
        }
    }

    private fun checkAuth(onFailure: (error: String) -> Unit) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(error = C.FETCH_INBOX_MESSAGE_CONTENT_CONTACT_IS_NOT_REGISTERED_ERROR, onFailure)
            }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = loginCheck,
            doOnError = { handleSystemError(error = C.NO_NETWORK_ERROR, onFailure) }
        )
        networkAvailableCheck.execute()
    }

    private fun checkCacheSizeAndSaveInboxMessageContentToCache(
        mcID: String,
        inboxMessageContent: String,
        onSaveRequestListener: (() -> Unit)
    ) {
        val maxCacheSize = CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheSize
        val maxCacheableMessageSize = CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheableMessageSize
        val inboxMessageContentSize = inboxMessageContent.getByteSize()
        if (inboxMessageContentSize > maxCacheableMessageSize) {
            CordialLoggerManager.info(
                "Failed to cache inbox message content, because max cacheable message size = $maxCacheableMessageSize has been exceeded"
            )
            onSaveRequestListener.invoke()
            return
        }
        inboxMessageContentDao?.getInboxMessageContentCacheSize { currentCacheSize ->
            if (currentCacheSize + inboxMessageContentSize <= maxCacheSize) {
                saveInboxMessageContentToCache(inboxMessageContentDao, mcID, inboxMessageContent, onSaveRequestListener)
            } else {
                freeInboxMessageContentCacheSpaceAndCacheNewContent(
                    onSaveRequestListener,
                    maxCacheSize,
                    currentCacheSize,
                    inboxMessageContentSize,
                    mcID,
                    inboxMessageContent
                )
            }
        }
    }

    private fun freeInboxMessageContentCacheSpaceAndCacheNewContent(
        onSaveRequestListener: () -> Unit,
        maxCacheSize: Int,
        currentCacheSize: Int,
        inboxMessageContentSize: Int,
        mcID: String,
        inboxMessageContent: String
    ) {
        getInboxMessageContentsMetadata { metadataList ->
            if (metadataList == null) {
                onSaveRequestListener.invoke()
                return@getInboxMessageContentsMetadata
            }
            val mcIDsToDelete = getMcIDsToDelete(maxCacheSize, currentCacheSize, metadataList, inboxMessageContentSize)
            CordialLoggerManager.info(
                "The max cache size of inbox messages content has been reached, the old cached content has been deleted"
            )
            inboxMessageContentDao?.deleteInboxMessageContents(mcIDsToDelete) {
                saveInboxMessageContentToCache(
                    inboxMessageContentDao,
                    mcID,
                    inboxMessageContent,
                    onSaveRequestListener
                )
            }
        }
    }

    private fun getMcIDsToDelete(
        maxCacheSize: Int,
        currentCacheSize: Int,
        metadataList: List<InboxMessageContentMetadata>,
        inboxMessageContentSize: Int
    ): MutableList<String> {
        val mcIDsToDelete = mutableListOf<String>()
        var freeSpace = maxCacheSize - currentCacheSize

        for (metadata in metadataList) {
            mcIDsToDelete.add(metadata.mcID)
            freeSpace += metadata.cacheSize
            if (freeSpace >= inboxMessageContentSize) {
                break
            }
        }
        return mcIDsToDelete
    }

    private fun saveInboxMessageContentToCache(
        inboxMessageContentDao: InboxMessageContentDao,
        mcID: String,
        inboxMessageContent: String,
        onSaveRequestListener: () -> Unit
    ) {
        inboxMessageContentDao.insert(mcID, inboxMessageContent, onSaveRequestListener)
    }

    private fun handleSystemError(error: String, onFailure: (error: String) -> Unit) {
        onFailure.invoke(error)
    }

    private fun handleLogicError(error: String, onFailure: (error: String) -> Unit) {
        onFailure.invoke(error)
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        inboxMessageContentDao?.clear(onCompleteListener)
    }

    override fun getCachedInboxMessageContent(
        mcID: String,
        onInboxMessageContentListener: ((inboxMessageContent: String?) -> Unit)?
    ) {
        inboxMessageContentDao?.getInboxMessageContent(mcID, onInboxMessageContentListener)
    }

    override fun getInboxMessageContentsMetadata(onInboxMessageContentMetadataListener: ((metadataList: List<InboxMessageContentMetadata>?) -> Unit)?) {
        inboxMessageContentDao?.getInboxMessageContentsMetadata(onInboxMessageContentMetadataListener)
    }
}