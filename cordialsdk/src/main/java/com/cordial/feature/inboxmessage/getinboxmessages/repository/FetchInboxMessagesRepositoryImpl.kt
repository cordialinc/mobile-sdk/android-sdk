package com.cordial.feature.inboxmessage.getinboxmessages.repository

import com.cordial.api.C
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.network.request.PageRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.TimeUtils

internal class FetchInboxMessagesRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : FetchInboxMessagesRepository {

    override fun fetchInboxMessages(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onResponseListener: OnResponseListener
    ) {
        val url = cordialApiEndpoints.getInboxMessagesUrl()
        val sdkRequest = SDKRequest(null, url, RequestMethod.GET)
        sdkRequest.queryParams = getQueryParams(pageRequest, inboxFilterParams)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getQueryParams(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?
    ): Map<String, String> {
        val queryParams = mutableMapOf(
            C.PAGE to pageRequest.page.toString(),
            C.PER_PAGE to pageRequest.size.toString()
        )
        inboxFilterParams?.let { params ->
            params.isRead?.let { isRead ->
                queryParams[C.FILTER_IS_READ] = isRead.toString()
            }
            params.dateAfter?.let { dateAfter ->
                val timestamp = TimeUtils.getTimestamp(dateAfter, isMillis = true)
                queryParams[C.FILTER_AFTER_DATE] = timestamp
            }
            params.dateBefore?.let { dateBefore ->
                val timestamp = TimeUtils.getTimestamp(dateBefore, isMillis = true)
                queryParams[C.FILTER_BEFORE_DATE] = timestamp
            }
        }
        return queryParams
    }
}