package com.cordial.feature.inboxmessage

interface CordialInboxMessageListener {
    fun newInboxMessageDelivered(mcID: String)
}