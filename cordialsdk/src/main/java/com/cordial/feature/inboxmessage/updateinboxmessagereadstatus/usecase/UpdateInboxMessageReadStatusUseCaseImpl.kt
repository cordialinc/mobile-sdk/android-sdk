package com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.usecase

import com.cordial.api.C
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.repository.UpdateInboxMessageReadStatusRepository
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.inboxmessage.updateinboxmessagereadstatus.UpdateInboxMessageReadStatusDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import org.json.JSONException
import org.json.JSONObject

internal class UpdateInboxMessageReadStatusUseCaseImpl(
    private val updateInboxMessageReadStatusRepository: UpdateInboxMessageReadStatusRepository,
    private val preferences: Preferences,
    private val updateInboxMessageReadStatusDao: UpdateInboxMessageReadStatusDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : UpdateInboxMessageReadStatusUseCase, CordialCheck() {

    override fun updateInboxMessageReadStatus(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        checkAuth(updateInboxMessageReadStatusRequest, onRequestFromDBListener)
        doAfterCheck {
            updateInboxMessageReadStatusRepository.updateInboxMessageReadStatus(updateInboxMessageReadStatusRequest,
                object : OnResponseListener {
                    override fun onSuccess(response: String) {
                        onRequestFromDBListener?.onSuccess()
                    }

                    override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                        val errorMessage = "Failed to update inbox message read status due to the error: $error, " +
                                "saving update inbox message read status request to the cache"
                        handleSystemError(
                            onRequestFromDBListener,
                            updateInboxMessageReadStatusRequest,
                            errorMessage,
                            onSaveRequestListener
                        )
                    }

                    override fun onLogicError(response: String) {
                        handleLogicError(response, updateInboxMessageReadStatusRequest, onRequestFromDBListener)
                    }
                }
            )
        }
    }

    private fun checkAuth(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    updateInboxMessageReadStatusRequest,
                    reason = "Failed to update inbox message read status because the contact is not registered, " +
                            "saving update inbox message read status request to the cache",
                    onSaveRequestListener = null
                )
            })
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    updateInboxMessageReadStatusRequest,
                    reason = "Failed to update inbox message read status because the jwt is absent, " +
                            "saving update inbox message read status request to the cache"
                ) {
                    sdkSecurityUseCase.updateJWT()
                }
            })
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    updateInboxMessageReadStatusRequest,
                    reason = "Failed to update inbox message read status because there is no network connection, " +
                            "saving update inbox message read status request to the cache",
                    onSaveRequestListener = null
                )
            })
        networkAvailableCheck.execute()
    }


    private fun handleSystemError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        CordialLoggerManager.info(reason)
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            saveUpdateInboxMessageReadStatusToDB(updateInboxMessageReadStatusRequest, onSaveRequestListener)
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    private fun saveUpdateInboxMessageReadStatusToDB(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onSaveRequestListener: (() -> Unit)?
    ) {
        updateInboxMessageReadStatusDao?.insert(updateInboxMessageReadStatusRequest, onSaveRequestListener)
    }

    override fun sendCachedUpdateInboxMessageReadStatus(isAnonymousContact: Boolean) {
        if (!SendingCacheState.updatingInboxMessageReadStatus.compareAndSet(false, true))
            return
        updateInboxMessageReadStatusDao?.getUpdateInboxMessageReadStatusRequests(isAnonymousContact) { requests ->
            if (requests.isEmpty()) {
                SendingCacheState.updatingInboxMessageReadStatus.set(false)
                if (isAnonymousContact) sendCachedUpdateInboxMessageReadStatus(false)
                return@getUpdateInboxMessageReadStatusRequests
            }
            val (requestIds, updateInboxMessageReadStatusRequest) = collectUpdateInboxMessageRequests(requests)
            updateInboxMessageReadStatusFromCache(updateInboxMessageReadStatusRequest, requestIds, isAnonymousContact)
        }
    }

    private fun collectUpdateInboxMessageRequests(
        requests: List<UpdateInboxMessageReadStatusRequest>
    ): Pair<MutableList<Int>, UpdateInboxMessageReadStatusRequest> {
        val requestIds = mutableListOf<Int>()
        val deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
        var primaryKey = ""
        val markAsReadIds = mutableListOf<String>()
        val markAsUnreadIds = mutableListOf<String>()
        requests.forEach { request ->
            request.id?.let { requestId ->
                requestIds.add(requestId)
            }
            primaryKey = request.primaryKey
            request.readInboxMessagesIds?.forEach { mcID ->
                if (!markAsReadIds.contains(mcID))
                    markAsReadIds.add(mcID)
                if (markAsUnreadIds.contains(mcID))
                    markAsUnreadIds.remove(mcID)
            }
            request.unreadInboxMessagesIds?.forEach { mcID ->
                if (!markAsUnreadIds.contains(mcID))
                    markAsUnreadIds.add(mcID)
                if (markAsReadIds.contains(mcID))
                    markAsReadIds.remove(mcID)
            }
        }
        val updateInboxMessageReadStatusRequest =
            UpdateInboxMessageReadStatusRequest(
                primaryKey, deviceId, markAsReadIds, markAsUnreadIds
            )
        return Pair(requestIds, updateInboxMessageReadStatusRequest)
    }

    private fun updateInboxMessageReadStatusFromCache(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        requestIds: MutableList<Int>,
        isAnonymousContact: Boolean
    ) {
        updateInboxMessageReadStatus(updateInboxMessageReadStatusRequest, object : OnRequestFromDBListener {
            override fun onSuccess() {
                updateInboxMessageReadStatusDao?.deleteUpdateInboxMessageReadStatusRequests(requestIds) {
                    CordialLoggerManager.info("Deleting sent inbox message read status requests from the cache")
                    SendingCacheState.updatingInboxMessageReadStatus.set(false)
                    if (isAnonymousContact) sendCachedUpdateInboxMessageReadStatus(false)
                }
            }

            override fun onFailure() {
                SendingCacheState.updatingInboxMessageReadStatus.set(false)
                if (isAnonymousContact) sendCachedUpdateInboxMessageReadStatus(false)
            }
        }
        )
    }

    private fun handleLogicError(
        response: String,
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        try {
            val error = JSONObject(response).getJSONObject("error")
            val errorCode = error.getInt("code")
            if (errorCode == 422) {
                val errors = error.getJSONObject("errors")
                val failedReadMcIds = mutableSetOf<Int>()
                val failedUnreadMcIds = mutableSetOf<Int>()
                errors.keys().forEach { errorKey ->
                    errorKey?.let { key ->
                        val id = key.replace("[^0-9]".toRegex(), "").toInt()
                        when {
                            key.contains(C.MARK_AS_READ_IDS) -> {
                                failedReadMcIds.add(id)
                            }
                            key.contains(C.MARK_AS_UNREAD_IDS) -> {
                                failedUnreadMcIds.add(id)
                            }
                            else -> {
                            }
                        }
                    }
                }
                updateInboxMessageReadStatusRequest.readInboxMessagesIds =
                    updateInboxMessageReadStatusRequest.readInboxMessagesIds?.filterIndexed { index, _ ->
                        !failedReadMcIds.contains(index)
                    }
                updateInboxMessageReadStatusRequest.unreadInboxMessagesIds =
                    updateInboxMessageReadStatusRequest.unreadInboxMessagesIds?.filterIndexed { index, _ ->
                        !failedUnreadMcIds.contains(index)
                    }
                if (updateInboxMessageReadStatusRequest.readInboxMessagesIds.isNullOrEmpty() &&
                    updateInboxMessageReadStatusRequest.unreadInboxMessagesIds.isNullOrEmpty()
                ) {
                    onRequestFromDBListener?.onSuccess()
                    return
                }
                val failedUnreadMcIDsString = failedUnreadMcIds.joinToString(" ")
                CordialLoggerManager.error("Inbox message update read status requests failed ($failedUnreadMcIDsString). Valid requests will be sent"
                )
                updateInboxMessageReadStatus(
                    updateInboxMessageReadStatusRequest,
                    onRequestFromDBListener
                )
            } else {
                CordialLoggerManager.error("Sending mark inbox message read status requests was failed due to the error: $response"
                )
                onRequestFromDBListener?.onFailure()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        }
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        updateInboxMessageReadStatusDao?.clear(onCompleteListener)
    }
}
