package com.cordial.feature.inboxmessage.getinboxmessage.usecase

import com.cordial.api.C
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.getinboxmessage.repository.FetchInboxMessageRepository
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDao
import org.json.JSONObject

internal class FetchInboxMessageUseCaseImpl(
    private val fetchInboxMessageRepository: FetchInboxMessageRepository,
    private val inboxMessageDao: InboxMessageDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : FetchInboxMessageUseCase, CordialCheck() {

    override fun fetchInboxMessage(
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        checkAuth(mcID, onSuccess, onFailure)
        doAfterCheck {
            fetchInboxMessageRepository.fetchInboxMessage(mcID, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    val inboxMessage = convertJsonToInboxMessage(mcID, response)
                    if (inboxMessage != null) {
                        saveInboxMessageToDB(inboxMessage)
                        onSuccess.invoke(inboxMessage)
                    } else {
                        onFailure.invoke("No inbox message found with $mcID")
                    }
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleSystemError(error, mcID, onSuccess, onFailure)
                }

                override fun onLogicError(response: String) {
                    handleLogicError(response, onFailure)
                }
            })
        }
    }

    private fun checkAuth(
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    error = C.FETCH_INBOX_MESSAGE_CONTACT_IS_NOT_REGISTERED_ERROR,
                    mcID,
                    onSuccess,
                    onFailure
                )
            })
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = { updateJWT(mcID, onSuccess, onFailure) }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(error = C.NO_NETWORK_ERROR, mcID, onSuccess, onFailure)
            })
        networkAvailableCheck.execute()
    }

    private fun convertJsonToInboxMessage(mcID: String, json: String): InboxMessage? {
        var inboxMessage: InboxMessage? = null
        val messageObject = JSONObject(json).optJSONObject("message")
        messageObject?.let {
            val read = messageObject.optBoolean("read", false)
            val sentAt = messageObject.optString("sentAt")
            val url = messageObject.optString("url")
            val urlExpireAt = messageObject.optString("urlExpireAt")
            val previewData = messageObject.optString("metadata")
            inboxMessage = InboxMessage(mcID, read, sentAt, previewData)
            inboxMessage?.url = url
            inboxMessage?.urlExpireAt = urlExpireAt
        }
        return inboxMessage
    }

    private fun saveInboxMessageToDB(inboxMessage: InboxMessage) {
        inboxMessageDao?.insert(inboxMessage)
    }

    private fun handleSystemError(
        error: String,
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        if (error == C.JWT_TOKEN_EXPIRED) {
            updateJWT(mcID, onSuccess, onFailure)
        }
        onFailure.invoke(error)
    }

    private fun updateJWT(
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        sdkSecurityUseCase.updateJWT {
            fetchInboxMessage(mcID, onSuccess, onFailure)
        }
    }

    private fun handleLogicError(
        error: String,
        onFailure: (error: String) -> Unit
    ) {
        onFailure.invoke(error)
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        inboxMessageDao?.clear(onCompleteListener)
    }

    override fun getCachedInboxMessage(mcID: String, onInboxMessageListener: ((inboxMessage: InboxMessage?) -> Unit)?) {
        inboxMessageDao?.getInboxMessage(mcID, onInboxMessageListener)
    }

    override fun getCachedInboxMessages(onInboxMessageListener: ((inboxMessages: List<InboxMessage>) -> Unit)?) {
        inboxMessageDao?.getInboxMessages(onInboxMessageListener)
    }
}