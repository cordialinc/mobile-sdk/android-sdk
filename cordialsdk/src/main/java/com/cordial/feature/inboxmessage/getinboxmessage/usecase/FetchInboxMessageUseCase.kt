package com.cordial.feature.inboxmessage.getinboxmessage.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.inboxmessage.InboxMessage

internal interface FetchInboxMessageUseCase : CacheableUseCase {
    fun fetchInboxMessage(
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    )

    fun getCachedInboxMessage(mcID: String, onInboxMessageListener: ((inboxMessage: InboxMessage?) -> Unit)?)

    fun getCachedInboxMessages(onInboxMessageListener: ((inboxMessages: List<InboxMessage>) -> Unit)?)

}