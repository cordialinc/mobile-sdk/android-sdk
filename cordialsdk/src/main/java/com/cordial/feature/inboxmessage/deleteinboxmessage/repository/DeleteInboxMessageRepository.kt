package com.cordial.feature.inboxmessage.deleteinboxmessage.repository

import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.network.response.OnResponseListener

internal interface DeleteInboxMessageRepository {
    fun deleteInboxMessage(deleteInboxMessageRequest: DeleteInboxMessageRequest, onResponseListener: OnResponseListener)
}