package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model

internal class InboxMessageContentMetadata(
    val mcID: String,
    val cacheSize: Int
)