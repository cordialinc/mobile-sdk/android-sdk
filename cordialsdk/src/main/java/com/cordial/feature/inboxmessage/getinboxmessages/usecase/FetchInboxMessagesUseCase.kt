package com.cordial.feature.inboxmessage.getinboxmessages.usecase

import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.network.request.Page
import com.cordial.network.request.PageRequest

internal interface FetchInboxMessagesUseCase {
    fun fetchInboxMessages(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    )
}