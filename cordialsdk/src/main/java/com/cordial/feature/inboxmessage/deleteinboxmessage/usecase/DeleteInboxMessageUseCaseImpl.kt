package com.cordial.feature.inboxmessage.deleteinboxmessage.usecase

import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.feature.inboxmessage.deleteinboxmessage.repository.DeleteInboxMessageRepository
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.inboxmessage.deleteinboxmessagee.DeleteInboxMessageDao
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDao
import com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent.InboxMessageContentDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences

internal class DeleteInboxMessageUseCaseImpl(
    private val deleteInboxMessageRepository: DeleteInboxMessageRepository,
    private val preferences: Preferences,
    private val deleteInboxMessageDao: DeleteInboxMessageDao?,
    private val inboxMessageDao: InboxMessageDao?,
    private val inboxMessageContentDao: InboxMessageContentDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : DeleteInboxMessageUseCase, CordialCheck() {

    override fun deleteInboxMessage(
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        checkAuth(deleteInboxMessageRequest, onRequestFromDBListener)
        doAfterCheck {
            deleteInboxMessageRepository.deleteInboxMessage(deleteInboxMessageRequest, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    onRequestFromDBListener?.onSuccess()
                    deleteInboxMessageFromCache(deleteInboxMessageRequest.mcID)
                }

                override fun onSystemError(
                    error: String,
                    onSaveRequestListener: (() -> Unit)?
                ) {
                    handleSystemError(onRequestFromDBListener, deleteInboxMessageRequest, error, onSaveRequestListener)
                }

                override fun onLogicError(response: String) {
                    onRequestFromDBListener?.onFailure()
                    CordialLoggerManager.error("Cannot delete inbox message due to the error: $response")
                }
            })
        }
    }

    private fun checkAuth(
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    deleteInboxMessageRequest,
                    reason = "Failed to delete inbox message because the contact is not registered, saving delete inbox message request to the cache",
                    onSaveRequestListener = null
                )
            })
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    deleteInboxMessageRequest,
                    reason = "Failed to delete inbox message because the jwt is absent, saving delete inbox message request to the cache"
                ) {
                    sdkSecurityUseCase.updateJWT()
                }
            })
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    deleteInboxMessageRequest,
                    reason = "Failed to delete inbox message because there is no network connection, saving delete inbox message request to the cache",
                    onSaveRequestListener = null
                )
            })
        networkAvailableCheck.execute()
    }

    private fun deleteInboxMessageFromCache(mcID: String) {
        inboxMessageDao?.deleteInboxMessage(mcID, null)
        inboxMessageContentDao?.deleteInboxMessageContent(mcID, null)
    }

    private fun handleSystemError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        CordialLoggerManager.info(reason)
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            saveDeleteInboxMessageToDB(deleteInboxMessageRequest, onSaveRequestListener)
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    private fun saveDeleteInboxMessageToDB(
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        onSaveRequestListener: (() -> Unit)?
    ) {
        deleteInboxMessageDao?.insert(deleteInboxMessageRequest, onSaveRequestListener)
    }

    override fun sendCachedDeleteInboxMessage() {
        if (!SendingCacheState.deletingInboxMessage.compareAndSet(false, true))
            return
        deleteInboxMessageDao?.getDeleteInboxMessageRequest { deleteInboxMessageRequest, requestId ->
            val isDeleteInboxMessageCacheEmptyCheck = Check(
                checkFunction = { deleteInboxMessageRequest != null },
                doOnSuccess = { setDeviceIdAndDeleteInboxMessage(deleteInboxMessageRequest, requestId) },
                doOnError = { SendingCacheState.deletingInboxMessage.set(false) }
            )
            isDeleteInboxMessageCacheEmptyCheck.execute()
        }
    }

    private fun setDeviceIdAndDeleteInboxMessage(
        deleteInboxMessageRequest: DeleteInboxMessageRequest?,
        requestId: Int
    ) {
        deleteInboxMessageRequest?.let {
            val deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
            deleteInboxMessageRequest.deviceId = deviceId
            deleteInboxMessageFromCache(deleteInboxMessageRequest, requestId)
        }
    }

    private fun deleteInboxMessageFromCache(deleteInboxMessageRequest: DeleteInboxMessageRequest, requestId: Int) {
        deleteInboxMessage(deleteInboxMessageRequest, object : OnRequestFromDBListener {
            override fun onSuccess() {
                SendingCacheState.deletingInboxMessage.set(false)
                CordialLoggerManager.info(
                    "Removing delete inbox message request from the cache with mcID = " +
                            deleteInboxMessageRequest.mcID
                )
                deleteInboxMessageDao?.deleteDeleteInboxMessageRequest(requestId) {
                    sendCachedDeleteInboxMessage()
                }
            }

            override fun onFailure() {
                SendingCacheState.deletingInboxMessage.set(false)
            }
        })
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        deleteInboxMessageDao?.clear(onCompleteListener)
    }
}