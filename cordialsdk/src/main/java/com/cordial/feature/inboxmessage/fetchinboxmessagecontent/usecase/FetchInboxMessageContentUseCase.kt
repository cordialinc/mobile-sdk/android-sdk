package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model.InboxMessageContentMetadata

internal interface FetchInboxMessageContentUseCase : CacheableUseCase {
    fun checkCacheAndFetchInboxMessageContent(
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    )

    fun getCachedInboxMessageContent(
        mcID: String,
        onInboxMessageContentListener: ((inboxMessageContent: String?) -> Unit)?
    )

    fun getInboxMessageContentsMetadata(
        onInboxMessageContentMetadataListener: ((metadataList: List<InboxMessageContentMetadata>?) -> Unit)?
    )
}