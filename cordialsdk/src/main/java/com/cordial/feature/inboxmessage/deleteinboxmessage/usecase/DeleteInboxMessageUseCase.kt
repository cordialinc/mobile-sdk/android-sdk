package com.cordial.feature.inboxmessage.deleteinboxmessage.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.storage.db.OnRequestFromDBListener

internal interface DeleteInboxMessageUseCase : CacheableUseCase {
    fun deleteInboxMessage(
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    )

    fun sendCachedDeleteInboxMessage()
}