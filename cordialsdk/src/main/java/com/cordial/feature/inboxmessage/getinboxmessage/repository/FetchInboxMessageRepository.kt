package com.cordial.feature.inboxmessage.getinboxmessage.repository

import com.cordial.network.response.OnResponseListener

internal interface FetchInboxMessageRepository {
    fun fetchInboxMessage(mcID: String, onResponseListener: OnResponseListener)
}