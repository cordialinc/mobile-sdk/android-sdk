package com.cordial.feature.inboxmessage.getinboxmessages.usecase

import com.cordial.api.C
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.feature.inboxmessage.getinboxmessages.repository.FetchInboxMessagesRepository
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.network.request.Page
import com.cordial.network.request.PageRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDao
import org.json.JSONObject

internal class FetchInboxMessagesUseCaseImpl(
    private val fetchInboxMessagesRepository: FetchInboxMessagesRepository,
    private val inboxMessageDao: InboxMessageDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : FetchInboxMessagesUseCase, CordialCheck() {

    override fun fetchInboxMessages(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        checkAuth(pageRequest, inboxFilterParams, onSuccess, onFailure)
        doAfterCheck {
            fetchInboxMessagesRepository.fetchInboxMessages(
                pageRequest,
                inboxFilterParams,
                object : OnResponseListener {
                    override fun onSuccess(response: String) {
                        val inboxMessages = convertJsonToInboxMessages(response)
                        saveInboxMessagesToDB(inboxMessages)
                        onSuccess.invoke(inboxMessages)
                    }

                    override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                        handleSystemError(error, pageRequest, inboxFilterParams, onSuccess, onFailure)
                    }

                    override fun onLogicError(response: String) {
                        handleLogicError(response, onFailure)
                    }
                })
        }
    }

    private fun checkAuth(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    error = C.FETCH_INBOX_CONTACT_IS_NOT_REGISTERED_ERROR,
                    pageRequest,
                    inboxFilterParams,
                    onSuccess,
                    onFailure
                )
            })
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = { updateJWT(pageRequest, inboxFilterParams, onSuccess, onFailure) }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(error = C.NO_NETWORK_ERROR, pageRequest, inboxFilterParams, onSuccess, onFailure)
            })
        networkAvailableCheck.execute()
    }

    private fun convertJsonToInboxMessages(json: String): Page<InboxMessage> {
        val inboxMessages = mutableListOf<InboxMessage>()
        val jsonObject = JSONObject(json)
        val messagesArray = jsonObject.optJSONArray("messages")
        messagesArray?.let {
            for (i in 0 until messagesArray.length()) {
                val messageObject = messagesArray.optJSONObject(i)
                val mcID = messageObject.optString("_id")
                val read = messageObject.optBoolean("read", false)
                val sentAt = messageObject.optString("sentAt")
                val url = messageObject.optString("url")
                val urlExpireAt = messageObject.optString("urlExpireAt")
                val previewData = messageObject.optString("metadata")
                val inboxMessage = InboxMessage(mcID, read, sentAt, previewData)
                inboxMessage.url = url
                inboxMessage.urlExpireAt = urlExpireAt
                inboxMessages.add(inboxMessage)
            }
        }
        val currentPage = jsonObject.optInt("currentPage")
        val lastPage = jsonObject.optInt("lastPage")
        val total = jsonObject.optInt("total")
        val perPage = jsonObject.optInt("perPage")
        return Page(content = inboxMessages, current = currentPage, last = lastPage, size = perPage, total = total)
    }

    private fun saveInboxMessagesToDB(inboxMessages: Page<InboxMessage>, onSaveRequestListener: (() -> Unit)? = null) {
        inboxMessageDao?.insert(inboxMessages.content, onSaveRequestListener)
    }

    private fun handleSystemError(
        error: String,
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        if (error == C.JWT_TOKEN_EXPIRED) {
            updateJWT(pageRequest, inboxFilterParams, onSuccess, onFailure)
        }
        onFailure.invoke(error)
    }

    private fun updateJWT(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        sdkSecurityUseCase.updateJWT {
            fetchInboxMessages(pageRequest, inboxFilterParams, onSuccess, onFailure)
        }
    }

    private fun handleLogicError(error: String, onFailure: (error: String) -> Unit) {
        onFailure.invoke(error)
    }
}