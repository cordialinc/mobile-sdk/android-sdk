package com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.repository

import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.network.response.OnResponseListener

internal interface UpdateInboxMessageReadStatusRepository {
    fun updateInboxMessageReadStatus(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onResponseListener: OnResponseListener
    )
}