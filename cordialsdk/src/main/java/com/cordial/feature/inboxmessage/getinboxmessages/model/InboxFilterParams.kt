package com.cordial.feature.inboxmessage.getinboxmessages.model

import java.util.*

class InboxFilterParams @JvmOverloads constructor(
    val isRead: Boolean? = null,
    val dateBefore: Date? = null,
    val dateAfter: Date? = null
)