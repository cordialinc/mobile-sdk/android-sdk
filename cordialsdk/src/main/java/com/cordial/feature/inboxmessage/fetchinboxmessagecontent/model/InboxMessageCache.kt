package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model

import com.cordial.api.C

/**
 * Configuration for inbox message cache
 *
 * @property maxCacheSize maximum cache size in bytes
 * @property maxCacheableMessageSize maximum cache size of single inbox message in bytes
 */
class InboxMessageCache {
    var maxCacheSize: Int = C.DEFAULT_MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE
    var maxCacheableMessageSize: Int = C.DEFAULT_MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
        get() {
            if (field > maxCacheSize) {
                field = maxCacheSize
            }
            return field
        }
}