package com.cordial.feature.inboxmessage

class InboxMessage(
    val mcID: String,
    val isRead: Boolean,
    val sentAt: String,
    val previewData: String?
) {
    internal var url: String? = null
    internal var urlExpireAt: String? = null
}