package com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model

internal class UpdateInboxMessageReadStatusRequest(
    val primaryKey: String,
    var deviceId: String,
    var readInboxMessagesIds: List<String>?,
    var unreadInboxMessagesIds: List<String>?
) {
    var id: Int? = null
}