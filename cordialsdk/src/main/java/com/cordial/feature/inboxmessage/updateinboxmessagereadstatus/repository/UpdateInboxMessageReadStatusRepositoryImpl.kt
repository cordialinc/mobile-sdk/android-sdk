package com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.repository

import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import org.json.JSONArray
import org.json.JSONObject

internal class UpdateInboxMessageReadStatusRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : UpdateInboxMessageReadStatusRepository {

    override fun updateInboxMessageReadStatus(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onResponseListener: OnResponseListener
    ) {
        val url = cordialApiEndpoints.getUpdateInboxMessageReadStatusUrl()
        val requestJson = getUpdateInboxMessageReadStatusRequestJSON(updateInboxMessageReadStatusRequest)
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getUpdateInboxMessageReadStatusRequestJSON(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest
    ): String {
        val param = JSONObject()
        param.put(C.DEVICE_ID, updateInboxMessageReadStatusRequest.deviceId)
        if (updateInboxMessageReadStatusRequest.primaryKey.trim().isNotEmpty()) {
            param.put(C.PRIMARY_KEY, updateInboxMessageReadStatusRequest.primaryKey)
        } else {
            val config = CordialApiConfiguration.getInstance()
            val channelKey = config.getChannelKey()
            val cordialApi = CordialApi()
            val pushToken = cordialApi.getFirebaseToken()
            val anonymousPrimaryKey = "$channelKey:$pushToken"
            param.put(C.PRIMARY_KEY, anonymousPrimaryKey)
        }
        updateInboxMessageReadStatusRequest.readInboxMessagesIds?.let { ids ->
            val markAsReadIds = JSONArray()
            ids.forEach { id ->
                markAsReadIds.put(id)
            }
            param.put(C.MARK_AS_READ_IDS, markAsReadIds)
        }
        updateInboxMessageReadStatusRequest.unreadInboxMessagesIds?.let { ids ->
            val markAsUnreadIds = JSONArray()
            ids.forEach { id ->
                markAsUnreadIds.put(id)
            }
            param.put(C.MARK_AS_UNREAD_IDS, markAsUnreadIds)
        }
        return param.toString()
    }
}