package com.cordial.feature.inboxmessage.deleteinboxmessage.model

internal class DeleteInboxMessageRequest(
    val primaryKey: String,
    var deviceId: String,
    val mcID: String
)