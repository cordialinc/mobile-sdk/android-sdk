package com.cordial.feature.inboxmessage.getinboxmessages.repository

import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.network.request.PageRequest
import com.cordial.network.response.OnResponseListener

internal interface FetchInboxMessagesRepository {
    fun fetchInboxMessages(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams?,
        onResponseListener: OnResponseListener
    )
}