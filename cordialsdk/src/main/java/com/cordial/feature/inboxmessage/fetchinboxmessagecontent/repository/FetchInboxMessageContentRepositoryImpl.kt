package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.repository

import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class FetchInboxMessageContentRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler
) : FetchInboxMessageContentRepository {

    override fun fetchInboxMessageContent(inboxMessageUrl: String, onResponseListener: OnResponseListener) {
        val request = SDKRequest(null, inboxMessageUrl, RequestMethod.GET)
        request.isCordial = false
        requestSender.send(request, responseHandler, onResponseListener)
    }
}