package com.cordial.feature.inboxmessage.getinboxmessage.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class FetchInboxMessageRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : FetchInboxMessageRepository {

    override fun fetchInboxMessage(mcID: String, onResponseListener: OnResponseListener) {
        val url = cordialApiEndpoints.getInboxMessageUrl(mcID)
        val sdkRequest = SDKRequest(null, url, RequestMethod.GET)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }
}