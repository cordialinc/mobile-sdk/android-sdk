package com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.storage.db.OnRequestFromDBListener

internal interface UpdateInboxMessageReadStatusUseCase : CacheableUseCase {
    fun updateInboxMessageReadStatus(
        updateInboxMessageReadStatusRequest: UpdateInboxMessageReadStatusRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    )

    fun sendCachedUpdateInboxMessageReadStatus(isAnonymousContact: Boolean)
}