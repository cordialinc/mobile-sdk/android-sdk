package com.cordial.feature.inboxmessage.fetchinboxmessagecontent.repository

import com.cordial.network.response.OnResponseListener

internal interface FetchInboxMessageContentRepository {
    fun fetchInboxMessageContent(inboxMessageUrl: String, onResponseListener: OnResponseListener)
}