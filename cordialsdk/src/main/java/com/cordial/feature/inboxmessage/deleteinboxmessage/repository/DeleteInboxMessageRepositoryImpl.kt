package com.cordial.feature.inboxmessage.deleteinboxmessage.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class DeleteInboxMessageRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : DeleteInboxMessageRepository {

    override fun deleteInboxMessage(
        deleteInboxMessageRequest: DeleteInboxMessageRequest,
        onResponseListener: OnResponseListener
    ) {
        val url = cordialApiEndpoints.getDeleteInboxMessageUrl(deleteInboxMessageRequest)
        val sdkRequest = SDKRequest(null, url, RequestMethod.DELETE)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }
}