package com.cordial.feature.timestamps.repository.timestampsurl

import com.cordial.network.response.OnResponseListener

internal interface TimestampsUrlRepository {
    fun getTimestampsUrl(onResponseListener: OnResponseListener)
}