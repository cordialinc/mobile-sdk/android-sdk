package com.cordial.feature.timestamps.repository.timestampsurl

import com.cordial.api.CordialApiEndpoints
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class TimestampsUrlRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : TimestampsUrlRepository {

    override fun getTimestampsUrl(onResponseListener: OnResponseListener) {
        val url = cordialApiEndpoints.getTimestampsUrl()
        val request = SDKRequest(null, url, RequestMethod.GET)
        requestSender.send(request, responseHandler, onResponseListener)
    }
}