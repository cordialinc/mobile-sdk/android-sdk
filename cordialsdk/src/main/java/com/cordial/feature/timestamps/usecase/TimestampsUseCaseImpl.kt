package com.cordial.feature.timestamps.usecase

import com.cordial.api.C
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inappmessage.getinappmessagesdata.usecase.InAppMessageDataUseCase
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.timestamps.repository.timestamps.TimestampsRepository
import com.cordial.feature.timestamps.repository.timestampsurl.TimestampsUrlRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.TimeUtils
import com.cordial.util.TimeUtils.isNewerThan
import org.json.JSONException
import org.json.JSONObject

internal class TimestampsUseCaseImpl(
    private val preferences: Preferences,
    private val timestampsUrlRepository: TimestampsUrlRepository,
    private val timestampsRepository: TimestampsRepository,
    private val inAppMessageDataUseCase: InAppMessageDataUseCase,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : TimestampsUseCase, CordialCheck() {

    override fun updateTimestamps() {
        checkAuthAndUpdateTimestamps()
    }

    private fun checkAuthAndUpdateTimestamps() {
        val timestampsUrl = preferences.getString(PreferenceKeys.TIMESTAMPS_URL)
        val timestampsUrlExpireAt = if (preferences.getString(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT).isEmpty()) null
        else preferences.getString(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT)
        val timestampsUrlValidCheck = Check(
            checkFunction = {
                timestampsUrl.isNotEmpty() && !TimeUtils.isDateExpired(timestampsUrlExpireAt, true)
            },
            doOnSuccess = { getTimestamps(timestampsUrl) },
            doOnError = { getTimestampsUrl() }
        )
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = timestampsUrlValidCheck,
            doOnError = { updateJWT() }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
        )
        networkAvailableCheck.execute()
    }

    private fun getTimestampsUrl() {
        timestampsUrlRepository.getTimestampsUrl(object : OnResponseListener {
            override fun onSuccess(response: String) {
                handleTimestampsUrl(response)
            }

            override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                handleGetTimestampsUrlSystemError(error)
            }

            override fun onLogicError(response: String) {
                handleGetTimestampsUrlLogicError(response)
            }
        })
    }

    private fun handleTimestampsUrl(response: String) {
        val jsonObject = JSONObject(response)
        val url = jsonObject.optString(C.URL)
        val urlExpireAt = jsonObject.optString(C.URL_EXPIRE_AT)
        saveTimestampsUrl(url, urlExpireAt)
        getTimestamps(url)
    }

    private fun saveTimestampsUrl(url: String, urlExpireAt: String) {
        preferences.put(PreferenceKeys.TIMESTAMPS_URL, url)
        preferences.put(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT, urlExpireAt)
    }

    private fun getTimestamps(url: String) {
        timestampsRepository.getTimestamps(url, object : OnResponseListener {
            override fun onSuccess(response: String) {
                handleTimestamps(response)
            }

            override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                handleGetTimestampsSystemError(error)
            }

            override fun onLogicError(response: String) {
                handleGetTimestampsLogicError(response)
            }
        })
    }

    private fun handleTimestamps(response: String) {
        val jsonObject = JSONObject(response)
        val inApp = jsonObject.optString(C.IN_APP)
        if (inApp.isNotEmpty()) {
            val inAppTimestamp = TimeUtils.getTime(jsonObject.optString(C.IN_APP), true)
            getInAppMessagesDataIfNeeded(inAppTimestamp)
        }
    }

    private fun getInAppMessagesDataIfNeeded(inAppTimestamp: Long) {
        val lastInAppTimestamp = preferences.getLong(PreferenceKeys.LAST_IN_APP_TIMESTAMP)
        if (inAppTimestamp.isNewerThan(lastInAppTimestamp)) {
            inAppMessageDataUseCase.getInAppMessagesData()
        }
    }

    private fun handleGetTimestampsUrlLogicError(error: String) {
        try {
            val jsonObject = JSONObject(error)
            val errorJsonObject = jsonObject.optJSONObject("message")
            val errorCode = errorJsonObject?.optString("errorCode")
            val errorMessage = errorJsonObject?.optString("message")
            if (errorCode != C.TIMESTAMPS_NOT_FOUND_ERROR_CODE) {
                CordialLoggerManager.info("Did not fetch timestamps url: $errorMessage. Will retry later")
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    private fun handleGetTimestampsUrlSystemError(error: String) {
        if (error == C.JWT_TOKEN_EXPIRED) {
            CordialLoggerManager.info("Did not fetch timestamps url: $error. Will retry later")
            updateJWT()
        }
    }

    private fun handleGetTimestampsSystemError(error: String) {
        CordialLoggerManager.info("Failed to fetch timestamps due to the error: $error")
    }

    private fun handleGetTimestampsLogicError(error: String) {
        if (error == C.PROVIDED_TOKEN_EXPIRED) {
            preferences.remove(PreferenceKeys.TIMESTAMPS_URL)
            preferences.remove(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT)
            updateTimestamps()
            CordialLoggerManager.info("Did not fetch timestamps url due to url expiration. Will retry later")
        } else {
            CordialLoggerManager.error("Failed to fetch timestamps due to the error: $error")
        }
    }

    private fun updateJWT() {
        sdkSecurityUseCase.updateJWT {
            updateTimestamps()
        }
    }
}