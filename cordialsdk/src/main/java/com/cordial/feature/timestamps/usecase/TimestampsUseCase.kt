package com.cordial.feature.timestamps.usecase

internal interface TimestampsUseCase {
    fun updateTimestamps()
}