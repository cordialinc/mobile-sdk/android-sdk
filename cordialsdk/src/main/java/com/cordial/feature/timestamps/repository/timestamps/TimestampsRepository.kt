package com.cordial.feature.timestamps.repository.timestamps

import com.cordial.network.response.OnResponseListener

internal interface TimestampsRepository {
    fun getTimestamps(url: String, onResponseListener: OnResponseListener)
}