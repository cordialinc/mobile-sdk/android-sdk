package com.cordial.feature.timestamps.repository.timestamps

import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class TimestampsRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler
) : TimestampsRepository {

    override fun getTimestamps(url: String, onResponseListener: OnResponseListener) {
        val request = SDKRequest(null, url, RequestMethod.GET)
        request.isCordial = false
        requestSender.send(request, responseHandler, onResponseListener)
    }
}