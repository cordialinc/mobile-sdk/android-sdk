package com.cordial.feature.sendevent.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.sendevent.model.EventCacheSendingReason
import com.cordial.feature.sendevent.model.EventRequest

internal interface SendEventUseCase : CacheableUseCase {
    fun sendEvent(eventRequest: EventRequest)
    fun sendEventBoxIfBulkReached()
    fun sendCachedEvents(reason: EventCacheSendingReason)
    fun getAllCachedEvents(onCachedEventsListener: (cachedEvents: List<EventRequest>) -> Unit)
    fun updateEventLimit(newQty: Int)
    fun getEventsCount(onEventsCount: (count: Long) -> Unit)
}