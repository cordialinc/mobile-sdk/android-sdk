package com.cordial.feature.sendevent.model.property

import com.cordial.api.CordialApi


object PropertyValueUtil {

    fun getMergedSystemEventProperties(properties: Map<String, PropertyValue>) : Map<String, PropertyValue> {
        val systemEventProperties = CordialApi().getSystemEventProperties() ?: return properties
        val mergedProperties = HashMap<String, PropertyValue>()
        mergedProperties.putAll(systemEventProperties)
        mergedProperties.putAll(properties)
        return mergedProperties.toMap()
    }
}