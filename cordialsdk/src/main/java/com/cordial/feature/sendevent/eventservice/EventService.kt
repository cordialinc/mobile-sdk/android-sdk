package com.cordial.feature.sendevent.eventservice

import android.app.Service
import android.content.Intent
import android.os.IBinder

class EventService: Service() {
    private var binder: EventBinder? = null

    override fun onCreate() {
        super.onCreate()
        binder = EventBinder()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }
}