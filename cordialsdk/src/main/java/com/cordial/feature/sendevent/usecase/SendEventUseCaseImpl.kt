package com.cordial.feature.sendevent.usecase

import android.content.Intent
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.sendevent.model.EventCacheSendingReason
import com.cordial.feature.sendevent.model.EventRequest
import com.cordial.feature.sendevent.repository.SendEventRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.event.EventDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

internal class SendEventUseCaseImpl(
    private val sendEventRepository: SendEventRepository,
    private val preferences: Preferences,
    private val eventDao: EventDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : SendEventUseCase, CordialCheck() {

    private val config = CordialApiConfiguration.getInstance()

    override fun sendEvent(eventRequest: EventRequest) {
        preferences.put(PreferenceKeys.IS_CACHED_EVENTS_EMPTY, false)
        saveEventToDB(eventRequest) {
            sendEventBoxIfBulkReached()
        }
    }

    override fun sendEventBoxIfBulkReached() {
        eventDao?.getEventsCount { count ->
            if (count >= config.eventsBulkSize) {
                sendCachedEvents(EventCacheSendingReason.EVENTS_BULK_SIZE_REACHED)
            }
        }
    }

    override fun sendCachedEvents(reason: EventCacheSendingReason) {
        checkAuthAndIsEventsNotSending(reason)
        doAfterCheck {
            getAllCachedEvents { cachedEvents ->
                if (cachedEvents.isEmpty()) {
                    SendingCacheState.sendingEvents.set(false)
                    return@getAllCachedEvents
                }
                cachedEvents.forEach {
                    it.deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
                    it.primaryKey = preferences.getString(PreferenceKeys.PRIMARY_KEY)
                }
                sendCachedEvents(cachedEvents, reason)
            }
        }
    }

    private fun checkAuthAndIsEventsNotSending(reason: EventCacheSendingReason) {
        val isEventBoxNotSendingFromCacheCheck = Check(
            checkFunction = { SendingCacheState.sendingEvents.compareAndSet(false, true) },
            doOnSuccess = { isChecked = true },
            doOnError = { CordialLoggerManager.info("Cancelled ${reason.reason}, events are already sending") }
        )
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = isEventBoxNotSendingFromCacheCheck,
            doOnError = {
                handleSystemError("Failed to send the events because the jwt is absent", null) {
                    sdkSecurityUseCase.updateJWT()
                }
            }
        )
        val isLoggedInCheck = Check(
            checkFunction = { isLoggedIn() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = { CordialLoggerManager.info("Failed to send events because the contact is not registered") }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = isLoggedInCheck,
            doOnError = { CordialLoggerManager.info("Failed to send the events because there is no network connection") }
        )
        val isCachedEventsEmptyCheck = Check(
            checkFunction = { !preferences.getBoolean(PreferenceKeys.IS_CACHED_EVENTS_EMPTY, false) },
            nextCheck = networkAvailableCheck
        )
        isCachedEventsEmptyCheck.execute()
    }

    private fun sendCachedEvents(cachedEvents: List<EventRequest>, reason: EventCacheSendingReason) {
        sendEvents(cachedEvents.toMutableList(), reason, object : OnRequestFromDBListener {
            override fun onSuccess() {
                eventDao?.deleteEvents(cachedEvents) {
                    CordialLoggerManager.info("Events sent, sent events deleted")
                    updateFieldIfCachedEventsAreEmpty()
                    SendingCacheState.sendingEvents.set(false)
                }
                config.restartEventBoxSendingTimer()
            }

            override fun onFailure() {
                SendingCacheState.sendingEvents.set(false)
            }
        })
    }

    override fun getAllCachedEvents(onCachedEventsListener: (cachedEvents: List<EventRequest>) -> Unit) {
        eventDao?.getAllEvents(onCachedEventsListener)
    }

    private fun sendEvents(
        eventRequests: MutableList<EventRequest>,
        reason: EventCacheSendingReason,
        onRequestFromDBListener: OnRequestFromDBListener
    ) {
        CordialLoggerManager.info(reason.reason)
        sendEventRepository.sendEvent(eventRequests, object : OnResponseListener {
            override fun onSuccess(response: String) {
                onRequestFromDBListener.onSuccess()
            }

            override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                handleSystemError(
                    reason = "Failed to send events due to the error: $error",
                    onRequestFromDBListener,
                    onSaveRequestListener
                )
            }

            override fun onLogicError(response: String) {
                handleLogicError(response, eventRequests, reason, onRequestFromDBListener)
            }
        })
    }

    private fun saveEventToDB(eventRequest: EventRequest, onFinishSaveListener: (() -> Unit)?) {
        CordialLoggerManager.info("Cache event until bulk size is reached : ${eventRequest.eventName}")
        eventDao?.insert(eventRequest, onFinishSaveListener)
    }

    private fun handleLogicError(
        response: String,
        eventRequests: MutableList<EventRequest>,
        reason: EventCacheSendingReason,
        onRequestFromDBListener: OnRequestFromDBListener
    ) {
        try {
            val error = JSONObject(response).getJSONObject("error")
            val errorCode = error.getInt("code")
            if (errorCode == 422) {
                val errors = error.getJSONObject("errors")
                val failedEvents = mutableSetOf<EventRequest>()
                errors.keys().forEach {
                    failedEvents.add(eventRequests[it.substring(0, 1).toInt()])
                }
                failedEvents.forEach {
                    eventRequests.remove(it)
                }
                deleteFailedEventsFromDB(failedEvents.toMutableList())
                notifyFailedEvents(failedEvents.toMutableList())
                if (eventRequests.size > 0)
                    sendEvents(eventRequests, reason, onRequestFromDBListener)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: NumberFormatException) {
            e.printStackTrace()
        } catch (e: IndexOutOfBoundsException) {
            e.printStackTrace()
        } finally {
            onRequestFromDBListener.onFailure()
        }
    }

    private fun handleSystemError(
        reason: String,
        onRequestFromDBListener: OnRequestFromDBListener?,
        onSaveRequestListener: (() -> Unit)?
    ) {
        CordialLoggerManager.info(reason)
        onSaveRequestListener?.invoke()
        onRequestFromDBListener?.onFailure()
    }

    private fun deleteFailedEventsFromDB(failedEvents: List<EventRequest>) {
        eventDao?.deleteEvents(failedEvents) {
            updateFieldIfCachedEventsAreEmpty()
        }
    }

    private fun updateFieldIfCachedEventsAreEmpty() {
        eventDao?.getEventsCount { count ->
            preferences.put(PreferenceKeys.IS_CACHED_EVENTS_EMPTY, count == 0L)
        }
    }

    private fun notifyFailedEvents(failedEvents: List<EventRequest>) {
        val manager = LocalBroadcastManager.getInstance(config.getContext())
        val intent = Intent(C.FAILED_EVENTS)
        intent.putExtra(C.EVENTS, failedEvents as Serializable)
        manager.sendBroadcast(intent)
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        eventDao?.clear(onCompleteListener)
    }

    override fun updateEventLimit(newQty: Int) {
        eventDao?.updateEventLimit(newQty)
    }

    override fun getEventsCount(onEventsCount: (count: Long) -> Unit) {
        eventDao?.getEventsCount(onEventsCount)
    }
}