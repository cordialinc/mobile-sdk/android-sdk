package com.cordial.feature.sendevent.model.property

import org.json.JSONArray
import org.json.JSONObject

sealed class PropertyValue(open val value: Any) {
    data class StringProperty(override val value: String) : PropertyValue(value)
    data class NumericProperty(override val value: Double) : PropertyValue(value) {
        constructor(value: Int) : this(value.toDouble())
    }

    data class BooleanProperty(override val value: Boolean) : PropertyValue(value)
    data class JSONObjectProperty(override val value: JSONObject) : PropertyValue(value)
    data class JSONArrayProperty(override val value: JSONArray) : PropertyValue(value)
}