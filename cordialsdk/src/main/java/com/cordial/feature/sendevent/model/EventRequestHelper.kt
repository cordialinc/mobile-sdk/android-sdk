package com.cordial.feature.sendevent.model

import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.util.JsonUtils
import com.cordial.util.TimeUtils
import org.json.JSONArray
import org.json.JSONObject

class EventRequestHelper {

    internal val cordialApi = CordialApi()

    fun getCustomEventJson(
        eventName: String,
        properties: Map<String, PropertyValue>?
    ): String {
        val customEventRequest = getCustomEventRequest(eventName, properties, cordialApi.getMcID())
        return getCustomEventRequestJSON(listOf(customEventRequest))
    }

    internal fun getCustomEventRequest(
        eventName: String,
        properties: Map<String, PropertyValue>?,
        mcID: String?
    ): EventRequest {
        return EventRequest(
            cordialApi.getDeviceIdentifier(),
            cordialApi.getPrimaryKey(),
            TimeUtils.getTimestamp(),
            eventName,
            cordialApi.getLongitude(),
            cordialApi.getLatitude(),
            properties,
            mcID
        )
    }

    internal fun getCustomEventRequestJSON(eventRequests: List<EventRequest>): String {
        val jsonArray = JSONArray()
        eventRequests.forEach { event ->
            val param = JSONObject()
            param.put(C.DEVICE_ID, event.deviceId)
            if (event.primaryKey.isNotEmpty()) {
                param.put(C.PRIMARY_KEY, event.primaryKey)
            }
            param.put(C.TIMESTAMP, event.timestamp)
            param.put(C.EVENT, event.eventName)
            event.longitude?.let {
                param.put(C.LON, it)
            }
            event.latitude?.let {
                param.put(C.LAT, it)
            }
            event.properties?.let { propertyList ->
                val properties = JsonUtils.getJsonFromPropertyValueMap(propertyList)
                param.put(C.PROPERTIES, properties)
            }
            event.mcID?.let { mcId ->
                param.put(C.MC_ID, mcId)
            }
            jsonArray.put(param)
        }
        return jsonArray.toString()
    }
}