package com.cordial.feature.sendevent.model

import com.cordial.feature.sendevent.model.property.PropertyValue
import java.io.Serializable

internal class EventRequest(
    var deviceId: String,
    var primaryKey: String,
    var timestamp: String,
    var eventName: String,
    var longitude: Double?,
    var latitude: Double?,
    var properties: Map<String, PropertyValue>?,
    var mcID: String?
) : Serializable {
    var id: Int = -1
}