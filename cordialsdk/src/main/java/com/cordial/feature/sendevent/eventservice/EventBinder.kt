package com.cordial.feature.sendevent.eventservice

import android.os.*
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.sendevent.model.EventCacheSendingReason

class EventBinder: Binder() {
    private var eventHandler: Handler
    private var eventSenderThread: EventSenderThread
    private lateinit var runnable: Runnable

    init {
        eventSenderThread = EventSenderThread().apply {
            start()
            val looper = looper
            eventHandler = Handler(looper)
            startTimer()
        }
    }

    fun restartTimer() {
        stopTimer()
        eventHandler = Handler(eventSenderThread.looper)
        startTimer()
    }

    fun stopTimer() {
        eventHandler.removeCallbacksAndMessages(null)
    }

    private fun startTimer() {
        val config = CordialApiConfiguration.getInstance()
        config.eventsBulkUploadInterval?.let { uploadInterval ->
            if (config.eventsBulkSize > 1 && uploadInterval >= 1) {
                val interval = uploadInterval * 1000L
                runnable = Runnable {
                    sendCachedEvents()
                    eventHandler.postDelayed(runnable, interval)
                }

                eventHandler.postDelayed(runnable, interval)
            }
        }
    }

    private fun sendCachedEvents() {
        val config = CordialApiConfiguration.getInstance()
        val injection = config.injection
        val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.sendCachedEvents(EventCacheSendingReason.EVENTS_BULK_UPLOAD_INTERVAL)
    }

    class EventSenderThread internal constructor(): HandlerThread("Background", Process.THREAD_PRIORITY_BACKGROUND)
}