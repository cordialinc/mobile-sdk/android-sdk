package com.cordial.feature.sendevent.model

internal enum class EventCacheSendingReason(val reason: String) {
    EVENTS_BULK_UPLOAD_INTERVAL("Sending cached events due to passing bulk upload interval"),
    APP_CLOSE("Sending cached events due to application closing"),
    EVENTS_BULK_SIZE_REACHED("Sending cached events due to reaching events bulk size"),
    EVENTS_FLUSH("Sending cached events due to triggering events flush"),
    DEFAULT("");

    companion object {
        fun findReason(reason: String): EventCacheSendingReason {
            return values().firstOrNull { it.reason == reason } ?: DEFAULT
        }
    }
}