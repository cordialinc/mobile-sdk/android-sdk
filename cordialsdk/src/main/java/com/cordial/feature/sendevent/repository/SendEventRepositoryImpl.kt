package com.cordial.feature.sendevent.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.sendevent.model.EventRequest
import com.cordial.feature.sendevent.model.EventRequestHelper
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class SendEventRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints,
    private val eventRequestHelper: EventRequestHelper
) : SendEventRepository {

    override fun sendEvent(eventRequests: List<EventRequest>, onResponseListener: OnResponseListener) {
        val requestJson = eventRequestHelper.getCustomEventRequestJSON(eventRequests)
        val url = cordialApiEndpoints.getSendEventUrl()
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }
}