package com.cordial.feature.sendevent.repository

import com.cordial.feature.sendevent.model.EventRequest
import com.cordial.network.response.OnResponseListener

internal interface SendEventRepository {
    fun sendEvent(eventRequests: List<EventRequest>, onResponseListener: OnResponseListener)
}