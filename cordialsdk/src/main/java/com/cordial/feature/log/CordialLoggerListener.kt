package com.cordial.feature.log

import com.cordial.api.C

interface CordialLoggerListener {
    fun info(message: String, timestamp: String, tag: String = C.LOG_TAG)
    fun debug(message: String, timestamp: String, tag: String = C.LOG_TAG)
    fun error(message: String, timestamp: String, tag: String = C.LOG_TAG)
    fun warning(message: String, timestamp: String, tag: String = C.LOG_TAG)
}