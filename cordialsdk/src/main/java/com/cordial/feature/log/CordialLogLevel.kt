package com.cordial.feature.log

enum class CordialLogLevel(val level: Int) {
    ALL(level = 0),
    INFO(level = 1),
    DEBUG(level = 2),
    WARNING(level = 3),
    ERROR(level = 4),
    NONE(level = 5)
}