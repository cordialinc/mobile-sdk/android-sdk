package com.cordial.feature.log

import android.util.Log

class CordialLogger: CordialLoggerListener {

    override fun info(message: String, timestamp: String, tag: String) {
        Log.i(tag, message)
    }

    override fun debug(message: String, timestamp: String, tag: String) {
        Log.d(tag, message)
    }

    override fun error(message: String, timestamp: String, tag: String) {
        Log.e(tag, message)
    }

    override fun warning(message: String, timestamp: String, tag: String) {
        Log.w(tag, message)
    }
}