package com.cordial.feature.log

import com.cordial.api.CordialApiConfiguration
import com.cordial.util.TimeUtils

object CordialLoggerManager {

    fun info(message: String) {
        val loggers = CordialApiConfiguration.getInstance().getLoggers()
        val logLevel = CordialApiConfiguration.getInstance().logLevel

        if (logLevel.level <= CordialLogLevel.INFO.level) {
            loggers.forEach { logger ->
                val timestamp = TimeUtils.getTimestamp()
                logger.info(message, timestamp)
            }
        }
    }

    fun debug(message: String) {
        val loggers = CordialApiConfiguration.getInstance().getLoggers()
        val logLevel = CordialApiConfiguration.getInstance().logLevel

        if (logLevel <= CordialLogLevel.DEBUG) {
            loggers.forEach { logger ->
                val timestamp = TimeUtils.getTimestamp()
                logger.debug(message, timestamp)
            }
        }
    }

    fun warning(message: String) {
        val loggers = CordialApiConfiguration.getInstance().getLoggers()
        val logLevel = CordialApiConfiguration.getInstance().logLevel

        if (logLevel <= CordialLogLevel.WARNING) {
            loggers.forEach { logger ->
                val timestamp = TimeUtils.getTimestamp()
                logger.warning(message, timestamp)
            }
        }
    }

    fun error(message: String) {
        val loggers = CordialApiConfiguration.getInstance().getLoggers()
        val logLevel = CordialApiConfiguration.getInstance().logLevel

        if (logLevel <= CordialLogLevel.ERROR) {
            loggers.forEach { logger ->
                val timestamp = TimeUtils.getTimestamp()
                logger.error(message, timestamp)
            }
        }
    }
}