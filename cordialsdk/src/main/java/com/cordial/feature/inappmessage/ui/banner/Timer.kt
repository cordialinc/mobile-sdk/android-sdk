package com.cordial.feature.inappmessage.ui.banner

import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import kotlin.math.max

/**
 * Creates a new timer.
 *
 * @param remainingTimeMs The duration of the timer in milliseconds.
 */
internal abstract class Timer(var remainingTimeMs: Long) {

    private var isStarted: Boolean = false
    private var startTimeMs: Long = 0

    private var elapsedTimeMs: Long = 0

    private val handler = Handler(Looper.getMainLooper())
    private val trigger = Runnable {
        if (isStarted) {
            stop()
            onFinish()
        }
    }

    /**
     * Gets the total run time in milliseconds.
     *
     * @return The total run time in milliseconds.
     */
    val runTime: Long
        get() = if (isStarted) {
            elapsedTimeMs + SystemClock.elapsedRealtime() - startTimeMs
        } else elapsedTimeMs

    /**
     * Starts the timer.
     */
    fun start() {
        if (isStarted) {
            return
        }

        isStarted = true
        startTimeMs = SystemClock.elapsedRealtime()

        if (remainingTimeMs > 0) {
            handler.postDelayed(trigger, remainingTimeMs)
        } else {
            handler.post(trigger)
        }
    }

    /**
     * Stops the timer.
     */
    fun stop() {
        if (!isStarted) {
            return
        }

        elapsedTimeMs = SystemClock.elapsedRealtime() - startTimeMs

        isStarted = false
        handler.removeCallbacks(trigger)
        remainingTimeMs =
            max(0, remainingTimeMs - (SystemClock.elapsedRealtime() - startTimeMs))
    }

    /**
     * Called when the timer finishes.
     */
    protected abstract fun onFinish()

}