package com.cordial.feature.inappmessage.model

import android.app.Activity
import com.cordial.api.CordialApiConfiguration

class InAppMessageDelayMode {
    internal var delayType: InAppMessageDelayType = InAppMessageDelayType.ACTIVITIES
    internal var showType: InAppMessageDelayShowType = InAppMessageDelayShowType.IMMEDIATELY
    var disallowedActivities: List<Class<out Activity>> = listOf()

    fun show() {
        show(InAppMessageDelayShowType.NEXT_APP_OPEN)
    }

    fun show(type: InAppMessageDelayShowType) {
        delayType = InAppMessageDelayType.SHOW
        showType = type
        if (type == InAppMessageDelayShowType.IMMEDIATELY) {
            CordialApiConfiguration.getInstance().onInAppShowEnabled(isEnabled = true)
        }
    }

    fun delayedShow() {
        delayType = InAppMessageDelayType.DELAYED_SHOW
        CordialApiConfiguration.getInstance().onInAppShowEnabled(isEnabled = false)
    }

    fun disallowedActivities(disallowedActivities: List<Class<out Activity>>) {
        delayType = InAppMessageDelayType.ACTIVITIES
        this.disallowedActivities = disallowedActivities
    }
}