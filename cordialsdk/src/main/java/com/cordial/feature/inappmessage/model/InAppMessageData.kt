package com.cordial.feature.inappmessage.model

import java.io.Serializable

internal class InAppMessageData(
    val mcID: String,
    val html: String,
    val type: InAppMessageType,
    val margin: InAppMessageMargin,
    val expirationTime: String?,
    val timestamp: Long
) : Serializable {
    var isInAppMessageShown: Boolean = false
}