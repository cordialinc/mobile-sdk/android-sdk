package com.cordial.feature.inappmessage.model

internal enum class InAppMessageDelayType {
    SHOW,
    ACTIVITIES,
    DELAYED_SHOW
}