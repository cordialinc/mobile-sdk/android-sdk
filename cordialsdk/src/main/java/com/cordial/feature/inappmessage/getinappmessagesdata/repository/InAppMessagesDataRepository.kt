package com.cordial.feature.inappmessage.getinappmessagesdata.repository

import com.cordial.network.response.OnResponseListener

internal interface InAppMessagesDataRepository {
    fun getInAppMessagesData(onResponseListener: OnResponseListener)
}