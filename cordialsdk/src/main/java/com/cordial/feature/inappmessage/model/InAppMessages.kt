package com.cordial.feature.inappmessage.model

class InAppMessages {
    var showOnPushDismiss: Boolean = false
    var displayDelayInSeconds: Double = 0.5
}