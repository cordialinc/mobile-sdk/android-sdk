package com.cordial.feature.inappmessage.getinappmessage.repository

import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.feature.inappmessage.model.OnInAppMessageResponseListener

internal interface InAppMessageRepository {
    fun getInAppMessage(
        inAppMessageProperties: InAppMessageProperties,
        onInAppMessageResponseListener: OnInAppMessageResponseListener
    )
}