package com.cordial.feature.inappmessage.model

internal enum class InAppMessageType(val type: String) {
    MODAL("modal"),
    BANNER_UP("banner_up"),
    BANNER_BOTTOM("banner_bottom"),
    FULLSCREEN("fullscreen");

    companion object {
        fun findKey(type: String): InAppMessageType {
            return values().firstOrNull { it.type == type } ?: MODAL
        }
    }
}