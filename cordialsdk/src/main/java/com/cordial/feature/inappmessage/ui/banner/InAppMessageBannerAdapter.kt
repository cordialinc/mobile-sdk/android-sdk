package com.cordial.feature.inappmessage.ui.banner

import android.app.Activity
import android.app.Application
import android.view.View
import android.view.ViewGroup
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.inappmessage.InAppMessageProcess
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.util.ActivityUtils
import com.cordial.util.castTo
import com.cordial.util.castToAndRun
import com.cordial.util.runOnMainThread


internal class InAppMessageBannerAdapter : ActivityLifecycleListener,
    InAppMessageBannerView.OnInAppBannerViewListener {

    private var inAppMessageData: InAppMessageData? = null
    private val config = CordialApiConfiguration.getInstance()
    private val context = config.getContext()
    private val cordialApi = CordialApi()
    var bannerView: InAppMessageBannerView? = null
    private var isDismissEventSent: Boolean = false
    private var timer: Timer? = null

    fun displayBanner(inAppMessageData: InAppMessageData) {
        this.inAppMessageData = inAppMessageData
        context.applicationContext.castToAndRun<Application> {
            registerActivityLifecycleCallbacks(this@InAppMessageBannerAdapter)
        }
        display(getTopActivity())
        sendInAppMessageShownEvent(inAppMessageData)
        isDismissEventSent = false
        setupTimer()
    }

    private fun display(activity: Activity?) {
        activity?.let { lastActivity ->
            addBannerToActivity(lastActivity)
        }
    }

    private fun addBannerToActivity(activity: Activity) {
        runOnMainThread {
            inAppMessageData?.let { inAppData ->
                val view = onCreateView(inAppData)
                val container = ActivityUtils.getContainerView(activity)
                with(container?.rootView?.castTo<ViewGroup>() ?: return@runOnMainThread) {
                    view?.let {
                        addView(it)
                    }
                }
            }
        }
    }

    private fun onCreateView(inAppData: InAppMessageData): View? {
        bannerView = InAppMessageBannerView(context, inAppData, this)
        return bannerView?.onCreateView()
    }

    private fun sendInAppMessageShownEvent(inAppMessageData: InAppMessageData) {
        if (!inAppMessageData.isInAppMessageShown) {
            cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_IN_APP_MESSAGE_SHOWN, inAppMessageData.mcID)
        }
    }

    private fun setupTimer() {
        timer = object : Timer(config.inAppBannerDisplayTime) {
            override fun onFinish() {
                bannerView?.hideBannerWithAnimation()
                onInAppBannerClose(isDismiss = true, isAutoClose = true)
                isDismissEventSent = true
            }
        }
        timer?.start()
    }

    override fun onActivityResumed(activity: Activity) {
        super.onActivityResumed(activity)
        showBanner(activity)
    }

    override fun onActivityPaused(activity: Activity) {
        super.onActivityPaused(activity)
        hideBanner()
    }

    private fun showBanner(activity: Activity) {
        bannerView?.let { _ ->
            display(activity)
            timer?.start()
        }
    }

    private fun hideBanner() {
        bannerView?.removeBannerView()
        timer?.stop()
    }

    override fun onCustomEvent(eventName: String, properties: Map<String, PropertyValue>?) {
        cordialApi.sendEvent(eventName, properties)
    }

    override fun onSetMcID(mcID: String) {
        cordialApi.setMcID(mcID)
    }

    override fun onInAppBannerClose(isDismiss: Boolean, isAutoClose: Boolean) {
        clearViews()
        if (isDismiss && !isDismissEventSent) sendDismissEvent(isAutoClose)
        inAppMessageData?.let { inAppData ->
            InAppMessageProcess.getInstance().onInAppMessageClose(inAppData.mcID)
        }
    }

    private fun clearViews() {
        context.applicationContext.castToAndRun<Application> {
            unregisterActivityLifecycleCallbacks(this@InAppMessageBannerAdapter)
        }
        timer?.stop()
        bannerView = null
        timer = null
    }

    private fun sendDismissEvent(isAutoClose: Boolean) {
        val dismissEvent = if (isAutoClose) {
            C.EVENT_NAME_IN_APP_MESSAGE_AUTO_DISMISS
        } else {
            C.EVENT_NAME_IN_APP_MESSAGE_MANUAL_DISMISS
        }
        cordialApi.sendSystemEventWithProperties(dismissEvent, inAppMessageData?.mcID)
    }

    private fun getTopActivity(): Activity? {
        return ActivityUtils.getTopActivity()
    }
}