package com.cordial.feature.inappmessage.getinappmessage.usecase

import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inappmessage.InAppMessageProcess
import com.cordial.feature.inappmessage.getinappmessage.repository.InAppMessageRepository
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageDelayShowType
import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.feature.inappmessage.model.OnInAppMessageResponseListener
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.lifecycle.AppLifecycleHandler
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.inappmessage.fetchinappmessage.FetchInAppMessageDao
import com.cordial.storage.db.dao.inappmessage.inappmessagedata.InAppMessageDao
import com.cordial.storage.db.dao.inappmessage.inappmessagetodelete.InAppMessageToDeleteDao

internal class InAppMessageUseCaseImpl(
    private val inAppMessageRepository: InAppMessageRepository,
    private val inAppMessageDao: InAppMessageDao?,
    private val fetchInAppMessageDao: FetchInAppMessageDao?,
    private val inAppMessageToDeleteDao: InAppMessageToDeleteDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : InAppMessageUseCase, CordialCheck() {

    override fun fetchInAppMessage(
        inAppMessageProperties: InAppMessageProperties,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        checkAuth(inAppMessageProperties, onRequestFromDBListener)
        doAfterCheck {
            inAppMessageRepository.getInAppMessage(inAppMessageProperties, object : OnInAppMessageResponseListener {
                override fun onSuccess(inAppMessageData: InAppMessageData) {
                    saveInAppMessageToCache(inAppMessageData)
                    processInAppMessageData(inAppMessageData)
                    onRequestFromDBListener?.onSuccess()
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleError(onRequestFromDBListener, inAppMessageProperties, error, onSaveRequestListener)
                }

                override fun onLogicError(response: String, onSaveRequestListener: (() -> Unit)?) {
                    handleError(onRequestFromDBListener, inAppMessageProperties, response, onSaveRequestListener)
                    CordialLoggerManager.warning("The in-app message has not been fetched due to the error: $response. Will retry later")
                }
            })
        }
    }

    private fun checkAuth(
        inAppMessageProperties: InAppMessageProperties,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleError(
                    onRequestFromDBListener,
                    inAppMessageProperties,
                    reason = "Jwt is absent"
                ) {
                    sdkSecurityUseCase.updateJWT()
                }
            }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleError(
                    onRequestFromDBListener,
                    inAppMessageProperties,
                    reason = "No network connection",
                    onSaveRequestListener = null
                )
            }
        )
        networkAvailableCheck.execute()
    }

    private fun saveInAppMessageToCache(inAppMessageData: InAppMessageData) {
        inAppMessageDao?.insert(inAppMessageData)
    }

    private fun processInAppMessageData(inAppMessageData: InAppMessageData) {
        val config = CordialApiConfiguration.getInstance()
        if (AppLifecycleHandler.isAppInForeground() && config.inAppShowEnabled && config.inAppMessageDelayMode.showType == InAppMessageDelayShowType.IMMEDIATELY) {
            InAppMessageProcess.getInstance().showInAppMessageIfNotTappedOnPush(inAppMessageData)
        }
    }

    private fun handleError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        inAppMessageProperties: InAppMessageProperties,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        if (reason == C.PROVIDED_TOKEN_EXPIRED && !inAppMessageProperties.isProvidedTokenExpired) {
            inAppMessageProperties.isProvidedTokenExpired = true
            fetchInAppMessage(inAppMessageProperties, onRequestFromDBListener)
            return
        }
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            fetchInAppMessageDao?.insert(inAppMessageProperties, onSaveRequestListener)
            CordialLoggerManager.info("Caching the in-app message fetch request with mcID = ${inAppMessageProperties.mcID} due to the error: $reason")
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    override fun fetchInAppMessageFromCache() {
        if (!SendingCacheState.gettingInAppMessage.compareAndSet(false, true))
            return
        fetchInAppMessageDao?.getInAppMessageProperties { inAppMessageProperties, id ->
            val isInAppMessagePropertiesEmptyCheck = Check(
                checkFunction = { inAppMessageProperties != null },
                doOnSuccess = {
                    inAppMessageProperties?.let {
                        fetchInAppMessageFromCache(inAppMessageProperties, id)
                    }
                },
                doOnError = { SendingCacheState.gettingInAppMessage.set(false) }
            )
            isInAppMessagePropertiesEmptyCheck.execute()
        }
    }

    private fun fetchInAppMessageFromCache(inAppMessageProperties: InAppMessageProperties, id: Int) {
        fetchInAppMessage(inAppMessageProperties, object : OnRequestFromDBListener {
            override fun onSuccess() {
                SendingCacheState.gettingInAppMessage.set(false)
                fetchInAppMessageDao?.deleteMcID(id) {
                    fetchInAppMessageFromCache()
                }
            }

            override fun onFailure() {
                SendingCacheState.gettingInAppMessage.set(false)
            }
        })
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        inAppMessageDao?.clear(onCompleteListener)
        inAppMessageToDeleteDao?.clear(onCompleteListener)
    }

    override fun getLatestInAppMessage(onInAppMessageListener: ((inAppMessageData: InAppMessageData?) -> Unit)?) {
        inAppMessageDao?.getLatestInAppMessage(onInAppMessageListener)
    }

    override fun updateInAppMessageShownData(mcID: String, isInAppMessageShown: Boolean) {
        inAppMessageDao?.updateInAppMessageShownData(mcID, isInAppMessageShown)
    }

    override fun deleteInAppIfExist(mcID: String) {
        inAppMessageDao?.deleteInAppIfExist(mcID)
    }

    override fun getInAppMessagesToDelete(onInAppMessagesMcIDListener: ((mcIDsToDelete: List<String>) -> Unit)?) {
        inAppMessageToDeleteDao?.getInAppMessagesToDelete(onInAppMessagesMcIDListener)
    }

    override fun saveInAppMessageToDelete(mcID: String) {
        inAppMessageToDeleteDao?.insert(mcID)
    }

    override fun removeInAppMessageToDelete(mcID: String) {
        inAppMessageToDeleteDao?.removeInAppMessageToDelete(mcID)
    }
}