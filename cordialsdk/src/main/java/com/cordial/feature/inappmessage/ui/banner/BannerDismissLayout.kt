package com.cordial.feature.inappmessage.ui.banner

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

internal class BannerDismissLayout : FrameLayout {

    private var overDragAmount: Float = 0.toFloat()

    private var isTopBanner = true

    private var dragHelper: ViewDragHelper? = null
    /**
     * Gets the minimum velocity needed to initiate a fling, as measured in pixels per second.
     *
     *
     * The default value is loaded from the `ViewConfiguration.getScaledMinimumFlingVelocity()`.
     *
     * @return The minimum fling velocity in pixels per second.
     */
    /**
     * Sets the minimum velocity needed to initiate a fling, as measured in pixels per second.
     *
     *
     * The default value is loaded from the `ViewConfiguration.getScaledMinimumFlingVelocity()`.
     *
     * @param minFlingVelocity The minimum fling velocity in pixels per second.
     */
    var minFlingVelocity: Float = 0.toFloat()
    internal var listener: Listener? = null

    /**
     * Interface to listen for dismissing the message view.
     */
    interface Listener {

        /**
         * Called when a child view was dismissed from a swipe. It is up to the listener to remove
         * or hide the view from the parent.
         */
        fun onDismissed(view: View?)
    }

    /**
     * BannerDismissLayout Constructor
     *
     * @param context A Context object used to access application assets.
     * @param attrs An AttributeSet passed to the parent.
     * @param defStyle The default style resource ID.
     */
    @JvmOverloads
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int = 0) : super(
        context,
        attrs,
        defStyle
    ) {
        init(context)
    }

    /**
     * BannerDismissLayout Constructor
     *
     * @param context A Context object used to access application assets.
     * @param attrs An AttributeSet passed to the parent.
     * @param defStyle The default style resource ID.
     * @param defResStyle A resource identifier of a style resource that supplies default values for
     * the view, used only if defStyle is 0 or cannot be found in the theme. Can be 0 to not
     * look for defaults.
     */
    constructor(context: Context, attrs: AttributeSet?, defStyle: Int, defResStyle: Int) : super(
        context,
        attrs,
        defStyle,
        defResStyle
    ) {
        init(context)
    }

    private fun init(context: Context) {
        if (isInEditMode) {
            return
        }

        dragHelper = ViewDragHelper.create(this, ViewDragCallback())

        val vc = ViewConfiguration.get(context)
        minFlingVelocity = vc.scaledMinimumFlingVelocity.toFloat()
        overDragAmount = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            DEFAULT_OVER_DRAG_DP.toFloat(),
            context.resources.displayMetrics
        )
    }

    /**
     * Sets the dismiss listener.
     *
     * @param listener The dismiss listener.
     */
    fun setListener(listener: Listener?) {
        synchronized(this) {
            this.listener = listener
        }
    }

    override fun computeScroll() {
        super.computeScroll()
        dragHelper?.let { dragHelper ->
            if (dragHelper.continueSettling(true)) {
                ViewCompat.postInvalidateOnAnimation(this)
            }
        }
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        dragHelper?.let { dragHelper ->
            if (dragHelper.shouldInterceptTouchEvent(event) || super.onInterceptTouchEvent(event)) {
                return true
            }

            if (dragHelper.viewDragState == ViewDragHelper.STATE_IDLE && event.actionMasked == MotionEvent.ACTION_MOVE) {
                /*
             * Check if the touch exceeded the touch slop. If so, check if we can drag and interrupt
             * any children. This breaks any children that are horizontally scrollable, unless they
             * prevent the parent view from intercepting the event.
             */
                if (dragHelper.checkTouchSlop(ViewDragHelper.DIRECTION_VERTICAL)) {
                    val child = dragHelper.findTopChildUnder(event.x.toInt(), event.y.toInt())
                    if (child != null && !child.canScrollVertically(dragHelper.touchSlop)) {
                        dragHelper.captureChildView(child, event.getPointerId(0))
                        return dragHelper.viewDragState == ViewDragHelper.STATE_DRAGGING
                    }
                }
            }
        }
        return false
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        dragHelper?.let { dragHelper ->
            dragHelper.processTouchEvent(event)

            dragHelper.capturedView?.let {
                if (event.actionMasked == MotionEvent.ACTION_MOVE) {
                    if (dragHelper.checkTouchSlop(ViewDragHelper.DIRECTION_VERTICAL)) {
                        val child = dragHelper.findTopChildUnder(event.x.toInt(), event.y.toInt())
                        child?.let { childView ->
                            if (!childView.canScrollVertically(dragHelper.touchSlop)) {
                                dragHelper.captureChildView(childView, event.getPointerId(0))
                            }
                        }
                    }
                }
            }
        }
        return dragHelper?.capturedView != null
    }

    /**
     * Sets the banner placement.
     *
     * @param isTopBanner The placement of banner.
     */
    fun setPlacement(isTopBanner: Boolean) {
        this.isTopBanner = isTopBanner
    }

    /**
     * Helper class to handle the the callbacks from the ViewDragHelper.
     */
    private inner class ViewDragCallback : ViewDragHelper.Callback() {

        private var initTop: Int = -1
        private var startTop: Int = 0
        private var startLeft: Int = 0
        private var dragPercent = 0f
        private var capturedView: View? = null
        private var isDismissed = false

        override fun tryCaptureView(view: View, i: Int): Boolean {
            return capturedView == null
        }

        override fun clampViewPositionHorizontal(child: View, left: Int, dx: Int): Int {
            return child.left
        }

        override fun clampViewPositionVertical(child: View, top: Int, dy: Int): Int {
            return if (isTopBanner) {
                val newTop = min(
                    top.toFloat(),
                    startTop + overDragAmount
                ).roundToInt()
                if (newTop > initTop)
                    initTop
                else newTop
            } else {
                val newTop = max(
                    top.toFloat(),
                    startTop - overDragAmount
                ).roundToInt()
                if (newTop < initTop)
                    initTop
                else newTop
            }
        }

        override fun onViewCaptured(view: View, activePointerId: Int) {
            if (initTop == -1) {
                initTop = view.top
            }
            capturedView = view
            startTop = view.top
            startLeft = view.left
            isDismissed = false
        }

        @SuppressLint("NewApi")
        override fun onViewPositionChanged(view: View, left: Int, top: Int, dx: Int, dy: Int) {
            val range = height
            val moved = abs(top - initTop)
            if (range > 0) {
                dragPercent = moved / range.toFloat()
            }

            invalidate()
        }

        override fun onViewDragStateChanged(state: Int) {
            if (capturedView == null) {
                return
            }

            synchronized(this) {
                capturedView?.let { captView ->
                    if (state == ViewDragHelper.STATE_IDLE) {
                        if (isDismissed) {
                            listener?.onDismissed(captView)

                            removeView(capturedView)
                        }

                        capturedView = null
                    }
                }
            }
        }

        override fun onViewReleased(view: View, xv: Float, yv: Float) {
            val absYv = abs(yv)
            if (if (isTopBanner) startTop >= view.top else startTop <= view.top) {
                isDismissed = absYv > minFlingVelocity && (dragPercent >= IDLE_MIN_DRAG_PERCENT ||
                        dragPercent > FLING_MIN_DRAG_PERCENT)
            }

            if (isDismissed) {
                val top =
                    if (isTopBanner) -view.height else height + view.height
                dragHelper?.settleCapturedViewAt(startLeft, top)
            } else {
                dragHelper?.settleCapturedViewAt(startLeft, initTop)
            }
            dragPercent = 0f
            invalidate()
        }

    }

    companion object {

        /**
         * The percent of a view's width it must be dragged before it is considered dismissible when the velocity
         * is less then the [.getMinFlingVelocity].
         */
        private const val IDLE_MIN_DRAG_PERCENT = .03f

        /**
         * The percent of a view's width it must be dragged before it is considered dismissible when the velocity
         * is greater then the [.getMinFlingVelocity].
         */
        private const val FLING_MIN_DRAG_PERCENT = .03f

        private const val DEFAULT_OVER_DRAG_DP = 24
    }
}