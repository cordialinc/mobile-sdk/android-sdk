package com.cordial.feature.inappmessage.getinappmessagesdata.usecase

import com.cordial.feature.CacheableUseCase
import com.google.firebase.messaging.RemoteMessage

internal interface InAppMessageDataUseCase : CacheableUseCase {
    fun getInAppMessagesData()
    fun processInAppMessage(remoteMessage: RemoteMessage)
}