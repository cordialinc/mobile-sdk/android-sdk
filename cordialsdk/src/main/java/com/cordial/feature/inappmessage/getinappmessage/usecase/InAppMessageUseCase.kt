package com.cordial.feature.inappmessage.getinappmessage.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.storage.db.OnRequestFromDBListener

internal interface InAppMessageUseCase : CacheableUseCase {
    fun fetchInAppMessage(
        inAppMessageProperties: InAppMessageProperties,
        onRequestFromDBListener: OnRequestFromDBListener?
    )

    fun fetchInAppMessageFromCache()

    fun getLatestInAppMessage(onInAppMessageListener: ((inAppMessageData: InAppMessageData?) -> Unit)?)

    fun updateInAppMessageShownData(mcID: String, isInAppMessageShown: Boolean)

    fun deleteInAppIfExist(mcID: String)

    fun getInAppMessagesToDelete(onInAppMessagesMcIDListener: ((mcIDsToDelete: List<String>) -> Unit)?)

    fun saveInAppMessageToDelete(mcID: String)

    fun removeInAppMessageToDelete(mcID: String)
}