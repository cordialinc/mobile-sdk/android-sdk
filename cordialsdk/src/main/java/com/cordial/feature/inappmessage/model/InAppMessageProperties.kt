package com.cordial.feature.inappmessage.model

internal class InAppMessageProperties(
    val mcID: String,
    val type: InAppMessageType,
    val expirationTime: String?,
    val timestamp: Long,
    val url: String?,
    val urlExpireAt: String?
) {
    var isProvidedTokenExpired: Boolean = false
}