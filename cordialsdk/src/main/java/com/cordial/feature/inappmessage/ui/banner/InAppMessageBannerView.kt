package com.cordial.feature.inappmessage.ui.banner

import android.annotation.SuppressLint
import android.content.Context
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.webkit.*
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.cordialsdk.R
import com.cordial.cordialsdk.databinding.ViewInAppMessageBannerBinding
import com.cordial.feature.inappmessage.InAppMessageProcess
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.util.*
import kotlin.math.min
import kotlin.math.roundToInt

@SuppressLint("ViewConstructor")
internal class InAppMessageBannerView(
    context: Context,
    private val inAppMessageData: InAppMessageData,
    private val onInAppBannerViewListener: OnInAppBannerViewListener
) : FrameLayout(context) {

    private lateinit var binding: ViewInAppMessageBannerBinding

    fun onCreateView(): View {
        binding = ViewInAppMessageBannerBinding.inflate(LayoutInflater.from(context), this, false)
        initDismissLayout()
        setupAnimation()
        initWebView()
        return binding.root
    }

    private fun initDismissLayout() {
        with(binding) {
            vgInAppParent.setPlacement(inAppMessageData.type == InAppMessageType.BANNER_UP)
            vgInAppParent.setListener(object : BannerDismissLayout.Listener {
                override fun onDismissed(view: View?) {
                    closeInAppMessage()
                }
            })
        }
    }

    private fun setupAnimation() {
        val animationRes = if (inAppMessageData.type == InAppMessageType.BANNER_UP) {
            R.anim.slide_in_up
        } else {
            R.anim.slide_in_down
        }
        val animation = AnimationUtils.loadAnimation(context, animationRes)
        binding.root.startAnimation(animation)
    }

    private fun initWebView() {
        with(binding) {
            disableScroll()
            disableLongClickAndVibration()
            setScaleAndZoomOptions()
            setLoadingSettings()
            setTransparentBackground()
            setCardViewElevation(0.0F)
        }
        loadDataIntoWebView()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun ViewInAppMessageBannerBinding.disableScroll() {
        with(wvInAppMessage) {
            setOnTouchListener { _, event -> (event.action == MotionEvent.ACTION_MOVE) }
            isHorizontalScrollBarEnabled = false
            isVerticalScrollBarEnabled = false
        }
    }

    private fun ViewInAppMessageBannerBinding.disableLongClickAndVibration() {
        with(wvInAppMessage) {
            setOnLongClickListener { true }
            isLongClickable = false
            isHapticFeedbackEnabled = false
        }
    }

    private fun ViewInAppMessageBannerBinding.setScaleAndZoomOptions() {
        with(wvInAppMessage) {
            settings.loadWithOverviewMode = true // zoom out web view
            settings.useWideViewPort = true // scale web view as defined in html
        }
    }

    private fun ViewInAppMessageBannerBinding.setLoadingSettings() {
        wvInAppMessage.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
    }

    private fun ViewInAppMessageBannerBinding.setTransparentBackground() {
        cvInAppMessage.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
    }

    private fun ViewInAppMessageBannerBinding.setCardViewElevation(elevation: Float) {
        cvInAppMessage.cardElevation = elevation
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadDataIntoWebView() {
        binding.wvInAppMessage.settings.javaScriptEnabled = true
        setInAppWidth()
        loadHtml(inAppMessageData.html)
        addJavaScriptInterface()
        setWebViewListener()
    }

    private fun setInAppWidth() {
        val screenWidth = ScreenUtils.getScreenWidth(context)
        val widthPartWithoutMargin = (100 - inAppMessageData.margin.start - inAppMessageData.margin.end) / 100.0
        val adjustedWidth = (screenWidth * widthPartWithoutMargin).toInt()
        val cardViewParams = LayoutParams(adjustedWidth, LayoutParams.MATCH_PARENT)
        cardViewParams.gravity = Gravity.CENTER
        binding.cvInAppMessage.layoutParams = cardViewParams
    }

    private fun loadHtml(html: String) {
        binding.wvInAppMessage.loadDataWithBaseURL(null, html, "text/html; charset=utf-8", "UTF-8", null)
    }

    @SuppressLint("AddJavascriptInterface")
    private fun addJavaScriptInterface() {
        binding.wvInAppMessage.addJavascriptInterface(object : Any() {
            @JavascriptInterface
            fun actionClick(deepLink: String?, eventName: String?) {
                onActionClick(deepLink, eventName)
            }

            @JavascriptInterface
            fun crdlEventWithProperties(eventName: String?, jsonObjectProperties: String) {
                onEventWithProperties(eventName, jsonObjectProperties)
            }

            @JavascriptInterface
            fun getContentHeight(contentHeight: Double) {
                val contentHeightDensityPixel = contentHeight * binding.wvInAppMessage.resources.displayMetrics.density
                setInAppSize(contentHeightDensityPixel)
            }
        }, "Android")
    }

    private fun setWebViewListener() {
        binding.wvInAppMessage.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                injectJS()
            }

            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                openInAppUrlLink(url)
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = if (request?.url == null) null else request.url.toString()
                openInAppUrlLink(url)
                return true
            }
        }
    }

    private fun injectJS() {
        val fileName = "InAppMessage.js"
        fileName.readAssetFile(context)?.let {
            with(binding.wvInAppMessage) {
                evaluateJavascript(it, null)
                evaluateJavascript("Android.getContentHeight(document.documentElement.scrollHeight);", null)
            }
        }
    }

    fun onActionClick(deepLink: String?, eventName: String?) {
        val isDismiss = deepLink == null && eventName == null
        if (!isDismiss) {
            saveMcIDAndOpenDeepLink(deepLink)
            eventName?.let { name ->
                onInAppBannerViewListener.onCustomEvent(name, null)
            }
        }
        closeInAppMessage(isDismiss)
    }

    private fun saveMcIDAndOpenDeepLink(deepLink: String?) {
        saveMcID()
        deepLink?.let {
            InAppMessageProcess.getInstance().openDeepLink(deepLink)
        }
    }

    private fun saveMcID() {
        onInAppBannerViewListener.onSetMcID(inAppMessageData.mcID)
    }

    fun onEventWithProperties(eventName: String?, jsonObjectProperties: String) {
        eventName?.let {
            saveMcID()
            val properties = JsonUtils.getPropertyMapFromJson(jsonObjectProperties)
            onInAppBannerViewListener.onCustomEvent(eventName, properties)
            CordialApiConfiguration.getInstance().inAppMessageInputsListener?.inputsCaptured(eventName, properties)
            closeInAppMessage(isDismiss = false)
        }
    }

    fun openInAppUrlLink(url: String?) {
        closeInAppMessage(isDismiss = false)
        saveMcIDAndOpenDeepLink(url)
    }

    private fun setInAppSize(contentHeightDensityPixel: Double) {
        val inAppHeightPercent = calculateInAppHeight(contentHeightDensityPixel)
        setMargins(inAppHeightPercent)
    }

    private fun calculateInAppHeight(inAppContentHeight: Double): Int {
        val applicationHeight = binding.vgInAppParent.height
        val inAppHeightPercent = (inAppContentHeight * 100 / applicationHeight).roundToInt()
        val maximumInAppBannerHeightPercent = calculateMaximumInAppBannerHeightPercent()
        return when {
            inAppHeightPercent < C.MINIMUM_IN_APP_HEIGHT_PERCENT -> C.MINIMUM_IN_APP_HEIGHT_PERCENT
            inAppHeightPercent > maximumInAppBannerHeightPercent -> maximumInAppBannerHeightPercent
            else -> inAppHeightPercent
        }
    }

    private fun calculateMaximumInAppBannerHeightPercent(): Int {
        val minVerticalMargin = min(inAppMessageData.margin.top, inAppMessageData.margin.bottom)
        return 100 - minVerticalMargin * 2
    }

    private fun setMargins(inAppHeightPercent: Int) {
        runOnMainThread {
            with(binding) {
                val screenWidth = ScreenUtils.getScreenWidth(context)
                val applicationHeight = vgInAppParent.height
                val topMarginPercent =
                    if (inAppMessageData.type == InAppMessageType.BANNER_UP) inAppMessageData.margin.top else
                        100 - inAppHeightPercent - inAppMessageData.margin.bottom
                val bottomMarginPercent =
                    if (inAppMessageData.type == InAppMessageType.BANNER_BOTTOM) inAppMessageData.margin.bottom else
                        100 - inAppHeightPercent - inAppMessageData.margin.top

                val topMargin = topMarginPercent * applicationHeight / 100
                val startMargin = inAppMessageData.margin.start * screenWidth / 100
                val bottomMargin = bottomMarginPercent * applicationHeight / 100
                val endMargin = inAppMessageData.margin.end * screenWidth / 100

                val cardViewParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                cardViewParams.setMargins(startMargin, topMargin, endMargin, bottomMargin)
                cvInAppMessage.layoutParams = cardViewParams
                cvInAppMessage.visibility = View.VISIBLE
                val webViewParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
                wvInAppMessage.layoutParams = webViewParams

                val inAppWidth = screenWidth - startMargin - endMargin
                wvInAppMessage.setInitialScale(ScreenUtils.getInitialScaleByWidth(context, inAppWidth))
                setCardViewElevation(8.0F)
            }
        }
    }

    fun closeInAppMessage(isDismiss: Boolean = true) {
        onInAppBannerViewListener.onInAppBannerClose(isDismiss)
        removeBannerView()
    }

    fun hideBannerWithAnimation() {
        val animationRes = if (inAppMessageData.type == InAppMessageType.BANNER_UP)
            R.anim.slide_out_up else R.anim.slide_out_down
        val animation = AnimationUtils.loadAnimation(context, animationRes)
        animation.setAnimationListener(object : BannerAnimationListener {
            override fun onAnimationEnd(anim: Animation?) {
                removeBannerView()
            }
        })
        binding.root.startAnimation(animation)
    }

    fun removeBannerView() {
        runOnMainThread {
            try {
                binding.root.parent?.let { parent ->
                    parent.castToAndRun<ViewGroup> {
                        removeView(binding.root)
                    }
                }
            } catch (error: NullPointerException) {
                CordialLoggerManager.error("Cannot remove banner view due to NullPointerException")
            }
        }
    }

    interface OnInAppBannerViewListener {
        fun onCustomEvent(eventName: String, properties: Map<String, PropertyValue>?)
        fun onSetMcID(mcID: String)
        fun onInAppBannerClose(isDismiss: Boolean, isAutoClose: Boolean = false)
    }

}