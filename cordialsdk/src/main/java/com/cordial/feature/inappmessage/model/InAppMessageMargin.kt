package com.cordial.feature.inappmessage.model

import java.io.Serializable

internal class InAppMessageMargin(
    val start: Int,
    val top: Int,
    val end: Int,
    val bottom: Int
) : Serializable {

    companion object {
        private val BANNER_TOP = InAppMessageMargin(5, 5, 5, 80)
        private val BANNER_BOTTOM = InAppMessageMargin(5, 80, 5, 5)
        private val MODAL = InAppMessageMargin(5, 10, 5, 10)
        private val FULLSCREEN = InAppMessageMargin(0, 0, 0, 0)

        fun findByInAppMessageType(type: InAppMessageType): InAppMessageMargin {
            return when (type) {
                InAppMessageType.BANNER_UP -> BANNER_TOP
                InAppMessageType.BANNER_BOTTOM -> BANNER_BOTTOM
                InAppMessageType.MODAL -> MODAL
                InAppMessageType.FULLSCREEN -> FULLSCREEN
            }
        }
    }
}