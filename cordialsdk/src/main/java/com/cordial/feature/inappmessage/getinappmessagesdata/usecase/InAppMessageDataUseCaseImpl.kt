package com.cordial.feature.inappmessage.getinappmessagesdata.usecase

import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.inappmessage.getinappmessage.usecase.InAppMessageUseCase
import com.cordial.feature.inappmessage.getinappmessagesdata.repository.InAppMessagesDataRepository
import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.notification.util.PushNotificationPayloadUtils
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.dao.inappmessage.fetchinappmessage.FetchInAppMessageDao
import com.cordial.storage.db.dao.inappmessage.inappmessagetodelete.InAppMessageToDeleteDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.JsonUtils.optNullableString
import com.cordial.util.TimeUtils
import com.cordial.util.TimeUtils.isNewerThan
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

internal class InAppMessageDataUseCaseImpl(
    private val preferences: Preferences,
    private val inAppMessagesDataRepository: InAppMessagesDataRepository,
    private val inAppMessageUseCase: InAppMessageUseCase?,
    private val fetchInAppMessageDao: FetchInAppMessageDao?,
    private val inAppMessageToDeleteDao: InAppMessageToDeleteDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : InAppMessageDataUseCase, CordialCheck() {

    override fun getInAppMessagesData() {
        checkAuth()
        doAfterCheck {
            inAppMessagesDataRepository.getInAppMessagesData(object : OnResponseListener {
                override fun onSuccess(response: String) {
                    processInAppMessagesData(response)
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleError(error)
                }

                override fun onLogicError(response: String) {
                    handleError(response)
                }
            })
        }
    }

    private fun checkAuth() {
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            doOnSuccess = { isChecked = true },
            doOnError = { updateJWT() }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
        )
        networkAvailableCheck.execute()
    }

    private fun processInAppMessagesData(response: String) {
        val inAppMessagesProperties = getInAppMessagesProperties(response)
        val isInAppMessagePropertiesEmptyCheck = Check(
            checkFunction = { inAppMessagesProperties.isNotEmpty() },
            doOnSuccess = { fetchInAppMessageIfNotTappedOnRelatedNotification(inAppMessagesProperties) },
            doOnError = { saveCurrentTimeAsLastInAppTimestamp() }
        )
        isInAppMessagePropertiesEmptyCheck.execute()
    }

    private fun saveCurrentTimeAsLastInAppTimestamp() {
        val currentTimeMinusMinute = TimeUtils.getCurrentTimeMinusMinutes(minutes = 1)
        preferences.put(PreferenceKeys.LAST_IN_APP_TIMESTAMP, currentTimeMinusMinute)
    }

    private fun fetchInAppMessageIfNotTappedOnRelatedNotification(inAppMessagesProperties: List<InAppMessageProperties>) {
        inAppMessageToDeleteDao?.getInAppMessagesToDelete { mcIdsToDelete ->
            inAppMessagesProperties.forEachIndexed { index, inAppMessageProperty ->
                updateLastInAppTimestamp(inAppMessageProperty)
                if (mcIdsToDelete.contains(inAppMessageProperty.mcID)) {
                    inAppMessageToDeleteDao.removeInAppMessageToDelete(inAppMessageProperty.mcID)
                    CordialLoggerManager.info("The in-app message was removed after clicking on the related push notification")
                } else {
                    fetchInAppMessage(index, inAppMessagesProperties, inAppMessageProperty)
                }
            }
        }
    }

    private fun fetchInAppMessage(
        index: Int,
        inAppMessagesProperties: List<InAppMessageProperties>,
        inAppMessageProperties: InAppMessageProperties
    ) {
        val isLastItem = index == (inAppMessagesProperties.size - 1)
        fetchInAppMessageDao?.insert(inAppMessageProperties) {
            if (isLastItem) {
                inAppMessageUseCase?.fetchInAppMessageFromCache()
            }
        }
    }

    private fun updateLastInAppTimestamp(inAppMessageProperties: InAppMessageProperties) {
        val lastInAppTimestamp = preferences.getLong(PreferenceKeys.LAST_IN_APP_TIMESTAMP)
        val inAppTimestamp = inAppMessageProperties.timestamp
        if (inAppTimestamp.isNewerThan(lastInAppTimestamp)) {
            preferences.put(PreferenceKeys.LAST_IN_APP_TIMESTAMP, inAppTimestamp)
        }
    }

    private fun getInAppMessagesProperties(response: String): List<InAppMessageProperties> {
        val inAppMessagesProperties = mutableListOf<InAppMessageProperties>()
        val jsonObject = JSONObject(response)
        val inAppMessagesArray = jsonObject.optJSONArray("messages")
        inAppMessagesArray?.let {
            for (i in 0 until inAppMessagesArray.length()) {
                val inAppMessageDataObject = inAppMessagesArray.optJSONObject(i)
                val mcID = inAppMessageDataObject.optString("_id")
                val type = InAppMessageType.findKey(inAppMessageDataObject.optString("type"))
                val expirationTime = inAppMessageDataObject.optString("expirationTime")
                val timestamp = TimeUtils.getTime(inAppMessageDataObject.optString("sentAt"), isMillis = true)
                val url = inAppMessageDataObject.optString("url")
                val urlExpireAt = inAppMessageDataObject.optString("urlExpireAt")
                inAppMessagesProperties.add(
                    InAppMessageProperties(mcID, type, expirationTime, timestamp, url, urlExpireAt)
                )
            }
        }
        return inAppMessagesProperties
    }

    private fun handleError(error: String) {
        if (error == C.JWT_TOKEN_EXPIRED) {
            CordialLoggerManager.info("Did not fetch the in-app messages data because the jwt is expired. Will retry later")
            updateJWT()
        } else {
            CordialLoggerManager.error("Failed to fetch the in-app messages data due to the error: $error")
        }
    }

    private fun updateJWT() {
        sdkSecurityUseCase.updateJWT {
            getInAppMessagesData()
        }
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        fetchInAppMessageDao?.clear(onCompleteListener)
    }

    override fun processInAppMessage(remoteMessage: RemoteMessage) {
        val mcID = remoteMessage.data["mcID"]
        mcID?.let { mcId ->
            val inAppMessageProperties = getInAppMessageProperties(mcId, remoteMessage)
            val inAppMessageUseCase = CordialApiConfiguration.getInstance().injection
                .inAppMessageInjection().inAppMessageUseCase
            inAppMessageUseCase.fetchInAppMessage(inAppMessageProperties, null)
        }
    }

    private fun getInAppMessageProperties(mcID: String, remoteMessage: RemoteMessage): InAppMessageProperties {
        val inAppMessagePushPayload = PushNotificationPayloadUtils.getInAppMessageJSONObject(remoteMessage)
        val timestamp = System.currentTimeMillis()
        var inAppType = InAppMessageType.MODAL
        inAppMessagePushPayload?.optNullableString("type")?.let { type ->
            inAppType = InAppMessageType.findKey(type)
        }
        val expirationTime = inAppMessagePushPayload?.optNullableString("expirationTime")
        val url = inAppMessagePushPayload?.optNullableString("url")
        val urlExpireAt = inAppMessagePushPayload?.optNullableString("urlExpireAt")

        return InAppMessageProperties(mcID, inAppType, expirationTime, timestamp, url, urlExpireAt)
    }
}