package com.cordial.feature.inappmessage.model

internal interface OnInAppMessageResponseListener {
    fun onSuccess(inAppMessageData: InAppMessageData)
    fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?)
    fun onLogicError(response: String, onSaveRequestListener: (() -> Unit)?)
}