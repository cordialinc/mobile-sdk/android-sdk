package com.cordial.feature.inappmessage.getinappmessage.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageMargin
import com.cordial.feature.inappmessage.model.InAppMessageProperties
import com.cordial.feature.inappmessage.model.OnInAppMessageResponseListener
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.TimeUtils
import org.json.JSONObject

internal class InAppMessageRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val s3ResponseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : InAppMessageRepository {

    override fun getInAppMessage(
        inAppMessageProperties: InAppMessageProperties,
        onInAppMessageResponseListener: OnInAppMessageResponseListener
    ) {
        val url = getUrl(inAppMessageProperties)
        val sdkRequest = SDKRequest(null, url, RequestMethod.GET)
        sdkRequest.isCordial = isCordialRequest(inAppMessageProperties)
        val responseHandler = if (sdkRequest.isCordial) responseHandler else s3ResponseHandler
        requestSender.send(sdkRequest, responseHandler, object : OnResponseListener {
            override fun onSuccess(response: String) {
                val inAppMessageData = getInApp(response, inAppMessageProperties)
                onInAppMessageResponseListener.onSuccess(inAppMessageData)
            }

            override fun onLogicError(response: String) {
                onInAppMessageResponseListener.onLogicError(response, null)
            }

            override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                onInAppMessageResponseListener.onSystemError(error, onSaveRequestListener)
            }
        })
    }

    private fun getUrl(inAppMessageProperties: InAppMessageProperties): String {
        return if (inAppMessageProperties.url != null &&
            !TimeUtils.isDateExpired(inAppMessageProperties.urlExpireAt, isMillis = true) &&
            !inAppMessageProperties.isProvidedTokenExpired
        ) {
            inAppMessageProperties.url
        } else {
            cordialApiEndpoints.getInAppMessageUrl(inAppMessageProperties.mcID)
        }
    }

    private fun isCordialRequest(inAppMessageProperties: InAppMessageProperties): Boolean {
        return inAppMessageProperties.url == null ||
                TimeUtils.isDateExpired(inAppMessageProperties.urlExpireAt, isMillis = true) ||
                inAppMessageProperties.isProvidedTokenExpired
    }

    private fun getInApp(response: String, inAppMessageProperties: InAppMessageProperties): InAppMessageData {
        val html = JSONObject(response).optString("content")
        val inAppMessageMargin =
            InAppMessageMargin.findByInAppMessageType(inAppMessageProperties.type)
        return InAppMessageData(
            inAppMessageProperties.mcID,
            html,
            inAppMessageProperties.type,
            inAppMessageMargin,
            inAppMessageProperties.expirationTime,
            inAppMessageProperties.timestamp
        )
    }
}