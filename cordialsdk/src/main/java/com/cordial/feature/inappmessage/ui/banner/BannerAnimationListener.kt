package com.cordial.feature.inappmessage.ui.banner

import android.view.animation.Animation

internal interface BannerAnimationListener : Animation.AnimationListener {
    override fun onAnimationStart(anim: Animation?) {
    }

    override fun onAnimationRepeat(anim: Animation?) {
    }

    override fun onAnimationEnd(anim: Animation?) {
    }
}