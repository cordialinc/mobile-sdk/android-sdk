package com.cordial.feature.inappmessage.model

enum class InAppMessageDelayShowType {
    NEXT_APP_OPEN,
    IMMEDIATELY
}