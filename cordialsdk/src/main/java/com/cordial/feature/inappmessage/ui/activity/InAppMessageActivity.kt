package com.cordial.feature.inappmessage.ui.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.Gravity
import android.webkit.*
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.cordialsdk.databinding.ActivityInAppMessageBinding
import com.cordial.feature.inappmessage.InAppMessageProcess
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.util.*


internal class InAppMessageActivity : AppCompatActivity() {
    private lateinit var binding: ActivityInAppMessageBinding

    private val cordialApi = CordialApi()
    private var inAppMessage: InAppMessageData? = null
    private var isInAppClosed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInAppMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        overridePendingTransition(0, 0)
        extractExtras()
        initWebView()
        setClickListeners()
        sendInAppMessageShownEvent()
    }

    private fun extractExtras() {
        intent?.extras?.let { extras ->
            if (extras.containsKey(C.IN_APP_MESSAGE)) {
                inAppMessage = extras.getSerializable(C.IN_APP_MESSAGE) as InAppMessageData
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initWebView() {
        with(binding) {
            disableLongClickAndVibration()
            setScaleAndZoomOptions()
            setLoadingSettings()
            setTransparentBackground()
            setCardViewElevation(0.0F)
        }
        setStatusBarTransparent()
        loadDataIntoWebView()
    }

    private fun ActivityInAppMessageBinding.disableLongClickAndVibration() {
        with(wvInAppMessage) {
            setOnLongClickListener { true }
            isLongClickable = false
            isHapticFeedbackEnabled = false
        }
    }

    private fun ActivityInAppMessageBinding.setScaleAndZoomOptions() {
        with(wvInAppMessage) {
            settings.loadWithOverviewMode = true // zoom out web view
            settings.useWideViewPort = true // scale web view as defined in html
        }
    }

    private fun ActivityInAppMessageBinding.setLoadingSettings() {
        wvInAppMessage.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
    }

    private fun ActivityInAppMessageBinding.setTransparentBackground() {
        val context = this@InAppMessageActivity
        cvInAppMessage.setCardBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent))
    }

    private fun ActivityInAppMessageBinding.setCardViewElevation(elevation: Float) {
        cvInAppMessage.cardElevation = elevation
    }

    private fun setStatusBarTransparent() {
        if (!isFullscreen()) {
            ActivityUtils.setStatusBarTransparent(this)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadDataIntoWebView() {
        inAppMessage?.let { inApp ->
            binding.wvInAppMessage.settings.javaScriptEnabled = true
            setInAppWidth(inApp)
            loadHtml(inApp.html)
            addJavaScriptInterface()
            setWebViewListener()
        }
    }

    private fun setInAppWidth(inApp: InAppMessageData) {
        if (!isFullscreen()) {
            val screenWidth = ScreenUtils.getScreenWidth(this)
            val widthPartWithoutMargin = (100 - inApp.margin.start - inApp.margin.end) / 100.0
            val adjustedWidth = (screenWidth * widthPartWithoutMargin).toInt()
            val cardViewParams = FrameLayout.LayoutParams(adjustedWidth, FrameLayout.LayoutParams.MATCH_PARENT)
            cardViewParams.gravity = Gravity.CENTER
            binding.cvInAppMessage.layoutParams = cardViewParams
        }
    }

    private fun loadHtml(html: String) {
        binding.wvInAppMessage.loadDataWithBaseURL(null, html, "text/html; charset=utf-8", "UTF-8", null)
    }

    @SuppressLint("AddJavascriptInterface")
    private fun addJavaScriptInterface() {
        binding.wvInAppMessage.addJavascriptInterface(object : Any() {
            @JavascriptInterface
            fun actionClick(deepLink: String?, eventName: String?) {
                onActionClick(deepLink, eventName)
            }

            @JavascriptInterface
            fun crdlEventWithProperties(eventName: String?, jsonObjectProperties: String) {
                onEventWithProperties(eventName, jsonObjectProperties)
            }

            @JavascriptInterface
            fun getContentHeight(contentHeight: Double) {
                val contentHeightDensityPixel = contentHeight * binding.wvInAppMessage.resources.displayMetrics.density
                setInAppSize(contentHeightDensityPixel)
            }
        }, "Android")
    }

    private fun setWebViewListener() {
        binding.wvInAppMessage.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                super.onPageFinished(view, url)
                injectJS()
            }

            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                openInAppUrlLink(url)
                return true
            }

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = if (request?.url == null) null else request.url.toString()
                openInAppUrlLink(url)
                return true
            }
        }
    }

    private fun injectJS() {
        val fileName = "InAppMessage.js"
        fileName.readAssetFile(this)?.let { jsScript ->
            with(binding.wvInAppMessage) {
                evaluateJavascript(jsScript, null)
                evaluateJavascript("Android.getContentHeight(document.documentElement.scrollHeight);", null)
            }
        }
    }

    fun onActionClick(deepLink: String?, eventName: String?) {
        val isDismiss = deepLink == null && eventName == null
        closeInAppMessage(isDismiss)
        if (!isDismiss) {
            saveMcIDAndOpenDeepLink(deepLink)
            eventName?.let { name ->
                cordialApi.sendEvent(name, null)
            }
        }
    }

    private fun saveMcIDAndOpenDeepLink(deepLink: String?) {
        saveMcID()
        deepLink?.let {
            InAppMessageProcess.getInstance().openDeepLink(deepLink)
        }
    }

    private fun saveMcID() {
        inAppMessage?.let { inApp ->
            cordialApi.setMcID(inApp.mcID)
        }
    }

    fun onEventWithProperties(eventName: String?, jsonObjectProperties: String) {
        eventName?.let {
            saveMcID()
            val properties = JsonUtils.getPropertyMapFromJson(jsonObjectProperties)
            cordialApi.sendEvent(eventName, properties)
            val config = CordialApiConfiguration.getInstance()
            config.inAppMessageInputsListener?.inputsCaptured(eventName, properties)
            closeInAppMessage(isDismiss = false)
        }
    }

    fun openInAppUrlLink(url: String?) {
        closeInAppMessage(isDismiss = false)
        saveMcIDAndOpenDeepLink(url)
    }

    private fun setInAppSize(contentHeightDensityPixel: Double) {
       inAppMessage?.let { inApp ->
        val inAppHeight = calculateInAppHeight(inApp, contentHeightDensityPixel)
        setMargins(inApp, inAppHeight)
       }
    }

    private fun calculateInAppHeight(inApp: InAppMessageData, inAppContentHeight: Double): Double {
        val applicationHeight = binding.vgInAppParent.height

        val maximumInAppModalHeight =
            (applicationHeight - applicationHeight * (inApp.margin.top + inApp.margin.bottom) / 100).toDouble()
        val minimumInAppModalHeight = (applicationHeight * C.MINIMUM_IN_APP_HEIGHT_PERCENT / 100).toDouble()
        return when {
            isFullscreen() -> applicationHeight.toDouble()
            inAppContentHeight < minimumInAppModalHeight -> minimumInAppModalHeight
            inAppContentHeight > maximumInAppModalHeight -> maximumInAppModalHeight
            else -> inAppContentHeight
        }
    }

    private fun setMargins(inApp: InAppMessageData, inAppContentHeight: Double) {
        runOnMainThread {
            with(binding) {
                val context = this@InAppMessageActivity
                val matchParent = FrameLayout.LayoutParams.MATCH_PARENT
                val screenWidth = ScreenUtils.getScreenWidth(context)
                val applicationHeight = binding.vgInAppParent.height

                val startMargin = inApp.margin.start * screenWidth / 100
                val endMargin = inApp.margin.end * screenWidth / 100
                val verticalMargin = ((applicationHeight - inAppContentHeight) / 2).toInt()

                val cardViewParams = FrameLayout.LayoutParams(matchParent, matchParent)
                cardViewParams.gravity = Gravity.CENTER
                cardViewParams.setMargins(startMargin, verticalMargin, endMargin, verticalMargin)
                cvInAppMessage.layoutParams = cardViewParams
                val webViewParams = FrameLayout.LayoutParams(matchParent, matchParent)
                wvInAppMessage.layoutParams = webViewParams

                val inAppWidth = screenWidth - startMargin - endMargin
                wvInAppMessage.setInitialScale(ScreenUtils.getInitialScaleByWidth(context, inAppWidth))
                setCardViewElevation(8.0F)
            }
        }
    }

    private fun setClickListeners() {
        with(binding) {
            vgInAppParent.setOnClickListener {
                closeInAppMessage()
            }
            ivCloseInAppMessage.setOnClickListener {
                closeInAppMessage()
            }
        }
        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                closeInAppMessage()
            }
        })
    }

    private fun sendInAppMessageShownEvent() {
        inAppMessage?.let { inAppMessageData ->
            if (!inAppMessageData.isInAppMessageShown) {
                cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_IN_APP_MESSAGE_SHOWN, inAppMessageData.mcID)
            }
        }
    }

    private fun isFullscreen(): Boolean {
        return inAppMessage?.type == InAppMessageType.FULLSCREEN
    }

    private fun closeInAppMessage(isDismiss: Boolean = true) {
        isInAppClosed = true
        if (isDismiss)
            cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_IN_APP_MESSAGE_MANUAL_DISMISS, inAppMessage?.mcID)
        inAppMessage?.let { inAppData ->
            InAppMessageProcess.getInstance().onInAppMessageClose(inAppData.mcID)
        }
        finish()
    }

    override fun onPause() {
        overridePendingTransition(0, 0)
        super.onPause()
    }

    override fun onDestroy() {
        if (!isInAppClosed) InAppMessageProcess.getInstance().redisplayTerminatedInAppMessage()
        super.onDestroy()
    }
}