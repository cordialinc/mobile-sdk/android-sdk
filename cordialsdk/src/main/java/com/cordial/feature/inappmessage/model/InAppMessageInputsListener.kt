package com.cordial.feature.inappmessage.model

import com.cordial.feature.sendevent.model.property.PropertyValue

interface InAppMessageInputsListener {
    fun inputsCaptured(eventName: String, properties: Map<String, PropertyValue>?)
}