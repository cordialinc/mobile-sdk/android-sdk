package com.cordial.feature.inappmessage.getinappmessagesdata.repository

import com.cordial.api.CordialApiEndpoints
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler

internal class InAppMessagesDataRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : InAppMessagesDataRepository {

    override fun getInAppMessagesData(onResponseListener: OnResponseListener) {
        val url = cordialApiEndpoints.getInAppMessagesUrl()
        val request = SDKRequest(null, url, RequestMethod.GET)
        requestSender.send(request, responseHandler, onResponseListener)
    }
}