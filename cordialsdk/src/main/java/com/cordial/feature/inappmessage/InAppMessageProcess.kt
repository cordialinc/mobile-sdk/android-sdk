package com.cordial.feature.inappmessage

import android.content.Intent
import android.net.Uri
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.deeplink.CordialDeepLinkProcessService
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.inappmessage.model.InAppMessageData
import com.cordial.feature.inappmessage.model.InAppMessageDelayType
import com.cordial.feature.inappmessage.model.InAppMessageType
import com.cordial.feature.inappmessage.ui.activity.InAppMessageActivity
import com.cordial.feature.inappmessage.ui.banner.InAppMessageBannerAdapter
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.util.ActivityUtils
import com.cordial.util.Delay
import com.cordial.util.TimeUtils


internal class InAppMessageProcess {

    var presentedInAppMessage: String? = null
    private var inAppMessageUseCase =
        CordialApiConfiguration.getInstance().injection.inAppMessageInjection().inAppMessageUseCase
    var bannerAdapter: InAppMessageBannerAdapter? = null

    private fun isInAppMessagePresented(): Boolean {
        return presentedInAppMessage != null
    }

    fun openDeepLink(deepLink: String) {
        val uri = Uri.parse(deepLink)
        val cordialDeepLink = CordialDeepLink(uri, isActivityRestarted = false)
        CordialDeepLinkProcessService().checkOnRedirect(cordialDeepLink)
    }

    fun redisplayTerminatedInAppMessage() {
        presentedInAppMessage = null
        showInAppMessageIfExistAndCanBePresented()
    }

    fun showInAppMessageIfExistAndCanBePresented() {
        if (!isInAppMessagePresented() && isInAppShowEnabled()) {
            inAppMessageUseCase.getLatestInAppMessage { inAppMessageData ->
                inAppMessageData?.let {
                    showInAppMessageIfNotTappedOnPush(inAppMessageData)
                }
            }
        }
    }

    private fun isInAppShowEnabled(): Boolean {
        val inAppMessageDelayMode = CordialApiConfiguration.getInstance().inAppMessageDelayMode
        return when (inAppMessageDelayMode.delayType) {
            InAppMessageDelayType.SHOW -> true
            InAppMessageDelayType.DELAYED_SHOW -> false
            InAppMessageDelayType.ACTIVITIES -> {
                var isEnabled = true
                var topActivity = ActivityUtils.getTopActivity()
                topActivity?.let { activity ->
                    inAppMessageDelayMode.disallowedActivities.let { activities ->
                        isEnabled = !activities.contains(activity::class.java)
                    }
                    topActivity = null
                }
                isEnabled
            }
        }
    }

    fun showInAppMessageIfNotTappedOnPush(inAppMessageData: InAppMessageData) {
        val inAppMessageDisplayDelay =
            (CordialApiConfiguration.getInstance().inAppMessages.displayDelayInSeconds * 1000).toLong()
        Delay(timeMillis = inAppMessageDisplayDelay).doAfterDelay {
            getInAppMessagesToDelete { inAppMessagesMcIDsToDelete ->
                if (inAppMessagesMcIDsToDelete.contains(inAppMessageData.mcID)) {
                    removeInAppMessageToDelete(inAppMessageData.mcID)
                    removeInAppAndShowNext(inAppMessageData)
                    CordialLoggerManager.info("The in-app message was removed after clicking on the related push notification")
                } else {
                    showInAppMessageIfAvailable(inAppMessageData)
                }
            }
        }
    }

    private fun showInAppMessageIfAvailable(inAppMessageData: InAppMessageData) {
        if (inAppMessageData.isInAppMessageAvailable()) {
            showInAppMessage(inAppMessageData)
        } else {
            removeInAppAndShowNext(inAppMessageData)
            CordialLoggerManager.info("The in-app message has expired and will not be displayed")
        }
    }

    private fun removeInAppAndShowNext(inAppMessageData: InAppMessageData) {
        deleteInAppMessageFromCache(inAppMessageData.mcID)
        showInAppMessageIfExistAndCanBePresented()
    }

    private fun showInAppMessage(inAppMessageData: InAppMessageData) {
        if (!isInAppMessagePresented()) {
            CordialLoggerManager.info("Displaying the in-app message with mcID = ${inAppMessageData.mcID}")
            presentedInAppMessage = inAppMessageData.mcID
            when (inAppMessageData.type) {
                InAppMessageType.MODAL -> {
                    showModalInAppMessage(inAppMessageData)
                }
                InAppMessageType.FULLSCREEN -> {
                    showModalInAppMessage(inAppMessageData)
                }
                InAppMessageType.BANNER_UP -> {
                    showBannerInAppMessage(inAppMessageData)
                }
                InAppMessageType.BANNER_BOTTOM -> {
                    showBannerInAppMessage(inAppMessageData)
                }
            }
            inAppMessageUseCase.updateInAppMessageShownData(
                inAppMessageData.mcID,
                isInAppMessageShown = true
            )
        }
    }

    private fun showModalInAppMessage(inAppMessageData: InAppMessageData) {
        val intent = Intent(CordialApiConfiguration.getInstance().getContext(), InAppMessageActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        intent.putExtra(C.IN_APP_MESSAGE, inAppMessageData)
        CordialApiConfiguration.getInstance().getContext().startActivity(intent)
    }

    private fun showBannerInAppMessage(inAppMessageData: InAppMessageData) {
        bannerAdapter = InAppMessageBannerAdapter()
        bannerAdapter?.displayBanner(inAppMessageData)
    }

    private fun InAppMessageData.isInAppMessageAvailable(): Boolean {
        return !TimeUtils.isDateExpired(expirationTime, isMillis = true)
    }

    fun onInAppMessageClose(mcID: String) {
        deleteInAppMessageFromCache(mcID)
        if (isInAppMessagePresented()) {
            presentedInAppMessage = null
            showInAppMessageIfExistAndCanBePresented()
        }
    }

    private fun deleteInAppMessageFromCache(mcID: String) {
        inAppMessageUseCase.deleteInAppIfExist(mcID)
    }

    private fun removeInAppMessageToDelete(mcID: String) {
        inAppMessageUseCase.removeInAppMessageToDelete(mcID)
    }

    private fun getInAppMessagesToDelete(onInAppMessagesMcIDListener: ((mcIDsToDelete: List<String>) -> Unit)?) {
        inAppMessageUseCase.getInAppMessagesToDelete(onInAppMessagesMcIDListener)
    }

    companion object {
        private var INSTANCE: InAppMessageProcess? = null

        fun getInstance(): InAppMessageProcess {
            if (INSTANCE == null) {
                INSTANCE = InAppMessageProcess()
            }
            return INSTANCE as InAppMessageProcess
        }
    }
}