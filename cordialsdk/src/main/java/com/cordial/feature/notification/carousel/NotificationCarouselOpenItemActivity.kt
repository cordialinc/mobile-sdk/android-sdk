package com.cordial.feature.notification.carousel

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cordial.cordialsdk.R
import com.cordial.feature.notification.CordialNotificationPendingIntent
import com.cordial.util.ActivityUtils

class NotificationCarouselOpenItemActivity: AppCompatActivity() {

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        ActivityUtils.setStatusBarTransparent(this)
        if (sendCarouselItemActionBroadcast()) return
        finish()
    }

    private fun sendCarouselItemActionBroadcast(): Boolean {
        val carouselItemAction = NotificationCarouselItemAction.findAction(intent.action) ?: return true
        CordialNotificationPendingIntent.sendNotificationCarouselItemActionBroadcast(this, intent, carouselItemAction)
        return false
    }
}