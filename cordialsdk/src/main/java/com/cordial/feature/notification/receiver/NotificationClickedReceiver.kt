package com.cordial.feature.notification.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.deeplink.CordialDeepLinkProcessService
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.notification.CordialNotificationPendingIntent
import com.cordial.feature.notification.carousel.NotificationCarouselItemAction
import com.cordial.feature.notification.util.PushNotificationPayloadUtils
import com.cordial.util.JsonUtils.optNullableString
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONObject

class NotificationClickedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val remoteMessage = intent?.getParcelableExtra(C.REMOTE_MESSAGE) as RemoteMessage? ?: return
        val isActivityRestarted = intent?.getBooleanExtra(C.IS_ACTIVITY_RESTARTED, true) ?: true
        remoteMessage.data["mcID"]?.let { mcID ->
            saveMcID(mcID)
            checkOnInAppDisplay(remoteMessage)
        }
        sendEvent()
        if (checkOnDeepLink(remoteMessage, isActivityRestarted))
            return
        CordialApiConfiguration.getInstance().pushNotificationListener?.onNotificationClicked(remoteMessage)

        sendCloseNotificationCarouselBroadcast(context, intent)
    }

    private fun sendEvent() {
        val eventName = C.EVENT_NAME_PUSH_NOTIFICATION_APP_OPEN_VIA_TAP
        CordialApi().sendSystemEventWithProperties(eventName)
    }

    private fun saveMcID(mcID: String) {
        CordialApi().setMcID(mcID)
    }

    private fun checkOnDeepLink(remoteMessage: RemoteMessage, isActivityRestarted: Boolean = true): Boolean {
        remoteMessage.data["deepLink"]?.let { deepLinkObject ->
            val deepLink = JSONObject(deepLinkObject)
            var uri: Uri? = null
            deepLink.optNullableString("url")?.let {
                val url = deepLink.getString("url")
                uri = Uri.parse(url)
            }
            var vanityUri: Uri? = null
            deepLink.optNullableString("vanityUrl")?.let {
                val vanityUrl = deepLink.getString("vanityUrl")
                vanityUri = Uri.parse(vanityUrl)
            }
            val cordialDeepLink = CordialDeepLink(uri, vanityUri, isActivityRestarted)
            var fallbackUri: Uri? = null
            deepLink.optNullableString("fallbackUrl")?.let {
                val fallbackUrl = deepLink.getString("fallbackUrl")
                fallbackUri = Uri.parse(fallbackUrl)
            }
            CordialDeepLinkProcessService().checkOnRedirect(cordialDeepLink, fallbackUri)
            return true
        }
        return false
    }

    private fun checkOnInAppDisplay(remoteMessage: RemoteMessage) {
        remoteMessage.data["mcID"]?.let { mcID ->
            val inAppMessagePushPayload = PushNotificationPayloadUtils.getInAppMessageJSONObject(remoteMessage)
            inAppMessagePushPayload?.optNullableString("inactiveSessionDisplay")
                ?.let { inactiveSessionDisplay ->
                    if (inactiveSessionDisplay == "hide-in-app") {
                        val inAppMessageUseCase =
                            CordialApiConfiguration.getInstance().injection.inAppMessageInjection().inAppMessageUseCase
                        inAppMessageUseCase.saveInAppMessageToDelete(mcID)
                    }
                }
        }
    }

    private fun sendCloseNotificationCarouselBroadcast(context: Context?, intent: Intent?) {
        if (context == null) return
        val carouselItemAction = NotificationCarouselItemAction.CLOSE
        CordialNotificationPendingIntent.sendNotificationCarouselItemActionBroadcast(context, intent, carouselItemAction)
    }
}