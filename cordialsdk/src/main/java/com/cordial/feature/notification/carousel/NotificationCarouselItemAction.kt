package com.cordial.feature.notification.carousel

enum class NotificationCarouselItemAction(val action: String) {
    PREVIOUS_ITEM("PREVIOUS_ITEM"),
    OPEN_ITEM("OPEN_ITEM"),
    NEXT_ITEM("NEXT_ITEM"),
    CLOSE("CLOSE");

    companion object {
        fun findAction(action: String?): NotificationCarouselItemAction? {
            return values().firstOrNull { it.action == action }
        }
    }
}