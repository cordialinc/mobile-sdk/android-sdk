package com.cordial.feature.notification

import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.TaskStackBuilder
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.notification.carousel.NotificationCarouselItemAction
import com.cordial.feature.notification.carousel.NotificationCarouselOpenItemActivity
import com.cordial.feature.notification.receiver.CarouselItemActionBroadcastReceiver
import com.cordial.feature.notification.receiver.NotificationClickedReceiver
import com.cordial.feature.notification.receiver.NotificationDismissedReceiver
import com.cordial.feature.notification.ui.NotificationTrampolineActivity
import com.google.firebase.messaging.RemoteMessage

object CordialNotificationPendingIntent {

    @SuppressLint("UnspecifiedImmutableFlag")
    fun createOnClickedIntent(
        context: Context,
        remoteMessage: RemoteMessage,
        notificationId: Int
    ): PendingIntent? {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val notificationClickActivity = CordialApiConfiguration.getInstance().notificationClickActivity
            return if (notificationClickActivity != null) {
                getActivityPendingIntent(
                    context,
                    notificationClickActivity,
                    notificationId,
                    remoteMessage
                )
            } else {
                getPendingIntentWithBackStack(context, notificationId, remoteMessage)
            }
        } else {
            val intent = Intent(context, NotificationClickedReceiver::class.java)
            val notificationIdBundle = Bundle()
            notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
            intent.putExtra(C.BUNDLE, notificationIdBundle)
            intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                PendingIntent.getBroadcast(
                    context.applicationContext,
                    notificationId,
                    intent,
                    PendingIntent.FLAG_IMMUTABLE
                )
            } else {
                PendingIntent.getBroadcast(
                    context.applicationContext,
                    notificationId,
                    intent,
                    0
                )
            }
        }
    }

    private fun getActivityPendingIntent(
        context: Context,
        notificationClickActivity: Class<*>?,
        notificationId: Int,
        remoteMessage: RemoteMessage
    ): PendingIntent? {
        val intent = Intent(context.applicationContext, notificationClickActivity)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        val notificationIdBundle = Bundle()
        notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
        intent.putExtra(C.BUNDLE, notificationIdBundle)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        return PendingIntent.getActivity(
            context,
            0,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_MUTABLE
        )
    }

    private fun getPendingIntentWithBackStack(
        context: Context,
        notificationId: Int,
        remoteMessage: RemoteMessage
    ): PendingIntent? {
        val trampolineActivityIntent =
            Intent(context.applicationContext, NotificationTrampolineActivity::class.java)
        val notificationIdBundle = Bundle()
        notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
        trampolineActivityIntent.putExtra(C.BUNDLE, notificationIdBundle)
        trampolineActivityIntent.putExtra(C.REMOTE_MESSAGE, remoteMessage)

        return TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(trampolineActivityIntent)
            getPendingIntent(notificationId, PendingIntent.FLAG_IMMUTABLE)
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    fun createOnDismissedIntent(context: Context, remoteMessage: RemoteMessage, notificationId: Int): PendingIntent {
        val intent = Intent(context, NotificationDismissedReceiver::class.java)
        val notificationIdBundle = Bundle()
        notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
        intent.putExtra(C.BUNDLE, notificationIdBundle)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getBroadcast(context.applicationContext, notificationId, intent, PendingIntent.FLAG_IMMUTABLE)
        } else {
            PendingIntent.getBroadcast(context.applicationContext, notificationId, intent, 0)
        }
    }

    fun sendNotificationCarouselItemActionBroadcast(
        context: Context,
        intent: Intent?,
        carouselItemAction: NotificationCarouselItemAction
    ) {
        val bundle = intent?.extras?.getBundle(C.BUNDLE) ?: return
        val notificationId = bundle.getInt(C.NOTIFICATION_ID)
        val carouselIntent = Intent("${C.NOTIFICATION_CAROUSEL_ITEM_ACTION}_$notificationId")
        val carouselBundle = Bundle()
        carouselBundle.putString(C.NOTIFICATION_CAROUSEL_ITEM_ACTION, carouselItemAction.action)
        carouselIntent.putExtra(C.BUNDLE, carouselBundle)
        context.sendBroadcast(carouselIntent)
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    fun setNotificationCarouselActionPendingIntent(
        context: Context,
        intentAction: String,
        notificationId: Int
    ): PendingIntent? {
        val intent = Intent(context, CarouselItemActionBroadcastReceiver::class.java)
        val notificationIdBundle = Bundle()
        notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
        intent.putExtra(C.BUNDLE, notificationIdBundle)
        intent.action = intentAction
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getBroadcast(
                context.applicationContext,
                notificationId,
                intent,
                PendingIntent.FLAG_IMMUTABLE
            )
        } else {
            PendingIntent.getBroadcast(context.applicationContext, notificationId, intent, 0)
        }
    }

    @SuppressLint("UnspecifiedImmutableFlag")
    fun setNotificationCarouselItemOpenActionPendingIntent(
        context: Context,
        intentAction: String,
        notificationId: Int
    ): PendingIntent? {
        val openActivityIntent = Intent(context.applicationContext, NotificationCarouselOpenItemActivity::class.java)
        openActivityIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val notificationIdBundle = Bundle()
        notificationIdBundle.putInt(C.NOTIFICATION_ID, notificationId)
        openActivityIntent.putExtra(C.BUNDLE, notificationIdBundle)
        openActivityIntent.action = intentAction
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PendingIntent.getActivity(
                context.applicationContext,
                notificationId,
                openActivityIntent,
                PendingIntent.FLAG_IMMUTABLE
            )
        } else {
            PendingIntent.getActivity(context.applicationContext, notificationId, openActivityIntent, 0)
        }
    }

    fun unregisterBroadcastReceiver(context: Context?, broadcastReceiver: BroadcastReceiver?) {
        if (broadcastReceiver != null) context?.unregisterReceiver(broadcastReceiver)
    }
}