package com.cordial.feature.notification

import android.content.Context
import android.os.Build
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.notification.util.PushNotificationPayloadUtils.isInAppMessage
import com.cordial.feature.notification.util.PushNotificationPayloadUtils.isInboxMessage
import com.cordial.feature.notification.util.PushNotificationPayloadUtils.isPushNotification
import com.cordial.lifecycle.AppLifecycleHandler
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import org.json.JSONObject

class CordialNotificationProcessService {

    private val config = CordialApiConfiguration.getInstance()

    fun processNewToken(context: Context, token: String?) {
        val preferences = Preferences(context)
        CordialLoggerManager.info("Firebase token = $token")
        token?.let {
            if (token != preferences.getString(PreferenceKeys.FIREBASE_TOKEN, "")) {
                preferences.put(PreferenceKeys.FIREBASE_TOKEN, token)
                val cordialApi = CordialApi()
                cordialApi.internalSetContact(cordialApi.getPrimaryKey())
            }
            config.pushNotificationListener?.onFcmTokenReceived(token)
        }
        sendEnabledNotificationCategories(context)
    }

    private fun sendEnabledNotificationCategories(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationCategoryUseCase =
                config.injection.notificationCategoryInjection().notificationCategoryUseCase
            notificationCategoryUseCase.sendEnabledNotificationCategories(context)
        }
    }

    fun isCordialMessage(remoteMessage: RemoteMessage): Boolean {
        return remoteMessage.data["mcID"] != null
    }

    fun processMessage(context: Context, remoteMessage: RemoteMessage) {
        val jsonObject = JSONObject(remoteMessage.data.toProperties() as MutableMap<Any?, Any?>)
        CordialLoggerManager.debug("Firebase push notification message = $jsonObject")

        checkOnInboxMessage(remoteMessage)
        checkOnInApp(remoteMessage)
        checkOnPushNotification(context, remoteMessage)
    }

    private fun checkOnInboxMessage(remoteMessage: RemoteMessage) {
        if (isInboxMessage(remoteMessage)) {
            processInboxMessage(remoteMessage)
        }
    }

    private fun processInboxMessage(remoteMessage: RemoteMessage) {
        val mcID = remoteMessage.data["mcID"]
        mcID?.let {
            val scope = CoroutineScope(SupervisorJob() + Dispatchers.Main)
            scope.launch {
                config.inboxMessageListener?.newInboxMessageDelivered(mcID)
            }
        }
    }

    private fun checkOnInApp(remoteMessage: RemoteMessage) {
        if (isInAppMessage(remoteMessage)) {
            val inAppMessagesDataUseCase = config.injection
                .inAppMessageDataInjection().inAppMessageDataUseCase
            inAppMessagesDataUseCase.getInAppMessagesData()
        }
    }

    private fun checkOnPushNotification(context: Context, remoteMessage: RemoteMessage) {
        if (AppLifecycleHandler.isAppInForeground() && isInAppMessage(remoteMessage)) {
            CordialLoggerManager.info("Push notification is not displayed because the application is in the foreground and an in-app message is displayed")
            return
        }
        if (AppLifecycleHandler.isAppInForeground() && !config.showPushInForeground) {
            CordialLoggerManager.info("Push notification is not displayed because the application is in the foreground and the showPushInForeground property is set to false in CordialApiConfiguration")
            return
        }
        if (isPushNotification(remoteMessage)) {
            sendEvent(remoteMessage)
            onNotificationReceived(remoteMessage)
            showNotification(context, remoteMessage)
        }
    }

    private fun sendEvent(remoteMessage: RemoteMessage?) {
        val eventName =
            if (AppLifecycleHandler.isAppInForeground()) C.EVENT_NAME_PUSH_NOTIFICATION_DELIVERED_FOREGROUND
            else C.EVENT_NAME_PUSH_NOTIFICATION_DELIVERED_BACKGROUND
        CordialApi().sendSystemEventWithProperties(eventName, remoteMessage?.data?.get("mcID"))
    }

    private fun onNotificationReceived(remoteMessage: RemoteMessage) {
        config.pushNotificationListener?.onNotificationReceived(
            remoteMessage,
            AppLifecycleHandler.isAppInForeground()
        )
    }

    private fun showNotification(context: Context, remoteMessage: RemoteMessage) {
        val cordialNotification = CordialNotification()
        cordialNotification.show(context, remoteMessage)
    }
}