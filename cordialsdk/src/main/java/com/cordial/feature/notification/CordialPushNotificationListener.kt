package com.cordial.feature.notification

import com.google.firebase.messaging.RemoteMessage

interface CordialPushNotificationListener {
    fun onNotificationReceived(remoteMessage: RemoteMessage, isReceivedInForeground: Boolean)
    fun onNotificationClicked(remoteMessage: RemoteMessage)
    fun onNotificationDismissed(remoteMessage: RemoteMessage)
    fun onFcmTokenReceived(token: String)
}