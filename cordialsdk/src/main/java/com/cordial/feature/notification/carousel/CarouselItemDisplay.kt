package com.cordial.feature.notification.carousel

import android.graphics.Bitmap
import java.io.Serializable

data class CarouselItemDisplay(
    val bitmap: Bitmap,
    val imageUrl: String?,
    val deepLink: String?
): Serializable