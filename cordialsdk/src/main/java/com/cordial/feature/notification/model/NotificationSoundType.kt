package com.cordial.feature.notification.model

enum class NotificationSoundType {
    DEFAULT,
    CUSTOM_SOUND,
    SILENT
}