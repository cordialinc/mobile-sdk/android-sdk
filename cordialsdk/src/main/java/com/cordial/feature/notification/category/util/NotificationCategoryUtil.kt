package com.cordial.feature.notification.category.util

import com.cordial.feature.notification.model.NotificationCategory
import org.json.JSONArray
import org.json.JSONObject

object NotificationCategoryUtil {
    const val NOTIFICATION_CATEGORIES = "notification_categories"
    private const val NOTIFICATION_CATEGORY_ID = "notification_category_id"
    private const val NOTIFICATION_CATEGORY_NAME = "notification_category_name"
    private const val NOTIFICATION_CATEGORY_STATE = "notification_category_state"
    private const val NOTIFICATION_CATEGORY_DESCRIPTION = "notification_category_description"

    fun notificationCategoriesToJson(categories: List<NotificationCategory>): String {
        val jsonArray = JSONArray()
        categories.forEach { category ->
            val jsonObject = JSONObject()
            jsonObject.put(NOTIFICATION_CATEGORY_ID, category.id)
            jsonObject.put(NOTIFICATION_CATEGORY_NAME, category.name)
            jsonObject.put(NOTIFICATION_CATEGORY_STATE, category.state)
            category.description?.let { description ->
                jsonObject.put(NOTIFICATION_CATEGORY_DESCRIPTION, description)
            }
            jsonArray.put(jsonObject)
        }
        return jsonArray.toString()
    }

    fun getNotificationCategoriesFromJson(json: String?): List<NotificationCategory>? {
        val categories = mutableListOf<NotificationCategory>()
        if (json == null)
            return null
        val jsonArray = JSONArray(json)
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val id = jsonObject.getString(NOTIFICATION_CATEGORY_ID)
            val name = jsonObject.getString(NOTIFICATION_CATEGORY_NAME)
            val state = jsonObject.getBoolean(NOTIFICATION_CATEGORY_STATE)
            val description = jsonObject.getString(NOTIFICATION_CATEGORY_DESCRIPTION)
            categories.add(NotificationCategory(id, name, state, description))
        }
        if (categories.isEmpty())
            return null
        return categories
    }
}