package com.cordial.feature.notification

import android.content.Context
import android.graphics.Bitmap
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.notification.carousel.Carousel
import com.cordial.feature.notification.model.NotificationSoundType
import com.cordial.feature.notification.util.PushNotificationPayloadUtils
import com.cordial.util.BitmapUtils
import com.cordial.util.downloadImage
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import java.util.*

class CordialNotification {

    fun show(context: Context, remoteMessage: RemoteMessage) {
        try {
            val notificationChannelID = getNotificationChannelID(context, remoteMessage)
            val notificationId = generateNotificationId()
            val scope = CoroutineScope(SupervisorJob() + Dispatchers.Main)
            scope.launch {
                val bitmap = downloadImage(remoteMessage.data["imageURL"])
                val compressedBitmap = BitmapUtils.compressBitmap(context, remoteMessage.data["imageURL"], bitmap)
                val builder = NotificationCompat.Builder(context, notificationChannelID)
                    .setSmallIcon(getNotificationIcon(context))
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentIntent(
                        CordialNotificationPendingIntent.createOnClickedIntent(
                            context,
                            remoteMessage,
                            notificationId
                        )
                    )
                    .setDeleteIntent(
                        CordialNotificationPendingIntent.createOnDismissedIntent(
                            context,
                            remoteMessage,
                            notificationId
                        )
                    )
                    .setAutoCancel(true)

                if (!PushNotificationPayloadUtils.isCarouselNotification(remoteMessage)) {
                    buildSimpleNotification(builder, remoteMessage, compressedBitmap)
                } else {
                    val carousel = Carousel(builder)
                    carousel.addCarouselToNotification(context, remoteMessage, compressedBitmap, notificationId)
                }
                setNotificationSound(context, remoteMessage, builder)
                with(NotificationManagerCompat.from(context)) {
                    notify(notificationId, builder.build())
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun getNotificationChannelID(context: Context, remoteMessage: RemoteMessage): String {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(context, remoteMessage)
        } else {
            C.NOTIFICATION_CHANNEL_ID
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(context: Context, remoteMessage: RemoteMessage): String {
        val cordialNotificationChannel = CordialNotificationChannel()
        val notificationChannelId = cordialNotificationChannel.createNotificationChannel(context, remoteMessage)

        return notificationChannelId
    }


    private fun setNotificationSound(
        context: Context,
        remoteMessage: RemoteMessage,
        builder: NotificationCompat.Builder,
    ) {
        val cordialNotificationChannel = CordialNotificationChannel()
        when (cordialNotificationChannel.getNotificationSoundType(remoteMessage)) {
            NotificationSoundType.CUSTOM_SOUND -> {
                val soundUri = cordialNotificationChannel.getNotificationSoundUri(context, remoteMessage)
                if (soundUri != null) builder.setSound(soundUri)
            }
            NotificationSoundType.SILENT -> builder.setSound(null)
            NotificationSoundType.DEFAULT -> {}
        }
    }

    private fun getNotificationIcon(context: Context): Int {
        val appLaunchIcon = context.applicationInfo.icon
        return CordialApiConfiguration.getInstance().pushNotificationIconSilhouette ?: appLaunchIcon
    }

    private fun generateNotificationId(): Int {
        return Random().nextInt(1000000000)
    }


    fun buildSimpleNotification(builder: NotificationCompat.Builder, remoteMessage: RemoteMessage, bitmap: Bitmap?) {
        val title = PushNotificationPayloadUtils.getNotificationTitle(remoteMessage)
        val subtitle = PushNotificationPayloadUtils.getNotificationSubtitle(remoteMessage)
        val body = PushNotificationPayloadUtils.getNotificationBody(remoteMessage)
        val bigLargeIconBitmap: Bitmap? = null
        builder.setLargeIcon(bitmap)
            .setContentTitle(title)
            .setSubText(subtitle)
            .setContentText(body)
            .setStyle(NotificationCompat.BigTextStyle().bigText(body))
            .setStyle(NotificationCompat.BigPictureStyle().bigPicture(bitmap).bigLargeIcon(bigLargeIconBitmap))
    }


    fun update(context: Context?, notificationId: Int, builder: NotificationCompat.Builder) {
        if (context == null) return
        with(NotificationManagerCompat.from(context)) {
            notify(notificationId, builder.build())
        }
    }
}