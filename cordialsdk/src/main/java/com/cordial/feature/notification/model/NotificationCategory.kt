package com.cordial.feature.notification.model

data class NotificationCategory(
    val id: String,
    val name: String,
    val state: Boolean,
    val description: String? = null
)