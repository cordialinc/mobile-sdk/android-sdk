package com.cordial.feature.notification.category.usecase

import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.text.TextUtils
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.feature.notification.CordialNotificationChannel
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.model.NotificationCategoryId
import com.cordial.feature.upsertcontact.model.attributes.ArrayValue
import com.cordial.feature.upsertcontact.model.attributes.AttributeValue
import com.cordial.storage.db.dao.notification.category.NotificationCategoryDao
import java.util.stream.Collectors

@RequiresApi(Build.VERSION_CODES.O)
internal class NotificationCategoryUseCaseImpl(private val notificationCategoryDao: NotificationCategoryDao?) :
    NotificationCategoryUseCase {

    override fun setNotificationCategories(context: Context, categories: List<NotificationCategory>) {
        getCategories { storedCategories ->
            if (storedCategories.isEmpty()) {
                storeCategoriesCreateNotificationChannels(categories, context)
            } else {
                checkOnCategoriesChange(storedCategories, categories, context)
            }
        }
    }

    private fun storeCategoriesCreateNotificationChannels(
        categories: List<NotificationCategory>,
        context: Context,
        onStoreCategoriesListener: (() -> Unit)? = null
    ) {
        val categoriesUpdatedIds = updateNotificationCategoryIds(categories)
        storeCategories(categoriesUpdatedIds) {
            createNotificationChannels(context, categoriesUpdatedIds)
            onStoreCategoriesListener?.invoke()
        }
    }

    private fun checkOnCategoriesChange(
        storedCategories: List<NotificationCategory>,
        categories: List<NotificationCategory>,
        context: Context
    ) {
        val newCategories = getNewCategories(storedCategories, categories)
        if (newCategories.isNotEmpty()) {
            storeCategoriesCreateNotificationChannels(newCategories, context) {
                sendEnabledNotificationCategories(context)
            }
        }

        val deletedCategories = getDeletedCategories(storedCategories, categories)
        if (deletedCategories.isNotEmpty()) {
            deleteNotificationCategories(context, deletedCategories) {
                sendEnabledNotificationCategories(context)
            }
        }
    }

    private fun updateNotificationCategoryIds(categories: List<NotificationCategory>): List<NotificationCategory> {
        return categories.map { category ->
            val id = NotificationCategoryId(category.id).toString()
            NotificationCategory(id, category.name, category.state, category.description)
        }
    }

    private fun createNotificationChannels(
        context: Context,
        categories: List<NotificationCategory>
    ) {
        val cordialNotificationChannel = CordialNotificationChannel()
        cordialNotificationChannel.createNotificationCategories(context, categories)
    }

    override fun checkNotificationCategoriesStatus(context: Context) {
        getCategories { categories ->
            val changedCategories = categories.filter { it.state != isNotificationChannelEnabled(context, it.id) }
            val enabledCategories = changedCategories.map {
                NotificationCategory(it.id, it.name, isNotificationChannelEnabled(context, it.id), it.description)
            }
            if (enabledCategories.isNotEmpty()) {
                storeCategories(enabledCategories) {
                    sendEnabledNotificationCategories(context, categories)
                }
            }
        }
    }

    override fun sendEnabledNotificationCategories(context: Context) {
        getCategories { categories ->
            sendEnabledNotificationCategories(context, categories)
        }
    }

    private fun sendEnabledNotificationCategories(context: Context, categories: List<NotificationCategory>) {
        val enabledCategories = categories.filter { isNotificationChannelEnabled(context, it.id) }
        val categoriesIdList = enabledCategories.map { category -> category.id }
        val parsedCategoriesIdList = categoriesIdList.map { NotificationCategoryId.from(it).id }
        sendNotificationCategoriesAsAttributes(parsedCategoriesIdList)
    }

    private fun isNotificationChannelEnabled(context: Context, channelId: String?): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (!TextUtils.isEmpty(channelId)) {
                val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val channel = manager.getNotificationChannel(channelId)
                return channel.importance != NotificationManager.IMPORTANCE_NONE
            }
            false
        } else {
            NotificationManagerCompat.from(context).areNotificationsEnabled()
        }
    }

    private fun sendNotificationCategoriesAsAttributes(categoriesIdList: List<String>) {
        val attributes = HashMap<String, AttributeValue>()
        attributes[C.NOTIFICATION_CATEGORIES_ATTRIBUTE_ID] = ArrayValue(categoriesIdList.toTypedArray())
        CordialApi().upsertContact(attributes)
    }

    private fun getNewCategories(
        storedCategories: List<NotificationCategory>,
        categories: List<NotificationCategory>
    ): List<NotificationCategory> {
        val storedCategoriesIds = storedCategories.map { it.id }
        val parsedStoredCategoriesIds =
            storedCategoriesIds.map { NotificationCategoryId.from(it).id }
        val newCategories = categories.stream()
            .filter { category -> !parsedStoredCategoriesIds.contains(category.id) }
            .collect(Collectors.toList())
        return newCategories
    }

    private fun getDeletedCategories(
        storedCategories: List<NotificationCategory>,
        categories: List<NotificationCategory>
    ): List<NotificationCategory> {
        val categoriesIds = categories.map { it.id }
        val deletedCategories = storedCategories.stream()
            .filter { category ->
                val parsedCategoryId = NotificationCategoryId.from(category.id).id
                !categoriesIds.contains(parsedCategoryId)
            }
            .collect(Collectors.toList())
        return deletedCategories
    }

    private fun deleteNotificationCategories(
        context: Context,
        categories: List<NotificationCategory>,
        onDeleteCategoriesListener: (() -> Unit)
    ) {
        categories.forEach { category ->
            deleteCategory(category.id) {
                val cordialNotificationChannel = CordialNotificationChannel()
                cordialNotificationChannel.unregisterNotificationChannel(context, category.id)
                onDeleteCategoriesListener.invoke()
            }
        }
    }

    override fun storeCategories(categories: Iterable<NotificationCategory>, onStoreCategoriesListener: (() -> Unit)?) {
        notificationCategoryDao?.insert(categories, onStoreCategoriesListener)
    }

    override fun getCategories(onCategoriesListener: (categories: List<NotificationCategory>) -> Unit) {
        notificationCategoryDao?.getNotificationCategories(onCategoriesListener)
    }

    override fun clearCategories(onClearCategoriesListener: (() -> Unit)?) {
        notificationCategoryDao?.clear(onClearCategoriesListener)
    }

    override fun deleteCategory(categoryId: String, onDeleteCategoryListener: (() -> Unit)?) {
        notificationCategoryDao?.deleteNotificationCategory(categoryId, onDeleteCategoryListener)
    }
}