package com.cordial.feature.notification.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.notification.CordialNotificationPendingIntent
import com.cordial.feature.notification.carousel.NotificationCarouselItemAction
import com.google.firebase.messaging.RemoteMessage

class NotificationDismissedReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val remoteMessage = intent?.getParcelableExtra(C.REMOTE_MESSAGE) as RemoteMessage? ?: return
        val eventName = C.EVENT_NAME_PUSH_NOTIFICATION_DISMISSED
        val mcID = remoteMessage.data["mcID"]
        CordialApi().sendSystemEventWithProperties(eventName, mcID)
        CordialApiConfiguration.getInstance().pushNotificationListener?.onNotificationDismissed(remoteMessage)
        checkInAppDisplayOnPushDismiss(mcID)
        sendCloseNotificationCarouselBroadcast(context, intent)
    }

    private fun sendCloseNotificationCarouselBroadcast(context: Context?, intent: Intent?) {
        if (context == null) return
        val carouselItemAction = NotificationCarouselItemAction.CLOSE
        CordialNotificationPendingIntent.sendNotificationCarouselItemActionBroadcast(context, intent, carouselItemAction)
    }

    private fun checkInAppDisplayOnPushDismiss(mcID: String?) {
        val showOnPushDismiss = CordialApiConfiguration.getInstance().inAppMessages.showOnPushDismiss
        if (!showOnPushDismiss) {
            mcID?.let {
                val inAppMessageUseCase =
                    CordialApiConfiguration.getInstance().injection.inAppMessageInjection().inAppMessageUseCase
                inAppMessageUseCase.saveInAppMessageToDelete(mcID)
            }
        }
    }
}