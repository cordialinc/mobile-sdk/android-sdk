package com.cordial.feature.notification.carousel

data class CarouselItemDownload(
    val imageURL: String?,
    val deepLink: String?
)