package com.cordial.feature.notification

import android.content.Context

interface OnNotificationStatusListener {
    fun areNotificationsEnabled(context: Context): Boolean
}