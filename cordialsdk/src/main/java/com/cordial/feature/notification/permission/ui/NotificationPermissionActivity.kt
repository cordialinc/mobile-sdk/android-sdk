package com.cordial.feature.notification.permission.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.cordialsdk.databinding.ActivityEmptyBinding
import com.cordial.feature.notification.category.util.NotificationCategoryUtil
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings
import com.cordial.feature.notification.permission.model.EducationalUiModeEnum
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.ActivityUtils
import com.cordial.util.serializable

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
internal class NotificationPermissionActivity : AppCompatActivity() {
    private var educationalUiMode: EducationalUiModeEnum? = null
    private var educationalUiSettings: NotificationPermissionEducationalUISettings? = null
    private var notificationCategories: List<NotificationCategory>? = null
    lateinit var preferences: Preferences
    private val activityLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            handleActivityResult(result)
        }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { result ->
            if (!result) incrementNotificationPermissionDeniedCount()
            CordialApiConfiguration.getInstance().checkNotificationStatus()
            finish()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityEmptyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        preferences = Preferences(this)
        ActivityUtils.setStatusBarTransparent(this)

        getDataFromIntent()
        checkNotificationPermission()
    }

    private fun getDataFromIntent() {
        val bundle = intent?.extras?.getBundle(C.BUNDLE)
        bundle?.let {
            educationalUiMode = CordialApiConfiguration.getInstance().educationalUiMode
            educationalUiSettings =
                bundle.serializable(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SETTINGS) as NotificationPermissionEducationalUISettings?
            val categoriesBundle = bundle.serializable<String>(NotificationCategoryUtil.NOTIFICATION_CATEGORIES)
            notificationCategories = NotificationCategoryUtil.getNotificationCategoriesFromJson(categoriesBundle)
        }
    }

    private fun checkNotificationPermission() {
        val notificationPermissionDeniedCount = getNotificationPermissionDeniedCount()
        if (notificationPermissionDeniedCount >= 2) {
            finish()
            return
        }
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS)
                    == PackageManager.PERMISSION_GRANTED -> {
                finish()
            }
            shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS) -> {
                if (educationalUiMode != EducationalUiModeEnum.NONE) {
                    checkNotificationPermissionOnRequest()
                } else {
                    requestNotificationPermission()
                }
            }
            educationalUiMode != EducationalUiModeEnum.NONE -> {
                checkNotificationPermissionOnRequest()
            }
            else -> {
                requestNotificationPermission()
            }
        }
    }

    private fun checkNotificationPermissionOnRequest() {
        val isShowedBefore = isEducationalUiShowedBefore()
        if (!isShowedBefore || educationalUiSettings?.showUntilAllowed == true)
            showNotificationPermissionEducationalActivity()
        else finish()
    }

    private fun isEducationalUiShowedBefore() =
        preferences.getBoolean(PreferenceKeys.IS_NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SHOWED, false)

    private fun saveEducationalUiIsShowed() =
        preferences.put(PreferenceKeys.IS_NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SHOWED, true)

    private fun getNotificationPermissionDeniedCount() =
        preferences.getInt(PreferenceKeys.NOTIFICATION_PERMISSION_DENIED_COUNT, 0)

    private fun incrementNotificationPermissionDeniedCount() {
        var count = preferences.getInt(PreferenceKeys.NOTIFICATION_PERMISSION_DENIED_COUNT, 0)
        count++
        if (count <= 2) preferences.put(PreferenceKeys.NOTIFICATION_PERMISSION_DENIED_COUNT, count)
    }

    private fun requestNotificationPermission() {
        requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
    }

    private fun showNotificationPermissionEducationalActivity() {
        val intent = Intent(this, NotificationPermissionEducationUIActivity::class.java)
        val bundle = Bundle()
        educationalUiMode?.mode?.let { mode ->
            bundle.putString(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_MODE, mode)
        }
        educationalUiSettings?.let { settings ->
            bundle.putSerializable(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SETTINGS, settings)
        }
        notificationCategories?.let { categories ->
            val categoriesJson = NotificationCategoryUtil.notificationCategoriesToJson(categories)
            bundle.putSerializable(NotificationCategoryUtil.NOTIFICATION_CATEGORIES, categoriesJson)
        }
        intent.putExtra(C.BUNDLE, bundle)
        activityLauncher.launch(intent)
        saveEducationalUiIsShowed()
    }

    private fun handleActivityResult(result: ActivityResult) {
        when (result.resultCode) {
            Activity.RESULT_OK -> {
                requestNotificationPermission()
            }
            Activity.RESULT_CANCELED -> {
                finish()
            }
            else -> {
                finish()
            }
        }
    }
}