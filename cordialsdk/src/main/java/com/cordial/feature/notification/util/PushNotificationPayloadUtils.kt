package com.cordial.feature.notification.util

import com.cordial.util.trimExtension
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONArray
import org.json.JSONObject

object PushNotificationPayloadUtils {

    fun getInAppMessageJSONObject(remoteMessage: RemoteMessage): JSONObject? {
        val iamJsonObject: JSONObject?
        val system = remoteMessage.data["system"]
        if (system != null) {
            val systemJsonObject = JSONObject(system)
            iamJsonObject = systemJsonObject.optJSONObject("iam")
        } else {
            iamJsonObject = JSONObject()
            iamJsonObject.put("type", remoteMessage.data["type"])
            iamJsonObject.put("expirationTime", remoteMessage.data["expirationTime"])
            iamJsonObject.put(
                "inactiveSessionDisplay",
                remoteMessage.data["inactiveSessionDisplay"]
            )
        }
        return iamJsonObject
    }

    fun isInboxMessage(remoteMessage: RemoteMessage): Boolean {
        val system = remoteMessage.data["system"]
        return if (system != null) {
            val systemJsonObject = JSONObject(system)
            systemJsonObject.optJSONObject("inbox") != null
        } else {
            false
        }
    }

    fun isInAppMessage(remoteMessage: RemoteMessage): Boolean {
        val system = remoteMessage.data["system"]
        return if (system != null) {
            val systemJsonObject = JSONObject(system)
            systemJsonObject.optJSONObject("iam") != null
        } else {
            false
        }
    }

    fun isPushNotification(remoteMessage: RemoteMessage): Boolean {
        return remoteMessage.data["title"] != null || remoteMessage.data["subtitle"] != null
                || remoteMessage.data["body"] != null
    }

    fun getNotificationSoundName(remoteMessage: RemoteMessage): String {
        val soundName = remoteMessage.data["sound"] ?: ""
        return soundName.trimExtension()
    }

    fun isCarouselNotification(remoteMessage: RemoteMessage): Boolean {
        return remoteMessage.data["carousel"] != null
    }

    fun getNotificationCarouselJson(remoteMessage: RemoteMessage): JSONArray {
        val carouselJsonString = remoteMessage.data["carousel"] ?: ""
        return JSONArray(carouselJsonString)
    }

    fun getNotificationTitle(remoteMessage: RemoteMessage): String? {
        return remoteMessage.data["title"]
    }

    fun getNotificationSubtitle(remoteMessage: RemoteMessage): String? {
        return remoteMessage.data["subtitle"]
    }

    fun getNotificationBody(remoteMessage: RemoteMessage): String? {
        return remoteMessage.data["body"]
    }
}