package com.cordial.feature.notification.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.cordial.api.C
import com.cordial.cordialsdk.databinding.ActivityEmptyBinding
import com.cordial.feature.notification.receiver.NotificationClickedReceiver
import com.cordial.util.ActivityUtils
import com.google.firebase.messaging.RemoteMessage

class NotificationTrampolineActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityEmptyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        ActivityUtils.setStatusBarTransparent(this)

        sendNotificationClickBroadcast()
        finish()
    }

    private fun sendNotificationClickBroadcast() {
        val remoteMessage = intent?.extras?.getParcelable<RemoteMessage?>(C.REMOTE_MESSAGE) ?: return
        val intent = Intent(this, NotificationClickedReceiver::class.java)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        sendBroadcast(intent)
    }
}