package com.cordial.feature.notification.permission.usecase

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.cordial.api.C
import com.cordial.feature.notification.category.usecase.NotificationCategoryUseCase
import com.cordial.feature.notification.category.util.NotificationCategoryUtil
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings
import com.cordial.feature.notification.permission.ui.NotificationPermissionActivity

internal class NotificationPermissionUseCaseImpl(
    private val notificationCategoryUseCase: NotificationCategoryUseCase
) : NotificationPermissionUseCase {

    override fun requestNotificationPermission(
        context: Context,
        educationalUiSettings: NotificationPermissionEducationalUISettings?
    ) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.POST_NOTIFICATIONS) !=
                PackageManager.PERMISSION_GRANTED
            ) {
                notificationCategoryUseCase.getCategories { categories ->
                    val intent = Intent(context, NotificationPermissionActivity::class.java)
                    val bundle = Bundle()
                    educationalUiSettings?.let { educationalUi ->
                        bundle.putSerializable(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SETTINGS, educationalUi)
                    }
                    if (categories.isNotEmpty()) {
                        val categoriesJson = NotificationCategoryUtil.notificationCategoriesToJson(categories)
                        bundle.putSerializable(NotificationCategoryUtil.NOTIFICATION_CATEGORIES, categoriesJson)
                    }
                    intent.putExtra(C.BUNDLE, bundle)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    context.startActivity(intent)
                }
            }
        }
    }
}