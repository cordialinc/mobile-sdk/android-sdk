package com.cordial.feature.notification.category.usecase

import android.content.Context
import com.cordial.feature.notification.model.NotificationCategory

interface NotificationCategoryUseCase {
    fun setNotificationCategories(context: Context, categories: List<NotificationCategory>)
    fun checkNotificationCategoriesStatus(context: Context)
    fun sendEnabledNotificationCategories(context: Context)
    fun storeCategories(categories: Iterable<NotificationCategory>, onStoreCategoriesListener: (() -> Unit)? = null)
    fun getCategories(onCategoriesListener: (categories: List<NotificationCategory>) -> Unit)
    fun clearCategories(onClearCategoriesListener: (() -> Unit)? = null)
    fun deleteCategory(categoryId: String, onDeleteCategoryListener: (() -> Unit)? = null)
}