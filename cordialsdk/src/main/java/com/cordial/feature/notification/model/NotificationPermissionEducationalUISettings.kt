package com.cordial.feature.notification.model

import java.io.Serializable

data class NotificationPermissionEducationalUISettings(
    val layoutId: Int?,
    val color: NotificationPermissionEducationalUiColor?,
    val showUntilAllowed: Boolean = false
): Serializable