package com.cordial.feature.notification.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.cordial.feature.notification.CordialNotificationPendingIntent
import com.cordial.feature.notification.carousel.NotificationCarouselItemAction

internal class CarouselItemActionBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context == null) return
        val actionString = intent?.action
        val action = NotificationCarouselItemAction.findAction(actionString) ?: return
        CordialNotificationPendingIntent.sendNotificationCarouselItemActionBroadcast(context, intent, action)
    }
}