package com.cordial.feature.notification

enum class PushesConfiguration(val type: String) {
    SDK("sdk"),
    APP("app");

    companion object {
        fun findKey(type: String): PushesConfiguration {
            return values().firstOrNull { it.type == type } ?: APP
        }
    }
}