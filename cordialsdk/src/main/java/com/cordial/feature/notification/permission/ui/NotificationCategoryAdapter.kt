package com.cordial.feature.notification.permission.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cordial.cordialsdk.databinding.ItemNotificationCategoryBinding
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.model.NotificationPermissionEducationalUiColor

class NotificationCategoryAdapter(
    val items: List<NotificationCategory>,
    private val color: NotificationPermissionEducationalUiColor?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemNotificationCategoryBinding =
            ItemNotificationCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NotificationCategoryVH(itemNotificationCategoryBinding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NotificationCategoryVH).bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class NotificationCategoryVH(private val itemNotificationCategoryBinding: ItemNotificationCategoryBinding) :
        RecyclerView.ViewHolder(itemNotificationCategoryBinding.root) {

        fun bind(notificationCategoryItem: NotificationCategory) {
            with(itemNotificationCategoryBinding) {
                tvItem.text = notificationCategoryItem.name
                tvItemDesc.text = notificationCategoryItem.description
                cbItem.isChecked = notificationCategoryItem.state
                color?.let { color ->
                    tvItem.setTextColor(ContextCompat.getColor(itemView.context, color.primary))
                    tvItemDesc.setTextColor(ContextCompat.getColor(itemView.context, color.primaryLight))
                    cbItem.buttonTintList = ContextCompat.getColorStateList(itemView.context, color.accent)
                }
            }
        }

        fun isCategoryEnabled(): Boolean {
            return itemNotificationCategoryBinding.cbItem.isChecked
        }
    }
}