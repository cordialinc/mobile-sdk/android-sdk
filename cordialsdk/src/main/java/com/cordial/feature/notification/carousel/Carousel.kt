package com.cordial.feature.notification.carousel

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.net.Uri
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import com.cordial.api.C
import com.cordial.cordialsdk.R
import com.cordial.feature.deeplink.CordialDeepLinkProcessService
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.notification.CordialNotificationPendingIntent
import com.cordial.feature.notification.CordialNotification
import com.cordial.feature.notification.util.PushNotificationPayloadUtils.getNotificationBody
import com.cordial.feature.notification.util.PushNotificationPayloadUtils.getNotificationTitle
import com.cordial.util.castTo
import com.google.firebase.messaging.RemoteMessage

class Carousel(val builder: NotificationCompat.Builder) {

    suspend fun addCarouselToNotification(
        context: Context,
        remoteMessage: RemoteMessage,
        bitmap: Bitmap?,
        notificationId: Int
    ) {
        val carouselItems = CarouselCompress().getCarouselItems(context, remoteMessage)
        if (carouselItems.isNotEmpty()) {
            val collapsedView = getNotificationCollapsedView(context, remoteMessage, bitmap)
            val carouselView = getNotificationCarouselView(context, remoteMessage, carouselItems, notificationId)
            builder.setCustomContentView(collapsedView)
                .setSubText(remoteMessage.data["subtitle"])
                .setStyle(NotificationCompat.DecoratedCustomViewStyle())
                .setOnlyAlertOnce(true)
                .setCustomBigContentView(carouselView)
        } else {
            CordialNotification().buildSimpleNotification(builder, remoteMessage, bitmap)
        }
    }

    private fun getNotificationCollapsedView(
        context: Context,
        remoteMessage: RemoteMessage,
        bitmap: Bitmap?
    ): RemoteViews {
        val carouselView = RemoteViews(context.packageName, R.layout.view_push_notification_collapsed_layout)
        carouselView.setTextViewText(R.id.tv_notification_title, getNotificationTitle(remoteMessage))
        carouselView.setTextViewText(R.id.tv_notification_body, getNotificationBody(remoteMessage))
        carouselView.setImageViewBitmap(R.id.iv_notification_image, bitmap)
        return carouselView
    }

    private fun getNotificationCarouselView(
        context: Context,
        remoteMessage: RemoteMessage,
        carouselItems: List<CarouselItemDisplay?>,
        notificationId: Int
    ): RemoteViews {
        val carouselView = RemoteViews(context.packageName, R.layout.view_push_notification_carousel)
        carouselView.setTextViewText(R.id.tv_notification_title, getNotificationTitle(remoteMessage))

        for (carouselItem: CarouselItemDisplay? in carouselItems) {
            val viewFlipperImage = RemoteViews(context.packageName, R.layout.view_push_notification_carousel_image)
            if (carouselItem?.bitmap != null) {
                viewFlipperImage.setImageViewBitmap(R.id.imageView, carouselItem.bitmap)
            }
            carouselView.addView(R.id.viewFlipper, viewFlipperImage)
        }
        setActionButtonVisibility(carouselView, carouselItems)
        setCarouselItemActionListener(context, notificationId, carouselView, carouselItems)
        setCarouselItemActionPendingIntents(context, notificationId, carouselView)
        return carouselView
    }

    private fun setActionButtonVisibility(
        carouselView: RemoteViews,
        carouselItems: List<CarouselItemDisplay?>
    ) {
        carouselView.setViewVisibility(R.id.btn_carousel_prev_item, View.INVISIBLE)
        if (carouselItems.size == 1) {
            carouselView.setViewVisibility(R.id.vg_carousel_action_buttons, View.GONE)
        }
    }

    private fun setCarouselItemActionListener(
        context: Context,
        notificationId: Int,
        carouselView: RemoteViews,
        carouselItems: List<CarouselItemDisplay?>
    ) {
        var currentCarouselItemIterator = 0
        val carouselItemActionIntentFilter = IntentFilter()
        carouselItemActionIntentFilter.addAction("${C.NOTIFICATION_CAROUSEL_ITEM_ACTION}_$notificationId")
        var carouselItemActionReceiver: BroadcastReceiver? = null
        carouselItemActionReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (context == null) return
                val bundle = intent?.extras?.getBundle(C.BUNDLE) ?: return
                val action =
                    NotificationCarouselItemAction.findAction(bundle.getString(C.NOTIFICATION_CAROUSEL_ITEM_ACTION))
                        ?: return
                when (action) {
                    NotificationCarouselItemAction.PREVIOUS_ITEM -> {
                        showPreviousItem(context)
                    }
                    NotificationCarouselItemAction.OPEN_ITEM -> {
                        openItem(context)
                    }
                    NotificationCarouselItemAction.NEXT_ITEM -> {
                        showNextItem(context)
                    }
                    NotificationCarouselItemAction.CLOSE -> {
                        unregisterBroadcastReceiver(context)
                    }
                }
            }

            private fun showNextItem(context: Context?) {
                carouselView.setViewVisibility(R.id.btn_carousel_prev_item, View.VISIBLE)
                if (currentCarouselItemIterator == carouselItems.size - 2) {
                    carouselView.setViewVisibility(R.id.btn_carousel_next_item, View.INVISIBLE)
                }
                if (currentCarouselItemIterator < carouselItems.size - 1) {
                    currentCarouselItemIterator++
                    carouselView.setDisplayedChild(R.id.viewFlipper, currentCarouselItemIterator)
                    CordialNotification().update(context, notificationId, builder)
                }
            }

            private fun openItem(context: Context) {
                val deepLinkUrl = carouselItems[currentCarouselItemIterator]?.deepLink
                val deepLinkUri = Uri.parse(deepLinkUrl)
                val cordialDeepLink = CordialDeepLink(deepLinkUri)
                CordialDeepLinkProcessService().sendEventAndPassDeepLink(cordialDeepLink)
                val notificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE).castTo<NotificationManager>()
                        ?: return
                notificationManager.cancel(notificationId)
                unregisterBroadcastReceiver(context)
            }

            private fun showPreviousItem(context: Context?) {
                carouselView.setViewVisibility(R.id.btn_carousel_next_item, View.VISIBLE)
                if (currentCarouselItemIterator == 1) {
                    carouselView.setViewVisibility(R.id.btn_carousel_prev_item, View.INVISIBLE)
                }
                if (currentCarouselItemIterator > 0) {
                    currentCarouselItemIterator--
                    carouselView.setDisplayedChild(R.id.viewFlipper, currentCarouselItemIterator)
                    CordialNotification().update(context, notificationId, builder)
                }
            }

            private fun unregisterBroadcastReceiver(context: Context?) {
                CordialNotificationPendingIntent.unregisterBroadcastReceiver(context, carouselItemActionReceiver)
            }
        }
        context.registerReceiver(carouselItemActionReceiver, carouselItemActionIntentFilter)
    }

    private fun setCarouselItemActionPendingIntents(
        context: Context,
        notificationId: Int,
        carouselView: RemoteViews
    ) {
        val nextItemPendingIntent = CordialNotificationPendingIntent.setNotificationCarouselActionPendingIntent(
            context,
            NotificationCarouselItemAction.NEXT_ITEM.action,
            notificationId
        )
        carouselView.setOnClickPendingIntent(R.id.btn_carousel_next_item, nextItemPendingIntent)

        val previousItemPendingIntent = CordialNotificationPendingIntent.setNotificationCarouselActionPendingIntent(
            context,
            NotificationCarouselItemAction.PREVIOUS_ITEM.action,
            notificationId
        )
        carouselView.setOnClickPendingIntent(R.id.btn_carousel_prev_item, previousItemPendingIntent)

        val openActivityPendingIntent = CordialNotificationPendingIntent.setNotificationCarouselItemOpenActionPendingIntent(
            context,
            NotificationCarouselItemAction.OPEN_ITEM.action,
            notificationId
        )
        carouselView.setOnClickPendingIntent(R.id.viewFlipper, openActivityPendingIntent)
    }
}