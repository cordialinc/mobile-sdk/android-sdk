package com.cordial.feature.notification

import android.content.Context
import androidx.core.app.NotificationManagerCompat

internal class NotificationStatusRequester : OnNotificationStatusListener {
    override fun areNotificationsEnabled(context: Context): Boolean {
        return NotificationManagerCompat.from(context).areNotificationsEnabled()
    }
}