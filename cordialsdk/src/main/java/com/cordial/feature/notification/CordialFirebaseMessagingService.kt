package com.cordial.feature.notification

import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.log.CordialLoggerManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

internal class CordialFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        runIfPushesConfigurationIsSetToSDK {
            CordialNotificationProcessService().processMessage(this, remoteMessage)
        }
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        runIfPushesConfigurationIsSetToSDK {
            CordialNotificationProcessService().processNewToken(this, token)
        }
    }

    private fun runIfPushesConfigurationIsSetToSDK(runBlock: () -> Unit) {
        val pushesConfiguration = CordialApiConfiguration.getInstance().pushesConfiguration
        if (pushesConfiguration == PushesConfiguration.SDK) {
            runBlock()
        } else {
            CordialLoggerManager.info("The push notification was received, but pushes configuration is not set to SDK")
        }
    }

}