package com.cordial.feature.notification.carousel

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.cordial.api.C
import com.cordial.feature.notification.util.PushNotificationPayloadUtils
import com.cordial.util.BitmapUtils.getBytesSize
import com.cordial.util.JsonUtils
import com.cordial.util.ScreenUtils
import com.cordial.util.ViewUtils.toPx
import com.cordial.util.downloadImage
import com.cordial.util.getImageBitmapOptions
import com.google.firebase.messaging.RemoteMessage


class CarouselCompress {

    suspend fun getCarouselItems(context: Context, remoteMessage: RemoteMessage): List<CarouselItemDisplay> {
        val carouselJsonArray = PushNotificationPayloadUtils.getNotificationCarouselJson(remoteMessage)
        val carouselItems = JsonUtils.getCarouselItemListFromJson(carouselJsonArray)
        val carouselImageBitmaps: MutableList<CarouselItemDisplay> = mutableListOf()
        var imageBitmapByteSizeCount = 0
        carouselItems?.let {
            carouselItems.forEach { carouselItem ->
                val itemImageURL = carouselItem.imageURL
                val imageBitmapOptions = getImageBitmapOptions(itemImageURL)
                val sampleSize = calculateInSampleSize(
                    imageBitmapOptions,
                    100F.toPx(context.resources),
                    75F.toPx(context.resources)
                )
                val imageBitmap = downloadImage(itemImageURL, sampleSize) ?: return@forEach
                val imageBitmapBytesSize = getBytesSize(imageBitmap)
                if (imageBitmapByteSizeCount + imageBitmapBytesSize < C.CAROUSEL_BYTE_LIMIT) {
                    imageBitmapByteSizeCount += imageBitmapBytesSize
                    val carouselItemDisplay = CarouselItemDisplay(imageBitmap, itemImageURL, carouselItem.deepLink)
                    carouselImageBitmaps.add(carouselItemDisplay)
                }
            }
        }
        return carouselImageBitmaps
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options?, reqWidth: Int, reqHeight: Int): Int {
        var inSampleSize = 1

        // Raw height and width of image
        if (options == null) return inSampleSize
        val (height: Int, width: Int) = options.run { outHeight to outWidth }

        if (height > reqHeight || width > reqWidth) {
            val halfHeight: Int = height / 2
            val halfWidth: Int = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }
        return inSampleSize
    }

    private fun getBytesSize(bitmap: Bitmap?): Int {
        return bitmap?.allocationByteCount ?: 1
    }
}