package com.cordial.feature.notification.permission.model

enum class EducationalUiModeEnum(val mode: String) {
    NOTIFICATION_CATEGORIES("notification_categories"),
    CUSTOM("custom"),
    NONE("none");

    companion object {
        fun findMode(mode: String?): EducationalUiModeEnum {
            return values().firstOrNull { it.mode == mode } ?: NONE
        }
    }
}