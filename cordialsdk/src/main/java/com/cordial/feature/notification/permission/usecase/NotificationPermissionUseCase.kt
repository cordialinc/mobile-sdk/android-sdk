package com.cordial.feature.notification.permission.usecase

import android.content.Context
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings

internal interface NotificationPermissionUseCase {
    fun requestNotificationPermission(
        context: Context,
        educationalUiSettings: NotificationPermissionEducationalUISettings? = null
    )
}