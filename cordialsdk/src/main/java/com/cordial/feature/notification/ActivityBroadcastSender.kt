package com.cordial.feature.notification

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.cordial.api.C
import com.cordial.feature.notification.receiver.NotificationClickedReceiver
import com.google.firebase.messaging.RemoteMessage

class ActivityBroadcastSender {

    fun sendNotificationClickedBroadcast(context: Context, extras: Bundle?, isActivityRestarted: Boolean = false) {
        val remoteMessage = extras?.getParcelable<RemoteMessage?>(C.REMOTE_MESSAGE) ?: return
        val intent = Intent(context, NotificationClickedReceiver::class.java)
        intent.putExtra(C.IS_ACTIVITY_RESTARTED, isActivityRestarted)
        intent.putExtra(C.REMOTE_MESSAGE, remoteMessage)
        context.sendBroadcast(intent)
    }
}