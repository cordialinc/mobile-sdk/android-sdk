package com.cordial.feature.notification

import android.app.NotificationChannel
import android.app.NotificationChannelGroup
import android.app.NotificationManager
import android.content.ContentResolver
import android.content.Context
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import com.cordial.api.C
import com.cordial.cordialsdk.R
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.model.NotificationSoundType
import com.cordial.feature.notification.util.PushNotificationPayloadUtils
import com.cordial.lifecycle.AppLifecycleHandler
import com.cordial.util.ResourceUtils
import com.cordial.util.castTo
import com.google.firebase.messaging.RemoteMessage


class CordialNotificationChannel {

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannel(context: Context, remoteMessage: RemoteMessage): String {
        val channelType = getNotificationSoundType(remoteMessage)
        val notificationSoundName = PushNotificationPayloadUtils.getNotificationSoundName(remoteMessage)
        val notificationSoundUri = getNotificationSoundUri(context, remoteMessage)
        val notificationChannelID = createNotificationChannelID(context, notificationSoundName, channelType)
        val notificationChannelName = createNotificationChannelName(context, notificationSoundName, channelType)
        val importance = NotificationManager.IMPORTANCE_HIGH
        val notificationChannel = NotificationChannel(notificationChannelID, notificationChannelName, importance)
        setNotificationChannelSettings(
            context,
            notificationChannel,
            notificationSoundUri,
            channelType
        )
        createSoundChannelGroup(context, notificationChannel, notificationSoundName, channelType)
        registerNotificationChannel(context, notificationChannel)
        return notificationChannelID
    }


    private fun createNotificationChannelName(
        context: Context,
        soundName: String,
        soundType: NotificationSoundType
    ): String {
        val isSoundExists = isSoundExists(context, soundName)
        return when (soundType) {
            NotificationSoundType.CUSTOM_SOUND -> {
                if (isSoundExists) soundName else context.getString(R.string.crdl_notification_channel_name)
            }
            NotificationSoundType.SILENT -> context.getString(R.string.crdl_notification_channel_name_silent)
            NotificationSoundType.DEFAULT -> context.getString(R.string.crdl_notification_channel_name)
        }
    }


    fun getNotificationSoundType(remoteMessage: RemoteMessage): NotificationSoundType {
        val isAppInForeground = AppLifecycleHandler.isAppInForeground()
        return when {
            isAppInForeground -> NotificationSoundType.SILENT
            PushNotificationPayloadUtils.getNotificationSoundName(remoteMessage)
                .isNotEmpty() -> NotificationSoundType.CUSTOM_SOUND
            else -> NotificationSoundType.DEFAULT
        }
    }

    fun getNotificationSoundUri(context: Context, remoteMessage: RemoteMessage): Uri? {
        val soundName = PushNotificationPayloadUtils.getNotificationSoundName(remoteMessage)
        val soundFileName = "raw/$soundName"
        val isResourceExists = ResourceUtils.isRawResourceExists(context, soundName)
        return if (isResourceExists)
            Uri.parse("${ContentResolver.SCHEME_ANDROID_RESOURCE}://${context.packageName}/$soundFileName")
        else null
    }


    private fun createNotificationChannelID(
        context: Context,
        soundName: String,
        soundType: NotificationSoundType
    ): String {
        val isSoundExists = isSoundExists(context, soundName)
        return when (soundType) {
            NotificationSoundType.CUSTOM_SOUND -> {
                if (isSoundExists)
                    "${C.NOTIFICATION_CHANNEL_ID}_$soundName" else C.NOTIFICATION_CHANNEL_ID
            }
            NotificationSoundType.SILENT -> C.NOTIFICATION_CHANNEL_SILENT_ID
            NotificationSoundType.DEFAULT -> C.NOTIFICATION_CHANNEL_ID
        }
    }

    private fun isSoundExists(context: Context, soundName: String): Boolean {
        val isResourceExists = ResourceUtils.isRawResourceExists(context, soundName)
        return soundName.isNotEmpty() && isResourceExists
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createSoundChannelGroup(
        context: Context,
        notificationChannel: NotificationChannel,
        notificationSoundName: String,
        soundType: NotificationSoundType
    ) {
        val isSoundExists = isSoundExists(context, notificationSoundName)
        if (soundType == NotificationSoundType.CUSTOM_SOUND && isSoundExists) {
            val groupId = C.NOTIFICATION_CHANNEL_SOUND_GROUP_ID
            val groupName = context.getString(R.string.crdl_notification_channel_sound_group_name)
            val notificationChannelSoundGroup = NotificationChannelGroup(groupId, groupName)
            createNotificationChannelGroup(context, notificationChannelSoundGroup)
            notificationChannel.group = groupId
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationChannelGroup(context: Context, notificationChannelGroup: NotificationChannelGroup) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE).castTo<NotificationManager>() ?: return
        notificationManager.createNotificationChannelGroup(notificationChannelGroup)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun setNotificationChannelSettings(
        context: Context,
        notificationChannel: NotificationChannel,
        soundUri: Uri?,
        soundType: NotificationSoundType
    ) {
        notificationChannel.apply {
            description = context.getString(R.string.crdl_notification_channel_description)
            enableVibration(true)
            enableLights(true)
            when (soundType) {
                NotificationSoundType.SILENT -> setSound(null, null)
                NotificationSoundType.CUSTOM_SOUND -> {
                    val audioAttributes = AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .build()
                    if (soundUri != null) setSound(soundUri, audioAttributes)
                }
                NotificationSoundType.DEFAULT -> {}
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun registerNotificationChannel(context: Context, notificationChannel: NotificationChannel) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE).castTo<NotificationManager>() ?: return
        notificationManager.createNotificationChannel(notificationChannel)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun unregisterNotificationChannel(context: Context, notificationChannelId: String) {
        val notificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE).castTo<NotificationManager>() ?: return
        notificationManager.deleteNotificationChannel(notificationChannelId)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun createNotificationCategories(context: Context, categories: List<NotificationCategory>) {
        categories.forEach { category ->
            createNotificationCategoryChannel(context, category)
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationCategoryChannel(context: Context, category: NotificationCategory) {
        val channelId = category.id
        val channelName = category.name
        val importance =
            if (category.state) NotificationManager.IMPORTANCE_HIGH else NotificationManager.IMPORTANCE_NONE
        val notificationChannel = NotificationChannel(channelId, channelName, importance)
        registerNotificationChannel(context, notificationChannel)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun recreateNotificationCategoryChannel(context: Context, category: NotificationCategory, oldCategoryId: String) {
        unregisterNotificationChannel(context, oldCategoryId)
        createNotificationCategoryChannel(context, category)
    }
}