package com.cordial.feature.notification.model

import java.io.Serializable

data class NotificationPermissionEducationalUiColor(
    val primary: Int,
    val accent: Int,
    val primaryLight: Int,
): Serializable
