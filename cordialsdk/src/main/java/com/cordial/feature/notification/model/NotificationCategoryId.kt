package com.cordial.feature.notification.model

import java.lang.IllegalArgumentException
import java.util.UUID

data class NotificationCategoryId(
    val id: String,
    val uuid: UUID
) {
    init {
        require(id.isNotBlank()) { "Category id is blank" }
    }

    constructor(categoryId: String): this(categoryId, UUID.randomUUID())

    override fun toString(): String = id + SEPARATOR + uuid.toString()

    companion object {
        const val SEPARATOR = '-'
        private const val SEPARATOR_LENGTH = 1
        private const val UUID_LENGTH = 36

        fun from(id: String): NotificationCategoryId {
            if (id.length <= UUID_LENGTH + SEPARATOR_LENGTH)
                throw IllegalArgumentException("Argument \"${id}\" is not a valid NotificationCategoryId, " +
                        "length ${id.length} <= ${UUID_LENGTH + SEPARATOR_LENGTH}")

            val uuidOffset = id.length - UUID_LENGTH
            val separatorOffset = uuidOffset - SEPARATOR_LENGTH

            if (id[separatorOffset] != SEPARATOR)
                throw IllegalArgumentException("Argument \"${id}\" is not a valid NotificationCategoryId, expected '${SEPARATOR}' " +
                        "at position ${separatorOffset}, found '${id.get(separatorOffset)}'")

            val categoryId = id.substring(0, separatorOffset)
            val uuid = UUID.fromString(id.substring(uuidOffset))
            return NotificationCategoryId(categoryId, uuid)
        }
    }
}