package com.cordial.feature.notification.permission.ui

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.cordialsdk.R
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.notification.CordialNotificationChannel
import com.cordial.feature.notification.category.util.NotificationCategoryUtil
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.model.NotificationCategoryId
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings
import com.cordial.feature.notification.model.NotificationPermissionEducationalUiColor

import com.cordial.feature.notification.permission.model.EducationalUiModeEnum
import com.cordial.feature.upsertcontact.model.attributes.ArrayValue
import com.cordial.feature.upsertcontact.model.attributes.AttributeValue
import com.cordial.util.castTo
import com.cordial.util.serializable

@RequiresApi(Build.VERSION_CODES.TIRAMISU)
class NotificationPermissionEducationUIActivity : AppCompatActivity() {
    private var educationalUiMode: EducationalUiModeEnum? = null
    private var educationalUiSettings: NotificationPermissionEducationalUISettings? = null
    private var notificationCategories: List<NotificationCategory>? = null
    private var notificationCategoryAdapter: NotificationCategoryAdapter? = null
    private var rvNotificationCategories: RecyclerView? = null
    private var btnOk: Button? = null
    private var btnSkip: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setViews()
        overridePendingTransition(0, 0)
    }

    private fun setViews() {
        getDataFromIntent()
        setContentView()
        initViews()
        setClickListeners()
    }

    private fun getDataFromIntent() {
        val bundle = intent?.extras?.getBundle(C.BUNDLE)
        bundle?.let {
            educationalUiMode =
                EducationalUiModeEnum.findMode(bundle.getString(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_MODE))
            educationalUiSettings = bundle.serializable(C.NOTIFICATION_PERMISSION_EDUCATIONAL_UI_SETTINGS)
            val categoriesBundle = bundle.serializable<String>(NotificationCategoryUtil.NOTIFICATION_CATEGORIES)
            notificationCategories = NotificationCategoryUtil.getNotificationCategoriesFromJson(categoriesBundle)
        }
    }

    private fun setContentView() {
        when (educationalUiMode) {
            EducationalUiModeEnum.NOTIFICATION_CATEGORIES -> {
                setContentView(R.layout.activity_notification_educational_ui_default)
            }
            EducationalUiModeEnum.CUSTOM -> {
                val layoutId = educationalUiSettings?.layoutId
                if (layoutId == null) {
                    sendOKResult()
                    CordialLoggerManager.info("The custom educational UI cannot be displayed because the layout has not been set")
                } else {
                    setContentView(layoutId)
                }
            }
            else -> sendOKResult()
        }
    }

    private fun initViews() {
        btnOk = findViewById(R.id.btn_ok)
        btnSkip = findViewById(R.id.btn_skip)
        if (btnOk == null && btnSkip == null) {
            sendOKResult()
            CordialLoggerManager.info("The custom educational UI cannot be displayed because the OK and Cancel buttons have not been set")
        }
        if (educationalUiMode == EducationalUiModeEnum.NOTIFICATION_CATEGORIES) {
            notificationCategories?.let { categories ->
                rvNotificationCategories = findViewById(R.id.rv_notification_categories)
                initRecyclerView(categories, educationalUiSettings?.color)
                educationalUiSettings?.color?.let { color ->
                    btnSkip?.setBackgroundColor(ContextCompat.getColor(this, color.accent))
                    btnSkip?.setTextColor(ContextCompat.getColor(this, color.primary))
                    btnOk?.setBackgroundColor(ContextCompat.getColor(this, color.accent))
                    btnOk?.setTextColor(ContextCompat.getColor(this, color.primary))
                    val tvTitle: TextView = findViewById(R.id.tv_title)
                    tvTitle.setTextColor(ContextCompat.getColor(this, color.primary))
                    val tvDesc: TextView = findViewById(R.id.tv_desc)
                    tvDesc.setTextColor(ContextCompat.getColor(this, color.primary))
                }
            }
        }
    }

    private fun initRecyclerView(
        categories: List<NotificationCategory>,
        color: NotificationPermissionEducationalUiColor?
    ) {
        notificationCategoryAdapter = NotificationCategoryAdapter(categories, color)
        rvNotificationCategories?.layoutManager =
            LinearLayoutManager(this@NotificationPermissionEducationUIActivity)
        rvNotificationCategories?.adapter = notificationCategoryAdapter
    }

    private fun setClickListeners() {
        btnSkip?.setOnClickListener {
            sendSkipResult()
        }
        btnOk?.setOnClickListener {
            updateNotificationCategories()
            sendOKResult()
        }
    }

    private fun updateNotificationCategories() {
        val categories = getNotificationCategories()
        if (categories.isEmpty()) return

        val enabledCategoriesIds = getEnabledCategoriesIds(categories)
        sendNotificationCategoriesAsAttributes(enabledCategoriesIds)

        val updatedCategories = recreateNotificationCategoryChannels(categories)

        storeAndSendCategories(updatedCategories)
    }

    private fun getNotificationCategories(): MutableList<NotificationCategory> {
        val updatedCategories = mutableListOf<NotificationCategory>()
        notificationCategoryAdapter?.items?.let { categories ->
            for (i in categories.indices) {
                val category = categories[i]
                val viewHolder = rvNotificationCategories?.findViewHolderForAdapterPosition(i)
                    ?.castTo<NotificationCategoryAdapter.NotificationCategoryVH>()
                val currentState = viewHolder?.isCategoryEnabled() ?: continue
                updatedCategories.add(
                    NotificationCategory(category.id, category.name, currentState, category.description)
                )
            }
        }
        return updatedCategories
    }

    private fun getEnabledCategoriesIds(categories: List<NotificationCategory>): List<String> {
        val enabledCategories = categories.filter { it.state }
        val enabledCategoriesIds = enabledCategories.map { it.id }
        val parsedEnabledCategoriesIds = enabledCategoriesIds.map { NotificationCategoryId.from(it).id }
        return parsedEnabledCategoriesIds
    }

    private fun sendNotificationCategoriesAsAttributes(categoriesIdList: List<String>) {
        val attributes = HashMap<String, AttributeValue>()
        attributes[C.NOTIFICATION_CATEGORIES_ATTRIBUTE_ID] = ArrayValue(categoriesIdList.toTypedArray())
        CordialApi().upsertContact(attributes)
    }

    private fun recreateNotificationCategoryChannels(updatedCategories: Iterable<NotificationCategory>): MutableList<NotificationCategory> {
        val cordialNotificationChannel = CordialNotificationChannel()
        val categories = mutableListOf<NotificationCategory>()
        updatedCategories.forEach { category ->
            val categoryId = NotificationCategoryId.from(category.id).id
            val updatedCategoryId = NotificationCategoryId(categoryId).toString()
            val newCategory =
                NotificationCategory(updatedCategoryId, category.name, category.state, category.description)
            categories.add(newCategory)
            cordialNotificationChannel.recreateNotificationCategoryChannel(
                context = this,
                category = newCategory,
                oldCategoryId = category.id
            )
        }
        return categories
    }

    private fun storeAndSendCategories(categoriesWithNewId: Iterable<NotificationCategory>) {
        val notificationCategoryUseCase =
            CordialApiConfiguration.getInstance().injection.notificationCategoryInjection().notificationCategoryUseCase
        notificationCategoryUseCase.clearCategories {
            notificationCategoryUseCase.storeCategories(categoriesWithNewId)
        }
    }

    private fun sendOKResult() {
        val intent = Intent()
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun sendSkipResult() {
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    override fun onPause() {
        overridePendingTransition(0, 0)
        super.onPause()
    }
}