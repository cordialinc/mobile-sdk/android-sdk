package com.cordial.feature.throttler

import Throttler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

object ThrottlerManager {
    val upsertContactThrottler = Throttler(
        delay = 1000L,
        coroutineScope = CoroutineScope(Dispatchers.IO)
    )
}