import kotlinx.coroutines.*

class Throttler(
    private val delay: Long, // Minimum delay in milliseconds
    private val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
) {
    private var job: Job? = null

    fun throttle(block: suspend () -> Unit) {
        // Cancel any existing job if it hasn't executed yet
        job?.cancel()

        // Schedule the new task
        job = coroutineScope.launch {
            delay(delay)
            block()
        }
    }
}