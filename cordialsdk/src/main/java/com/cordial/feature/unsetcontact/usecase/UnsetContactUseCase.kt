package com.cordial.feature.unsetcontact.usecase

import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.storage.db.OnRequestFromDBListener

internal interface UnsetContactUseCase {
    fun unsetContact(unsetContactRequest: UnsetContactRequest, onRequestFromDBListener: OnRequestFromDBListener?)
    fun sendCachedContactLogout()
}