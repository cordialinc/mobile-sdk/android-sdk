package com.cordial.feature.unsetcontact.repository

import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.network.response.OnResponseListener

internal interface UnsetContactRepository {
    fun unsetContact(unsetContactRequest: UnsetContactRequest, onResponseListener: OnResponseListener)
}