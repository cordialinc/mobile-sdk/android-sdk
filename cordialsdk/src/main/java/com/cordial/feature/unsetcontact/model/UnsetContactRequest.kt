package com.cordial.feature.unsetcontact.model

internal class UnsetContactRequest(
    var deviceId: String,
    var primaryKey: String
)