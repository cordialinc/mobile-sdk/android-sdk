package com.cordial.feature.unsetcontact.usecase

import com.cordial.api.MessageAttributionManager
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.feature.unsetcontact.repository.UnsetContactRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.contactlogout.UnsetContactDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences

internal class UnsetContactUseCaseImpl(
    private val unsetContactRepository: UnsetContactRepository,
    private val preferences: Preferences,
    private val unsetContactDao: UnsetContactDao?,
    private val messageAttributionManager: MessageAttributionManager,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : UnsetContactUseCase, CordialCheck() {

    override fun unsetContact(
        unsetContactRequest: UnsetContactRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        messageAttributionManager.clearMessageAttributes()
        clearPrimaryKey(unsetContactRequest)
        checkAuth(unsetContactRequest, onRequestFromDBListener)
        doAfterCheck {
            unsetContactRepository.unsetContact(unsetContactRequest, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    onRequestFromDBListener?.onSuccess()
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleSystemError(
                        onRequestFromDBListener,
                        unsetContactRequest,
                        reason = "Failed to unset contact due to the error : $error, saving unset contact request to the cache",
                        onSaveRequestListener
                    )
                }

                override fun onLogicError(response: String) {
                    onRequestFromDBListener?.onFailure()
                    CordialLoggerManager.error("Failed to unset contact due to the error: $response")
                }
            })
        }
    }

    private fun checkAuth(
        unsetContactRequest: UnsetContactRequest,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    unsetContactRequest,
                    reason = "Failed to unset contact because the jwt is absent, saving unset contact request to the cache"
                ) {
                    sdkSecurityUseCase.updateJWT()
                }
            }
        )
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    unsetContactRequest,
                    reason = "Failed to unset contact because there is no network connection, saving unset contact request to the cache",
                    onSaveRequestListener = null
                )
            }
        )
        networkAvailableCheck.execute()
    }

    private fun clearPrimaryKey(unsetContactRequest: UnsetContactRequest) {
        preferences.put(PreferenceKeys.IS_LOGGED_IN, false)
        preferences.put(PreferenceKeys.IS_CONTACT_SET, false)
        preferences.put(PreferenceKeys.PREVIOUS_PRIMARY_KEY, unsetContactRequest.primaryKey)
        preferences.remove(PreferenceKeys.PRIMARY_KEY)
        CordialLoggerManager.info("Clearing primaryKey, saving previous primaryKey : ${unsetContactRequest.primaryKey}")
    }

    private fun handleSystemError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        unsetContactRequest: UnsetContactRequest,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            CordialLoggerManager.info("$reason : ${unsetContactRequest.primaryKey}")
            saveContactLogoutToDB(unsetContactRequest, onSaveRequestListener)
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    private fun saveContactLogoutToDB(unsetContactRequest: UnsetContactRequest, onSaveRequestListener: (() -> Unit)?) {
        // contact cart table is clearing, because by logic we need to save only one last request
        clearUnsetContactCache {
            unsetContactDao?.insert(unsetContactRequest, onSaveRequestListener)
        }
    }

    private fun clearUnsetContactCache(onLogoutCacheClearListener: (() -> Unit)? = null) {
        unsetContactDao?.clear(onLogoutCacheClearListener)
    }

    override fun sendCachedContactLogout() {
        if (!SendingCacheState.sendingContactLogout.compareAndSet(false, true))
            return
        unsetContactDao?.getContactLogout { contactLogoutRequest ->
            if (contactLogoutRequest == null) {
                SendingCacheState.sendingContactLogout.set(false)
                return@getContactLogout
            }
            contactLogoutRequest.deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
            unsetCachedContact(contactLogoutRequest)
        }
    }

    private fun unsetCachedContact(contactLogoutRequest: UnsetContactRequest) {
        unsetContact(contactLogoutRequest, object :
            OnRequestFromDBListener {
            override fun onSuccess() {
                clearUnsetContactCache()
                CordialLoggerManager.info("Contact unset, clearing unset contact cache")
                SendingCacheState.sendingContactLogout.set(false)
            }

            override fun onFailure() {
                SendingCacheState.sendingContactLogout.set(false)
            }
        })
    }
}