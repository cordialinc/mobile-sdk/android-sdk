package com.cordial.feature.unsetcontact.repository

import com.cordial.api.C
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import org.json.JSONObject

internal class UnsetContactRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : UnsetContactRepository {

    override fun unsetContact(unsetContactRequest: UnsetContactRequest, onResponseListener: OnResponseListener) {
        val requestJson = getContactLogoutRequestJSON(unsetContactRequest)
        val url = cordialApiEndpoints.getContactLogoutUrl()
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getContactLogoutRequestJSON(unsetContactRequest: UnsetContactRequest): String {
        val param = JSONObject()
        param.put(C.DEVICE_ID, unsetContactRequest.deviceId)
        if (unsetContactRequest.primaryKey.trim().isNotEmpty()) {
            param.put(C.PRIMARY_KEY, unsetContactRequest.primaryKey)
        }
        return param.toString()
    }
}