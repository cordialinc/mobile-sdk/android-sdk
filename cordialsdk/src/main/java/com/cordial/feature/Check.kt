package com.cordial.feature

internal class Check(
    val checkFunction: () -> Boolean,
    val nextCheck: Check? = null,
    val doOnSuccess: (() -> Unit)? = null,
    val doOnError: (() -> Unit)? = null
) {
    fun execute() {
        if (checkFunction()) {
            doOnSuccess?.let { onSuccess ->
                onSuccess()
            }
            nextCheck?.execute()
        } else {
            doOnError?.let { onError ->
                onError()
            }
        }
    }
}