package com.cordial.feature.deeplink

import android.net.Uri
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.deeplink.model.CordialDeepLink

internal class CordialDeepLinkProcessService {
    private val injection = CordialApiConfiguration.getInstance().injection
    private val processDeepLinkUseCase = injection.processDeepLinkInjection().processDeepLinkUseCase

    fun checkOnRedirectAndWebOnly(cordialDeepLink: CordialDeepLink, fallBackUri: Uri? = null, redirectCount: Int = 0){
        processDeepLinkUseCase.checkOnRedirectAndWebOnly(cordialDeepLink, fallBackUri, redirectCount)
    }

    fun checkOnRedirect(cordialDeepLink: CordialDeepLink, fallBackUri: Uri? = null, redirectCount: Int = 0) {
        processDeepLinkUseCase.checkOnRedirect(cordialDeepLink, fallBackUri, redirectCount)
    }

    fun sendEventAndPassDeepLink(cordialDeepLink: CordialDeepLink, fallBackUri: Uri? = null) {
        processDeepLinkUseCase.sendEventAndPassDeepLink(cordialDeepLink, fallBackUri)
    }
}