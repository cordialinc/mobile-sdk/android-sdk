package com.cordial.feature.deeplink

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.cordial.feature.deeplink.model.CordialDeepLink

class DeepLinkHandlerActivity : Activity() {

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
        handleIntent(intent)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        handleIntent(intent)
    }

    private fun handleIntent(intent: Intent?) {
        if (intent?.action == Intent.ACTION_VIEW) {
            val uri = intent.data
            val cordialDeepLink = CordialDeepLink(uri)
            CordialDeepLinkProcessService().checkOnRedirectAndWebOnly(cordialDeepLink)
        }
        finish()
    }
}