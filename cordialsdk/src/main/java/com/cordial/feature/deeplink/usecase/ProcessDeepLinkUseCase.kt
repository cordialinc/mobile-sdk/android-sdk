package com.cordial.feature.deeplink.usecase

import android.net.Uri
import com.cordial.feature.deeplink.model.CordialDeepLink

internal interface ProcessDeepLinkUseCase {
    fun checkOnRedirectAndWebOnly(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?, redirectCount: Int = 0)
    fun checkOnRedirect(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?, redirectCount: Int = 0, originalVanityUri: Uri? = null)
    fun sendEventAndPassDeepLink(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?, originalVanityUri: Uri?= null)
}