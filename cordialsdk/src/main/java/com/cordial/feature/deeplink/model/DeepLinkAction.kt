package com.cordial.feature.deeplink.model

enum class DeepLinkAction {
    NO_ACTION,
    OPEN_IN_BROWSER
}