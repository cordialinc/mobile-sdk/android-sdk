package com.cordial.feature.deeplink.model.browser

import android.net.Uri

internal interface Browser {
    fun openLink(uri: Uri)
}