package com.cordial.feature.deeplink.model

import android.net.Uri

class CordialDeepLink(
    val uri: Uri?,
    val vanityUri: Uri? = null,
    val isActivityRestarted: Boolean = true
)