package com.cordial.feature.deeplink.repository

import android.net.Uri
import com.cordial.feature.deeplink.model.OnProcessDeepLinkListener
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.decode

internal class ProcessDeepLinkRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler
) : ProcessDeepLinkRepository {

    override fun processRedirectLink(
        uri: Uri?,
        redirectCount: Int,
        onProcessDeepLinkListener: OnProcessDeepLinkListener
    ) {
        val url = uri?.decode().toString()
        val sdkRequest = SDKRequest(null, url, RequestMethod.GET)
        sdkRequest.followRedirect = false
        requestSender.send(sdkRequest, responseHandler, object : OnResponseListener {
            override fun onSuccess(response: String) {
                val redirectedUri = Uri.parse(response)
                onProcessDeepLinkListener.onProcessDeepLink(
                    uri = redirectedUri,
                    originalVanityUri = uri,
                    redirectCount + 1
                )
            }

            override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                handleError(error, originalVanityUri = uri, onProcessDeepLinkListener)
            }

            override fun onLogicError(response: String) {
                handleError(error = response, originalVanityUri = uri, onProcessDeepLinkListener)
            }
        })
    }

    private fun handleError(
        error: String,
        originalVanityUri: Uri?,
        onProcessDeepLinkListener: OnProcessDeepLinkListener
    ) {
        CordialLoggerManager.warning("Failed to follow redirect for the link $originalVanityUri due to the error: $error")
        openDeepLink(originalVanityUri, onProcessDeepLinkListener)
    }

    private fun openDeepLink(uri: Uri?, onProcessDeepLinkListener: OnProcessDeepLinkListener) {
        onProcessDeepLinkListener.onOpenDeepLink(uri)
    }
}