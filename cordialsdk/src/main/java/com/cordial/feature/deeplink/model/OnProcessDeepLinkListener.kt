package com.cordial.feature.deeplink.model

import android.net.Uri

internal interface OnProcessDeepLinkListener {
    fun onOpenDeepLink(uri: Uri?)
    fun onProcessDeepLink(uri: Uri?, originalVanityUri: Uri?, redirectCount: Int)
}