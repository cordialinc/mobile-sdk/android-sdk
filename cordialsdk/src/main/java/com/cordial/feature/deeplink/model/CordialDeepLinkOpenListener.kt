package com.cordial.feature.deeplink.model

import android.content.Context
import android.net.Uri

interface CordialDeepLinkOpenListener {

    fun appOpenViaDeepLink(
        context: Context?,
        cordialDeepLink: CordialDeepLink,
        fallBackUri: Uri?,
        onComplete: ((action: DeepLinkAction) -> Unit)? = null
    )
}