package com.cordial.feature.deeplink.repository

import android.net.Uri
import com.cordial.feature.deeplink.model.OnProcessDeepLinkListener

internal interface ProcessDeepLinkRepository {
    fun processRedirectLink(uri: Uri?, redirectCount: Int, onProcessDeepLinkListener: OnProcessDeepLinkListener)
}