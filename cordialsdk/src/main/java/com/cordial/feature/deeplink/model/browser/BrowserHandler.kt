package com.cordial.feature.deeplink.model.browser

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import androidx.core.content.ContextCompat.startActivity
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.util.getDefaultUri
import com.cordial.util.getUriWithScheme

internal class BrowserHandler : Browser {

    override fun openLink(uri: Uri) {
        val context = CordialApiConfiguration.getInstance().getContext()
        val validUri = uri.getUriWithScheme()
        val browserIntent = Intent(Intent.ACTION_VIEW, getDefaultUri())
        val resolveInfo = context.packageManager.resolveActivity(browserIntent, PackageManager.MATCH_DEFAULT_ONLY)
        if (resolveInfo?.activityInfo != null && resolveInfo.activityInfo.packageName != C.DEFAULT_ANDROID_PACKAGE) {
            openBrowserWithoutAppSelector(context, browserIntent, resolveInfo, validUri)
        } else {
            openBrowserWithAppSelector(validUri, context)
        }
    }

    private fun openBrowserWithoutAppSelector(
        context: Context,
        browserIntent: Intent,
        resolveInfo: ResolveInfo,
        validUri: Uri?
    ) {
        browserIntent.setClassName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name)
        browserIntent.data = validUri
        browserIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        if (browserIntent.resolveActivity(context.packageManager) != null) {
            context.startActivity(browserIntent)
        }
    }

    private fun openBrowserWithAppSelector(validUri: Uri?, context: Context) {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = validUri
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(context, intent, null)
    }
}