package com.cordial.feature.deeplink.usecase

import android.net.Uri
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.MessageAttributionManager
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.deeplink.model.DeepLinkAction
import com.cordial.feature.deeplink.model.OnProcessDeepLinkListener
import com.cordial.feature.deeplink.repository.ProcessDeepLinkRepository
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.sendevent.model.property.PropertyValueUtil.getMergedSystemEventProperties
import com.cordial.util.addNoCookiesQueryParam
import com.cordial.util.decode

internal class ProcessDeepLinkUseCaseImpl(
    private val processDeepLinkRepository: ProcessDeepLinkRepository,
    private val messageAttributionManager: MessageAttributionManager
) : ProcessDeepLinkUseCase {
    var firstCheckOnRedirectUri: Uri? = null

    override fun checkOnRedirectAndWebOnly(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?, redirectCount: Int) {
        if (checkOnWebFragment(cordialDeepLink.uri)) return

        checkOnRedirect(cordialDeepLink, fallBackUri, redirectCount)
    }

    override fun checkOnRedirect(
        cordialDeepLink: CordialDeepLink,
        fallBackUri: Uri?,
        redirectCount: Int,
        originalVanityUri: Uri?
    ) {
        saveFirstUriToProcess(cordialDeepLink.uri, redirectCount)
        val vanityDomains = CordialApiConfiguration.getInstance().vanityDomains
        if (vanityDomains != null && vanityDomains.isNotEmpty() && redirectCount < C.DEEP_LINK_REDIRECT_LIMIT) {
            val isUriContainsVanityDomain = cordialDeepLink.uri.toString().findAnyOf(vanityDomains) != null
            if (isUriContainsVanityDomain) {
                processRedirectLink(cordialDeepLink, redirectCount, fallBackUri, originalVanityUri)
                return
            }
        }
        onOpenDeepLink(cordialDeepLink, fallBackUri, originalVanityUri, redirectCount)
    }

    private fun saveFirstUriToProcess(uri: Uri?, redirectCount: Int) {
        if (redirectCount > 0) return
        firstCheckOnRedirectUri = uri
    }

    private fun processRedirectLink(
        cordialDeepLink: CordialDeepLink,
        redirectCount: Int,
        fallBackUri: Uri?,
        originalVanityUri: Uri?
    ) {
        processDeepLinkRepository.processRedirectLink(
            cordialDeepLink.uri,
            redirectCount,
            object : OnProcessDeepLinkListener {
                override fun onOpenDeepLink(uri: Uri?) {
                    val cordialDeepLinkToOpen = CordialDeepLink(uri, cordialDeepLink.vanityUri, cordialDeepLink.isActivityRestarted)
                    onOpenDeepLink(cordialDeepLinkToOpen, fallBackUri, originalVanityUri, redirectCount)
                }

                override fun onProcessDeepLink(uri: Uri?, originalVanityUri: Uri?, redirectCount: Int) {
                    val cordialDeepLinkToCheckOnRedirect = CordialDeepLink(uri, cordialDeepLink.vanityUri, cordialDeepLink.isActivityRestarted)
                    checkOnRedirect(cordialDeepLinkToCheckOnRedirect, fallBackUri, redirectCount, originalVanityUri)
                }
            })
    }

    private fun onOpenDeepLink(
        cordialDeepLink: CordialDeepLink,
        fallBackUri: Uri?,
        originalVanityUri: Uri?,
        redirectCount: Int
    ) {
        val vanityUri = if (redirectCount > 0)
            getEncodedUriWithCookieQueryParam(firstCheckOnRedirectUri, cordialDeepLink.vanityUri)
        else cordialDeepLink.vanityUri
        val cordialDeepLinkWithEncodedUri = CordialDeepLink(cordialDeepLink.uri, vanityUri, cordialDeepLink.isActivityRestarted)
        sendEventAndPassDeepLink(cordialDeepLinkWithEncodedUri, fallBackUri, originalVanityUri)
    }

    private fun getEncodedUriWithCookieQueryParam(uri: Uri?, vanityUri: Uri?): Uri? {
        return if (vanityUri == null) uri.addNoCookiesQueryParam() else vanityUri.addNoCookiesQueryParam()
    }

    override fun sendEventAndPassDeepLink(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?, originalVanityUri: Uri?) {
        sendDeepLinkOpenEvent(cordialDeepLink.uri)

        val config = CordialApiConfiguration.getInstance()
        config.deepLinkListener?.appOpenViaDeepLink(config.getContext(), cordialDeepLink, fallBackUri) { action ->
            when (action) {
                DeepLinkAction.OPEN_IN_BROWSER -> {
                    restoreMessageAttributionAndOpenInBrowser(originalVanityUri, cordialDeepLink.uri, fallBackUri)
                }
                else -> {
                }
            }
        }
    }

    private fun sendDeepLinkOpenEvent(uri: Uri?) {
        uri?.let {
            val eventName = C.EVENT_NAME_DEEP_LINK_APP_OPEN
            val properties = getSystemEventProperties(uri)
            CordialApi().sendSystemEvent(eventName, properties = properties)
        }
    }

    private fun getSystemEventProperties(uri: Uri): Map<String, PropertyValue> {
        val properties = mapOf(C.DEEP_LINK_URL to PropertyValue.StringProperty(uri.toString()))
        return getMergedSystemEventProperties(properties)
    }

    private fun restoreMessageAttributionAndOpenInBrowser(originalVanityUri: Uri?, uri: Uri?, fallBackUri: Uri?) {
        messageAttributionManager.restorePreviousMessageAttributionData()
        var openUri = originalVanityUri ?: uri ?: fallBackUri
        openUri = openUri.addNoCookiesQueryParam()
        openBrowser(openUri)
    }

    private fun checkOnWebFragment(uri: Uri?): Boolean {
        try {
            val scheme = uri?.scheme
            if (scheme != C.HTTPS_URL_SCHEME && scheme != C.HTTP_URL_SCHEME)
                return false
            val decodedUrl = uri.decode()
            val fragment = Uri.parse(decodedUrl).fragment ?: ""
            val deepLinkUrlWebFragments = CordialApiConfiguration.getInstance().deepLinkWebOnlyFragments
            if (deepLinkUrlWebFragments.contains(fragment)) {
                openBrowser(uri)
                return true
            }
            return false
        } catch (e: IllegalArgumentException) {
            return false
        }
    }

    private fun openBrowser(uri: Uri?) {
        uri?.let {
            val browserHandler = DependencyConfiguration.getInstance().browser()
            browserHandler.openLink(uri)
        }
    }
}