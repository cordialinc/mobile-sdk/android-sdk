package com.cordial.feature

import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.upsertcontact.model.OnSetContactRequestFromDBListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.preferences.PreferenceKeys

internal abstract class CordialCheck {

    fun isNetworkAvailable(): Boolean {
        return CordialApiConfiguration.getInstance().networkStateHandler?.isNetworkAvailable() ?: false
    }

    fun isJwtTokenNotEmpty(): Boolean {
        val preferences = CordialApiConfiguration.getInstance().injection.preferences
        val jwtToken = preferences.getString(PreferenceKeys.JWT_TOKEN)
        return jwtToken.isNotEmpty()
    }

    fun isLoggedIn(): Boolean {
        val preferences = CordialApiConfiguration.getInstance().injection.preferences
        return preferences.getBoolean(PreferenceKeys.IS_LOGGED_IN)
    }

    fun isRequestNotFromCache(onRequestFromDBListener: OnRequestFromDBListener?): Boolean {
        return onRequestFromDBListener == null
    }

    var isChecked: Boolean = false

    fun CordialCheck.doAfterCheck(runBlock: (() -> Unit)?) {
        if (isChecked) {
            runBlock?.let { run ->
                run()
            }
            isChecked = false
        }
    }
}