package com.cordial.feature.sendcontactorder.usecase

import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.feature.sendcontactorder.repository.SendContactOrderRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.OnRequestFromDBListener
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.contactorder.ContactOrderDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences

internal class SendContactOrderUseCaseImpl(
    private val sendContactOrderRepository: SendContactOrderRepository,
    private val preferences: Preferences,
    private val contactOrderDao: ContactOrderDao?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : SendContactOrderUseCase, CordialCheck() {

    override fun sendContactOrders(
        contactOrdersRequests: List<ContactOrderRequest>,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        checkAuth(contactOrdersRequests, onRequestFromDBListener)
        doAfterCheck {
            sendContactOrderRepository.sendContactOrders(contactOrdersRequests, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    onRequestFromDBListener?.onSuccess()
                }

                override fun onSystemError(
                    error: String,
                    onSaveRequestListener: (() -> Unit)?
                ) {
                    handleSystemError(
                        onRequestFromDBListener,
                        contactOrdersRequests,
                        "Failed to sending contact order due to the error: $error. Saving contact orders to the cache",
                        onSaveRequestListener
                    )
                }

                override fun onLogicError(response: String) {
                    onRequestFromDBListener?.onFailure()
                    CordialLoggerManager.error("Failed to send contact order due to the error: $response")
                }
            })
        }
    }

    private fun checkAuth(
        contactOrderRequests: List<ContactOrderRequest>,
        onRequestFromDBListener: OnRequestFromDBListener?
    ) {
        val loginCheck = Check(
            checkFunction = { isLoggedIn() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    contactOrderRequests,
                    reason = "Failed to send the contact orders because the contact is not registered. " +
                            "Saving contact orders to the cache",
                    onSaveRequestListener = null
                )
            }
        )
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            nextCheck = loginCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    contactOrderRequests,
                    reason = "Failed to send the contact orders because the jwt is absent. " +
                            "Saving contact orders to the cache"
                ) {
                    sdkSecurityUseCase.updateJWT()
                }
            })
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    contactOrderRequests,
                    reason = "Failed to send the contact orders because there is no network connection. " +
                            "Saving contact orders to the cache",
                    onSaveRequestListener = null
                )
            })
        networkAvailableCheck.execute()
    }

    private fun handleSystemError(
        onRequestFromDBListener: OnRequestFromDBListener?,
        contactOrdersRequests: List<ContactOrderRequest>,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        CordialLoggerManager.info(reason)
        if (isRequestNotFromCache(onRequestFromDBListener)) {
            saveContactOrdersToDB(contactOrdersRequests, onSaveRequestListener)
        } else {
            onSaveRequestListener?.invoke()
            onRequestFromDBListener?.onFailure()
        }
    }

    private fun saveContactOrdersToDB(
        contactOrdersRequests: List<ContactOrderRequest>,
        onSaveRequestListener: (() -> Unit)?
    ) {
        contactOrdersRequests.forEachIndexed { index, order ->
            val isLastItem = index == (contactOrdersRequests.size - 1)
            contactOrderDao?.insert(order) {
                if (isLastItem) {
                    onSaveRequestListener?.invoke()
                }
            }
        }
    }

    override fun sendOrderCachedData() {
        if (!SendingCacheState.sendingContactOrder.compareAndSet(false, true))
            return
        contactOrderDao?.getAllContactOrders { requests ->
            if (requests.isEmpty()) {
                SendingCacheState.sendingContactOrder.set(false)
                return@getAllContactOrders
            }
            val contactOrderRequests = setPkAndDeviceIdToContactOrders(requests)
            sendCachedContactOrders(contactOrderRequests)
        }
    }

    private fun setPkAndDeviceIdToContactOrders(requests: List<ContactOrderRequest>): List<ContactOrderRequest> {
        return requests.map { contactOrderRequest ->
            ContactOrderRequest(
                preferences.getString(PreferenceKeys.DEVICE_ID),
                preferences.getString(PreferenceKeys.PRIMARY_KEY),
                contactOrderRequest.mcID,
                contactOrderRequest.mcTapTime,
                contactOrderRequest.order
            )
        }
    }

    private fun sendCachedContactOrders(contactOrderRequests: List<ContactOrderRequest>) {
        sendContactOrders(contactOrderRequests, object : OnRequestFromDBListener {
            override fun onSuccess() {
                contactOrderDao?.clear()
                CordialLoggerManager.info("Contact order requests sent, clearing contact order cache")
                SendingCacheState.sendingContactOrder.set(false)
            }

            override fun onFailure() {
                SendingCacheState.sendingContactOrder.set(false)
            }
        }
        )
    }

    override fun clearCache(onCompleteListener: (() -> Unit)?) {
        contactOrderDao?.clear(onCompleteListener)
    }
}