package com.cordial.feature.sendcontactorder.repository

import com.cordial.api.C
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.sendcontactorder.model.Address
import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.feature.upsertcontactcart.model.CartItemJsonUtil
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.JsonUtils
import org.json.JSONArray
import org.json.JSONObject

internal class SendContactOrderRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : SendContactOrderRepository {

    override fun sendContactOrders(
        contactOrdersRequests: List<ContactOrderRequest>,
        onResponseListener: OnResponseListener
    ) {
        val requestJson = getSendContactOrdersJSON(contactOrdersRequests)
        val url = cordialApiEndpoints.getSendContactOrderUrl()
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getSendContactOrdersJSON(contactOrdersRequests: List<ContactOrderRequest>): String {
        val jsonArray = JSONArray()
        contactOrdersRequests.forEach { contactOrdersRequest ->
            val param = JSONObject()
            param.put(C.DEVICE_ID, contactOrdersRequest.deviceId)
            if (contactOrdersRequest.primaryKey.isNotEmpty()) {
                param.put(C.PRIMARY_KEY, contactOrdersRequest.primaryKey)
            }
            contactOrdersRequest.mcID?.let { mcID ->
                param.put(C.MC_ID, mcID)
            }
            contactOrdersRequest.mcTapTime?.let { mcTapTime ->
                param.put(C.MC_TAP_TIME, mcTapTime)
            }
            val order = contactOrdersRequest.order
            val orderJSON = JSONObject()
            orderJSON.put(C.ORDER_ID, order.orderId)
            orderJSON.put(C.STATUS, order.status)
            orderJSON.put(C.STORE_ID, order.storeId)
            orderJSON.put(C.CUSTOMER_ID, order.customerId)
            orderJSON.put(C.PURCHASE_DATE, order.getPurchaseDate())
            orderJSON.put(C.SHIPPING_ADDRESS, getAddressObjectJSON(order.shippingAddress))
            orderJSON.put(C.BILLING_ADDRESS, getAddressObjectJSON(order.billingAddress))
            val itemsArray = JSONArray()
            order.items.forEach { cartItem ->
                val item = CartItemJsonUtil.getCartItemJsonObject(cartItem)
                itemsArray.put(item)
            }
            orderJSON.put(C.ITEMS, itemsArray)
            order.tax?.let {
                orderJSON.put(C.TAX, it)
            }
            order.shippingAndHandling?.let {
                orderJSON.put(C.SHIPPING_AND_HANDLING, it)
            }
            order.properties?.let { propertyList ->
                val properties = JsonUtils.getJsonFromPropertyValueMap(propertyList)
                orderJSON.put(C.PROPERTIES, properties)
            }
            param.put(C.ORDER, orderJSON)
            jsonArray.put(param)
        }
        return jsonArray.toString()
    }

    private fun getAddressObjectJSON(address: Address): JSONObject {
        val param = JSONObject()
        param.put(C.NAME, address.name)
        param.put(C.ADDRESS, address.address)
        param.put(C.CITY, address.city)
        param.put(C.STATE, address.state)
        param.put(C.POSTAL_CODE, address.postalCode)
        param.put(C.COUNTRY, address.country)
        return param
    }
}