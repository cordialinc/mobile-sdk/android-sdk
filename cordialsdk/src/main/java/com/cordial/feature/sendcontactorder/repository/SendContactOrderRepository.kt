package com.cordial.feature.sendcontactorder.repository

import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.network.response.OnResponseListener

internal interface SendContactOrderRepository {
    fun sendContactOrders(contactOrdersRequests: List<ContactOrderRequest>, onResponseListener: OnResponseListener)
}