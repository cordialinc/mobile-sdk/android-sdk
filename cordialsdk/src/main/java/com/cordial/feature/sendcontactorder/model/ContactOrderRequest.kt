package com.cordial.feature.sendcontactorder.model

internal class ContactOrderRequest(
    val deviceId: String,
    val primaryKey: String,
    val mcID: String?,
    val mcTapTime: String?,
    val order: Order
)