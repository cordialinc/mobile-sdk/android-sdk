package com.cordial.feature.sendcontactorder.model

import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.util.TimeUtils
import java.util.*

data class Order(
    val orderId: String,
    val status: String,
    val storeId: String,
    val customerId: String,
    val shippingAddress: Address,
    val billingAddress: Address,
    var items: List<CartItem>
) {
    @Deprecated(
        level = DeprecationLevel.WARNING,
        message = "Replaced by Order(String, String, String, String, Address, Address, List<CartItem>)"
    )
    constructor(
        orderId: String,
        status: String,
        storeId: String,
        customerId: String,
        shippingAddress: Address,
        billingAddress: Address,
        items: List<CartItem>,
        tax: Double?,
        shippingAndHandling: Double?,
        properties: Map<String, String>?
    ) : this(
        orderId,
        status,
        storeId,
        customerId,
        shippingAddress,
        billingAddress,
        items
    ) {
        this.tax = tax
        this.shippingAndHandling = shippingAndHandling
        val propertyValues = properties?.mapValues { PropertyValue.StringProperty(it.value) }
        this.properties = propertyValues
    }

    private var purchaseDate: String = TimeUtils.getTimestamp()
    var tax: Double? = null
    var shippingAndHandling: Double? = null
    var properties: Map<String, PropertyValue>? = null

    fun setPurchaseDate(date: Date) {
        setPurchaseDate(TimeUtils.getTimestamp(date))
    }

    internal fun setPurchaseDate(timestamp: String) {
        this.purchaseDate = timestamp
    }

    fun getPurchaseDate(): String {
        return purchaseDate
    }

    fun withPurchaseDate(date: Date): Order {
        setPurchaseDate(date)
        return this
    }

    fun withTax(tax: Double?): Order {
        this.tax = tax
        return this
    }

    fun withShippingAndHandling(shippingAndHandling: Double?): Order {
        this.shippingAndHandling = shippingAndHandling
        return this
    }

    fun withProperties(properties: Map<String, PropertyValue>?): Order {
        this.properties = properties
        return this
    }
}