package com.cordial.feature.sendcontactorder.model

class Address(
    val name: String,
    val address: String,
    val city: String,
    val state: String,
    val postalCode: String,
    val country: String
)