package com.cordial.feature.sendcontactorder.usecase

import com.cordial.feature.CacheableUseCase
import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.storage.db.OnRequestFromDBListener

internal interface SendContactOrderUseCase : CacheableUseCase {
    fun sendContactOrders(
        contactOrdersRequests: List<ContactOrderRequest>,
        onRequestFromDBListener: OnRequestFromDBListener?
    )

    fun sendOrderCachedData()
}