package com.cordial.feature.upsertcontact.model.attributes

import java.util.*

class DateValue(val date: Date?) : AttributeValue