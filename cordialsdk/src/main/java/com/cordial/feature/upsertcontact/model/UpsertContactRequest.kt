package com.cordial.feature.upsertcontact.model

import com.cordial.feature.upsertcontact.model.attributes.AttributeValue

internal class UpsertContactRequest(
    var primaryKey: String,
    var attributes: Map<String, AttributeValue>?
) {
    constructor(primaryKey: String) : this(primaryKey, null)

    constructor(
        deviceId: String,
        primaryKey: String,
        token: String?,
        subscribeStatus: SubscribeStatus?,
        status: NotificationStatus,
        attributes: Map<String, AttributeValue>?
    ) : this(primaryKey, attributes) {
        this.deviceId = deviceId
        this.token = token
        this.subscribeStatus = subscribeStatus
        this.status = status
    }

    constructor(
        primaryKey: String,
        token: String?,
        subscribeStatus: SubscribeStatus?,
        status: NotificationStatus,
        attributes: Map<String, AttributeValue>?,
        dbId: Int? = null
    ) : this(primaryKey, attributes) {
        this.deviceId = ""
        this.token = token
        this.subscribeStatus = subscribeStatus
        this.status = status
        this.dbId = dbId
    }

    lateinit var deviceId: String
    var token: String? = null
    var subscribeStatus: SubscribeStatus? = null
    lateinit var status: NotificationStatus
    var isInternalRequest: Boolean = false
    var dbId: Int? = null

    fun isPushToken(): Boolean {
        return token != null
    }
}