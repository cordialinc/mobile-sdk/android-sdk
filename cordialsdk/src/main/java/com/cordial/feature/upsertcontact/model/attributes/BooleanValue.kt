package com.cordial.feature.upsertcontact.model.attributes

class BooleanValue(val value: Boolean) : AttributeValue