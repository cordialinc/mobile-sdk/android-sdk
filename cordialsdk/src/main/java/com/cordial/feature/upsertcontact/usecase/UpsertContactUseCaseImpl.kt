package com.cordial.feature.upsertcontact.usecase

import com.cordial.api.CordialApiConfiguration
import com.cordial.api.MessageAttributionManager
import com.cordial.feature.Check
import com.cordial.feature.CordialCheck
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.throttler.ThrottlerManager
import com.cordial.feature.upsertcontact.model.NotificationStatus
import com.cordial.feature.upsertcontact.model.OnSetContactRequestFromDBListener
import com.cordial.feature.upsertcontact.model.UpsertContactRequest
import com.cordial.feature.upsertcontact.model.attributes.AttributeValue
import com.cordial.feature.upsertcontact.repository.UpsertContactRepository
import com.cordial.network.response.OnResponseListener
import com.cordial.storage.db.CacheManager
import com.cordial.storage.db.SendingCacheState
import com.cordial.storage.db.dao.contactlogout.UnsetContactDao
import com.cordial.storage.db.dao.setcontact.SetContactDao
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.TimeUtils

internal class UpsertContactUseCaseImpl(
    private val upsertContactRepository: UpsertContactRepository,
    private val preferences: Preferences,
    private val setContactDao: SetContactDao?,
    private val unsetContactDao: UnsetContactDao?,
    private val messageAttributionManager: MessageAttributionManager?,
    private val cacheManager: CacheManager?,
    private val sdkSecurityUseCase: SDKSecurityUseCase
) : UpsertContactUseCase, CordialCheck() {

    override fun upsertContact(
        primaryKey: String?,
        attributes: Map<String, AttributeValue>?,
        isInternalRequest: Boolean
    ) {
        val upsertContactRequest = getUpsertContactRequest(primaryKey, attributes, isInternalRequest)
        if (!upsertContactRequest.isPushToken()) {
            CordialLoggerManager.info("Failed to set contact because the push token is absent")
            return
        }
        saveUpsertContactToDB(upsertContactRequest, onSaveRequestListener =  {
            ThrottlerManager.upsertContactThrottler.throttle {
                sendCachedUpsertContacts(onSendLogoutListener = null)
            }
        })
    }

    private fun getUpsertContactRequest(
        primaryKey: String?,
        attributes: Map<String, AttributeValue>?,
        isInternalRequest: Boolean
    ): UpsertContactRequest {
        val upsertContactRequest = UpsertContactRequest(primaryKey ?: "", attributes)
        upsertContactRequest.isInternalRequest = isInternalRequest
        val deviceIdentifier = preferences.getString(PreferenceKeys.DEVICE_ID)
        upsertContactRequest.deviceId = deviceIdentifier
        val pushToken = if (preferences.contains(PreferenceKeys.FIREBASE_TOKEN)) {
            preferences.getString(PreferenceKeys.FIREBASE_TOKEN)
        } else null
        upsertContactRequest.token = pushToken
        upsertContactRequest.status =
            NotificationStatus.findKey(preferences.getString(PreferenceKeys.NOTIFICATION_STATUS))
        return upsertContactRequest
    }

    private fun saveUpsertContactToDB(
        upsertContactRequest: UpsertContactRequest,
        onSaveRequestListener: (() -> Unit)? = null
    ) {
        setContactDao?.insert(upsertContactRequest, onSaveRequestListener)
    }

    private fun upsertContacts(
        upsertContactRequests: List<UpsertContactRequest>,
        onRequestFromDBListener: OnSetContactRequestFromDBListener?
    ) {
        checkAuth(upsertContactRequests, onRequestFromDBListener)
        doAfterCheck {
            upsertContactRepository.upsertContacts(upsertContactRequests, object : OnResponseListener {
                override fun onSuccess(response: String) {
                    handleSuccessResponse(onRequestFromDBListener)
                }

                override fun onSystemError(error: String, onSaveRequestListener: (() -> Unit)?) {
                    handleSystemError(
                        onRequestFromDBListener,
                        upsertContactRequests,
                        reason = "Failed to upsert contact due to the error: $error, saving upsert contact request to the cache",
                        onSaveRequestListener
                    )
                }

                override fun onLogicError(response: String) {
                    onRequestFromDBListener?.onFailure()
                    CordialLoggerManager.error("Failed to upsert contact due to the error: $response")
                }
            })
        }
    }

    private fun checkAuth(
        upsertContactRequests: List<UpsertContactRequest>,
        onRequestFromDBListener: OnSetContactRequestFromDBListener?
    ) {
        val jwtTokenEmptyCheck = Check(
            checkFunction = { isJwtTokenNotEmpty() },
            doOnSuccess = { isChecked = true },
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    upsertContactRequests,
                    reason = "Failed to set contact because the jwt is absent, saving set contact request to the cache",
                    onSaveRequestListener = {
                        sdkSecurityUseCase.updateJWT()
                    }
                )
            })
        val networkAvailableCheck = Check(
            checkFunction = { isNetworkAvailable() },
            nextCheck = jwtTokenEmptyCheck,
            doOnError = {
                handleSystemError(
                    onRequestFromDBListener,
                    upsertContactRequests,
                    reason = "Failed to set contact because there is no network connection, saving set contact request to the cache",
                    onSaveRequestListener = null
                )
            })
        val emptyRequestCheck = Check(
            checkFunction = { upsertContactRequests.isNotEmpty() },
            doOnSuccess = {
                checkOnClearCache(upsertContactRequests)
                savePrimaryKey(upsertContactRequests)
            },
            nextCheck = networkAvailableCheck,
            doOnError = { CordialLoggerManager.info("Cannot send an empty list of set contact requests") }
        )
        emptyRequestCheck.execute()
    }

    private fun handleSuccessResponse(onRequestFromDBListener: OnSetContactRequestFromDBListener?) {
        preferences.put(PreferenceKeys.IS_LOGGED_IN, true)
        preferences.put(PreferenceKeys.LAST_SET_CONTACT_TIMESTAMP, TimeUtils.getTimestamp())
        onRequestFromDBListener?.onSuccess {
            cacheManager?.sendCachedData()
        }
        CordialApiConfiguration.getInstance().checkNotificationStatus()
    }

    private fun checkOnClearCache(upsertContactRequests: List<UpsertContactRequest>) {
        upsertContactRequests.forEach { upsertContactRequest ->
            clearTimestampsData(upsertContactRequest)
            val previousPrimaryKey = preferences.getString(PreferenceKeys.PREVIOUS_PRIMARY_KEY, "")
            if (previousPrimaryKey.isNotEmpty() && previousPrimaryKey != upsertContactRequest.primaryKey) {
                messageAttributionManager?.clearMessageAttributes()
                cacheManager?.clearCache()
                CordialLoggerManager.info(
                    "Clearing all cache because the previous primaryKey ($previousPrimaryKey) " +
                            "is not equal to the current primaryKey (${upsertContactRequest.primaryKey})"
                )
                preferences.remove(PreferenceKeys.PREVIOUS_PRIMARY_KEY)
                preferences.remove(PreferenceKeys.NOTIFICATION_STATUS)
            }
        }
    }

    private fun savePrimaryKey(
        upsertContactRequests: List<UpsertContactRequest>,
    ) {
        upsertContactRequests.forEach { upsertContactRequest ->
            preferences.put(PreferenceKeys.PRIMARY_KEY, upsertContactRequest.primaryKey)
            preferences.put(PreferenceKeys.IS_CONTACT_SET, true)
        }
    }

    private fun clearTimestampsData(upsertContactRequest: UpsertContactRequest) {
        val primaryKey = upsertContactRequest.primaryKey
        val isInternalRequest = upsertContactRequest.isInternalRequest
        val previousPrimaryKey = preferences.getString(PreferenceKeys.PREVIOUS_PRIMARY_KEY, "")
        if (!isInternalRequest && previousPrimaryKey != primaryKey) {
            CordialLoggerManager.info(
                "Clearing timestamps data because the previous primaryKey ($previousPrimaryKey) " +
                        "is not equal to the current primaryKey ($primaryKey)"
            )
            preferences.remove(PreferenceKeys.TIMESTAMPS_URL)
            preferences.remove(PreferenceKeys.TIMESTAMPS_URL_EXPIRE_AT)
            preferences.remove(PreferenceKeys.LAST_IN_APP_TIMESTAMP)
        }
    }

    private fun handleSystemError(
        onRequestFromDBListener: OnSetContactRequestFromDBListener?,
        upsertContactRequests: List<UpsertContactRequest>,
        reason: String,
        onSaveRequestListener: (() -> Unit)?
    ) {
        upsertContactRequests.forEach { upsertContactRequest ->
            CordialLoggerManager.info("$reason : ${upsertContactRequest.primaryKey}")
        }
        onSaveRequestListener?.invoke()
        onRequestFromDBListener?.onFailure()
    }

    override fun sendCachedUpsertContacts(onSendLogoutListener: (() -> Unit)?) {
        if (!SendingCacheState.sendingUpsertContact.compareAndSet(false, true)) {
            return
        }
        setContactDao?.getSetContacts { upsertContactRequests ->
            if (upsertContactRequests.isEmpty()) {
                SendingCacheState.sendingUpsertContact.set(false)
                onSendLogoutListener?.invoke()
                return@getSetContacts
            }
            addDeviceIDToUpsertContactRequests(upsertContactRequests)
            upsertCachedContacts(upsertContactRequests)
        }
    }

    private fun addDeviceIDToUpsertContactRequests(upsertContactRequests: List<UpsertContactRequest>) {
        upsertContactRequests.forEach { upsertContactRequest ->
            upsertContactRequest.deviceId = preferences.getString(PreferenceKeys.DEVICE_ID)
        }
    }

    private fun upsertCachedContacts(upsertContactRequests: List<UpsertContactRequest>) {
        upsertContacts(upsertContactRequests, object : OnSetContactRequestFromDBListener {
            override fun onSuccess(onSetContactDBReadyListener: (() -> Unit)?) {
                unsetContactDao?.clear()
                deleteUpsertContactRequests(upsertContactRequests)
                CordialLoggerManager.info("Upsert contact sent")
                SendingCacheState.sendingUpsertContact.set(false)
                onSetContactDBReadyListener?.invoke()
            }

            override fun onFailure() {
                SendingCacheState.sendingUpsertContact.set(false)
            }
        })
    }

    override fun sendSelfHealingIfNotLoggedOut() {
        setContactDao?.getSetContacts { upsertContactRequests ->
            if (upsertContactRequests.isEmpty()) {
                setContactIfNotLoggedOut(isSelfHealing = true)
            }
        }
    }

    override fun setContactIfNotLoggedOut(isSelfHealing: Boolean) {
        val isLoggedOut = preferences.contains(PreferenceKeys.IS_LOGGED_IN)
                && !preferences.getBoolean(PreferenceKeys.IS_LOGGED_IN, false)
        if (!isLoggedOut) {
            val primaryKey = preferences.getString(PreferenceKeys.PRIMARY_KEY)
            upsertContact(primaryKey, null, isInternalRequest = true)
            if (isSelfHealing)
                CordialLoggerManager.info("Sending contact self-healing request")
        }
    }

    override fun clearUpsertContactCache(onSendLogoutListener: (() -> Unit)?) {
        setContactDao?.clear(onSendLogoutListener)
    }

    private fun deleteUpsertContactRequests(upsertContactRequests: List<UpsertContactRequest>) {
        setContactDao?.deleteSetContacts(upsertContactRequests, onDeleteCompleteListener = null)
    }
}