package com.cordial.feature.upsertcontact.model

internal enum class NotificationStatus(val status: String) {
    OPT_IN("opt-in"),
    OPT_OUT("opt-out");

    companion object {
        fun findKey(status: String): NotificationStatus {
            return values().firstOrNull { it.status == status } ?: OPT_IN
        }

        fun findKey(isEnabled: Boolean): NotificationStatus {
            return when (isEnabled) {
                true -> OPT_IN
                false -> OPT_OUT
            }
        }

        fun isEnabled(status: NotificationStatus): Boolean {
            return when (status) {
                OPT_IN -> true
                OPT_OUT -> false
            }
        }
    }
}