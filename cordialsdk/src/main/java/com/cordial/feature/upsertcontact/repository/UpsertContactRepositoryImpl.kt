package com.cordial.feature.upsertcontact.repository

import com.cordial.api.C
import com.cordial.api.CordialApiEndpoints
import com.cordial.feature.upsertcontact.model.UpsertContactRequest
import com.cordial.network.request.RequestMethod
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequest
import com.cordial.network.response.OnResponseListener
import com.cordial.network.response.ResponseHandler
import com.cordial.util.JsonUtils
import org.json.JSONArray
import org.json.JSONObject

internal class UpsertContactRepositoryImpl(
    private val requestSender: RequestSender,
    private val responseHandler: ResponseHandler,
    private val cordialApiEndpoints: CordialApiEndpoints
) : UpsertContactRepository {

    override fun upsertContacts(
        upsertContactRequests: List<UpsertContactRequest>,
        onResponseListener: OnResponseListener
    ) {
        val requestJson = getUpsertContactRequestJSON(upsertContactRequests)
        val url = cordialApiEndpoints.getContactsUrl()
        val sdkRequest = SDKRequest(requestJson, url, RequestMethod.POST)
        requestSender.send(sdkRequest, responseHandler, onResponseListener)
    }

    private fun getUpsertContactRequestJSON(upsertContactRequests: List<UpsertContactRequest>): String {
        val jsonArray = JSONArray()
        upsertContactRequests.forEach { upsertContactRequest ->
            val param = JSONObject()
            param.put(C.DEVICE_ID, upsertContactRequest.deviceId)
            if (upsertContactRequest.primaryKey.trim().isNotEmpty()) {
                param.put(C.PRIMARY_KEY, upsertContactRequest.primaryKey)
            }
            param.put(C.STATUS, upsertContactRequest.status.status)
            upsertContactRequest.token?.let {
                param.put(C.TOKEN, it)
            }
            upsertContactRequest.attributes?.let { attrList ->
                param.put(C.ATTRIBUTES, JsonUtils.getJsonFromAttributeValueMap(attrList))
            }
            upsertContactRequest.subscribeStatus?.let {
                param.put(C.SUBSCRIBE_STATUS, it.status)
            }
            jsonArray.put(param)
        }
        return jsonArray.toString()

    }
}