package com.cordial.feature.upsertcontact.model

internal interface OnSetContactRequestFromDBListener {
    fun onSuccess(onSetContactDBReadyListener: (() -> Unit)?)
    fun onFailure()
}