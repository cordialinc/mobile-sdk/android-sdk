package com.cordial.feature.upsertcontact.repository

import com.cordial.feature.upsertcontact.model.UpsertContactRequest
import com.cordial.network.response.OnResponseListener

internal interface UpsertContactRepository {
    fun upsertContacts(upsertContactRequests: List<UpsertContactRequest>, onResponseListener: OnResponseListener)
}