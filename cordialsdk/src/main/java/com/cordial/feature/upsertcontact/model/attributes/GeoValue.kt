package com.cordial.feature.upsertcontact.model.attributes

class GeoValue : AttributeValue {
    var streetAddress: String = ""
    var streetAddress2: String = ""
    var postalCode: String = ""
    var country: String = ""
    var state: String = ""
    var city: String = ""
    var timezone: String = ""
}