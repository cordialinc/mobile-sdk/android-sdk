package com.cordial.feature.upsertcontact.usecase

import com.cordial.feature.upsertcontact.model.attributes.AttributeValue

internal interface UpsertContactUseCase {
    fun upsertContact(primaryKey: String?, attributes: Map<String, AttributeValue>?, isInternalRequest: Boolean = false)

    fun sendCachedUpsertContacts(onSendLogoutListener: (() -> Unit)?)

    fun sendSelfHealingIfNotLoggedOut()

    fun setContactIfNotLoggedOut(isSelfHealing: Boolean = false)

    fun clearUpsertContactCache(onSendLogoutListener: (() -> Unit)? = null)
}