package com.cordial.feature.upsertcontact.model

internal enum class SubscribeStatus(val status: String) {
    SUBSCRIBED("subscribed"),
    UNSUBSCRIBED("unsubscribed");

    companion object {
        fun findKey(status: String): SubscribeStatus {
            return values().firstOrNull { it.status == status } ?: SUBSCRIBED
        }
    }
}