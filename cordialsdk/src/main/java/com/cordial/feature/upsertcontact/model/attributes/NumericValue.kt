package com.cordial.feature.upsertcontact.model.attributes

class NumericValue(val value: Double?) : AttributeValue {
    constructor(value: Int?) : this(value?.toDouble())
}