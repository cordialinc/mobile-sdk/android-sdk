package com.cordial.lifecycle.devicelock

import android.app.KeyguardManager
import android.content.Context
import com.cordial.api.CordialApiConfiguration

internal class DeviceLockStatusRequester: OnDeviceLockStatus {
    override fun isDeviceLocked(): Boolean {
        val context = CordialApiConfiguration.getInstance().getContext()
        val keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
        return keyguardManager.isKeyguardLocked
    }
}