package com.cordial.lifecycle.devicelock

internal interface OnDeviceLockStatus {
    fun isDeviceLocked(): Boolean
}