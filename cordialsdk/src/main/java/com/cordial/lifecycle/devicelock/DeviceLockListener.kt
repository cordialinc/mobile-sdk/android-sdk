package com.cordial.lifecycle.devicelock

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import com.cordial.dependency.DependencyConfiguration
import com.cordial.lifecycle.AppLifecycleHandler

internal class DeviceLockListener(private val lifecycleDelegate: AppLifecycleHandler.LifecycleDelegate) :
    BroadcastReceiver() {

    fun register(context: Context) {
        val intentFilter = IntentFilter()
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF)
        intentFilter.addAction(Intent.ACTION_USER_PRESENT)
        context.registerReceiver(this, intentFilter)
    }

    fun unregister(context: Context) {
        try {
            context.unregisterReceiver(this)
        } catch (e: IllegalArgumentException) {
            e.printStackTrace()
        }
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        intent?.action?.let { action ->
            if (action == Intent.ACTION_USER_PRESENT || action == Intent.ACTION_SCREEN_OFF) {
                val deviceLockStatusRequester = DependencyConfiguration.getInstance().deviceLockStatusRequester
                if (deviceLockStatusRequester.isDeviceLocked() && AppLifecycleHandler.appInForeground) {
                    AppLifecycleHandler.appInForeground = false
                    lifecycleDelegate.onAppBackgrounded()
                }
            }
        }
    }
}