package com.cordial.lifecycle

import android.app.Activity
import android.app.Application
import android.content.ComponentCallbacks2
import android.content.res.Configuration
import android.os.Bundle
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.inappmessage.model.InAppMessageDelayMode
import com.cordial.feature.inappmessage.model.InAppMessageDelayType

internal class AppLifecycleHandler(
    private val lifecycleDelegate: LifecycleDelegate,
    private var inAppMessageDelayMode: InAppMessageDelayMode
) : Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    override fun onLowMemory() {
    }

    override fun onConfigurationChanged(p0: Configuration) {
    }

    override fun onTrimMemory(level: Int) {
        if (level >= ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN && appInForeground) {
            appInForeground = false
            lifecycleDelegate.onAppBackgrounded()
        }
    }

    override fun onActivityPaused(p0: Activity) {
    }

    override fun onActivityStarted(p0: Activity) {
    }

    override fun onActivityDestroyed(p0: Activity) {
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
    }

    override fun onActivityStopped(p0: Activity) {
    }

    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
        if (!isAppInForeground()) {
            appInForeground = true
            lifecycleDelegate.onAppForegrounded()
        }
        if (inAppMessageDelayMode.delayType == InAppMessageDelayType.ACTIVITIES) {
            inAppMessageDelayMode.disallowedActivities.let { activities ->
                val isInAppShowEnabled = !activities.contains(activity::class.java)
                lifecycleDelegate.onInAppShowEnabled(isInAppShowEnabled)
            }
        }
    }

    companion object {
        var appInForeground = false

        fun isAppInForeground(): Boolean {
            val deviceLockStatusRequester = DependencyConfiguration.getInstance().deviceLockStatusRequester
            return appInForeground && !deviceLockStatusRequester.isDeviceLocked()
        }
    }

    interface LifecycleDelegate {
        fun onAppBackgrounded()
        fun onAppForegrounded()
        fun onInAppShowEnabled(isEnabled: Boolean)
    }
}