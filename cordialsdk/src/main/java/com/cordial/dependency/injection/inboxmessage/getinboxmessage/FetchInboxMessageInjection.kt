package com.cordial.dependency.injection.inboxmessage.getinboxmessage

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inboxmessage.getinboxmessage.repository.FetchInboxMessageRepository
import com.cordial.feature.inboxmessage.getinboxmessage.repository.FetchInboxMessageRepositoryImpl
import com.cordial.feature.inboxmessage.getinboxmessage.usecase.FetchInboxMessageUseCase
import com.cordial.feature.inboxmessage.getinboxmessage.usecase.FetchInboxMessageUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler

internal class FetchInboxMessageInjection(
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val fetchInboxMessageRepository: FetchInboxMessageRepository =
        FetchInboxMessageRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val fetchInboxMessageUseCase: FetchInboxMessageUseCase =
        FetchInboxMessageUseCaseImpl(
            fetchInboxMessageRepository,
            localStorageInjection.inboxMessageDao,
            sdkSecurityUseCase
        )
}