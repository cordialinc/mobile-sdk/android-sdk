package com.cordial.dependency.injection.inboxmessage.updateinboxmessagereadstatus

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.repository.UpdateInboxMessageReadStatusRepository
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.repository.UpdateInboxMessageReadStatusRepositoryImpl
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.usecase.UpdateInboxMessageReadStatusUseCase
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.usecase.UpdateInboxMessageReadStatusUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class UpdateInboxMessageReadStatusInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val updateInboxMessageReadStatusRepository: UpdateInboxMessageReadStatusRepository =
        UpdateInboxMessageReadStatusRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val updateInboxMessageReadStatusUseCase: UpdateInboxMessageReadStatusUseCase =
        UpdateInboxMessageReadStatusUseCaseImpl(
            updateInboxMessageReadStatusRepository,
            preferences,
            localStorageInjection.updateInboxMessageReadStatusDao,
            sdkSecurityUseCase
        )
}