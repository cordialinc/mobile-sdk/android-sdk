package com.cordial.dependency.injection.inappmessage

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inappmessage.getinappmessagesdata.repository.InAppMessagesDataRepository
import com.cordial.feature.inappmessage.getinappmessagesdata.repository.InAppMessagesDataRepositoryImpl
import com.cordial.feature.inappmessage.getinappmessagesdata.usecase.InAppMessageDataUseCase
import com.cordial.feature.inappmessage.getinappmessagesdata.usecase.InAppMessageDataUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class InAppMessageDataInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    inAppMessageInjection: InAppMessageInjection,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {

    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val inAppMessagesDataRepository: InAppMessagesDataRepository =
        InAppMessagesDataRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val inAppMessageDataUseCase: InAppMessageDataUseCase =
        InAppMessageDataUseCaseImpl(
            preferences,
            inAppMessagesDataRepository,
            inAppMessageInjection.inAppMessageUseCase,
            localStorageInjection.fetchInAppMessageDao,
            localStorageInjection.inAppMessageToDeleteDao,
            sdkSecurityUseCase
        )
}