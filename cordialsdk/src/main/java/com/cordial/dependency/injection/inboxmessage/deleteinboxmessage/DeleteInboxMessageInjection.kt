package com.cordial.dependency.injection.inboxmessage.deleteinboxmessage

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inboxmessage.deleteinboxmessage.repository.DeleteInboxMessageRepository
import com.cordial.feature.inboxmessage.deleteinboxmessage.repository.DeleteInboxMessageRepositoryImpl
import com.cordial.feature.inboxmessage.deleteinboxmessage.usecase.DeleteInboxMessageUseCase
import com.cordial.feature.inboxmessage.deleteinboxmessage.usecase.DeleteInboxMessageUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class DeleteInboxMessageInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val deleteInboxMessageRepository: DeleteInboxMessageRepository =
        DeleteInboxMessageRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val deleteInboxMessageUseCase: DeleteInboxMessageUseCase =
        DeleteInboxMessageUseCaseImpl(
            deleteInboxMessageRepository,
            preferences,
            localStorageInjection.deleteInboxMessageDao,
            localStorageInjection.inboxMessageDao,
            localStorageInjection.inboxMessageContentDao,
            sdkSecurityUseCase
        )
}