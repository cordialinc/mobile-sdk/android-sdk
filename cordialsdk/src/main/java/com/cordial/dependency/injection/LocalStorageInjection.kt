package com.cordial.dependency.injection

import com.cordial.storage.db.dao.contactcart.ContactCartDBHelper
import com.cordial.storage.db.dao.contactcart.ContactCartDao
import com.cordial.storage.db.dao.contactlogout.UnsetContactDBHelper
import com.cordial.storage.db.dao.contactlogout.UnsetContactDao
import com.cordial.storage.db.dao.contactorder.ContactOrderDBHelper
import com.cordial.storage.db.dao.contactorder.ContactOrderDao
import com.cordial.storage.db.dao.event.EventDBHelper
import com.cordial.storage.db.dao.event.EventDao
import com.cordial.storage.db.dao.inappmessage.fetchinappmessage.FetchInAppMessageDBHelper
import com.cordial.storage.db.dao.inappmessage.fetchinappmessage.FetchInAppMessageDao
import com.cordial.storage.db.dao.inappmessage.inappmessagedata.InAppMessageDBHelper
import com.cordial.storage.db.dao.inappmessage.inappmessagedata.InAppMessageDao
import com.cordial.storage.db.dao.inappmessage.inappmessagetodelete.InAppMessageToDeleteDBHelper
import com.cordial.storage.db.dao.inappmessage.inappmessagetodelete.InAppMessageToDeleteDao
import com.cordial.storage.db.dao.inboxmessage.deleteinboxmessagee.DeleteInboxMessageDBHelper
import com.cordial.storage.db.dao.inboxmessage.deleteinboxmessagee.DeleteInboxMessageDao
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDBHelper
import com.cordial.storage.db.dao.inboxmessage.inboxmessage.InboxMessageDao
import com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent.InboxMessageContentDBHelper
import com.cordial.storage.db.dao.inboxmessage.inboxmessagecontent.InboxMessageContentDao
import com.cordial.storage.db.dao.inboxmessage.updateinboxmessagereadstatus.UpdateInboxMessageReadStatusDBHelper
import com.cordial.storage.db.dao.inboxmessage.updateinboxmessagereadstatus.UpdateInboxMessageReadStatusDao
import com.cordial.storage.db.dao.notification.category.NotificationCategoryDBHelper
import com.cordial.storage.db.dao.notification.category.NotificationCategoryDao
import com.cordial.storage.db.dao.setcontact.SetContactDBHelper
import com.cordial.storage.db.dao.setcontact.SetContactDao

internal class LocalStorageInjection {

    val setContactDao: SetContactDao? = null
        get() {
            if (field == null) {
                return SetContactDBHelper()
            }
            return field
        }
    val unsetContactDao: UnsetContactDao? = null
        get() {
            if (field == null) {
                return UnsetContactDBHelper()
            }
            return field
        }
    val eventDao: EventDao? = null
        get() {
            if (field == null) {
                return EventDBHelper()
            }
            return field
        }
    val contactCartDao: ContactCartDao? = null
        get() {
            if (field == null) {
                return ContactCartDBHelper()
            }
            return field
        }
    val contactOrderDao: ContactOrderDao? = null
        get() {
            if (field == null) {
                return ContactOrderDBHelper()
            }
            return field
        }
    val inAppMessageDao: InAppMessageDao? = null
        get() {
            if (field == null) {
                return InAppMessageDBHelper()
            }
            return field
        }
    val fetchInAppMessageDao: FetchInAppMessageDao? = null
        get() {
            if (field == null) {
                return FetchInAppMessageDBHelper()
            }
            return field
        }
    val inAppMessageToDeleteDao: InAppMessageToDeleteDao? = null
        get() {
            if (field == null) {
                return InAppMessageToDeleteDBHelper()
            }
            return field
        }
    val inboxMessageDao: InboxMessageDao? = null
        get() {
            if (field == null) {
                return InboxMessageDBHelper()
            }
            return field
        }
    val inboxMessageContentDao: InboxMessageContentDao? = null
        get() {
            if (field == null) {
                return InboxMessageContentDBHelper()
            }
            return field
        }
    val deleteInboxMessageDao: DeleteInboxMessageDao? = null
        get() {
            if (field == null) {
                return DeleteInboxMessageDBHelper()
            }
            return field
        }
    val updateInboxMessageReadStatusDao: UpdateInboxMessageReadStatusDao? = null
        get() {
            if (field == null) {
                return UpdateInboxMessageReadStatusDBHelper()
            }
            return field
        }
    val notificationCategoriesDao: NotificationCategoryDao? = null
        get() {
            if (field == null) {
                return NotificationCategoryDBHelper()
            }
            return field
        }
}