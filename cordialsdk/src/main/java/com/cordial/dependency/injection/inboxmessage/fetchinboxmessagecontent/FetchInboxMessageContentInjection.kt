package com.cordial.dependency.injection.inboxmessage.fetchinboxmessagecontent

import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.repository.FetchInboxMessageContentRepository
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.repository.FetchInboxMessageContentRepositoryImpl
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.usecase.FetchInboxMessageContentUseCase
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.usecase.FetchInboxMessageContentUseCaseImpl
import com.cordial.feature.inboxmessage.getinboxmessage.usecase.FetchInboxMessageUseCase
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.S3SDKResponseHandler

internal class FetchInboxMessageContentInjection(
    localStorageInjection: LocalStorageInjection,
    fetchInboxMessageUseCase: FetchInboxMessageUseCase
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val s3ResponseHandler: ResponseHandler = S3SDKResponseHandler()

    private val fetchInboxMessageContentRepository: FetchInboxMessageContentRepository =
        FetchInboxMessageContentRepositoryImpl(requestSender, s3ResponseHandler)

    val fetchInboxMessageContentUseCase: FetchInboxMessageContentUseCase =
        FetchInboxMessageContentUseCaseImpl(
            fetchInboxMessageContentRepository,
            localStorageInjection.inboxMessageDao,
            localStorageInjection.inboxMessageContentDao,
            fetchInboxMessageUseCase
        )
}