package com.cordial.dependency.injection

import android.os.Build
import androidx.annotation.RequiresApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialApiEndpoints
import com.cordial.api.MessageAttributionManager
import com.cordial.dependency.injection.deeplink.ProcessDeepLinkInjection
import com.cordial.dependency.injection.inappmessage.InAppMessageDataInjection
import com.cordial.dependency.injection.inappmessage.InAppMessageInjection
import com.cordial.dependency.injection.inboxmessage.deleteinboxmessage.DeleteInboxMessageInjection
import com.cordial.dependency.injection.inboxmessage.fetchinboxmessagecontent.FetchInboxMessageContentInjection
import com.cordial.dependency.injection.inboxmessage.getinboxmessage.FetchInboxMessageInjection
import com.cordial.dependency.injection.inboxmessage.getinboxmessages.FetchInboxMessagesInjection
import com.cordial.dependency.injection.inboxmessage.updateinboxmessagereadstatus.UpdateInboxMessageReadStatusInjection
import com.cordial.dependency.injection.notification.NotificationCategoryInjection
import com.cordial.dependency.injection.notification.NotificationPermissionInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.dependency.injection.sendcontactorder.SendContactOrderInjection
import com.cordial.dependency.injection.sendevent.SendEventInjection
import com.cordial.dependency.injection.timestamps.TimestampsInjection
import com.cordial.dependency.injection.unsetcontact.UnsetContactInjection
import com.cordial.dependency.injection.upsertcontact.UpsertContactInjection
import com.cordial.dependency.injection.upsertcontactcart.UpsertContactCartInjection
import com.cordial.storage.db.CacheManager
import com.cordial.storage.preferences.Preferences

internal class CordialInjection {

    private val context = CordialApiConfiguration.getInstance().getContext()
    val preferences = Preferences(context)
    var messageAttributionManager: MessageAttributionManager = MessageAttributionManager(preferences)
    val cacheManager: () -> CacheManager = { CacheManager() }
    val cordialApiEndpoints: CordialApiEndpoints = CordialApiEndpoints()

    val localStorageInjection: () -> LocalStorageInjection = { LocalStorageInjection() }

    var upsertContactInjection: () -> UpsertContactInjection = {
        UpsertContactInjection(
            preferences,
            cordialApiEndpoints,
            localStorageInjection(),
            messageAttributionManager,
            cacheManager(),
            sdkSecurityInjection()
        )
    }

    var unsetContactInjection: () -> UnsetContactInjection = {
        UnsetContactInjection(
            preferences,
            cordialApiEndpoints,
            localStorageInjection(),
            messageAttributionManager,
            sdkSecurityInjection()
        )
    }

    var sendEventInjection: () -> SendEventInjection = {
        SendEventInjection(preferences, cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var upsertContactCartInjection: () -> UpsertContactCartInjection = {
        UpsertContactCartInjection(preferences, cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var sendContactOrderInjection: () -> SendContactOrderInjection = {
        SendContactOrderInjection(preferences, cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var inAppMessageInjection: () -> InAppMessageInjection =
        { InAppMessageInjection(cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection()) }

    var inAppMessageDataInjection: () -> InAppMessageDataInjection = {
        InAppMessageDataInjection(
            preferences,
            cordialApiEndpoints,
            inAppMessageInjection(),
            localStorageInjection(),
            sdkSecurityInjection()
        )
    }

    var timestampsInjection: () -> TimestampsInjection = {
        TimestampsInjection(preferences, cordialApiEndpoints, inAppMessageDataInjection(), sdkSecurityInjection())
    }

    var fetchInboxMessageInjection: () -> FetchInboxMessageInjection = {
        FetchInboxMessageInjection(cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var fetchInboxMessagesInjection: () -> FetchInboxMessagesInjection = {
        FetchInboxMessagesInjection(cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var fetchInboxMessageContentInjection: () -> FetchInboxMessageContentInjection = {
        val fetchInboxMessageUseCase = fetchInboxMessageInjection().fetchInboxMessageUseCase
        FetchInboxMessageContentInjection(localStorageInjection(), fetchInboxMessageUseCase)
    }

    var deleteInboxMessageInjection: () -> DeleteInboxMessageInjection = {
        DeleteInboxMessageInjection(preferences, cordialApiEndpoints, localStorageInjection(), sdkSecurityInjection())
    }

    var updateInboxMessageReadStatusInjection: () -> UpdateInboxMessageReadStatusInjection = {
        UpdateInboxMessageReadStatusInjection(
            preferences,
            cordialApiEndpoints,
            localStorageInjection(),
            sdkSecurityInjection()
        )
    }

    var processDeepLinkInjection: () -> ProcessDeepLinkInjection = {
        ProcessDeepLinkInjection(messageAttributionManager)
    }

    var sdkSecurityInjection: () -> SDKSecurityInjection = {
        SDKSecurityInjection(cordialApiEndpoints)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    var notificationCategoryInjection: () -> NotificationCategoryInjection = {
        NotificationCategoryInjection(localStorageInjection())
    }

    @RequiresApi(Build.VERSION_CODES.O)
    var notificationPermissionInjection: () -> NotificationPermissionInjection = {
        NotificationPermissionInjection(notificationCategoryInjection().notificationCategoryUseCase)
    }
}