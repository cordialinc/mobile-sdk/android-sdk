package com.cordial.dependency.injection.unsetcontact

import com.cordial.api.CordialApiEndpoints
import com.cordial.api.MessageAttributionManager
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.unsetcontact.repository.UnsetContactRepository
import com.cordial.feature.unsetcontact.repository.UnsetContactRepositoryImpl
import com.cordial.feature.unsetcontact.usecase.UnsetContactUseCase
import com.cordial.feature.unsetcontact.usecase.UnsetContactUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class UnsetContactInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    messageAttributionManager: MessageAttributionManager,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val unsetContactRepository: UnsetContactRepository =
        UnsetContactRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val unsetContactUseCase: UnsetContactUseCase =
        UnsetContactUseCaseImpl(
            unsetContactRepository,
            preferences,
            localStorageInjection.unsetContactDao,
            messageAttributionManager,
            sdkSecurityUseCase
        )
}