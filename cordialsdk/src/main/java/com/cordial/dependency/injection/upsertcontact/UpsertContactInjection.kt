package com.cordial.dependency.injection.upsertcontact

import com.cordial.api.CordialApiEndpoints
import com.cordial.api.MessageAttributionManager
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.upsertcontact.repository.UpsertContactRepository
import com.cordial.feature.upsertcontact.repository.UpsertContactRepositoryImpl
import com.cordial.feature.upsertcontact.usecase.UpsertContactUseCase
import com.cordial.feature.upsertcontact.usecase.UpsertContactUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.db.CacheManager
import com.cordial.storage.preferences.Preferences

internal class UpsertContactInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    messageAttributionManager: MessageAttributionManager,
    cacheManager: CacheManager,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val upsertContactRepository: UpsertContactRepository =
        UpsertContactRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val upsertContactUseCase: UpsertContactUseCase =
        UpsertContactUseCaseImpl(
            upsertContactRepository,
            preferences,
            localStorageInjection.setContactDao,
            localStorageInjection.unsetContactDao,
            messageAttributionManager,
            cacheManager,
            sdkSecurityUseCase
        )
}