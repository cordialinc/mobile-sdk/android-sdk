package com.cordial.dependency.injection.notification

import com.cordial.feature.notification.category.usecase.NotificationCategoryUseCase
import com.cordial.feature.notification.permission.usecase.NotificationPermissionUseCase
import com.cordial.feature.notification.permission.usecase.NotificationPermissionUseCaseImpl

internal class NotificationPermissionInjection(notificationCategoryUseCase: NotificationCategoryUseCase) {
    val notificationPermissionUseCase: NotificationPermissionUseCase =
        NotificationPermissionUseCaseImpl(notificationCategoryUseCase)
}