package com.cordial.dependency.injection.notification

import android.os.Build
import androidx.annotation.RequiresApi
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.feature.notification.category.usecase.NotificationCategoryUseCase
import com.cordial.feature.notification.category.usecase.NotificationCategoryUseCaseImpl

@RequiresApi(Build.VERSION_CODES.O)
internal class NotificationCategoryInjection(
    val localStorageInjection: LocalStorageInjection
) {

    val notificationCategoryUseCase: NotificationCategoryUseCase =
        NotificationCategoryUseCaseImpl(localStorageInjection.notificationCategoriesDao)
}