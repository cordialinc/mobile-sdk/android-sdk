package com.cordial.dependency.injection.upsertcontactcart

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.upsertcontactcart.repository.UpsertContactCartRepository
import com.cordial.feature.upsertcontactcart.repository.UpsertContactCartRepositoryImpl
import com.cordial.feature.upsertcontactcart.usecase.UpsertContactCartUseCase
import com.cordial.feature.upsertcontactcart.usecase.UpsertContactCartUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class UpsertContactCartInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val upsertContactCartRepository: UpsertContactCartRepository =
        UpsertContactCartRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val upsertContactCartUseCase: UpsertContactCartUseCase =
        UpsertContactCartUseCaseImpl(
            upsertContactCartRepository,
            preferences,
            localStorageInjection.contactCartDao,
            sdkSecurityUseCase
        )
}