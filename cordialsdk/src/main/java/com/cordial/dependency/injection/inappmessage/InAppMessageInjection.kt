package com.cordial.dependency.injection.inappmessage

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inappmessage.getinappmessage.repository.InAppMessageRepository
import com.cordial.feature.inappmessage.getinappmessage.repository.InAppMessageRepositoryImpl
import com.cordial.feature.inappmessage.getinappmessage.usecase.InAppMessageUseCase
import com.cordial.feature.inappmessage.getinappmessage.usecase.InAppMessageUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.S3SDKResponseHandler
import com.cordial.network.response.SDKResponseHandler

internal class InAppMessageInjection(
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val s3ResponseHandler: ResponseHandler = S3SDKResponseHandler()

    private val inAppMessageRepository: InAppMessageRepository =
        InAppMessageRepositoryImpl(requestSender, responseHandler, s3ResponseHandler, cordialApiEndpoints)

    val inAppMessageUseCase: InAppMessageUseCase =
        InAppMessageUseCaseImpl(
            inAppMessageRepository,
            localStorageInjection.inAppMessageDao,
            localStorageInjection.fetchInAppMessageDao,
            localStorageInjection.inAppMessageToDeleteDao,
            sdkSecurityUseCase
        )
}