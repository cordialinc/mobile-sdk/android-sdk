package com.cordial.dependency.injection.inboxmessage.getinboxmessages

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.inboxmessage.getinboxmessages.repository.FetchInboxMessagesRepository
import com.cordial.feature.inboxmessage.getinboxmessages.repository.FetchInboxMessagesRepositoryImpl
import com.cordial.feature.inboxmessage.getinboxmessages.usecase.FetchInboxMessagesUseCase
import com.cordial.feature.inboxmessage.getinboxmessages.usecase.FetchInboxMessagesUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler

internal class FetchInboxMessagesInjection(
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val fetchInboxMessagesRepository: FetchInboxMessagesRepository =
        FetchInboxMessagesRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val fetchInboxMessagesUseCase: FetchInboxMessagesUseCase =
        FetchInboxMessagesUseCaseImpl(
            fetchInboxMessagesRepository,
            localStorageInjection.inboxMessageDao,
            sdkSecurityUseCase
        )
}