package com.cordial.dependency.injection.deeplink

import com.cordial.api.MessageAttributionManager
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.deeplink.repository.ProcessDeepLinkRepository
import com.cordial.feature.deeplink.repository.ProcessDeepLinkRepositoryImpl
import com.cordial.feature.deeplink.usecase.ProcessDeepLinkUseCase
import com.cordial.feature.deeplink.usecase.ProcessDeepLinkUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.DeepLinkResponseHandler
import com.cordial.network.response.ResponseHandler

internal class ProcessDeepLinkInjection(messageAttributionManager: MessageAttributionManager) {

    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val responseHandler: ResponseHandler = DeepLinkResponseHandler()

    private val processDeepLinkRepository: ProcessDeepLinkRepository =
        ProcessDeepLinkRepositoryImpl(requestSender, responseHandler)

    val processDeepLinkUseCase: ProcessDeepLinkUseCase =
        ProcessDeepLinkUseCaseImpl(processDeepLinkRepository, messageAttributionManager)
}