package com.cordial.dependency.injection.timestamps

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.inappmessage.InAppMessageDataInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.timestamps.repository.timestamps.TimestampsRepository
import com.cordial.feature.timestamps.repository.timestamps.TimestampsRepositoryImpl
import com.cordial.feature.timestamps.repository.timestampsurl.TimestampsUrlRepository
import com.cordial.feature.timestamps.repository.timestampsurl.TimestampsUrlRepositoryImpl
import com.cordial.feature.timestamps.usecase.TimestampsUseCase
import com.cordial.feature.timestamps.usecase.TimestampsUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.S3SDKResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class TimestampsInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    inAppMessageDataInjection: InAppMessageDataInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val s3ResponseHandler: ResponseHandler = S3SDKResponseHandler()

    private val timestampsRepository: TimestampsRepository =
        TimestampsRepositoryImpl(requestSender, s3ResponseHandler)

    private val timestampsUrlRepository: TimestampsUrlRepository =
        TimestampsUrlRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val timestampsUseCase: TimestampsUseCase =
        TimestampsUseCaseImpl(
            preferences,
            timestampsUrlRepository,
            timestampsRepository,
            inAppMessageDataInjection.inAppMessageDataUseCase,
            sdkSecurityUseCase
        )
}