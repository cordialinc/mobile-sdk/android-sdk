package com.cordial.dependency.injection.sendevent

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.sendevent.model.EventRequestHelper
import com.cordial.feature.sendevent.repository.SendEventRepository
import com.cordial.feature.sendevent.repository.SendEventRepositoryImpl
import com.cordial.feature.sendevent.usecase.SendEventUseCase
import com.cordial.feature.sendevent.usecase.SendEventUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class SendEventInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    val eventRequestHelper: EventRequestHelper = EventRequestHelper()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val sendEventRepository: SendEventRepository =
        SendEventRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints, eventRequestHelper)

    val sendEventUseCase: SendEventUseCase =
        SendEventUseCaseImpl(sendEventRepository, preferences, localStorageInjection.eventDao, sdkSecurityUseCase)

}