package com.cordial.dependency.injection.security

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.feature.sdksecurity.repository.SDKSecurityRepository
import com.cordial.feature.sdksecurity.repository.SDKSecurityRepositoryImpl
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCase
import com.cordial.feature.sdksecurity.usecase.SDKSecurityUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler

internal class SDKSecurityInjection(cordialApiEndpoints: CordialApiEndpoints) {

    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val responseHandler: ResponseHandler = SDKResponseHandler()

    private val sdkSecurityRepository: SDKSecurityRepository =
        SDKSecurityRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val sdkSecurityUseCase: SDKSecurityUseCase = SDKSecurityUseCaseImpl(sdkSecurityRepository)
}