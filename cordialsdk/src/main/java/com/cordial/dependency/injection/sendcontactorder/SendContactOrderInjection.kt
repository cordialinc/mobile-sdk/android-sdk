package com.cordial.dependency.injection.sendcontactorder

import com.cordial.api.CordialApiEndpoints
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.LocalStorageInjection
import com.cordial.dependency.injection.security.SDKSecurityInjection
import com.cordial.feature.sendcontactorder.repository.SendContactOrderRepository
import com.cordial.feature.sendcontactorder.repository.SendContactOrderRepositoryImpl
import com.cordial.feature.sendcontactorder.usecase.SendContactOrderUseCase
import com.cordial.feature.sendcontactorder.usecase.SendContactOrderUseCaseImpl
import com.cordial.network.request.RequestSender
import com.cordial.network.response.ResponseHandler
import com.cordial.network.response.SDKResponseHandler
import com.cordial.storage.preferences.Preferences

internal class SendContactOrderInjection(
    preferences: Preferences,
    cordialApiEndpoints: CordialApiEndpoints,
    localStorageInjection: LocalStorageInjection,
    sdkSecurityInjection: SDKSecurityInjection
) {
    private val requestSender: RequestSender = DependencyConfiguration.getInstance().requestSender()

    private val sdkSecurityUseCase = sdkSecurityInjection.sdkSecurityUseCase

    private val responseHandler: ResponseHandler = SDKResponseHandler(sdkSecurityUseCase)

    private val sendContactOrderRepository: SendContactOrderRepository =
        SendContactOrderRepositoryImpl(requestSender, responseHandler, cordialApiEndpoints)

    val sendContactOrderUseCase: SendContactOrderUseCase =
        SendContactOrderUseCaseImpl(
            sendContactOrderRepository,
            preferences,
            localStorageInjection.contactOrderDao,
            sdkSecurityUseCase
        )
}