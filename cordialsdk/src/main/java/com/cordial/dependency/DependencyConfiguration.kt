package com.cordial.dependency

import com.cordial.feature.deeplink.model.browser.Browser
import com.cordial.feature.deeplink.model.browser.BrowserHandler
import com.cordial.feature.notification.NotificationStatusRequester
import com.cordial.feature.notification.OnNotificationStatusListener
import com.cordial.lifecycle.devicelock.DeviceLockStatusRequester
import com.cordial.lifecycle.devicelock.OnDeviceLockStatus
import com.cordial.network.NetworkState
import com.cordial.network.NetworkStateListener
import com.cordial.network.request.RequestSender
import com.cordial.network.request.SDKRequestSender

internal class DependencyConfiguration {

    var requestSender: () -> RequestSender = { SDKRequestSender() }

    var networkState: NetworkState = NetworkStateListener()

    var notificationStatusRequester: OnNotificationStatusListener = NotificationStatusRequester()

    var deviceLockStatusRequester: OnDeviceLockStatus = DeviceLockStatusRequester()

    var browser: () -> Browser = { BrowserHandler() }

    companion object {
        private var INSTANCE: DependencyConfiguration? = null

        fun getInstance(): DependencyConfiguration {
            if (INSTANCE == null) {
                INSTANCE =
                    DependencyConfiguration()
            }
            return INSTANCE as DependencyConfiguration
        }
    }
}