package com.cordial.api

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import com.cordial.dependency.DependencyConfiguration
import com.cordial.dependency.injection.CordialInjection
import com.cordial.feature.deeplink.model.CordialDeepLinkOpenListener
import com.cordial.feature.inappmessage.InAppMessageProcess
import com.cordial.feature.inappmessage.model.InAppMessageDelayMode
import com.cordial.feature.inappmessage.model.InAppMessageDelayType
import com.cordial.feature.inappmessage.model.InAppMessageInputsListener
import com.cordial.feature.inappmessage.model.InAppMessages
import com.cordial.feature.inboxmessage.CordialInboxMessageListener
import com.cordial.feature.inboxmessage.fetchinboxmessagecontent.model.InboxMessageCache
import com.cordial.feature.log.CordialLogLevel
import com.cordial.feature.log.CordialLogger
import com.cordial.feature.log.CordialLoggerListener
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.notification.CordialPushNotificationListener
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.sendevent.eventservice.EventBinder
import com.cordial.feature.sendevent.eventservice.EventService
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.permission.model.EducationalUiModeEnum
import com.cordial.feature.sendevent.model.EventCacheSendingReason
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.upsertcontact.model.NotificationStatus
import com.cordial.lifecycle.AppLifecycleHandler
import com.cordial.lifecycle.devicelock.DeviceLockListener
import com.cordial.network.NetworkStateHandler
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.JsonUtils
import com.cordial.util.TimeUtils
import com.cordial.util.castTo
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import java.util.*
import kotlin.math.absoluteValue

class CordialApiConfiguration private constructor() : AppLifecycleHandler.LifecycleDelegate,
    ServiceConnection {

    private var accountKey: String = ""
    private var channelKey: String = ""
    private var qty: Int = C.EVENTS_QTY
    private var eventsStreamServiceUrl: String = C.PRODUCTION_BASE_URL
    private var messageHubServiceUrl: String? = null
    private lateinit var context: Context
    private lateinit var preferences: Preferences
    var pushesConfiguration: PushesConfiguration = PushesConfiguration.APP
    var pushNotificationListener: CordialPushNotificationListener? = null
    var deepLinkListener: CordialDeepLinkOpenListener? = null
    var vanityDomains: List<String>? = null
    var inboxMessageListener: CordialInboxMessageListener? = null
    var inboxMessageCache = InboxMessageCache()
    var inAppMessageInputsListener: InAppMessageInputsListener? = null
    var inAppMessages: InAppMessages = InAppMessages()

    var eventsBulkSize: Int = 1
        set(value) {
            field = if (value.absoluteValue < 1) 1
            else value.absoluteValue
        }
    var eventsBulkUploadInterval: Int? = 30
        set(value) {
            if (field?.absoluteValue != value?.absoluteValue) {
                field = value?.absoluteValue
                stopEventSenderService()
                if (field != null) startEventSenderService()
            }
        }
    private var isBounded = false
    private var eventBinder: EventBinder? = null

    var inAppMessageDelayMode = InAppMessageDelayMode()
    internal var inAppBannerDisplayTime = C.IN_APP_BANNER_SHOW_TIMER
    private val appLifecycleHandler = AppLifecycleHandler(this, inAppMessageDelayMode)
    internal var deviceLockListener: DeviceLockListener? = null
    internal var inAppShowEnabled = false

    internal var networkStateHandler: NetworkStateHandler? = null
    var pushNotificationIconSilhouette: Int? = null
    var deepLinkWebOnlyFragments = listOf("webonly")
    private var loggers: MutableList<CordialLoggerListener> = mutableListOf()

    private lateinit var cordialApi: CordialApi
    internal lateinit var injection: CordialInjection
    var educationalUiMode: EducationalUiModeEnum = EducationalUiModeEnum.NONE
    var showPushInForeground: Boolean = true
    var notificationClickActivity: Class<*>? = null
    var logLevel: CordialLogLevel = CordialLogLevel.ALL

    @Deprecated(
        message = "Use initialize(Context, String, String, String) instead",
        ReplaceWith("initialize(context, accountKey, channelKey, eventsStreamUrl)"),
        level = DeprecationLevel.WARNING
    )
    fun initialize(context: Context, accountKey: String, channelKey: String, qty: Int = C.EVENTS_QTY) {
        initialize(
            context,
            accountKey,
            channelKey,
            eventsStreamServiceUrl = C.PRODUCTION_BASE_URL,
            messageHubServiceUrl = null,
        )
        setQty(qty)
    }

    @Deprecated(
        message = "Use initialize(Context, String, String, String) instead",
        ReplaceWith("initialize(context, accountKey, channelKey, eventsStreamUrl)"),
        level = DeprecationLevel.WARNING
    )
    fun initialize(
        context: Context,
        host: String,
        accountKey: String,
        channelKey: String,
        qty: Int
    ) {
        initialize(
            context,
            accountKey,
            channelKey,
            eventsStreamServiceUrl = host,
            messageHubServiceUrl = null,
        )
        setQty(qty)
    }

    @Deprecated(
        message = "Use initialize(Context, String, String, String) instead",
        ReplaceWith("initialize(context, accountKey, channelKey, eventsStreamUrl)"),
        level = DeprecationLevel.WARNING
    )
    fun initialize(context: Context, accountKey: String, channelKey: String) {
        initialize(
            context,
            accountKey,
            channelKey,
            eventsStreamServiceUrl = C.PRODUCTION_BASE_URL,
            messageHubServiceUrl = null
        )
    }

    fun initialize(
        context: Context,
        accountKey: String,
        channelKey: String,
        eventsStreamServiceUrl: String,
        messageHubServiceUrl: String? = null
    ) {
        initializeContext(context)
        saveHostAuth(accountKey, channelKey, eventsStreamServiceUrl, messageHubServiceUrl)

        registerNetworkStateHandler()
        prepareDeviceIdentifier()
        checkIsFirstLaunch()
        registerLifecycleHandler(appLifecycleHandler)
        registerDeviceLockListener()
        updateAndroidSecurityProvider()
        clearSystemEventsProperties()
        startEventSenderService()
        setCordialLogger()
    }

    internal fun initializeContext(context: Context) {
        this.context = context.applicationContext
        injection = CordialInjection()
        cordialApi = CordialApi()
        preferences = Preferences(context)
    }

    private fun saveHostAuth(
        accountKey: String,
        channelKey: String,
        eventsStreamServiceUrl: String,
        messageHubServiceUrl: String?
    ) {
        if (preferences.contains(PreferenceKeys.ACCOUNT_KEY) &&
            accountKey != preferences.getString(PreferenceKeys.ACCOUNT_KEY)
        ) {
            clearMessageAttribution()
        }
        preferences.put(PreferenceKeys.ACCOUNT_KEY, accountKey)
        this.accountKey = accountKey

        if (preferences.contains(PreferenceKeys.CHANNEL_KEY) &&
            channelKey != preferences.getString(PreferenceKeys.CHANNEL_KEY)
        ) {
            clearMessageAttribution()
        }
        preferences.put(PreferenceKeys.CHANNEL_KEY, channelKey)
        this.channelKey = channelKey

        if (preferences.contains(PreferenceKeys.BASE_URL) &&
            eventsStreamServiceUrl != preferences.getString(PreferenceKeys.BASE_URL)
        ) {
            clearMessageAttributionAndCache()
        }
        preferences.put(PreferenceKeys.BASE_URL, eventsStreamServiceUrl)
        this.eventsStreamServiceUrl = eventsStreamServiceUrl

        this.messageHubServiceUrl = messageHubServiceUrl
    }

    private fun clearMessageAttributionAndCache() {
        clearMessageAttribution()
        clearCache()
        CordialLoggerManager.info("Clearing all cache due to base url change")
    }

    private fun clearMessageAttribution() {
        val messageAttributionManager = injection.messageAttributionManager
        messageAttributionManager.clearMessageAttributes()
    }

    private fun clearCache() {
        val cacheManager = injection.cacheManager()
        cacheManager.clearCache()
    }

    fun setLogger(logger: CordialLoggerListener) {
        loggers.add(logger)
    }

    fun setLoggers(loggers: List<CordialLoggerListener>) {
        this.loggers.clear()
        this.loggers.addAll(loggers)
        setCordialLogger()
    }

    fun getLoggers(): List<CordialLoggerListener> {
        return loggers
    }

    fun setQty(newQty: Int) {
        qty = if (newQty < 1) 1 else newQty
        val cachedQty = preferences.getInt(PreferenceKeys.QTY)
        if (cachedQty != qty) {
            preferences.put(PreferenceKeys.QTY, qty)
            val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
            sendEventUseCase.updateEventLimit(qty)
        }
    }

    private fun registerNetworkStateHandler() {
        networkStateHandler = NetworkStateHandler(context)
        networkStateHandler?.register()
    }

    private fun prepareDeviceIdentifier() {
        if (!preferences.contains(PreferenceKeys.DEVICE_ID)) {
            preferences.put(PreferenceKeys.DEVICE_ID, UUID.randomUUID().toString())
        }
    }

    private fun checkIsFirstLaunch() {
        if (!preferences.contains(PreferenceKeys.WAS_LAUNCHED_BEFORE)) {
            preferences.put(PreferenceKeys.WAS_LAUNCHED_BEFORE, true)
            cordialApi.sendSystemEventWithProperties(eventName = C.EVENT_NAME_APP_INSTALL)
        }
    }

    private fun registerLifecycleHandler(lifecycleHandler: AppLifecycleHandler) {
        with(context.applicationContext.castTo<Application>() ?: return) {
            registerActivityLifecycleCallbacks(lifecycleHandler)
            registerComponentCallbacks(lifecycleHandler)
        }
    }

    private fun registerDeviceLockListener() {
        deviceLockListener = DeviceLockListener(this)
        deviceLockListener?.register(context)
    }

    private fun clearSystemEventsProperties() {
        preferences.remove(PreferenceKeys.PROPERTIES)
    }

    fun startEventSenderService() {
        if (::context.isInitialized && AppLifecycleHandler.appInForeground && eventsBulkUploadInterval != null) {
            val intent = Intent(context, EventService::class.java)
            context.bindService(intent, this, Context.BIND_AUTO_CREATE)
        }
    }

    private fun setCordialLogger() {
        val cordialLogger = CordialLogger()
        setLogger(cordialLogger)
    }

    internal fun restartEventBoxSendingTimer() {
        eventBinder?.restartTimer()
    }

    /**
     * @see CordialApiConfiguration.setSystemEventProperties
     */
    @Deprecated(
        level = DeprecationLevel.WARNING,
        message = "Use setSystemEventProperties(Map<String, PropertyValue>) instead"
    )
    fun setPropertiesOfSystemEvents(properties: Map<String, String>) {
        val propertyValues = properties.mapValues { PropertyValue.StringProperty(it.value) }
        setSystemEventProperties(propertyValues)
    }

    fun setSystemEventProperties(properties: Map<String, PropertyValue>) {
        if (properties.isNotEmpty()) {
            preferences.put(
                PreferenceKeys.PROPERTIES,
                JsonUtils.getJsonFromPropertyValueMap(properties).toString()
            )
        } else {
            clearSystemEventsProperties()
        }
    }

    fun getContext(): Context {
        return context.applicationContext
    }

    fun getAccountKey(): String {
        return accountKey
    }

    fun getChannelKey(): String {
        return channelKey
    }

    /**
     * @see CordialApiConfiguration.getEventsStreamServiceUrl
     */
    @Deprecated(
        level = DeprecationLevel.WARNING,
        message = "Use getEventsStreamServiceUrl() instead"
    )
    fun getBaseUrl(): String {
        return eventsStreamServiceUrl
    }

    fun getEventsStreamServiceUrl(): String {
        return eventsStreamServiceUrl
    }

    fun getMessageHubServiceUrl(): String? {
        return messageHubServiceUrl
    }

    fun getQty(): Int {
        return qty
    }

    fun setNotificationCategories(categories: List<NotificationCategory>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationCategoryUseCase = injection.notificationCategoryInjection().notificationCategoryUseCase
            notificationCategoryUseCase.setNotificationCategories(context, categories)
        }
    }

    override fun onAppBackgrounded() {
        inAppShowEnabled = false
        sendAppCloseEvent()
        stopEventSenderService()
    }

    private fun sendAppCloseEvent() {
        cordialApi.sendSystemEventWithProperties(eventName = C.EVENT_NAME_APP_CLOSE)
        val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.sendCachedEvents(EventCacheSendingReason.APP_CLOSE)
    }

    private fun stopEventSenderService() {
        if (isBounded) {
            context.unbindService(this)
            isBounded = false
            eventBinder?.stopTimer()
            eventBinder = null
        }
    }

    override fun onAppForegrounded() {
        if (inAppMessageDelayMode.delayType == InAppMessageDelayType.SHOW) {
            onInAppShowEnabled(isEnabled = true)
        }
        startEventSenderService()
        cordialApi.sendSystemEventWithProperties(eventName = C.EVENT_NAME_APP_OPEN)
        checkNotificationStatus()
        sendContactSelfHealingIfNotLoggedOut()
        updateTimestamps()
        sendCachedData()
        checkNotificationCategoriesStatus()
    }

    private fun checkNotificationCategoriesStatus() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationCategoryUseCase = injection.notificationCategoryInjection().notificationCategoryUseCase
            notificationCategoryUseCase.checkNotificationCategoriesStatus(context)
        }
    }

    private fun updateTimestamps() {
        val isLoggedIn = preferences.getBoolean(PreferenceKeys.IS_LOGGED_IN, false)
        if (isLoggedIn || cordialApi.getFirebaseToken() != null) {
            val timestampsUseCase = injection.timestampsInjection().timestampsUseCase
            timestampsUseCase.updateTimestamps()
        }
    }

    private fun sendCachedData() {
        val isNetworkCallbackRegistered = networkStateHandler?.isNetworkCallbackRegistered ?: return
        if (!isNetworkCallbackRegistered) {
            val cacheManager = injection.cacheManager()
            cacheManager.sendCachedData()
        }
    }

    override fun onServiceDisconnected(p0: ComponentName?) {
        isBounded = false
        eventBinder?.stopTimer()
        eventBinder = null
    }

    override fun onServiceConnected(p0: ComponentName?, iBinder: IBinder?) {
        isBounded = true
        eventBinder = iBinder?.castTo<EventBinder>()
    }

    override fun onInAppShowEnabled(isEnabled: Boolean) {
        if (isEnabled) InAppMessageProcess.getInstance().showInAppMessageIfExistAndCanBePresented()
        inAppShowEnabled = isEnabled
    }

    fun checkNotificationStatus() {
        val isNotificationsEnabled =
            DependencyConfiguration.getInstance().notificationStatusRequester.areNotificationsEnabled(context)
        when {
            !isNotificationStatusStored() -> updateNotificationStatus(isNotificationsEnabled)
            isNotificationStatusChanged(isNotificationsEnabled) -> updateNotificationStatus(isNotificationsEnabled)
        }
    }

    private fun isNotificationStatusStored(): Boolean {
        return preferences.contains(PreferenceKeys.NOTIFICATION_STATUS)
    }

    private fun isNotificationStatusChanged(isNotificationsEnabled: Boolean): Boolean {
        val notificationStatus =
            NotificationStatus.findKey(preferences.getString(PreferenceKeys.NOTIFICATION_STATUS, ""))
        return isNotificationsEnabled != NotificationStatus.isEnabled(notificationStatus)
    }

    private fun updateNotificationStatus(isNotificationsEnabled: Boolean) {
        preferences.put(
            PreferenceKeys.NOTIFICATION_STATUS,
            NotificationStatus.findKey(isNotificationsEnabled).status
        )
        val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.setContactIfNotLoggedOut()
        sendNotificationStatusEvent(isNotificationsEnabled)
    }

    private fun sendContactSelfHealingIfNotLoggedOut() {
        val lastSetContactTimestamp =
            preferences.getString(PreferenceKeys.LAST_SET_CONTACT_TIMESTAMP, "")
        if (lastSetContactTimestamp.isNotEmpty() && TimeUtils.is24HoursPassed(lastSetContactTimestamp)) {
            val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
            upsertContactUseCase.sendSelfHealingIfNotLoggedOut()
        }
    }

    private fun sendNotificationStatusEvent(isEnabled: Boolean) {
        val eventName =
            if (isEnabled) C.EVENT_DEVICE_NOTIFICATIONS_MANUAL_OPTIN else C.EVENT_DEVICE_NOTIFICATIONS_MANUAL_OPTOUT
        cordialApi.sendSystemEventWithProperties(eventName)
    }

    private fun updateAndroidSecurityProvider() {
        Handler(Looper.getMainLooper()).post {
            try {
                ProviderInstaller.installIfNeededAsync(
                    context,
                    object : ProviderInstaller.ProviderInstallListener {
                        override fun onProviderInstallFailed(p0: Int, p1: Intent?) {
                        }

                        override fun onProviderInstalled() {
                        }
                    })
            } catch (e: GooglePlayServicesRepairableException) {
            } catch (e: GooglePlayServicesNotAvailableException) {
            }
        }
    }

    companion object {
        private var INSTANCE: CordialApiConfiguration? = null

        @JvmStatic
        fun getInstance(): CordialApiConfiguration {
            if (INSTANCE == null) {
                INSTANCE = CordialApiConfiguration()
            }
            return INSTANCE as CordialApiConfiguration
        }
    }
}