package com.cordial.api

import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.storage.preferences.Preferences
import com.cordial.util.TimeUtils


internal class MessageAttributionManager(private val preferences: Preferences) {

    fun saveMessageAttributionData(mcID: String) {
        saveCurrentMessageAttributionDataAsPrevious()
        val tapTime = TimeUtils.getTimestamp()
        preferences.put(PreferenceKeys.MC_ID, mcID)
        preferences.put(PreferenceKeys.MC_TAP_TIME, tapTime)
    }

    private fun saveCurrentMessageAttributionDataAsPrevious() {
        val currentMcID = preferences.getString(PreferenceKeys.MC_ID)
        val currentMcTapTime = preferences.getString(PreferenceKeys.MC_ID)
        preferences.put(PreferenceKeys.PREVIOUS_MC_ID, currentMcID)
        preferences.put(PreferenceKeys.PREVIOUS_MC_TAP_TIME, currentMcTapTime)
    }

    fun restorePreviousMessageAttributionData() {
        val previousMcID = preferences.getString(PreferenceKeys.PREVIOUS_MC_ID)
        val previousMcTapTime = preferences.getString(PreferenceKeys.PREVIOUS_MC_TAP_TIME)
        preferences.put(PreferenceKeys.MC_ID, previousMcID)
        preferences.put(PreferenceKeys.MC_TAP_TIME, previousMcTapTime)
    }

    fun clearMessageAttributes() {
        preferences.remove(PreferenceKeys.MC_ID)
        preferences.remove(PreferenceKeys.MC_TAP_TIME)
    }
}