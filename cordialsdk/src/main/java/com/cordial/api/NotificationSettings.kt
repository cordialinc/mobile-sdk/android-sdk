package com.cordial.api

import android.content.Intent
import android.net.Uri
import android.provider.Settings

class NotificationSettings {
    fun openNotificationSettings() {
        val config = CordialApiConfiguration.getInstance()
        val context = config.getContext()
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }
}