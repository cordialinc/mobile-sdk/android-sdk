package com.cordial.api

import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.feature.inboxmessage.updateinboxmessagereadstatus.model.UpdateInboxMessageReadStatusRequest
import com.cordial.network.request.Page
import com.cordial.network.request.PageRequest

class CordialInboxMessageApi {

    internal val injection = CordialApiConfiguration.getInstance().injection

    fun sendInboxMessageReadEvent(mcID: String?) {
        val cordialApi = CordialApi()
        cordialApi.sendSystemEventWithProperties(C.EVENT_NAME_INBOX_MESSAGE_READ, mcID)
    }

    @JvmOverloads
    fun fetchInboxMessages(
        pageRequest: PageRequest,
        inboxFilterParams: InboxFilterParams? = null,
        onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        val fetchInboxMessagesUseCase = injection.fetchInboxMessagesInjection().fetchInboxMessagesUseCase
        fetchInboxMessagesUseCase.fetchInboxMessages(pageRequest, inboxFilterParams, onSuccess, onFailure)
    }

    fun fetchInboxMessage(
        mcID: String,
        onSuccess: (inboxMessage: InboxMessage) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        val fetchInboxMessageUseCase = injection.fetchInboxMessageInjection().fetchInboxMessageUseCase
        fetchInboxMessageUseCase.fetchInboxMessage(mcID, onSuccess, onFailure)
    }

    fun fetchInboxMessageContent(
        mcID: String,
        onSuccess: (content: String) -> Unit,
        onFailure: (error: String) -> Unit
    ) {
        val fetchInboxMessageContentUseCase =
            injection.fetchInboxMessageContentInjection().fetchInboxMessageContentUseCase
        fetchInboxMessageContentUseCase.checkCacheAndFetchInboxMessageContent(mcID, onSuccess, onFailure)
    }

    fun markInboxMessagesRead(mcIDs: List<String>) {
        val cordialApi = CordialApi()
        val primaryKey = cordialApi.getPrimaryKey()
        val deviceID = cordialApi.getDeviceIdentifier()
        val updateInboxMessageReadStatusRequest =
            UpdateInboxMessageReadStatusRequest(primaryKey, deviceID, mcIDs, null)
        val updateInboxMessageReadStatusUseCase =
            injection.updateInboxMessageReadStatusInjection().updateInboxMessageReadStatusUseCase
        updateInboxMessageReadStatusUseCase.updateInboxMessageReadStatus(
            updateInboxMessageReadStatusRequest,
            null
        )
    }

    fun markInboxMessagesUnread(mcIDs: List<String>) {
        val cordialApi = CordialApi()
        val primaryKey = cordialApi.getPrimaryKey()
        val deviceID = cordialApi.getDeviceIdentifier()
        val updateInboxMessageReadStatusRequest =
            UpdateInboxMessageReadStatusRequest(primaryKey, deviceID, null, mcIDs)
        val updateInboxMessageReadStatusUseCase =
            injection.updateInboxMessageReadStatusInjection().updateInboxMessageReadStatusUseCase
        updateInboxMessageReadStatusUseCase.updateInboxMessageReadStatus(
            updateInboxMessageReadStatusRequest,
            null
        )
    }

    fun deleteInboxMessage(mcID: String) {
        val cordialApi = CordialApi()
        val primaryKey = cordialApi.getPrimaryKey()
        val deviceID = cordialApi.getDeviceIdentifier()
        val deleteInboxMessageRequest = DeleteInboxMessageRequest(primaryKey, deviceID, mcID)
        val deleteInboxMessageUseCase = injection.deleteInboxMessageInjection().deleteInboxMessageUseCase
        deleteInboxMessageUseCase.deleteInboxMessage(deleteInboxMessageRequest, null)
    }
}