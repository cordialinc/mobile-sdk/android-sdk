package com.cordial.api

import android.os.Build
import com.cordial.feature.inboxmessage.deleteinboxmessage.model.DeleteInboxMessageRequest
import com.cordial.util.MD5

internal class CordialApiEndpoints {

    internal object Url {
        const val CONTACTS_URL = "/mobile/contacts"
        const val CONTACT_LOGOUT_URL = "/mobile/contact/logout"
        const val SEND_EVENT = "/mobile/events"
        const val UPSERT_CONTACT_CART = "/mobile/contact/cart"
        const val SEND_CONTACT_ORDER = "/mobile/orders"
        const val SDK_SECURITY_URL = "/mobile/auth/"
        const val IN_APP_MESSAGE_URL = "/mobile/message/"
        const val IN_APP_MESSAGES_URL = "/mobile/messages/"
        const val GET_INBOX_MESSAGES_URL = "/inbox/messages/"
        const val GET_INBOX_MESSAGE_URL = "/inbox/message/"
        const val UPDATE_INBOX_MESSAGES_READ_STATUS_URL = "/inbox/read"
        const val DELETE_INBOX_MESSAGE_URL = "/inbox/message/"
        const val TIMESTAMPS_URL = "/timestamps/"
    }

    private var cordialApi: CordialApi? = null
        get() {
            if (field == null)
                field = CordialApi()
            return field
        }

    private var config = CordialApiConfiguration.getInstance()

    fun getContactsUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.CONTACTS_URL
    }

    fun getContactLogoutUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.CONTACT_LOGOUT_URL
    }

    fun getSendEventUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.SEND_EVENT
    }

    fun getUpsertContactCartUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.UPSERT_CONTACT_CART
    }

    fun getSendContactOrderUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.SEND_CONTACT_ORDER
    }

    fun getSdkSecurityUrl(): String {
        val accountKey = config.getAccountKey()
        val channelKey = config.getChannelKey()
        val os = C.OS
        val osVersion = Build.VERSION.RELEASE
        val secretString =
            "{\"accountKey\":\"$accountKey\",\"channelKey\":\"$channelKey\",\"os\":\"$os\",\"version\":\"$osVersion\"}"
        val secret = MD5.getMd5(secretString)
        val baseUrl = config.getEventsStreamServiceUrl()
        return baseUrl + Url.SDK_SECURITY_URL + secret
    }

    fun getInAppMessageUrl(mcID: String): String {
        val baseUrl = getMessageHubBaseUrl()
        return baseUrl + Url.IN_APP_MESSAGE_URL + mcID
    }

    fun getInAppMessagesUrl(): String {
        val baseUrl = getMessageHubBaseUrl()
        val deviceID = cordialApi?.getDeviceIdentifier()
        val primaryKey = cordialApi?.getPrimaryKey() ?: ""
        return if (primaryKey.isNotEmpty()) {
            "$baseUrl${Url.IN_APP_MESSAGES_URL}$primaryKey/$deviceID"
        } else {
            val channelKey = config.getChannelKey()
            val pushToken = cordialApi?.getFirebaseToken()
            "$baseUrl${Url.IN_APP_MESSAGES_URL}$channelKey:$pushToken/$deviceID"
        }
    }

    fun getInboxMessagesUrl(): String {
        val baseUrl = getMessageHubBaseUrl()
        val deviceID = cordialApi?.getDeviceIdentifier()
        val primaryKey = cordialApi?.getPrimaryKey() ?: ""
        return if (primaryKey.isNotEmpty()) {
            "$baseUrl${Url.GET_INBOX_MESSAGES_URL}$primaryKey/$deviceID"
        } else {
            val channelKey = config.getChannelKey()
            val pushToken = cordialApi?.getFirebaseToken()
            "$baseUrl${Url.GET_INBOX_MESSAGES_URL}$channelKey:$pushToken/$deviceID"
        }
    }

    fun getInboxMessageUrl(mcID: String): String {
        val baseUrl = getMessageHubBaseUrl()
        val deviceID = cordialApi?.getDeviceIdentifier()
        val primaryKey = cordialApi?.getPrimaryKey() ?: ""
        return if (primaryKey.isNotEmpty()) {
            "$baseUrl${Url.GET_INBOX_MESSAGE_URL}$primaryKey/$deviceID/$mcID"
        } else {
            val channelKey = config.getChannelKey()
            val pushToken = cordialApi?.getFirebaseToken()
            "$baseUrl${Url.GET_INBOX_MESSAGE_URL}$channelKey:$pushToken/$deviceID/$mcID"
        }
    }

    fun getUpdateInboxMessageReadStatusUrl(): String {
        val baseUrl = getMessageHubBaseUrl()
        return baseUrl + Url.UPDATE_INBOX_MESSAGES_READ_STATUS_URL
    }

    fun getDeleteInboxMessageUrl(deleteInboxMessageRequest: DeleteInboxMessageRequest): String {
        val baseUrl = getMessageHubBaseUrl()
        val primaryKey = deleteInboxMessageRequest.primaryKey
        val deviceId = deleteInboxMessageRequest.deviceId
        val mcID = deleteInboxMessageRequest.mcID
        val contactId = primaryKey.ifEmpty {
            getChannelKeyPushTokenContactId()
        }
        return "$baseUrl${Url.DELETE_INBOX_MESSAGE_URL}$contactId/$deviceId/$mcID"
    }

    private fun getChannelKeyPushTokenContactId(): String {
        val channelKey = config.getChannelKey()
        val pushToken = cordialApi?.getFirebaseToken()
        return "$channelKey:$pushToken"
    }

    private fun getMessageHubBaseUrl(): String {
        val baseUrl = config.getEventsStreamServiceUrl()
        var messageHubUrl = config.getMessageHubServiceUrl()
        if (messageHubUrl == null) {
            messageHubUrl = if (baseUrl.contains(C.EVENTS_STREAM)) {
                baseUrl.replaceFirst(C.EVENTS_STREAM, C.MESSAGE_HUB)
            } else baseUrl
        }
        return messageHubUrl
    }

    fun getTimestampsUrl(): String {
        val baseUrl = getMessageHubBaseUrl()
        val deviceID = cordialApi?.getDeviceIdentifier()
        val primaryKey = cordialApi?.getPrimaryKey() ?: ""
        return if (primaryKey.isNotEmpty()) {
            "$baseUrl${Url.TIMESTAMPS_URL}$primaryKey/$deviceID"
        } else {
            val channelKey = config.getChannelKey()
            val pushToken = cordialApi?.getFirebaseToken()
            "$baseUrl${Url.TIMESTAMPS_URL}$channelKey:$pushToken/$deviceID"
        }
    }
}