package com.cordial.api

import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import com.cordial.cordialsdk.BuildConfig
import com.cordial.feature.deeplink.CordialDeepLinkProcessService
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings
import com.cordial.feature.log.CordialLoggerManager
import com.cordial.feature.sendcontactorder.model.ContactOrderRequest
import com.cordial.feature.sendcontactorder.model.Order
import com.cordial.feature.sendevent.model.EventCacheSendingReason
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.unsetcontact.model.UnsetContactRequest
import com.cordial.feature.upsertcontact.model.attributes.AttributeValue
import com.cordial.feature.upsertcontactcart.model.CartItem
import com.cordial.feature.upsertcontactcart.model.UpsertContactCartRequest
import com.cordial.storage.preferences.PreferenceKeys
import com.cordial.util.JsonUtils

class CordialApi {
    private val config = CordialApiConfiguration.getInstance()
    private val injection = config.injection
    private val preferences = injection.preferences

    fun getPrimaryKey(): String {
        return preferences.getString(PreferenceKeys.PRIMARY_KEY)
    }

    fun setLongLat(longitude: Double, latitude: Double) {
        saveLongitude(longitude)
        saveLatitude(latitude)
    }

    private fun saveLongitude(longitude: Double) {
        preferences.put(PreferenceKeys.LONGITUDE, longitude.toFloat())
    }

    internal fun getLongitude(): Double? {
        val longitude = preferences.getFloat(PreferenceKeys.LONGITUDE)
        return if (longitude == -1f) {
            null
        } else
            longitude.toDouble()
    }

    private fun saveLatitude(latitude: Double) {
        preferences.put(PreferenceKeys.LATITUDE, latitude.toFloat())
    }

    internal fun getLatitude(): Double? {
        val latitude = preferences.getFloat(PreferenceKeys.LATITUDE)
        return if (latitude == -1f) {
            null
        } else
            latitude.toDouble()
    }

    internal fun setCurrentJWT(token: String) {
        preferences.put(PreferenceKeys.JWT_TOKEN, token)
        sendCachedData()
    }

    private fun sendCachedData() {
        val cacheManager = injection.cacheManager()
        cacheManager.sendCachedData()
    }

    internal fun getCurrentJWT(): String {
        return preferences.getString(PreferenceKeys.JWT_TOKEN)
    }

    internal fun getFirebaseToken(): String? {
        if (preferences.contains(PreferenceKeys.FIREBASE_TOKEN)) {
            return preferences.getString(PreferenceKeys.FIREBASE_TOKEN)
        }
        return null
    }

    fun getDeviceIdentifier(): String {
        return preferences.getString(PreferenceKeys.DEVICE_ID)
    }

    internal fun getUserAgent(): String {
        val sdkVersion = BuildConfig.CORDIAL_SDK_VERSION_NAME
        val model = Build.MANUFACTURER + Build.MODEL.replace("\\s".toRegex(), "")
        val androidVersion = Build.VERSION.RELEASE
        val context = config.getContext()
        val applicationVersion = context.packageManager.getPackageInfo(context.packageName, 0)
            .versionName
        return "CordialSDK/$sdkVersion $model android/$androidVersion App/$applicationVersion"
    }

    fun getMcID(): String? {
        val mcID = preferences.getString(PreferenceKeys.MC_ID)
        return if (mcID.isNotEmpty()) mcID else null
    }

    fun setMcID(mcID: String) {
        val messageAttributionManager = injection.messageAttributionManager
        messageAttributionManager.saveMessageAttributionData(mcID)
    }

    private fun getMcTapTime(): String? {
        val mcTapTime = preferences.getString(PreferenceKeys.MC_TAP_TIME)
        return if (mcTapTime.isNotEmpty()) mcTapTime else null
    }

    fun setContact(primaryKey: String?) {
        savePrimaryKeyAsPreviousIfNew(primaryKey)
        val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.upsertContact(primaryKey, null)
    }

    internal fun internalSetContact(primaryKey: String?) {
        savePrimaryKeyAsPreviousIfNew(primaryKey)
        val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.upsertContact(primaryKey, null, isInternalRequest = true)
    }

    private fun savePrimaryKeyAsPreviousIfNew(primaryKey: String?) {
        val oldPrimaryKey = getPrimaryKey()
        if (preferences.getBoolean(PreferenceKeys.IS_CONTACT_SET, false) &&
            (primaryKey == null || primaryKey != oldPrimaryKey || oldPrimaryKey.isEmpty())
        ) {
            preferences.put(PreferenceKeys.PREVIOUS_PRIMARY_KEY, getPrimaryKey())
            preferences.remove(PreferenceKeys.PRIMARY_KEY)
        }
    }

    fun upsertContact(attributes: Map<String, AttributeValue>?) {
        val upsertContactUseCase = injection.upsertContactInjection().upsertContactUseCase
        upsertContactUseCase.upsertContact(getPrimaryKey(), attributes)
    }

    fun unsetContact() {
        val contactLogoutRequest = UnsetContactRequest(getDeviceIdentifier(), getPrimaryKey())
        val unsetContactUseCase = injection.unsetContactInjection().unsetContactUseCase
        unsetContactUseCase.unsetContact(contactLogoutRequest, null)
    }

    internal fun sendSystemEventWithProperties(eventName: String, mcID: String? = getMcID()) {
        val systemEventProperties: Map<String, PropertyValue> = getSystemEventProperties()
        sendCustomEvent(eventName, systemEventProperties, mcID)
    }

    internal fun getSystemEventProperties(): Map<String, PropertyValue> {
        var properties: MutableMap<String, PropertyValue> = mutableMapOf()
        if (preferences.contains(PreferenceKeys.PROPERTIES)) {
            val propertiesJson = preferences.getString(PreferenceKeys.PROPERTIES)
            if (propertiesJson.isNotEmpty()) {
                properties = JsonUtils.getPropertyMapFromJson(propertiesJson)?.toMutableMap() ?: mutableMapOf()
            }
        }
        properties = addDeviceIdPushTokenToProperties(properties)
        return properties.toMap()
    }

    private fun addDeviceIdPushTokenToProperties(properties: MutableMap<String, PropertyValue>): MutableMap<String, PropertyValue> {
        val primaryKey = getDeviceIdentifier()
        val pushToken = getFirebaseToken()
        properties[C.DEVICE_ID] = PropertyValue.StringProperty(primaryKey)
        pushToken?.let {
            properties[C.PUSH_TOKEN] = PropertyValue.StringProperty(pushToken)
        }
        return properties
    }

    internal fun sendSystemEvent(
        eventName: String,
        mcID: String? = getMcID(),
        properties: Map<String, PropertyValue>? = null
    ) {
        val systemEventProperties = getSystemEventProperties()
        val mergedProperties =
            if (properties != null) systemEventProperties.plus(properties) else systemEventProperties
        sendCustomEvent(eventName, mergedProperties, mcID)
    }

    /**
     * @see [CordialApi.sendEvent]
     */
    @Deprecated(
        message = "Use sendEvent(String, Map<String, PropertyValue>?) instead",
        level = DeprecationLevel.WARNING
    )
    fun sendCustomEvent(eventName: String, properties: Map<String, String>?) {
        if (eventName.startsWith("crdl_", false)) {
            CordialLoggerManager.error(
                "The event name \"$eventName\" cannot begin with the prefix \"crdl_\", the event will not be sent"
            )
            return
        }
        val propertyValues = properties?.mapValues {
            PropertyValue.StringProperty(it.value)
        }
        sendCustomEvent(eventName, propertyValues)
    }

    fun sendEvent(eventName: String, properties: Map<String, PropertyValue>?) {
        if (eventName.startsWith("crdl_", false)) {
            CordialLoggerManager.error(
                "The event name \"$eventName\" cannot begin with the prefix \"crdl_\", the event will not be sent"
            )
            return
        }
        sendCustomEvent(eventName, properties, getMcID())
    }

    private fun sendCustomEvent(
        eventName: String,
        properties: Map<String, PropertyValue>?,
        mcID: String? = getMcID()
    ) {
        val sendEventInjection = injection.sendEventInjection()
        val eventRequestHelper = sendEventInjection.eventRequestHelper
        val customEventRequest = eventRequestHelper.getCustomEventRequest(eventName, properties, mcID)
        val sendEventUseCase = sendEventInjection.sendEventUseCase
        sendEventUseCase.sendEvent(customEventRequest)
    }

    fun flushEvents() {
        val sendEventUseCase = injection.sendEventInjection().sendEventUseCase
        sendEventUseCase.sendCachedEvents(EventCacheSendingReason.EVENTS_FLUSH)
    }

    fun upsertContactCart(cartItems: List<CartItem>) {
        val contactCartRequest = UpsertContactCartRequest(getDeviceIdentifier(), getPrimaryKey(), cartItems)
        val upsertContactCartUseCase = injection.upsertContactCartInjection().upsertContactCartUseCase
        upsertContactCartUseCase.upsertContactCart(contactCartRequest, null)
    }

    fun sendContactOrder(order: Order) {
        val contactOrderRequest = ContactOrderRequest(
            getDeviceIdentifier(),
            getPrimaryKey(),
            getMcID(),
            getMcTapTime(),
            order
        )
        val sendContactOrderUseCase = injection.sendContactOrderInjection().sendContactOrderUseCase
        sendContactOrderUseCase.sendContactOrders(listOf(contactOrderRequest), null)
    }

    fun openDeepLink(deepLinkUrl: String, fallbackUrl: String? = null) {
        val uri = Uri.parse(deepLinkUrl)
        val fallbackUri = if (fallbackUrl != null) Uri.parse(fallbackUrl) else null
        val cordialDeepLink = CordialDeepLink(uri, isActivityRestarted = false)
        CordialDeepLinkProcessService().checkOnRedirect(cordialDeepLink, fallbackUri)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun requestNotificationPermission(educationalUiSettings: NotificationPermissionEducationalUISettings? = null) {
        val context = config.getContext()
        val notificationPermissionUseCase = injection.notificationPermissionInjection().notificationPermissionUseCase
        notificationPermissionUseCase.requestNotificationPermission(context, educationalUiSettings)
    }
}