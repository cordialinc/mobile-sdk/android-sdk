package com.cordial.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.cordial.api.C
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.URL
import java.net.URLDecoder

fun Uri.decode(): String {
    var data = this.toString()
    try {
        data = data.replace("%(?![0-9a-fA-F]{2})".toRegex(), "%25")
        data = data.replace("\\+".toRegex(), "%2B")
        data = URLDecoder.decode(data, "utf-8")
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return data
}

fun Uri?.addNoCookiesQueryParam(): Uri? {
    return this?.buildUpon()?.appendQueryParameter(C.COOKIE_ONLY, "1")?.build()
}

fun Uri.getUriWithScheme(): Uri? {
    val scheme = this.scheme
    return if (scheme == null || scheme.isEmpty()) Uri.parse("http://$this") else this
}

fun getDefaultUri(): Uri {
    return Uri.parse(C.DEFAULT_TEST_URL)
}

@Suppress("BlockingMethodInNonBlockingContext")
suspend fun downloadImage(uri: String?, sampleSize: Int = 1): Bitmap? {
    return withContext(Dispatchers.IO) {
        val bitmapOptions = BitmapFactory.Options().apply {
            inSampleSize = sampleSize
        }
        var bitmap: Bitmap? = null
        if (uri != null) {
            try {
                val url = URL(uri.toString())
                with(url.openConnection()) {
                    val inputStream = this.getInputStream()
                    bitmap = BitmapFactory.decodeStream(inputStream, null, bitmapOptions)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        bitmap
    }
}

@Suppress("BlockingMethodInNonBlockingContext")
suspend fun getImageBitmapOptions(uri: String?): BitmapFactory.Options {
    return withContext(Dispatchers.IO) {
        val bitmapOptions = BitmapFactory.Options().apply {
            inJustDecodeBounds = true
        }
        if (uri != null) {
            try {
                val url = URL(uri.toString())
                with(url.openConnection()) {
                    val inputStream = this.getInputStream()
                    BitmapFactory.decodeStream(inputStream, null, bitmapOptions)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        bitmapOptions
    }
}