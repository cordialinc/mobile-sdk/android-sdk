package com.cordial.util

import android.content.Context
import android.graphics.Insets
import android.graphics.Point
import android.graphics.Rect
import android.os.Build
import android.util.Size
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import android.view.WindowMetrics
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi

object ScreenUtils {

    fun getScreenWidth(context: Context): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val legacySize = getLegacySize(context)
            legacySize.width
        } else {
            val point = getPoint(context)
            point.x
        }
    }

    fun getScreenHeight(context: Context): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            val legacySize = getLegacySize(context)
            legacySize.height
        } else {
            val point = getPoint(context)
            point.y
        }
    }

    private fun getPoint(context: Context): Point {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val displayMetrics = windowManager.defaultDisplay
        val point = Point()
        displayMetrics?.getRealSize(point)
        return point
    }

    @RequiresApi(Build.VERSION_CODES.R)
    private fun getLegacySize(context: Context): Size {
        val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val metrics: WindowMetrics = windowManager.currentWindowMetrics
        val windowInsets = metrics.windowInsets
        val insets: Insets = windowInsets.getInsetsIgnoringVisibility(
            WindowInsets.Type.displayCutout()
        )

        val insetsWidth: Int = insets.right + insets.left
        val insetsHeight: Int = insets.top + insets.bottom

        val bounds: Rect = metrics.bounds
        return Size(
            bounds.width() - insetsWidth,
            bounds.height() - insetsHeight
        )
    }

    fun getInitialScaleByWidth(context: Context, windowWidth: Int): Int {
        return (windowWidth * 100 / getScreenWidth(context))
    }

    fun getInitialScaleByHeight(context: Context, windowHeight: Int): Int {
        return (windowHeight * 100 / getScreenHeight(context))
    }

    fun hideKeyboard(view: View, context: Context) {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }
}