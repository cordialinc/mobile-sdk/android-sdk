package com.cordial.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

internal class Delay(private val timeMillis: Long, private val isIOThread: Boolean = false) {

    fun doAfterDelay(runBlock: () -> Unit) {
        val dispatcher = if (isIOThread) Dispatchers.IO else Dispatchers.Main
        val scope = CoroutineScope(dispatcher)
        scope.launch {
            delay(timeMillis)
            runBlock()
        }
    }
}