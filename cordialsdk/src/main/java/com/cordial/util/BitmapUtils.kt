package com.cordial.util

import android.content.Context
import android.graphics.Bitmap
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.ByteArrayOutputStream
import kotlin.math.ceil

object BitmapUtils {
    fun getRemoteViewsBitmapMemoryLimitInBytes(): Double {
        return 1024 * 1024.0
    }

    fun Bitmap.getBytesSize(): Int {
        val bitmap = this
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val imageInByte = stream.toByteArray()
        return imageInByte.size
    }

    suspend fun compressBitmap(context: Context, imageUrl: String?, bitmap: Bitmap?): Bitmap? {
        val bitmapMemoryLimitInBytes = getRemoteViewsBitmapMemoryLimitInBytes()
        val bitmapBytesSize = bitmap?.getBytesSize() ?: 1
        val div = ceil(bitmapBytesSize / bitmapMemoryLimitInBytes).toInt()
        if (div < 1) return bitmap
        return downloadBitmapWithCompression(imageUrl, div) ?: return null
    }

    private suspend fun downloadBitmapWithCompression(imageUrl: String?, scale: Int): Bitmap? {
        val scope = CoroutineScope(Dispatchers.IO)
        return withContext(scope.coroutineContext) {
            downloadImage(imageUrl, scale)
        }
    }
}