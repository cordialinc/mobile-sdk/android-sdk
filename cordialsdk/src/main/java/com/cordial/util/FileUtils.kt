package com.cordial.util

import android.content.Context
import java.io.BufferedReader

fun String.trimExtension(): String {
    return if (this.contains('.')) this.substring(0, this.lastIndexOf('.')) else this
}

fun String.readAssetFile(context: Context): String? {
    return try {
        val inputStream = context.assets.open(this)
        inputStream.bufferedReader().use(BufferedReader::readText)
    } catch (e: Exception) {
        null
    }
}