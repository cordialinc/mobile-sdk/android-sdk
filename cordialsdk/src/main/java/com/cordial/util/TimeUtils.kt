package com.cordial.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

internal object TimeUtils {

    fun getTimestamp(date: Date = Date(), isMillis: Boolean = false): String {
        val tz = TimeZone.getTimeZone("GMT")
        val sdf = if (isMillis) SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK)
        else SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
        sdf.timeZone = tz
        return sdf.format(date)
    }

    fun getDate(timestamp: String, isMillis: Boolean = false): Date {
        val tz = TimeZone.getTimeZone("GMT")
        val sdf = if (isMillis) SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK)
        else SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
        sdf.timeZone = tz
        return sdf.parse(timestamp) ?: Date()
    }

    fun getTime(timestamp: String, isMillis: Boolean): Long {
        val date = getDate(timestamp, isMillis)
        return date.time
    }

    fun is24HoursPassed(timestamp: String): Boolean {
        val tz = TimeZone.getTimeZone("GMT")
        val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
        sdf.timeZone = tz
        val checkedTimestamp = sdf.parse(timestamp)
        checkedTimestamp?.let { checkedTime ->
            val checkedTimeInMillis = checkedTime.time
            return System.currentTimeMillis() >= checkedTimeInMillis + 24 * 60 * 60 * 1000
        }
        return false
    }

    fun Long?.isNewerThan(time: Long?): Boolean {
        this?.let { firstTime ->
            time?.let { secondTime ->
                return firstTime > secondTime
            }
        }
        return false
    }

    fun isDateExpired(expirationDate: String?, isMillis: Boolean = false): Boolean {
        val currentTime = getCurrentTime()
        expirationDate?.let { date ->
            val expirationTime = getTime(date, isMillis)
            return currentTime.isNewerThan(expirationTime)
        }
        return false
    }

    fun getCurrentTime(): Long {
        return System.currentTimeMillis()
    }

    fun getCurrentTimeMinusMinutes(minutes: Long = 0): Long {
        return System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(minutes)
    }

    fun getDateIfValid(timestamp: String, isMillis: Boolean = false): Date? {
        return try {
            val tz = TimeZone.getTimeZone("GMT")
            val sdf = if (isMillis) SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK)
            else SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.UK)
            sdf.timeZone = tz
            sdf.parse(timestamp) ?: Date()
        } catch (e: ParseException) {
            null
        }
    }
}