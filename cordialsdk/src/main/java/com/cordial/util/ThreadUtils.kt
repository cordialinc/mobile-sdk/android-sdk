package com.cordial.util

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

fun runOnMainThread(runBlock: () -> Unit) {
    val dispatcher = Dispatchers.Main
    val scope = CoroutineScope(dispatcher)
    scope.launch {
        runBlock()
    }
}