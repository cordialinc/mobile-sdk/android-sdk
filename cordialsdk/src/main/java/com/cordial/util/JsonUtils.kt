package com.cordial.util

import com.cordial.api.C
import com.cordial.feature.notification.carousel.CarouselItemDownload
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.upsertcontact.model.attributes.*
import org.json.JSONArray
import org.json.JSONObject

internal object JsonUtils {

    fun getMapFromJson(jsonString: String?): Map<String, String>? {
        if (jsonString == null)
            return null
        val properties: HashMap<String, String> = HashMap()
        try {
            val json = JSONObject(jsonString)
            val names = json.names()
            names?.let {
                for (i in 0 until names.length()) {
                    val key = names.getString(i)
                    val value = json.opt(key)
                    value?.let {
                        properties[key] = value.toString()
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return properties
    }

    fun getPropertyMapFromJson(jsonString: String?): Map<String, PropertyValue>? {
        if (jsonString == null)
            return null
        val properties: HashMap<String, PropertyValue> = HashMap()
        try {
            val json = JSONObject(jsonString)
            val names = json.names()
            names?.let {
                for (i in 0 until names.length()) {
                    val key = names.getString(i)
                    val value: PropertyValue = when (json.opt(key)) {
                        is String -> PropertyValue.StringProperty(json.optString(key))
                        is Int -> PropertyValue.NumericProperty(json.opt(key) as Int)
                        is Double -> PropertyValue.NumericProperty(json.opt(key) as Double)
                        is Boolean -> PropertyValue.BooleanProperty(json.opt(key) as Boolean)
                        is JSONObject -> PropertyValue.JSONObjectProperty(json.optJSONObject(key) as JSONObject)
                        is JSONArray -> {
                            val jsonArray = json.opt(key) as JSONArray
                            PropertyValue.JSONArrayProperty(jsonArray)
                        }
                        else -> PropertyValue.StringProperty(json.opt(key) as String)
                    }
                    properties[key] = value
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return properties
    }

    fun getAttributeValueMapFromJson(jsonString: String?): Map<String, AttributeValue>? {
        if (jsonString == null)
            return null
        val properties: HashMap<String, AttributeValue> = HashMap()
        try {
            val json = JSONObject(jsonString)
            val names = json.names()
            names?.let {
                for (i in 0 until names.length()) {
                    val key = names.getString(i)
                     when (json.opt(key)) {
                        is String -> {
                            val string = json.opt(key) as String
                            val date = TimeUtils.getDateIfValid(string)
                            val value = if (date == null) {
                                StringValue(string)
                            } else {
                                DateValue(date)
                            }
                            properties[key] = value
                        }
                        is Int -> properties[key] = NumericValue(json.opt(key) as Int)
                        is Double -> properties[key] = NumericValue(json.opt(key) as Double)
                        is Boolean -> properties[key] = BooleanValue(json.opt(key) as Boolean)
                        is JSONArray -> {
                            val jsonArray = json.opt(key) as JSONArray
                            val array = arrayListOf<String>()
                            for (j in 0 until (jsonArray.length())) {
                                array.add(jsonArray.getString(j))
                            }
                            properties[key] = ArrayValue(array.toTypedArray())
                        }
                        is JSONObject -> {
                            val jsonObject = json.optJSONObject(key)
                            if (jsonObject == null || jsonObject.has(C.STREET_ADDRESS)) {
                                getGeoAttributeValueFromJson(jsonObject)
                            }
                        }
                        else -> {
                            val value = if (json.opt(key) is String) json.opt(key)?.castTo<String>() else null
                            properties[key] = StringValue(value)
                        }
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return properties.toSortedMap()
    }

    fun getJsonFromPropertyValueMap(properties: Map<String, PropertyValue>): JSONObject {
        val propertiesJson = JSONObject()
        for (property in properties) {
            if (property.key.isEmpty()) {
                continue
            }
            propertiesJson.put(property.key, property.value.value)
        }
        return propertiesJson
    }

    fun getJsonFromAttributeValueMap(attributes: Map<String, AttributeValue>): JSONObject {
        var attrs = JSONObject()
        for (attr in attributes) {
            if (attr.key.isEmpty()) {
                continue
            }
            val value: Any? = when (attr.value) {
                is StringValue -> (attr.value as StringValue).value ?: JSONObject.NULL
                is NumericValue -> (attr.value as NumericValue).value ?: JSONObject.NULL
                is BooleanValue -> (attr.value as BooleanValue).value
                is ArrayValue -> {
                    val attrArrayValues = JSONArray()
                    (attr.value as ArrayValue).value.forEach {
                        attrArrayValues.put(it)
                    }
                    attrArrayValues
                }
                is DateValue -> {
                    val date = (attr.value as DateValue).date
                    if (date != null) TimeUtils.getTimestamp(date) else JSONObject.NULL
                }
                is GeoValue -> {
                    val geo = attr.value as GeoValue
                    getJsonFromGeoAttributeValue(geo)
                }
                else -> {
                    JSONObject.NULL
                }
            }
            attrs = getAttributes(attrs, attr, value)
        }
        return attrs
    }

    private fun getAttributes(
        attrs: JSONObject,
        attr: Map.Entry<String, AttributeValue>,
        value: Any?,
    ): JSONObject {
        try {
            if (!attr.key.contains(".")) {
                attrs.put(attr.key, value)
                return attrs
            }
            var innerAttr = attrs
            val attrKeys = attr.key.split(".")
            val prevKeys = mutableListOf<String>()
            val prevAttrJsonObjects = mutableListOf<JSONObject>()
            for (i in 0..attrKeys.lastIndex) {
                val key = attrKeys[i]
                if (!innerAttr.has(key)) {
                    if (attrKeys.lastIndex - i == 0) {
                        innerAttr.put(key, value)
                        return attrs
                    }
                    var jsonObjectValue = value
                    var result = JSONObject()
                    for (j in attrKeys.lastIndex downTo i + 1) {
                        val jsonObject = JSONObject()
                        jsonObject.put(attrKeys[j], jsonObjectValue)
                        jsonObjectValue = jsonObject
                        result = jsonObject
                    }
                    innerAttr.put(key, result)
                    return attrs
                } else {
                    prevKeys.add(key)
                    prevAttrJsonObjects.add(innerAttr)
                    innerAttr = innerAttr.getJSONObject(key)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return attrs
    }

    fun getListFromJson(jsonList: String?, arrayKey: String): List<Any>? {
        var list: MutableList<Any>? = null
        jsonList?.let { json ->
            val jsonObject = JSONObject(json)
            val jsonArray = jsonObject.optJSONArray(arrayKey)
            jsonArray?.let { array ->
                list = mutableListOf()
                for (i in 0 until array.length()) {
                    list?.add(array.get(i))
                }
            }
        }
        return list?.toList()
    }

    fun getJsonFromGeoAttributeValue(geo: GeoValue): JSONObject {
        val jsonObject = JSONObject()
        jsonObject.put(C.STREET_ADDRESS, geo.streetAddress)
        jsonObject.put(C.STREET_ADDRESS2, geo.streetAddress2)
        jsonObject.put(C.GEO_POSTAL_CODE, geo.postalCode)
        jsonObject.put(C.COUNTRY, geo.country)
        jsonObject.put(C.STATE, geo.state)
        jsonObject.put(C.CITY, geo.city)
        jsonObject.put(C.TIMEZONE, geo.timezone)
        return jsonObject
    }

    fun getGeoAttributeValueFromJson(jsonObject: JSONObject?): GeoValue {
        val geoValue = GeoValue()
        jsonObject?.let {
            if (jsonObject.has(C.STREET_ADDRESS))
                geoValue.streetAddress = jsonObject.getString(C.STREET_ADDRESS)
            if (jsonObject.has(C.STREET_ADDRESS2))
                geoValue.streetAddress2 = jsonObject.getString(C.STREET_ADDRESS2)
            if (jsonObject.has(C.GEO_POSTAL_CODE))
                geoValue.postalCode = jsonObject.getString(C.GEO_POSTAL_CODE)
            if (jsonObject.has(C.COUNTRY))
                geoValue.country = jsonObject.getString(C.COUNTRY)
            if (jsonObject.has(C.STATE))
                geoValue.state = jsonObject.getString(C.STATE)
            if (jsonObject.has(C.CITY))
                geoValue.city = jsonObject.getString(C.CITY)
            if (jsonObject.has(C.TIMEZONE))
                geoValue.timezone = jsonObject.getString(C.TIMEZONE)
        }
        return geoValue
    }

    fun JSONObject.optNullableString(name: String, fallback: String? = null): String? {
        return if (this.has(name) && !this.isNull(name)) {
            this.getString(name)
        } else {
            fallback
        }
    }

    fun JSONArray.toMutableListOfStrings(): MutableList<String> =
        MutableList(length(), this::getString)

    fun getCarouselItemListFromJson(jsonArray: JSONArray?): List<CarouselItemDownload>? {
        var carouselItems: MutableList<CarouselItemDownload>? = null
        jsonArray?.let { array ->
            carouselItems = mutableListOf()
            for (i in 0 until array.length()) {
                val carouselItemJsonObject = array.getJSONObject(i)
                val carouselItemImageUrl = carouselItemJsonObject.optString("imageURL")
                val carouselItemDeepLink = carouselItemJsonObject.optString("deepLink")
                carouselItems?.add(CarouselItemDownload(carouselItemImageUrl, carouselItemDeepLink))
            }
        }
        return carouselItems?.toList()
    }

}