package com.cordial.util

fun String.getByteSize(): Int {
    val byteArray = this.toByteArray(Charsets.UTF_8)
    return byteArray.size
}

fun String.removeLastSlash(): String {
    return if (this.endsWith("/")) {
        this.substring(0, this.lastIndexOf("/"))
    } else {
        this
    }
}