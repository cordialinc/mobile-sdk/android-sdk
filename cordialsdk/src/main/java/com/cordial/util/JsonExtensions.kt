package com.cordial.util

import org.json.JSONArray

fun JSONArray.toMutableList(): MutableList<Any> = MutableList(length(), this::get)