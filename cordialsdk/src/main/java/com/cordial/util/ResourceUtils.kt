package com.cordial.util

import android.content.Context

object ResourceUtils {

    fun isRawResourceExists(context: Context, resourceName: String?): Boolean {
        return isResourceExists(context, resourceName, "raw")
    }

    private fun isResourceExists(context: Context, resourceName: String?, defType: String): Boolean {
        if (resourceName == null) return false
        return context.resources.getIdentifier(resourceName, defType, context.packageName) != 0
    }
}