package com.cordial.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ComponentName
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.cordial.api.CordialApiConfiguration

internal object ActivityUtils {

    @SuppressLint("PrivateApi")
    fun getTopActivity(): Activity? {
        val activityThreadClass = Class.forName("android.app.ActivityThread")
        val activityThread = activityThreadClass.getMethod("currentActivityThread").invoke(null)
        val activitiesField = activityThreadClass.getDeclaredField("mActivities")
        activitiesField.isAccessible = true

        val activities = activitiesField.get(activityThread)?.castTo<Map<Any, Any>>() ?: return null
        for (activityRecord in activities.values) {
            val activityRecordClass = activityRecord.javaClass
            val pausedField = activityRecordClass.getDeclaredField("paused")
            pausedField.isAccessible = true
            if (!pausedField.getBoolean(activityRecord)) {
                val activityField = activityRecordClass.getDeclaredField("activity")
                activityField.isAccessible = true
                return activityField.get(activityRecord) as Activity
            }
        }
        return null
    }

    fun getActivityInfo(activity: Class<*>): ActivityInfo? {
        var activityInfo: ActivityInfo? = null
        activity.canonicalName?.let { canonicalName ->
            val context = CordialApiConfiguration.getInstance().getContext()

            val componentName = ComponentName(
                context,
                canonicalName
            )
            activityInfo = try {
                context.packageManager.getActivityInfo(
                    componentName,
                    PackageManager.GET_META_DATA
                )

            } catch (ex: Exception) {
                null
            }
        }
        return activityInfo
    }

    fun getContainerView(activity: Activity): ViewGroup? {
        val containerId = getContainerId(activity)
        var view: View? = null
        if (containerId != 0) {
            view = activity.findViewById(containerId)
        }

        if (view == null) {
            view = activity.findViewById(android.R.id.content)
        }

        return if (view is ViewGroup) {
            view
        } else null

    }

    private fun getContainerId(activity: Activity): Int {
        var containerId = 0

        val info = getActivityInfo(activity.javaClass)
        info?.metaData?.let { metaData ->
            val bannerContainerId = "com.cordial.feature.inappmessage.ui.banner.BANNER_CONTAINER_ID"
            containerId =
                metaData.getInt(bannerContainerId, containerId)
        }
        return containerId
    }

    fun setStatusBarTransparent(activity: Activity) {
        setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        activity.window.statusBarColor = Color.TRANSPARENT
        activity.window.decorView.systemUiVisibility =
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        setWindowFlag(activity, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, true)
    }

    private fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }
}