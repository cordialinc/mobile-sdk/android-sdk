package com.cordial.util

import android.content.res.Resources
import android.util.TypedValue

object ViewUtils {

    fun Float.toPx(resources: Resources): Int {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            this,
            resources.displayMetrics
        ).toInt()
    }
}