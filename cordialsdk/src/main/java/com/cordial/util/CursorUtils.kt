package com.cordial.util

fun Double.nullable(): Double? {
    return if (this == 0.0) {
        null
    } else {
        this
    }
}