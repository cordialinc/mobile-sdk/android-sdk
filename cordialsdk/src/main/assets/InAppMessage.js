function crdlAction(deepLink, eventName){
    if (typeof deepLink === 'undefined') deepLink = null
    if (typeof eventName === 'undefined') eventName = null
    Android.actionClick(deepLink, eventName);
}
// Init head variable
var head = document.head || document.getElementsByTagName('head')[0];

// Disable users selecting text in WKWebView
var style = document.createElement('style');
style.type = 'text/css';
style.appendChild(document.createTextNode('*{-webkit-touch-callout:none;-webkit-user-select:none}'));
head.appendChild(style);

function crdlCaptureAllInputs(eventName) {

    var inputs, index;
    var inputsMapping = {};

    inputs = document.getElementsByTagName('input');
    for (index = 0; index < inputs.length; ++index) {
        if (inputs[index].getAttribute('id')) {
            if (inputs[index].type === 'radio' || inputs[index].type === 'checkbox') {
                inputsMapping[inputs[index].getAttribute('id')] = inputs[index].checked;
            } else if (inputs[index].type === 'number') {
                inputsMapping[inputs[index].getAttribute('id')] = parseFloat(inputs[index].value);
            } else {
                inputsMapping[inputs[index].getAttribute('id')] = inputs[index].value;
            }
        }
    }

    selects = document.getElementsByTagName('select');
    for (index = 0; index < selects.length; ++index) {
        if (selects[index].getAttribute('id')) {
            inputsMapping[selects[index].getAttribute('id')] = selects[index].value;
        }
    }

    if (typeof eventName === 'undefined') eventName = null
    var properties = JSON.stringify(inputsMapping);
    Android.crdlEventWithProperties(eventName, properties);
}