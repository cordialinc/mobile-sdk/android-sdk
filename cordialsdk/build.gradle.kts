plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.kotlin.serialization)
}

apply {
    from("jacoco.gradle")
}

android {
    namespace = "com.cordial.cordialsdk"
    compileSdk = 34

    defaultConfig {
        minSdk = 21

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        multiDexEnabled = true

        buildConfigField("int", "CORDIAL_SDK_VERSION_CODE", "32")
        buildConfigField("String", "CORDIAL_SDK_VERSION_NAME", "\"4.17.1\"")
    }

    buildTypes {
        debug {
            enableUnitTestCoverage = true
            enableAndroidTestCoverage = true
        }

        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        viewBinding = true
        buildConfig = true
    }
}

extra.apply {
    set("PUBLISH_GROUP_ID", "com.cordial.cordialsdk")
    set("PUBLISH_VERSION", "4.17.1")
    set("PUBLISH_VERSION_CODE", "32")
    set("PUBLISH_ARTIFACT_ID", "android")
}

apply {
    from("${rootProject.projectDir}/scripts/publish-module.gradle")
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    implementation(libs.android.support.multidex)
    implementation (libs.androidx.appcompat)
    implementation(libs.androidx.core.ktx)
    implementation(libs.android.material)

    implementation(libs.android.stdlib.jdk7)
    implementation(libs.android.coroutines.ktx)

    // Firebase
    implementation(platform(libs.firebase.bom))
    implementation(libs.firebase.core)
    implementation(libs.firebase.messaging.ktx)
    implementation(libs.firebase.gcm)

    // Play Services Auth
    implementation(libs.android.gms.auth)

    // Test
    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.runner)
    androidTestImplementation(libs.androidx.espresso)
}


afterEvaluate {
    tasks.named("generateMetadataFileForReleasePublication") {
        dependsOn(":cordialsdk:androidSourcesJar")
    }
}