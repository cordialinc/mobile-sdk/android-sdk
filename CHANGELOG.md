# Android SDK changelog
The latest version of this file can be found at the master branch.
## 4.17.1 (2025-02-19)
### Fixed
- Displaying in-app message when received on the background
- Displaying in-app message on next app open
## 4.17.0 (2025-01-15)
### Changed
- Compile sdk version from 35 to 34
## 4.16.0 (2025-01-06)
### Added
- Gradle version catalogs
### Changed
- Compile sdk version from 33 to 35
- Gradle version, third-party dependencies versions
### Fixed
- Opening deep links in inbox messages
- Sending upsert contact requests in the order they are created
- Opening links without a schema in in-app messages
- Notification permission educational UI crash
## 4.15.1 (2025-02-17)
### Fixed
- Displaying in-app message when received on the background
- Displaying in-app message on next app open
## 4.15.0 (2024-09-13)
### Added
- Configuring log levels
### Fixed
- Overriding allowBackup in AndroidManifest
## 4.14.1 (2024-07-03)
### Changed
- Style names in the SDK to avoid conflicts with the application
## 4.14.0 (2024-06-27)
### Added
- Public method to get primary key
## 4.13.0 (2024-06-14)
### Added
- Ability to set the activity to be opened on notification click
### Fixed
- Deleting inbox message for contact with no primary key
## 4.12.0 (2024-05-17)
### Added
- A property to control the display of push notifications when the application is in the foreground.
## 4.11.2 (2024-05-08)
### Fixed
- In-app message display delay
## 4.11.1 (2024-05-03)
### Changed
- Compile sdk version from 34 to 33
- Decreased versions of sdk dependencies
## 4.11.0 (2024-04-26)
### Added
- Configuration of in-app message display delay
- Public method to get device identifier
## 4.10.0 (2024-02-22)
### Added
- A setting controlling if an in-app message is displayed if a push notification was dismissed
### Changed
- Allowed the use of vanity domains for all cordial URLs
- Updated the minimum Android target SDK version to 21
- Updated the Gradle version to 8.5
- Updated the JDK version to 17
- Updated proguard rules
- Migrated gradle files from Groovy to Kotlin DSL
## 4.9.0 (2023-11-21)
### Added
- Push notification categories
- Integration of push notification categories into the educational UI
- Push notification educational UI localisation
- A setting to control display of push notification educational UI
- Device ID and push token to system events properties
- SDK logging
### Changed
- Customization options for push notification educational UI
## 4.8.0 (2023-08-01)
### Added
- An option to manually retrieve in-app messages
## 4.7.0 (2023-07-19)
### Added
- An option to disable sending events bulk on timer
## 4.6.0 (2023-07-11)
### Added
- Objects support for contact attributes
## 4.5.0 (2023-06-13)
### Added
- Sending notification opt-in and opt-out system events
## 4.4.2 (2023-05-03)
### Fixed
- Event Send Service memory leaks
## 4.4.1 (2023-03-28)
### Fixed
- Crashing banner in-apps on auto dismiss
## 4.4.0 (2023-01-26)
### Added
- Educational ui for requesting notification permission
## 4.3.0 (2022-11-15)
### Added
- Push Notification Carousel
## 4.2.0 (2022-10-17)
### Added
- Android 13 support
- New API method to request notification permission for Android 13 and higher
### Changed
- Android target sdk version 32 to 33
- Gradle tools, kotlin version and android dependencies
## 4.1.0 (2022-09-13)
### Changed
- String, Numeric and Date attributes can be set to null
### Fixed
- In-app message height calculation
## 4.0.1 (2022-06-23)
### Fixed
- Opening cached deep links
## 4.0.0 (2022-06-17)
### Added
- Handling vanity url in deep links
- Public CordialDeepLink entity that includes uri and vanityUri fields
### Changed
- uri parameter to CordialDeepLink parameter in appOpenViaDeepLink() callback of CordialDeepLinkOpenListener interface
## 3.3.1 (2022-06-10)
### Fixed
- Opening application after clicking on notifications on Android 12
### Changed
- Android target sdk version 31 to 32
- Gradle tools, kotlin version and android dependencies
## 3.3.0 (2022-05-02)
### Added
- In-app message dynamic height
- deepLinkUrl property to deep link open event
## 3.2.0 (2022-01-25)
### Added
- Push notification custom sound
## 3.1.2 (2022-01-11)
### Fixed
- Network callback registering error on android 11
### Added
- Sending cached data on app open if network callback is not registered
## 3.1.1 (2021-12-07)
### Fixed
- Automatic SDK release to the maven central
## 3.1.0 (2021-12-06)
### Added
- Automatic SDK release to the maven central
### Changed
- Android minimum sdk version 16 to 19
- Android target sdk version 30 to 31
- Log levels and rephrased log messages
- Gradle tools, kotlin version and android dependencies
### Fixed
- Deep link maximum redirect number
## 3.0.1 (2021-10-29)
### Changed
- Opening links in default browser without chooser
## 3.0.0 (2021-10-12)
### Added
- Have the SDK opening deep links unknown to the application
## 2.9.1 (2021-09-24)
### Added
- View binding feature
### Changed
- Gradle tools and android dependencies
### Deprecated
- Kotlin android extension plugin
### Fixed
- Closing in-app message banner from a non-UI thread
## 2.9.0 (2021-09-13)
### Added
- Possibility to add empty fragment as web only fragment
### Changed
- Base host url and message hub host url setting
- Events qty setting
- Jwt token updating implementation
### Deprecated
- CordialApiConfiguration's initialize method
### Fixed
- Crash on opening deep link while in-app message banner was displaying
## 2.8.0 (2021-08-31)
### Changed
- Checking auth states before sending request
### Fixed
- Saving mcID after input capturing in in-app message
## 2.7.3 (2021-08-19)
### Changed
- CartItem's required properties
### Deprecated
- CartItem's constructor
- CartItem's category and qty set methods
## 2.7.2 (2021-08-18)
### Fixed
- Jacoco version property requirement
## 2.7.1 (2021-08-16)
### Fixed
- In-app message removing after related push notification was clicked
## 2.7.0 (2021-07-26)
### Changed
- Package structure
- SDK Internal classes visibility
- Dependency injection
- Handling network requests
## 2.6.0 (2021-07-14)
### Added
- Deep link web only fragments support
### Fixed
- Decoding of invalid symbols in deep links
## 2.5.2 (2021-06-15)
### Deprecated
- Deprecated methods for firebase token requesting
### Fixed
- Getting the proper screen height
## 2.5.1 (2021-06-02)
### Fixed
- Sending automatic upsert contacts after unsetting a contact
## 2.5.0 (2021-05-28)
### Added
- Public method for opening deep links via CordialApi
- Rendering WebView for inbox message content in CordialDemo app
### Fixed
- Handling expired S3 urls for in-app message content
## 2.4.0 (2021-05-12)
### Added
- Link tracking for in-app message links
- Handling html hyperlinks in in-app messages
## 2.3.0 (2021-04-26)
### Deprecated
- Deprecated methods for sending string only property values with events, orders and cart
## 2.2.0 (2021-04-14)
### Added
- InAppMessageInputsListener callback that signals what inputs were captured for the specified custom event in the in-app message
### Changed
- Naming of CordialPushNotificationListener, CordialDeepLinkOpenListener, CordialInboxMessageListener callbacks in CordialApiConfiguration
- Do not send in-app message dismiss events on action clicks and input capturing
## 2.1.0 (2021-04-07)
### Added
- Capturing inputs values from in-app messages
## 2.0.0 (2021-03-30)
### Added
- Inbox messages
- Handling deep links with link tracking on
- In-app messages reliability
- Current mcID can be set from a client app
## 0.17.3 (2021-02-15)
### Added
- Support for new android sdk version(30), gradle tools, dependencies and deprecations
## 0.17.2 (2020-08-11)
### Removed
- Removed sqliteonweb reader dependency
## 0.17.1 (2020-08-05)
### Added
- Landscape orientation for in-app messages
### Fixed
- Fixed sending duplicate in-app message show events on app close and open
## 0.17.0 (2020-08-04)
### Added
- Multidex enabled
- New push notification payload format support
## 0.16.0 (2020-07-24)
### Added
- `action` function in in-apps HTML renamed to `crdlAction`
## 0.15.0 (2020-07-22)
### Added
- Support for custom push notification icon silhouette
## 0.14.0 (2020-07-17)
### Added
- Support for multiple push notification providers