package com.cordial.android.data.model.log

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "logs")
class LogDataModel @Ignore constructor(
    @PrimaryKey(autoGenerate = true)
    var logId: Int,
    var tag: String,
    var message: String,
    var timestamp: String
) {
    constructor(
        tag: String,
        message: String,
        timestamp: String
    ) : this(logId = 0, tag, message, timestamp)
}