package com.cordial.android.data.model.attribute

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profile_attributes")
class ProfileAttributeDataModel(
    @PrimaryKey
    var attrKey: String,
    var type: Int,
    var value: String?
) {
    var streetAddress: String = ""
    var streetAddress2: String = ""
    var postalCode: String = ""
    var country: String = ""
    var state: String = ""
    var city: String = ""
    var timezone: String = ""
}