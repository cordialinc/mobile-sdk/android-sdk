package com.cordial.android.data.model.product

class Product(
    val id: Int,
    val img: Int,
    val brand: String,
    val name: String,
    val price: Double,
    val sku: String,
    val shortDesc: String,
    val path: String,
    val imgUrl: String
)