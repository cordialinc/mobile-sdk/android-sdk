package com.cordial.android.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.cordial.android.data.model.log.LogDataModel
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface LogDao {
    @Insert
    fun insert(log: LogDataModel): Completable

    @Insert
    fun insertAll(logs: List<LogDataModel>): Completable

    @Query("SELECT * FROM logs")
    fun getLogs(): Single<List<LogDataModel>>

    @Query("DELETE FROM logs WHERE logId IN (SELECT logId FROM logs LIMIT 1)")
    fun deleteFirstLog(): Completable

    @Query("DELETE FROM logs")
    fun deleteAllLogs(): Completable
}