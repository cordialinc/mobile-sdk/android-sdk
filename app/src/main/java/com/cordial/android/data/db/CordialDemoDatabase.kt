package com.cordial.android.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cordial.android.data.db.dao.AttributeDao
import com.cordial.android.data.db.dao.LogDao
import com.cordial.android.data.db.dao.ProductDao
import com.cordial.android.data.db.typeconverters.DateConverter
import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import com.cordial.android.data.model.log.LogDataModel
import com.cordial.android.data.model.product.CartItemDataModel

@Database(
    entities = [CartItemDataModel::class, ProfileAttributeDataModel::class, LogDataModel::class],
    version = 6,
    exportSchema = false
)
@TypeConverters(DateConverter::class)
abstract class CordialDemoDatabase : RoomDatabase() {
    abstract fun productDao(): ProductDao
    abstract fun attributeDao(): AttributeDao
    abstract fun logDao(): LogDao

    companion object {
        const val DATABASE_NAME: String = "cordialDemo.db"
        private var INSTANCE: CordialDemoDatabase? = null

        fun getInstance(context: Context): CordialDemoDatabase? {
            if (INSTANCE == null) {
                synchronized(CordialDemoDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        CordialDemoDatabase::class.java, DATABASE_NAME
                    )
                        .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}