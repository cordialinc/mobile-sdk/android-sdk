package com.cordial.android.data.model.product

import androidx.room.*
import java.util.*

@Entity(tableName = "cart_products")
class CartItemDataModel @Ignore constructor(
    @PrimaryKey(autoGenerate = true)
    var tableId: Int,
    @Embedded
    var product: Product,
    var productSku: String,
    var quantity: Int,
    var timestamp: Date?
) {
    constructor(
        product: Product,
        productSku: String,
        quantity: Int,
        timestamp: Date?
    ) : this(0, product, productSku, quantity, timestamp)
}