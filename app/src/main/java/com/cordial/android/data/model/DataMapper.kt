package com.cordial.android.data.model

import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import com.cordial.android.data.model.log.LogDataModel
import com.cordial.android.data.model.product.CartItemDataModel
import com.cordial.android.data.model.product.Product
import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.domain.model.product.ProductDomainModel
import org.json.JSONArray

object DataMapper {

    private fun productToDataModel(model: ProductDomainModel): Product {
        val pathJsonArray = convertListToJsonArray(model.path)
        return Product(
            model.id,
            model.img,
            model.brand,
            model.name,
            model.price,
            model.sku,
            model.shortDesc,
            pathJsonArray,
            model.imgUrl
        )
    }

    private fun convertListToJsonArray(path: List<String>): String {
        val jsonArray = JSONArray()
        path.forEach { item ->
            jsonArray.put(item)
        }
        return jsonArray.toString()
    }

    fun cartItemToDataModel(model: CartItemDomainModel): CartItemDataModel {
        return CartItemDataModel(
            productToDataModel(model.product),
            model.productSku,
            model.quantity,
            model.timestamp,
        )
    }

    fun attributeToDataModel(model: ProfileAttributeDomainModel): ProfileAttributeDataModel {
        val dataModel = ProfileAttributeDataModel(
            model.key,
            model.type,
            model.value
        )
        dataModel.streetAddress = model.streetAddress
        dataModel.streetAddress2 = model.streetAddress2
        dataModel.postalCode = model.postalCode
        dataModel.country = model.country
        dataModel.state = model.state
        dataModel.city = model.city
        dataModel.timezone = model.timezone
        return dataModel
    }


    fun logsToDataModel(logs: List<LogDomainModel>): List<LogDataModel> {
        return logs.map { log ->
            logToDataModel(log)
        }
    }

    fun logToDataModel(log: LogDomainModel): LogDataModel {
        return LogDataModel(log.tag, log.message, log.timestamp)
    }
}