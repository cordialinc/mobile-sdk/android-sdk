package com.cordial.android.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.cordial.android.data.model.product.CartItemDataModel
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*

@Dao
interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cartItemDataModel: CartItemDataModel): Completable

    @Query("UPDATE cart_products SET quantity = :quantity, timestamp = :timestamp WHERE sku = :sku")
    fun update(quantity: Int, sku: String, timestamp: Date?): Completable

    @Query("SELECT * FROM cart_products")
    fun getAllCartItems(): Single<List<CartItemDataModel>>

    @Query("SELECT * FROM cart_products")
    fun getAllProducts(): List<CartItemDataModel>

    @Query("DELETE FROM cart_products WHERE productSku = :productSku")
    fun delete(productSku: String): Completable

    @Query("DELETE FROM cart_products")
    fun deleteAll(): Completable
}