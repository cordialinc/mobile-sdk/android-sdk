package com.cordial.android.data.db.dao

import androidx.room.*
import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface AttributeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(profileAttribute: ProfileAttributeDataModel): Completable

    @Update
    fun update(profileAttribute: ProfileAttributeDataModel): Completable

    @Query("SELECT * FROM profile_attributes")
    fun getAllProfileAttributes(): Single<List<ProfileAttributeDataModel>>

    @Query("DELETE FROM profile_attributes WHERE attrKey = :attrKey")
    fun delete(attrKey: String): Completable

    @Query("DELETE FROM profile_attributes")
    fun deleteAll(): Completable
}