package com.cordial.android.data.gateway

import com.cordial.android.data.db.dao.AttributeDao
import com.cordial.android.data.db.dao.LogDao
import com.cordial.android.data.db.dao.ProductDao
import com.cordial.android.data.model.DataMapper
import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import com.cordial.android.data.model.log.LogDataModel
import com.cordial.android.data.model.product.CartItemDataModel
import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.domain.usecase.LocalStorageGateway
import io.reactivex.Completable
import io.reactivex.Single

class LocalStorageGatewayImpl(
    private val productDao: ProductDao,
    private val attributeDao: AttributeDao,
    private val logDao: LogDao
) : LocalStorageGateway {

    override fun insertProduct(cartItemDomainModel: CartItemDomainModel): Completable {
        val model = DataMapper.cartItemToDataModel(cartItemDomainModel)
        return productDao.insert(model)
    }

    override fun updateProduct(cartItemDomainModel: CartItemDomainModel): Completable {
        val model = DataMapper.cartItemToDataModel(cartItemDomainModel)
        return productDao.update(model.quantity, model.productSku, model.timestamp)
    }

    override fun getAllCartItems(): Single<List<CartItemDataModel>> {
        return productDao.getAllCartItems()
    }

    override fun getAllProducts(): List<CartItemDataModel> {
        return productDao.getAllProducts()
    }

    override fun deleteProductFromCart(cartItemDomainModel: CartItemDomainModel): Completable {
        val model = DataMapper.cartItemToDataModel(cartItemDomainModel)
        return productDao.delete(model.productSku)
    }

    override fun clearCart(): Completable {
        return productDao.deleteAll()
    }

    override fun saveAttribute(attribute: ProfileAttributeDomainModel): Completable {
        val model = DataMapper.attributeToDataModel(attribute)
        return attributeDao.insert(model)
    }

    override fun updateAttribute(attribute: ProfileAttributeDomainModel): Completable {
        val model = DataMapper.attributeToDataModel(attribute)
        return attributeDao.update(model)
    }

    override fun getAllAttributes(): Single<List<ProfileAttributeDataModel>> {
        return attributeDao.getAllProfileAttributes()
    }

    override fun deleteAttribute(attributeKey: String): Completable {
        return attributeDao.delete(attributeKey)
    }

    override fun deleteAllAttributes(): Completable {
        return attributeDao.deleteAll()
    }

    override fun insertLog(logDomainModel: LogDomainModel): Completable {
        val model = DataMapper.logToDataModel(logDomainModel)
        return logDao.insert(model)
    }

    override fun insertLogs(logs: List<LogDomainModel>): Completable {
        val model = DataMapper.logsToDataModel(logs)
        return logDao.insertAll(model)
    }

    override fun getLogs(): Single<List<LogDataModel>> {
        return logDao.getLogs()
    }

    override fun deleteFirstLog(): Completable {
        return logDao.deleteFirstLog()
    }

    override fun deleteAllLogs(): Completable {
        return logDao.deleteAllLogs()
    }
}