package com.cordial.android.di.module

import com.cordial.android.di.scope.ProfileAttributeScope
import com.cordial.android.domain.usecase.LocalStorageGateway
import com.cordial.android.domain.usecase.ProfileAttributeUseCaseImpl
import com.cordial.android.presentation.addattribute.AddAttributePresenter
import com.cordial.android.presentation.updateattributes.ProfileAttributeUseCase
import com.cordial.android.presentation.updateattributes.UpdateAttributesPresenter
import com.cordial.android.util.Const
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class ProfileAttributeModule {

    @ProfileAttributeScope
    @Provides
    fun provideProfileAttributeUseCase(
        localStorageGateway: LocalStorageGateway
    ): ProfileAttributeUseCase {
        return ProfileAttributeUseCaseImpl(localStorageGateway)
    }

    @ProfileAttributeScope
    @Provides
    fun provideProfileAttributePresenter(
        profileAttributeUseCase: ProfileAttributeUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): UpdateAttributesPresenter {
        return UpdateAttributesPresenter(profileAttributeUseCase, observeOnSchedulers)
    }

    @ProfileAttributeScope
    @Provides
    fun provideAddAttributePresenter(
        profileAttributeUseCase: ProfileAttributeUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): AddAttributePresenter {
        return AddAttributePresenter(profileAttributeUseCase, observeOnSchedulers)
    }
}