package com.cordial.android.di.component

import android.content.Context
import com.cordial.android.di.module.AppModule
import com.cordial.android.di.module.LocalStorageModule
import com.cordial.android.domain.usecase.LocalStorageGateway
import com.cordial.android.util.Const
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        LocalStorageModule::class
    ]
)
interface AppComponent {
    fun provideContext(): Context

    @Named(Const.MAIN_THREAD_SCHEDULER)
    fun provideMainThreadScheduler(): Scheduler

    fun provideLocalStorage(): LocalStorageGateway
}