package com.cordial.android.di.module

import com.cordial.android.di.scope.CartScope
import com.cordial.android.domain.usecase.CartUseCaseImpl
import com.cordial.android.domain.usecase.LocalStorageGateway
import com.cordial.android.presentation.cart.CartPresenter
import com.cordial.android.presentation.cart.CartUseCase
import com.cordial.android.presentation.catalogcategory.ProductPresenter
import com.cordial.android.util.Const
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class CartModule {

    @CartScope
    @Provides
    fun provideCartUseCase(
        localStorageGateway: LocalStorageGateway
    ): CartUseCase {
        return CartUseCaseImpl(localStorageGateway)
    }

    @CartScope
    @Provides
    fun provideCartPresenter(
        cartUseCase: CartUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): CartPresenter {
        return CartPresenter(cartUseCase, observeOnSchedulers)
    }

    @CartScope
    @Provides
    fun provideProductPresenter(
        cartUseCase: CartUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): ProductPresenter {
        return ProductPresenter(cartUseCase, observeOnSchedulers)
    }

}