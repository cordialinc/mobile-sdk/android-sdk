package com.cordial.android.di.module

import android.content.Context
import androidx.room.Room
import com.cordial.android.data.db.CordialDemoDatabase
import com.cordial.android.data.db.dao.AttributeDao
import com.cordial.android.data.db.dao.LogDao
import com.cordial.android.data.db.dao.ProductDao
import com.cordial.android.data.gateway.LocalStorageGatewayImpl
import com.cordial.android.domain.usecase.LocalStorageGateway
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalStorageModule(private val context: Context) {

    @Singleton
    @Provides
    fun provideAppDatabase(): CordialDemoDatabase {
        return Room.databaseBuilder(
            context.applicationContext,
            CordialDemoDatabase::class.java,
            CordialDemoDatabase.DATABASE_NAME
        ).allowMainThreadQueries()
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun provideProductDao(appDatabase: CordialDemoDatabase) = appDatabase.productDao()

    @Singleton
    @Provides
    fun provideAttributeDao(appDatabase: CordialDemoDatabase) = appDatabase.attributeDao()

    @Singleton
    @Provides
    fun provideLogDao(appDatabase: CordialDemoDatabase) = appDatabase.logDao()

    @Singleton
    @Provides
    fun provideLocalStorage(
        productDao: ProductDao,
        attributeDao: AttributeDao,
        logDao: LogDao
    ): LocalStorageGateway {
        return LocalStorageGatewayImpl(productDao, attributeDao, logDao)
    }
}