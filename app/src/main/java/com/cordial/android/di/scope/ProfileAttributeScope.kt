package com.cordial.android.di.scope

import javax.inject.Scope

@Scope
annotation class ProfileAttributeScope