package com.cordial.android.di.component

import com.cordial.android.di.module.LogModule
import com.cordial.android.di.scope.LogScope
import com.cordial.android.presentation.logs.LogActivity
import dagger.Component

@LogScope
@Component(
    modules = [LogModule::class],
    dependencies = [AppComponent::class]
)
interface LogComponent {
    fun inject(logActivity: LogActivity)
}