package com.cordial.android.di.component

import com.cordial.android.di.module.CartModule
import com.cordial.android.di.scope.CartScope
import com.cordial.android.presentation.cart.CartActivity
import com.cordial.android.presentation.catalogcategory.ProductActivity
import dagger.Component

@CartScope
@Component(
    modules = [CartModule::class],
    dependencies = [AppComponent::class]
)
interface CartComponent {
    fun inject(cartActivity: CartActivity)
    fun inject(productActivity: ProductActivity)
}