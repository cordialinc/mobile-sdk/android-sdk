package com.cordial.android.di.module

import com.cordial.android.di.scope.LogScope
import com.cordial.android.domain.usecase.LocalStorageGateway
import com.cordial.android.domain.usecase.LogUseCaseImpl
import com.cordial.android.presentation.logs.LogPresenter
import com.cordial.android.presentation.logs.LogUseCase
import com.cordial.android.util.Const
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class LogModule {
    @LogScope
    @Provides
    fun provideLogUseCase(
        localStorageGateway: LocalStorageGateway
    ): LogUseCase {
        return LogUseCaseImpl(localStorageGateway)
    }

    @LogScope
    @Provides
    fun provideLogPresenter(
        logUseCase: LogUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): LogPresenter {
        return LogPresenter(logUseCase, observeOnSchedulers)
    }
}