package com.cordial.android.di.component

import com.cordial.android.di.module.ProfileAttributeModule
import com.cordial.android.di.scope.ProfileAttributeScope
import com.cordial.android.presentation.addattribute.AddAttributeActivity
import com.cordial.android.presentation.updateattributes.UpdateAttributesActivity
import dagger.Component

@ProfileAttributeScope
@Component(
    modules = [ProfileAttributeModule::class],
    dependencies = [AppComponent::class]
)
interface ProfileAttributeComponent {
    fun inject(updateAttributesActivity: UpdateAttributesActivity)
    fun inject(addAttributeActivity: AddAttributeActivity)
}