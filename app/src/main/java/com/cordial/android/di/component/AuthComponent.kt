package com.cordial.android.di.component

import com.cordial.android.di.module.AuthModule
import com.cordial.android.di.scope.AuthScope
import com.cordial.android.presentation.auth.AuthActivity
import dagger.Component

@AuthScope
@Component(
    modules = [AuthModule::class],
    dependencies = [AppComponent::class]
)
interface AuthComponent {
    fun inject(authActivity: AuthActivity)
}