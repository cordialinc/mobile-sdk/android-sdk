package com.cordial.android.di.module

import com.cordial.android.di.scope.AuthScope
import com.cordial.android.domain.usecase.AuthUseCaseImpl
import com.cordial.android.domain.usecase.LocalStorageGateway
import com.cordial.android.presentation.auth.AuthPresenter
import com.cordial.android.presentation.auth.AuthUseCase
import com.cordial.android.util.Const
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class AuthModule {
    @AuthScope
    @Provides
    fun provideAuthUseCase(
        localStorageGateway: LocalStorageGateway
    ): AuthUseCase {
        return AuthUseCaseImpl(localStorageGateway)
    }

    @AuthScope
    @Provides
    fun provideAuthPresenter(
        authUseCase: AuthUseCase,
        @Named(Const.MAIN_THREAD_SCHEDULER) observeOnSchedulers: Scheduler
    ): AuthPresenter {
        return AuthPresenter(
            authUseCase,
            observeOnSchedulers
        )
    }
}