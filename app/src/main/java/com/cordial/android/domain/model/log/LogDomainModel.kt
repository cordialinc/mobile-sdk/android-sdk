package com.cordial.android.domain.model.log

data class LogDomainModel(val tag: String, val message: String, val timestamp: String)
