package com.cordial.android.domain.usecase

import com.cordial.android.domain.model.DomainMapper
import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.updateattributes.ProfileAttributeUseCase
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class ProfileAttributeUseCaseImpl(private val localStorageGateway: LocalStorageGateway) :
    ProfileAttributeUseCase {

    override fun saveAttribute(attribute: ProfileAttribute): Completable {
        val domainModel = DomainMapper.attributeToDomainModel(attribute)
        return localStorageGateway.saveAttribute(domainModel).subscribeOn(Schedulers.computation())
    }

    override fun updateAttribute(attribute: ProfileAttribute): Completable {
        val domainModel = DomainMapper.attributeToDomainModel(attribute)
        return localStorageGateway.updateAttribute(domainModel).subscribeOn(Schedulers.computation())
    }

    override fun getAllAttributes(): Single<List<ProfileAttributeDomainModel>> {
        return localStorageGateway.getAllAttributes().map {
            DomainMapper.attributesDataModelToDomainModel(it)
        }.subscribeOn(Schedulers.computation())
    }

    override fun deleteAttribute(attributeKey: String): Completable {
        return localStorageGateway.deleteAttribute(attributeKey).subscribeOn(Schedulers.computation())
    }

    override fun deleteAllAttributes(): Completable {
        return localStorageGateway.deleteAllAttributes().subscribeOn(Schedulers.computation())
    }
}