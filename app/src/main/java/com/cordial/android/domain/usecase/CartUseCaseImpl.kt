package com.cordial.android.domain.usecase

import com.cordial.android.domain.model.DomainMapper
import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.presentation.cart.CartUseCase
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.model.product.ProductPresentationModel
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import java.util.*

class CartUseCaseImpl(private val localStorageGateway: LocalStorageGateway) : CartUseCase {

    override fun addProductToCart(productPresentationModel: ProductPresentationModel, timestamp: Date?): Completable {
        val product = DomainMapper.productToDomainModel(productPresentationModel)
        var model = CartItemDomainModel(
            product,
            product.sku,
            1,
            timestamp
        )
        val allCartProducts: List<CartItemDomainModel> = localStorageGateway.getAllProducts().map {
            DomainMapper.cartItemToDomainModel(it)
        }
        allCartProducts.forEach { cartItem ->
            if (cartItem.productSku == product.sku) {
                val newQuantity = cartItem.quantity + 1
                model =
                    CartItemDomainModel(
                        cartItem.product,
                        cartItem.productSku,
                        newQuantity,
                        timestamp
                    )
                return localStorageGateway.updateProduct(model).subscribeOn(Schedulers.computation())
            }
        }
        return localStorageGateway.insertProduct(model).subscribeOn(Schedulers.computation())
    }

    override fun getAllCartProducts(): Single<List<CartItemDomainModel>> {
        return localStorageGateway.getAllCartItems()
            .map {
                DomainMapper.cartItemsToDomainModel(it)
            }.subscribeOn(Schedulers.computation())
    }

    override fun deleteProductFromCart(cartItemPresentationModel: CartItemPresentationModel): Completable {
        val model = DomainMapper.cartItemToDomainModel(cartItemPresentationModel)
        return localStorageGateway.deleteProductFromCart(model).subscribeOn(Schedulers.computation())
    }

    override fun clearCart(): Completable {
        return localStorageGateway.clearCart().subscribeOn(Schedulers.computation())
    }
}