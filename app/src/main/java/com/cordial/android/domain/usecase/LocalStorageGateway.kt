package com.cordial.android.domain.usecase

import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import com.cordial.android.data.model.log.LogDataModel
import com.cordial.android.data.model.product.CartItemDataModel
import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.domain.model.product.CartItemDomainModel
import io.reactivex.Completable
import io.reactivex.Single

interface LocalStorageGateway {
    fun insertProduct(cartItemDomainModel: CartItemDomainModel): Completable
    fun updateProduct(cartItemDomainModel: CartItemDomainModel): Completable
    fun getAllCartItems(): Single<List<CartItemDataModel>>
    fun getAllProducts(): List<CartItemDataModel>
    fun deleteProductFromCart(cartItemDomainModel: CartItemDomainModel): Completable
    fun clearCart(): Completable
    fun saveAttribute(attribute: ProfileAttributeDomainModel): Completable
    fun updateAttribute(attribute: ProfileAttributeDomainModel): Completable
    fun getAllAttributes(): Single<List<ProfileAttributeDataModel>>
    fun deleteAttribute(attributeKey: String): Completable
    fun deleteAllAttributes(): Completable
    fun insertLog(logDomainModel: LogDomainModel): Completable
    fun insertLogs(logs: List<LogDomainModel>): Completable
    fun getLogs(): Single<List<LogDataModel>>
    fun deleteFirstLog(): Completable
    fun deleteAllLogs(): Completable
}