package com.cordial.android.domain.model

import com.cordial.android.data.model.attribute.ProfileAttributeDataModel
import com.cordial.android.data.model.log.LogDataModel
import com.cordial.android.data.model.product.CartItemDataModel
import com.cordial.android.data.model.product.Product
import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.domain.model.product.ProductDomainModel
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.model.log.Log
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.model.product.ProductPresentationModel
import com.cordial.util.castTo
import org.json.JSONArray

object DomainMapper {

    fun cartItemsToDomainModel(cartItems: List<CartItemDataModel>): List<CartItemDomainModel> {
        return cartItems.map {
            cartItemToDomainModel(it)
        }
    }

    private fun productToDomainModel(model: Product): ProductDomainModel {
        val path = convertJsonArrayToList(model.path)
        return ProductDomainModel(
            model.id,
            model.img,
            model.brand,
            model.name,
            model.price,
            model.sku,
            model.shortDesc,
            path,
            model.imgUrl
        )
    }

    private fun convertJsonArrayToList(pathJsonString: String): List<String> {
        val pathList: MutableList<String> = mutableListOf()
        val pathJsonArray = JSONArray(pathJsonString)
        for (i in 0 until pathJsonArray.length()) {
            val item = pathJsonArray.get(i).castTo<String>()
            item?.let {
                pathList.add(item)
            }
        }
        return pathList.toList()
    }

    fun productToDomainModel(model: ProductPresentationModel): ProductDomainModel {
        return ProductDomainModel(
            model.id,
            model.img,
            model.brand,
            model.name,
            model.price,
            model.sku,
            model.shortDesc,
            model.path,
            model.imgUrl
        )
    }

    fun cartItemToDomainModel(model: CartItemPresentationModel): CartItemDomainModel {
        return CartItemDomainModel(
            productToDomainModel(model.product),
            model.productSku,
            model.quantity,
            model.timestamp
        )
    }

    fun cartItemToDomainModel(model: CartItemDataModel): CartItemDomainModel {
        return CartItemDomainModel(
            productToDomainModel(model.product),
            model.productSku,
            model.quantity,
            model.timestamp
        )
    }

    fun attributesToDomainModel(attributes: List<ProfileAttribute>): List<ProfileAttributeDomainModel> {
        return attributes.map {
            attributeToDomainModel(it)
        }
    }

    fun attributeToDomainModel(model: ProfileAttribute): ProfileAttributeDomainModel {
        val domainModel = ProfileAttributeDomainModel(
            model.key,
            model.type.typePosition,
            model.value
        )
        domainModel.streetAddress = model.streetAddress
        domainModel.streetAddress2 = model.streetAddress2
        domainModel.postalCode = model.postalCode
        domainModel.country = model.country
        domainModel.state = model.state
        domainModel.city = model.city
        domainModel.timezone = model.timezone
        return domainModel
    }

    fun attributesDataModelToDomainModel(attributes: List<ProfileAttributeDataModel>): List<ProfileAttributeDomainModel> {
        return attributes.map {
            attributeToDomainModel(it)
        }
    }

    private fun attributeToDomainModel(model: ProfileAttributeDataModel): ProfileAttributeDomainModel {
        val domainModel = ProfileAttributeDomainModel(
            model.attrKey,
            model.type,
            model.value
        )
        domainModel.streetAddress = model.streetAddress
        domainModel.streetAddress2 = model.streetAddress2
        domainModel.postalCode = model.postalCode
        domainModel.country = model.country
        domainModel.state = model.state
        domainModel.city = model.city
        domainModel.timezone = model.timezone
        return domainModel
    }

    fun logsDataModelToDomainModel(logs: List<LogDataModel>): List<LogDomainModel> {
        return logs.map { log ->
            logDataModelToDomainModel(log)
        }
    }

    private fun logDataModelToDomainModel(log: LogDataModel): LogDomainModel {
        return LogDomainModel(log.tag, log.message, log.timestamp)
    }

    fun logsToDomainModel(logs: List<Log>): List<LogDomainModel> {
        return logs.map { log ->
            logToDomainModel(log)
        }
    }

    fun logToDomainModel(log: Log): LogDomainModel {
        return LogDomainModel(log.tag, log.message, log.timestamp)
    }
}