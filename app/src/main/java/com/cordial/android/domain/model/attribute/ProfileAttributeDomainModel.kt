package com.cordial.android.domain.model.attribute

class ProfileAttributeDomainModel(
    var key: String,
    var type: Int,
    var value: String?
) {
    var streetAddress: String = ""
    var streetAddress2: String = ""
    var postalCode: String = ""
    var country: String = ""
    var state: String = ""
    var city: String = ""
    var timezone: String = ""
}