package com.cordial.android.domain.usecase

import com.cordial.android.domain.model.DomainMapper
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.presentation.logs.LogUseCase
import com.cordial.android.presentation.model.log.Log
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class LogUseCaseImpl(private val localStorageGateway: LocalStorageGateway): LogUseCase {
    override fun storeLog(log: Log): Completable {
        val logDomainModel = DomainMapper.logToDomainModel(log)
        return localStorageGateway.insertLog(logDomainModel)
    }

    override fun storeLogs(logs: List<Log>): Completable {
        val logsDomainModel = DomainMapper.logsToDomainModel(logs)
        return localStorageGateway.insertLogs(logsDomainModel)
    }


    override fun getAllLogs(): Single<List<LogDomainModel>> {
        return localStorageGateway.getLogs().map { logs ->
            DomainMapper.logsDataModelToDomainModel(logs)
        }.subscribeOn(Schedulers.computation())
    }

    override fun deleteFirstLog(): Completable {
        return localStorageGateway.deleteFirstLog()
    }

    override fun deleteAllLogs(): Completable {
        return localStorageGateway.deleteAllLogs()
    }
}