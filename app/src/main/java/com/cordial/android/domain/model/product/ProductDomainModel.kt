package com.cordial.android.domain.model.product

class ProductDomainModel(
    val id: Int,
    val img: Int,
    val brand: String,
    val name: String,
    val price: Double,
    val sku: String,
    val shortDesc: String,
    val path: List<String>,
    val imgUrl: String
)