package com.cordial.android.domain.usecase

import com.cordial.android.presentation.auth.AuthUseCase
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers

class AuthUseCaseImpl(private val localStorageGateway: LocalStorageGateway) : AuthUseCase {

    override fun clearUserCart(): Completable {
        return localStorageGateway.clearCart().subscribeOn(Schedulers.computation())
    }

    override fun clearProfileAttributes(): Completable {
        return localStorageGateway.deleteAllAttributes().subscribeOn(Schedulers.computation())
    }
}