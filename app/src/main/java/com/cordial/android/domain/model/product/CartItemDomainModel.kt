package com.cordial.android.domain.model.product

import java.util.*

class CartItemDomainModel(
    val product: ProductDomainModel,
    val productSku: String,
    val quantity: Int,
    val timestamp: Date?
)