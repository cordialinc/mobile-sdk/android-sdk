package com.cordial.android.util

import com.cordial.android.R
import com.cordial.android.presentation.model.product.ProductPresentationModel

object ProductUtil {

    fun getCatalogProducts(): List<ProductPresentationModel> {
        return mutableListOf(
            ProductPresentationModel(
                1,
                R.drawable.cordialthreads_men1,
                "BROOKLYN CLOTHING CO.",
                "THE COTTON CREW",
                30.00,
                "ab26ec3a-6110-11e9-8647-d663bd873d93",
                "The Cotton Crew - a classic T-shirt that is designed to be one of the most comfortable tees " +
                        "you'll ever own. Featuring the brushed cotton fabric for exceptional comfort, and the body " +
                        "length that is well proportioned to complement your body - whether on its own or when " +
                        "layering with other pieces.",
                listOf("prep-tj1.html", "crew-neck-cotton-undershirt"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_tshirts_tanks_mens_2_1000x1000.png?v=1630359513"
            ),
            ProductPresentationModel(
                2,
                R.drawable.cordialthreads_men2,
                "BROOKLYN CLOTHING CO.",
                "THE BASIC SHIRT",
                45.00,
                "ab26efdc-6110-11e9-8647-d663bd873d93",
                "Get yourself into a crisp new white T for the summer. This go-to street wear never goes " +
                        "out of style, and it can go anywhere you want it to in the hot streets of NYC. 100% cotton " +
                        "to catch those cool breezes, and 100% cool to catch those breezy babes",
                listOf("prep-tj2.html", "the-basic-shirt"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_mens_2.1_1000x1000.png?v=1628798426"
            ),
            ProductPresentationModel(
                3,
                R.drawable.cordialthreads_men3,
                "HUGO BOSS",
                "MODERN CREWNECK T-SHIRT",
                50.00,
                "ab26f446-6110-11e9-8647-d663bd873d93",
                "Stay stylish and comfortable all day long with our Modern Crewneck T-Shirt - featuring " +
                        "a lightweight cotton blend fabric that also retains a flattering shape.",
                listOf("crew-neck-t"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_tshirts_tanks_mens_1_1000x1000.png?v=1630359423"
            ),
            ProductPresentationModel(
                4,
                R.drawable.cordialthreads_men4,
                "CARIUMA",
                "RELAXED SHIRT",
                55.00,
                "ab26f5b8-6110-11e9-8647-d663bd873d93",
                "This 100% cotton long sleeve will drape and flow and furl around you as you vibe your " +
                        "way through life. Chill out, relax and max out all your cool, play a little B-ball " +
                        "outside of the school...\n" +
                        "That's Cariuma fresh.",
                listOf("relaxed-shirt"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_mens_1.1_1000x1000.png?v=1628798361"
            ),
            ProductPresentationModel(
                5,
                R.drawable.cordialthreads_men5,
                "JOHN ELLIOT",
                "CLASSIC DENIM SHIRT",
                55.00,
                "ab26f75c-6110-11e9-8647-d663bd873d93",
                "Nothing adds touch of Western authenticity to your outfit like our Classic Denim Shirt. " +
                        "Perfect for the office, date night, or when you're feeling like pulling a " +
                        "Canadian Tuxedo outfit.",
                listOf("classic-denim-long-sleeve"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_shirts_mens_4_1000x1000.png?v=1630353838"
            ),
            ProductPresentationModel(
                6,
                R.drawable.cordialthreads_men6,
                "JOHN ELLIOT",
                "WIDE-NECK LONGSLEEVE",
                55.00,
                "ab26f89c-6110-11e9-8647-d663bd873d93",
                "Stay cozy this Fall and Winter season with our Wide-Neck Longsleeve. " +
                        "Featuring the classic crewneck with strong ladder stitch for a slim yet relaxed fit.",
                listOf("wide-neck-long-sleeve"),
                "https://cdn.shopify.com/s/files/1/0592/9789/0510/products/Tops_tshirts_tanks_mens_3_1000x1000.png?v=1630359664"
            )
        )
    }
}