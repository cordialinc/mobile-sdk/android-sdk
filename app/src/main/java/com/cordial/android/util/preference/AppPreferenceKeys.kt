package com.cordial.android.util.preference

enum class AppPreferenceKeys(val key: String) {
    IS_LOGGED_IN("IS_LOGGED_IN"),
    PRIMARY_KEY("PRIMARY_KEY"),
    HOST("HOST"),
    MESSAGE_HUB_HOST("MESSAGE_HUB_HOST"),
    MESSAGE_HUB_HOST_ENABLED("MESSAGE_HUB_HOST_ENABLED"),
    ACCOUNT_KEY("ACCOUNT_KEY"),
    CHANNEL_KEY("CHANNEL_KEY"),
    QTY("QTY"),
    EVENTS_BULK_SIZE("EVENTS_BULK_SIZE"),
    EVENTS_BULK_UPLOAD_INTERVAL("EVENTS_BULK_UPLOAD_INTERVAL"),
    MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE("MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE"),
    MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE("MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE"),
    SHOW_CATEGORIES_EDUCATIONAL_UI("SHOW_CATEGORIES_EDUCATIONAL_UI"),
    PRESET("PRESET"),
    LOGS("LOGS"),
    DEFAULT("DEFAULT");

    companion object {
        fun findKey(key: String): AppPreferenceKeys {
            return values().firstOrNull { it.key == key } ?: DEFAULT
        }
    }
}