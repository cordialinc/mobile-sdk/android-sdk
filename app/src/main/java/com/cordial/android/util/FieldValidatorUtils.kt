package com.cordial.android.util

import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

object FieldValidatorUtils {
    const val REGEX_EMAIL = "^\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,3}\$"

    fun checkSymbols(text: String, type: String): Boolean {
        val pat = Pattern.compile(type)
        val m = pat.matcher(text)
        return m.find()
    }

    fun parseDate(date: String, pattern: String): String {
        val parser = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val formatter = SimpleDateFormat(pattern, Locale.getDefault())
        return formatter.format(parser.parse(date) ?: Date())
    }

    fun parseStringToCalendar(date: String): Calendar {
        val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val calendar = Calendar.getInstance()
        calendar.time = formatter.parse(date) ?: return Calendar.getInstance()
        return calendar
    }
}