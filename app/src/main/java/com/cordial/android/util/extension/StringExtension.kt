package com.cordial.android.util.extension

fun CharSequence.splitIgnoreEmpty(vararg delimiters: String): List<String> {
    return this.split(*delimiters).filter {
        it.isNotEmpty()
    }
}

fun String.removeLastSlash(): String {
    return if (this.endsWith("/")) {
        this.substring(0, this.lastIndexOf("/"))
    } else {
        this
    }
}