package com.cordial.android.util

import com.cordial.android.presentation.model.JsonType
import com.cordial.android.presentation.model.log.Log
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONTokener

object JsonUtils {
    fun getMapFromJson(jsonString: String?): Map<String, String>? {
        if (jsonString == null)
            return null
        val properties: HashMap<String, String> = HashMap()
        try {
            val json = JSONObject(jsonString)
            val names = json.names()
            names?.let {
                for (i in 0 until names.length()) {
                    val key = names.getString(i)
                    val value = json.opt(key)
                    value?.let {
                        properties[key] = value.toString()
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return properties
    }

    fun getJsonType(string: String): JsonType {
        return try {
            when (JSONTokener(string).nextValue()) {
                is JSONObject -> JsonType.OBJECT
                is JSONArray -> JsonType.ARRAY
                else -> JsonType.NONE
            }
        } catch (ex: JSONException) {
            JsonType.NONE
        }
    }

    fun getLogsFromJson(json: String): List<Log>{
        if (json.isEmpty()) return listOf()
        val logs: MutableList<Log> = mutableListOf()
        val jsonArray = JSONArray(json)
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val tag = jsonObject.getString(Const.TAG)
            val message = jsonObject.getString(Const.MESSAGE)
            val timestamp = jsonObject.getString(Const.TIMESTAMP)
            logs.add(Log(tag, message, timestamp))
        }
        return logs.toList()
    }
}