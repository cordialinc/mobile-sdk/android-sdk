package com.cordial.android.util.preference

import android.content.Context
import android.content.Context.MODE_PRIVATE

class PreferenceHelper(context: Context) {
    private val source = "com.cordialdemo.api"
    private val preferences = context.getSharedPreferences(source, MODE_PRIVATE)

    fun put(key: AppPreferenceKeys, obj: Any) {
        val editor = preferences.edit()
        when (obj) {
            is String -> editor.putString(key.key, obj)
            is Int -> editor.putInt(key.key, obj)
            is Boolean -> editor.putBoolean(key.key, obj)
            is Float -> editor.putFloat(key.key, obj)
            is Long -> editor.putLong(key.key, obj)
            else -> editor.putString(key.key, obj.toString())
        }
        editor.apply()
    }

    fun contains(key: AppPreferenceKeys): Boolean {
        return preferences.contains(key.key)
    }

    fun getString(key: AppPreferenceKeys, defaultObject: String = ""): String {
        return preferences.getString(key.key, defaultObject).toString()
    }

    fun getInt(key: AppPreferenceKeys, defaultObject: Int = -1): Int {
        return preferences.getInt(key.key, defaultObject)
    }

    fun getBoolean(
        key: AppPreferenceKeys,
        defaultObject: Boolean = false
    ): Boolean {
        return preferences.getBoolean(key.key, defaultObject)
    }

    fun getFloat(key: AppPreferenceKeys, defaultObject: Float = -1f): Float {
        return preferences.getFloat(key.key, defaultObject)
    }

    fun getLong(key: AppPreferenceKeys, defaultObject: Long = -1): Long {
        return preferences.getLong(key.key, defaultObject)
    }

    fun remove(key: AppPreferenceKeys) {
        val editor = preferences.edit()
        editor.remove(key.key)
        editor.apply()
    }

    fun clear() {
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
    }
}