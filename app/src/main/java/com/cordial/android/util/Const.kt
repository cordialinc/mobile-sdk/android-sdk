package com.cordial.android.util

object Const {
    const val EMAIL_MAX_SYMBOLS = 256
    const val NUMBER_REGEX = "[\\-\\+]?[0-9]*(\\.[0-9]+)?"
    const val PRODUCT = "PRODUCT"
    const val PRODUCT_PATH = "PRODUCT_PATH"
    const val FALLBACK_PRODUCT_PATH = "FALLBACK_PRODUCT_PATH"
    const val MAIN_THREAD_SCHEDULER = "MAIN_THREAD_SCHEDULER"
    const val REQUEST_LOCATION_PERMISSIONS = 91
    const val LAST_PRIMARY_KEY = "LAST_PRIMARY_KEY"
    const val CHECK_ON_AUTH_STATE = "CHECK_ON_AUTH_STATE"
    const val SKIP_AUTH_CHECK = "SKIP_AUTH_CHECK"
    const val PROFILE_ATTRIBUTE = "PROFILE_ATTRIBUTE"
    const val ADD_EDIT_ATTRIBUTE = 101
    const val ADD_CUSTOM_EVENT_PROPERTY = 102
    const val CUSTOM_EVENT_JSON = 103
    const val INBOX_MESSAGE_DETAILS = 104
    const val INBOX_FILTER = 105
    const val CUSTOM_EVENT_PROPERTY = "CUSTOM_EVENT_PROPERTY"
    const val CUSTOM_EVENT = "CUSTOM_EVENT"
    const val INBOX_MESSAGE_MODEL_VIEW = "INBOX_MESSAGE_MODEL_VIEW"
    const val INBOX_FILTER_PARAMS_MODEL_VIEW = "INBOX_FILTER_PARAMS_MODEL_VIEW"
    const val INBOX_MESSAGE_METADATA_DEEP_LINK = "deepLink"
    const val INBOX_MESSAGE_METADATA_DEEP_LINK_ERROR = "No deep link associated with this card"
    const val APP_NOTIFICATION_SETTINGS_URL = "https://notification-settings"
    const val CORDIAL_DEMO_DEEP_LINK_HOST = "tjs.cordialdev.com"
    const val INVALID_DEEP_LINK_FRAGMENT = "invalid-deep-link"
    const val CORDIAL_WEBSITE = "https://cordial.com/"
    const val CORDIAL_SUPPORT_WEBSITE = "https://support.cordial.com/hc/en-us/articles/360036578792-Mobile-App-Channel-Content"
    const val IS_LOGGED_IN = "IS_LOGGED_IN"
    const val TAG = "TAG"
    const val MESSAGE = "MESSAGE"
    const val TIMESTAMP = "TIMESTAMP"
    const val COPY = "Copy"
    const val CLEAR_ALL_ABOVE = "Clear all above"
}