package com.cordial.android.util.extension

import android.net.Uri
import com.cordial.util.removeLastSlash
import java.net.URLDecoder

fun Uri.decode(): String {
    var data = this.toString()
    try {
        data = data.replace("%(?![0-9a-fA-F]{2})".toRegex(), "%25")
        data = data.replace("\\+".toRegex(), "%2B")
        data = URLDecoder.decode(data, "utf-8")
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return data.removeLastSlash()
}