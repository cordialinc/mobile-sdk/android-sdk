package com.cordial.android.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    fun getTimestamp(date: Date = Date()): String {
        val tz = TimeZone.getDefault()
        val sdf = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.UK)
        sdf.timeZone = tz
        return sdf.format(date)
    }

    fun getDate(timestamp: String): Date {
        val tz = TimeZone.getDefault()
        val sdf = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.UK)
        sdf.timeZone = tz
        return sdf.parse(timestamp) ?: Date()
    }

    fun formatDate(year: Int, monthOfYear: Int, dayOfMonth: Int): String {
        val parsedDate =
            SimpleDateFormat("yyyy-MM-dd", Locale.UK).parse("$year-$monthOfYear-$dayOfMonth")
                ?: Date()
        return SimpleDateFormat("dd MMM yyyy", Locale.UK).format(parsedDate)
    }

    fun formatTime(hourOfDay: Int, minute: Int, second: Int): String {
        val parsedTime =
            SimpleDateFormat("HH:mm:ss", Locale.UK).parse("$hourOfDay:$minute:$second")
                ?: Date()
        return SimpleDateFormat("HH:mm:ss", Locale.UK).format(parsedTime)
    }

    fun formatDateTime(dateTime: String): String {
        val date = getDate(dateTime)
        val timeZone = TimeZone.getDefault()
        val sdf = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.UK)
        sdf.timeZone = timeZone
        return sdf.format(date)
    }

    fun parseDate(dateTime: String): String {
        val parsedDate =
            SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.UK).parse(dateTime) ?: Date()
        return SimpleDateFormat("dd MMM yyyy", Locale.UK).format(parsedDate)
    }

    fun parseCordialDateTime(timestamp: String, pattern: String = "dd MMM yyyy HH:mm:ss"): String {
        return try {
            val tz = TimeZone.getTimeZone("GMT")
            val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.UK)
            sdf.timeZone = tz
            val parsedDate = sdf.parse(timestamp) ?: Date()
            SimpleDateFormat(pattern, Locale.UK).format(parsedDate)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    fun parseDateNumbers(date: String): Triple<Int, Int, Int> {
        val parsedDate = SimpleDateFormat("dd MMM yyyy", Locale.UK).parse(date) ?: Date()
        val formattedDate = SimpleDateFormat("yyyy MM dd", Locale.UK).format(parsedDate)
        val numbers = formattedDate.split(" ").map {
            it.toInt()
        }
        return Triple(numbers[0], numbers[1], numbers[2])
    }

    fun parseTime(dateTime: String): String {
        val parsedTime =
            SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.UK).parse(dateTime) ?: Date()
        return SimpleDateFormat("HH:mm:ss", Locale.UK).format(parsedTime)
    }

    fun parseTimeNumbers(time: String): Triple<Int, Int, Int> {
        val numbers = time.split(":").map {
            it.toInt()
        }
        return Triple(numbers[0], numbers[1], numbers[2])
    }
}