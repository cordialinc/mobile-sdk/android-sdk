package com.cordial.android.presentation.inbox

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cordial.android.R
import com.cordial.android.databinding.ItemCardInboxMessageBinding
import com.cordial.android.databinding.ItemListInboxMessageBinding
import com.cordial.android.databinding.ItemLoadingProgressBinding
import com.cordial.android.presentation.custom.HeaderFooterAdapter
import com.cordial.android.presentation.custom.LoadingViewHolder
import com.cordial.android.util.DateTimeUtils
import com.cordial.android.util.JsonUtils

class InboxMessageAdapter(
    items: List<InboxMessageModelView>?,
    private var inboxView: InboxView,
    private val onItemClickListener: (position: Int) -> Unit,
    private val onDeleteItemListener: (inboxMessage: InboxMessageModelView?) -> Unit
) : HeaderFooterAdapter<RecyclerView.ViewHolder, Any?, InboxMessageModelView, Any>(
    null,
    items,
    Any()
) {

    fun changeInboxView(inboxView: InboxView) {
        this.inboxView = inboxView
    }

    fun deleteInboxMessage(position: Int) {
        val inboxMessage = getItem(position)
        deleteItem(position)
        onDeleteItemListener.invoke(inboxMessage)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooterPosition(position)) {
            TYPE_FOOTER
        } else when (inboxView) {
            InboxView.LIST -> InboxView.LIST.viewType
            InboxView.CARD -> InboxView.CARD.viewType
        }
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == InboxView.LIST.viewType) {
            val itemBinding = ItemListInboxMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            InboxMessageListViewHolder(itemBinding, onItemClickListener)
        } else {
            val itemBinding = ItemCardInboxMessageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            InboxMessageCardViewHolder(itemBinding, onItemClickListener)
        }
    }

    override fun onCreateFooterViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val footerBinding = ItemLoadingProgressBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LoadingViewHolder(footerBinding)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == InboxView.LIST.viewType)
            (holder as InboxMessageListViewHolder).bind(getItem(position))
        else
            (holder as InboxMessageCardViewHolder).bind(getItem(position))
    }

    fun configureFooter(isDataLoading: Boolean) {
        if (isDataLoading)
            showFooter()
        else
            hideFooter()
    }


    inner class InboxMessageCardViewHolder(
        private val binding: ItemCardInboxMessageBinding,
        private val onItemClickListener: (position: Int) -> Unit
    ) :
        RecyclerView.ViewHolder(binding.root) {


        init {
            itemView.setOnClickListener {
                onItemClickListener.invoke(bindingAdapterPosition)
            }
        }

        fun bind(inboxMessage: InboxMessageModelView) {
            with(binding) {
                val previewData = JsonUtils.getMapFromJson(inboxMessage.previewData)
                tvInboxMessageTitle.text =
                    if (previewData != null && previewData["title"] != null) previewData["title"] else "Title"
                tvInboxMessageSubtitle.text =
                    if (previewData != null && previewData["subtitle"] != null) previewData["subtitle"] else "Subtitle"
                if (previewData != null && previewData["imageUrl"] != null) {
                    Glide.with(itemView.context).load(previewData["imageUrl"]).into(ivInboxMessageImage)
                } else {
                    ivInboxMessageImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            itemView.context,
                            R.drawable.ic_default_image
                        )
                    )
                }
            }
        }
    }

    inner class InboxMessageListViewHolder(
        private val binding: ItemListInboxMessageBinding,
        private val onItemClickListener: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            itemView.setOnClickListener {
                onItemClickListener.invoke(bindingAdapterPosition)
            }
        }

        fun bind(inboxMessage: InboxMessageModelView) {
            with(binding) {
                val previewData = JsonUtils.getMapFromJson(inboxMessage.previewData)
                tvInboxMessageTitle.text =
                    if (previewData != null && previewData["title"] != null) previewData["title"] else "Title"
                tvInboxMessageSubtitle.text =
                    if (previewData != null && previewData["subtitle"] != null) previewData["subtitle"] else "Subtitle"
                tvInboxMessageSentAt.text = DateTimeUtils.parseCordialDateTime(inboxMessage.sentAt, "dd MMM")

                if (previewData != null && previewData["imageUrl"] != null) {
                    Glide.with(itemView.context).load(previewData["imageUrl"]).into(ivInboxMessageImage)
                } else {
                    ivInboxMessageImage.setImageDrawable(
                        ContextCompat.getDrawable(itemView.context, R.drawable.ic_default_image)
                    )
                }
                if (inboxMessage.isRead) {
                    tvInboxMessageTitle.setTypeface(null, Typeface.NORMAL)
                    tvInboxMessageTitle.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorGrey))
                    tvInboxMessageSubtitle.setTypeface(null, Typeface.NORMAL)
                    tvInboxMessageSubtitle.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorGrey))
                    tvInboxMessageSentAt.setTypeface(null, Typeface.NORMAL)
                    tvInboxMessageSentAt.setTextColor(ContextCompat.getColor(itemView.context, R.color.colorGrey))
                } else {
                    tvInboxMessageTitle.setTypeface(null, Typeface.BOLD)
                    tvInboxMessageTitle.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.black))
                    tvInboxMessageSubtitle.setTypeface(null, Typeface.BOLD)
                    tvInboxMessageSubtitle.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.black))
                    tvInboxMessageSentAt.setTypeface(null, Typeface.BOLD)
                    tvInboxMessageSentAt.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.black))
                }
            }
        }
    }
}