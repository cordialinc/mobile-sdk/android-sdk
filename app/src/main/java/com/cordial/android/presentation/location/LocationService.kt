package com.cordial.android.presentation.location

import android.annotation.TargetApi
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration


class LocationService : LocationListener {
    private var locationManager: LocationManager? = null
    private var location: Location? = null
    private var longitude: Double = -1.0
    private var latitude: Double = -1.0
    private var isGPSEnabled = false
    private var isNetworkEnabled = false

    init {
        initLocationService()
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun initLocationService() {
        val context = CordialApiConfiguration.getInstance().getContext()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            ContextCompat.checkSelfPermission(
                context,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(
                context,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        try {
            this.locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
            locationManager?.let { locationManager ->
                this.isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
                this.isNetworkEnabled =
                    locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

                if (!isNetworkEnabled && !isGPSEnabled) {
                    return
                }
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                    )
                    location =
                        locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    location?.let {
                        onLocationChanged(it)
                    }
                }
                if (isGPSEnabled) {
                    locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                    )
                    location =
                        locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    location?.let {
                        onLocationChanged(it)
                    }
                }
            }
        } catch (ex: Exception) {
        }

    }

    override fun onLocationChanged(location: Location) {
        longitude = location.longitude
        latitude = location.latitude
        CordialApi().setLongLat(longitude, latitude)
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String) {
    }

    override fun onProviderDisabled(p0: String) {
    }

    companion object {

        private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 100

        private const val MIN_TIME_BW_UPDATES: Long = 60000

        private var instance: LocationService? = null

        fun getLocationManager(): LocationService? {
            if (instance == null) {
                instance = LocationService()
            }
            return instance
        }
    }
}