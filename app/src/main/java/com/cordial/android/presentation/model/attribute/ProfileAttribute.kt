package com.cordial.android.presentation.model.attribute

import java.io.Serializable

class ProfileAttribute(
    var key: String,
    var type: ProfileAttributeType,
    var value: String?
) : Serializable {
    var streetAddress: String = ""
    var streetAddress2: String = ""
    var postalCode: String = ""
    var country: String = ""
    var state: String = ""
    var city: String = ""
    var timezone: String = ""
}