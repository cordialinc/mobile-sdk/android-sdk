package com.cordial.android.presentation.auth

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.product.CartItemPresentationModel

interface AuthView : BaseView {
    fun showCartProducts(items: List<CartItemPresentationModel>)
    fun cartIsCleared()
    fun profileAttributesClear()
}