package com.cordial.android.presentation.addproperty

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.cordial.android.R
import com.cordial.android.databinding.ActivityAddPropertyBinding
import com.cordial.android.presentation.model.property.CustomEventProperty
import com.cordial.android.util.Const

class AddPropertyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAddPropertyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddPropertyBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setClickListeners()
        configureActionBar()
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.add_property)
    }

    private fun setClickListeners() {
        binding.btnAddProperty.setOnClickListener {
            validateProperty()
        }
    }

    private fun validateProperty() {
        if (validateAttrKey()) {
            addProperty()
        }
    }

    private fun validateAttrKey(): Boolean {
        with(binding) {
            val key = etPropertyKey.text.toString().trim()
            return if (key.length in 1..Const.EMAIL_MAX_SYMBOLS) {
                tilPropertyKey.isErrorEnabled = false
                true
            } else {
                tilPropertyKey.error = resources.getString(R.string.property_key_error)
                tilPropertyKey.isErrorEnabled = true
                false
            }
        }
    }

    private fun addProperty() {
        val propertyKey = binding.etPropertyKey.text.toString().trim()
        val propertyValue = binding.etPropertyValue.text.toString().trim()
        val property = CustomEventProperty(propertyKey, propertyValue)
        val intent = Intent()
        intent.putExtra(Const.CUSTOM_EVENT_PROPERTY, property)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}