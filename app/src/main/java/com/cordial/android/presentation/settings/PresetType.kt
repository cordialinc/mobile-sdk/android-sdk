package com.cordial.android.presentation.settings

import com.cordial.api.C

enum class PresetType(
    val presetName: String,
    val eventsStreamUrl: String,
    val messageHubUrl: String,
    val accountKey: String,
    val channelKey: String,
    val qtyCached: Int,
    val eventsBulkSize: Int,
    val eventsBulkUploadInterval: Int,
    val maxInboxMessageContentCacheSize: Int,
    val maxCacheableInboxMessageContentSize: Int
) {
    QC(
        presetName = "Qc",
        eventsStreamUrl = "https://events-stream-svc.cordial-core.master.cordialdev.com",
        messageHubUrl = "https://message-hub-svc.cordial-core.master.cordialdev.com",
        accountKey = "qc-all-channels-cID-pk",
        channelKey = "push",
        qtyCached = 100,
        eventsBulkSize = 5,
        eventsBulkUploadInterval = 30,
        C.DEFAULT_MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE,
        C.DEFAULT_MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
    ),
    STAGING(
        presetName = "Staging",
        eventsStreamUrl = "https://events-stream-svc.stg.cordialdev.com",
        messageHubUrl = "https://message-hub-svc.stg.cordialdev.com",
        accountKey = "stgtaras",
        channelKey = "sdk",
        qtyCached = 100,
        eventsBulkSize = 5,
        eventsBulkUploadInterval = 30,
        C.DEFAULT_MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE,
        C.DEFAULT_MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
    ),
    PRODUCTION(
        presetName = "Production",
        eventsStreamUrl = "https://events-stream-svc.cordial.com",
        messageHubUrl = "https://message-hub-svc.cordial.com",
        accountKey = "cordialdev",
        channelKey = "push",
        qtyCached = 100,
        eventsBulkSize = 5,
        eventsBulkUploadInterval = 30,
        C.DEFAULT_MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE,
        C.DEFAULT_MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
    ),
    CUSTOM(
        presetName = "Custom",
        eventsStreamUrl = "https://events-stream-svc.stg.cordialdev.com",
        messageHubUrl = "https://message-hub-svc.stg.cordialdev.com",
        accountKey = "qc-all-channels-cID-pk",
        channelKey = "push",
        qtyCached = 100,
        eventsBulkSize = 5,
        eventsBulkUploadInterval = 30,
        C.DEFAULT_MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE,
        C.DEFAULT_MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
    );

    companion object {
        fun findPresetType(presetName: String): PresetType {
            return values().firstOrNull { it.presetName == presetName } ?: CUSTOM
        }
    }
}