package com.cordial.android.presentation.updateattributes

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.attribute.ProfileAttribute

interface UpdateAttributesView : BaseView {
    fun showAllAttributes(attributes: List<ProfileAttribute>)
    fun attributeIsDeleted(attribute: ProfileAttribute)
}