package com.cordial.android.presentation.catalogcategory

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityProductBinding
import com.cordial.android.presentation.auth.AuthActivity
import com.cordial.android.presentation.cart.CartActivity
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.model.product.ProductPresentationModel
import com.cordial.android.util.Const
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.CordialApi
import com.cordial.feature.sendevent.model.property.PropertyValue
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import javax.inject.Inject

class ProductActivity : AppCompatActivity(), ProductView {
    private lateinit var binding: ActivityProductBinding

    private var product: ProductPresentationModel? = null
    private lateinit var cordialApi: CordialApi

    @Inject
    protected lateinit var presenter: ProductPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        binding = ActivityProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        cordialApi = CordialApi()
        setupProduct()
        configureActionBar()
        fillViews()
        setClickListeners()
    }

    private fun inject() {
        App.injector?.cartComponent?.inject(this)
    }

    private fun setupProduct() {
        if (intent.hasExtra(Const.PRODUCT)) {
            product = intent.getSerializableExtra(Const.PRODUCT) as ProductPresentationModel
        }
    }

    @SuppressLint("InflateParams")
    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val customActionBar = LayoutInflater.from(this).inflate(R.layout.action_bar_product, null)
        val tvTitle = customActionBar.findViewById<TextView>(R.id.tv_action_bar_product_title)
        tvTitle.text = product?.name
        supportActionBar?.customView = customActionBar
    }


    private fun fillViews() {
        product?.let {
            with(binding) {
                ivProductImage.setImageDrawable(ContextCompat.getDrawable(this@ProductActivity, it.img))
                tvProductName.text = it.name
                tvProductBrand.text = it.brand
                val productPrice = "$ ${it.price}"
                tvProductPrice.text = productPrice
                tvProductDescription.text = it.shortDesc
            }
        }
    }

    private fun setClickListeners() {
        binding.btnAddToCart.setOnClickListener {
            product?.let {
                presenter.addProductToCart(it, Date())
                sendEvent(eventName = "add_to_cart")
            }
        }
    }

    private fun openCartActivity() {
        val intent = Intent(this@ProductActivity, CartActivity::class.java)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
        sendEvent(eventName = "browse_product")
    }

    private fun sendEvent(eventName: String) {
        var properties: Map<String, PropertyValue>? = null
        product?.let {
            properties = mapOf(
                "productName" to PropertyValue.StringProperty(it.name),
                "price" to PropertyValue.NumericProperty(it.price),
                "isWishList" to PropertyValue.BooleanProperty(true),
                "brand" to PropertyValue.JSONObjectProperty(JSONObject().put("name", it.brand)),
                "availableColors" to PropertyValue.JSONArrayProperty(
                    JSONArray(listOf("blue", "black"))
                )
            )
        }
        cordialApi.sendEvent(eventName, properties)
    }


    override fun productCartAdded() {
        openCartActivity()
        presenter.getAllCartProducts()
    }

    override fun showCartProducts(items: List<CartItemPresentationModel>) {
        upsertContactCart(items)
    }

    private fun upsertContactCart(items: List<CartItemPresentationModel>) {
        val cartItems = CartActivity.getCartItems(items)
        cordialApi.upsertContactCart(cartItems)
    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_product, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.getItem(1)?.isVisible = !PreferenceHelper(this).getBoolean(AppPreferenceKeys.IS_LOGGED_IN)
        menu?.getItem(2)?.isVisible = !PreferenceHelper(this).getBoolean(AppPreferenceKeys.IS_LOGGED_IN)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_cart -> {
                openCartActivity()
                true
            }
            android.R.id.home -> {
                finish()
                true
            }
            R.id.item_login -> {
                openAuthScreen()
                true
            }
            R.id.item_logout -> {
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun logout() {
        cordialApi.unsetContact()
        PreferenceHelper(this).put(AppPreferenceKeys.IS_LOGGED_IN, false)
        openAuthScreen()
    }

    private fun openAuthScreen() {
        val intent = Intent(this@ProductActivity, AuthActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit)
        finish()
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.injector?.recycleCartComponent()
    }
}