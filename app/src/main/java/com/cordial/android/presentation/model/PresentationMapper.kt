package com.cordial.android.presentation.model

import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.domain.model.product.ProductDomainModel
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.model.attribute.ProfileAttributeType
import com.cordial.android.presentation.model.log.Log
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.model.product.ProductPresentationModel

object PresentationMapper {

    fun cartItemsToDomainModel(cartItems: List<CartItemDomainModel>): List<CartItemPresentationModel> {
        return cartItems.map {
            cartItemToPresentationModel(it)
        }
    }

    private fun productToPresentationModel(model: ProductDomainModel): ProductPresentationModel {
        return ProductPresentationModel(
            model.id,
            model.img,
            model.brand,
            model.name,
            model.price,
            model.sku,
            model.shortDesc,
            model.path,
            model.imgUrl
        )
    }

    private fun cartItemToPresentationModel(model: CartItemDomainModel): CartItemPresentationModel {
        return CartItemPresentationModel(
            productToPresentationModel(model.product),
            model.productSku,
            model.quantity,
            model.timestamp
        )
    }

    fun attributesToPresentationModel(attributes: List<ProfileAttributeDomainModel>): List<ProfileAttribute> {
        return attributes.map {
            attributeToPresentationModel(it)
        }
    }

    private fun attributeToPresentationModel(model: ProfileAttributeDomainModel): ProfileAttribute {
        val profileAttribute = ProfileAttribute(
            model.key,
            ProfileAttributeType.findType(model.type),
            model.value
        )
        profileAttribute.streetAddress = model.streetAddress
        profileAttribute.streetAddress2 = model.streetAddress2
        profileAttribute.postalCode = model.postalCode
        profileAttribute.country = model.country
        profileAttribute.state = model.state
        profileAttribute.city = model.city
        profileAttribute.timezone = model.timezone
        return profileAttribute
    }

    fun logsDomainModelToLog(logs: List<LogDomainModel>): List<Log> {
        return logs.map { log ->
            logDomainModelToLog(log)
        }
    }

    private fun logDomainModelToLog(log: LogDomainModel): Log {
        return Log(log.tag, log.message, log.timestamp)
    }
}