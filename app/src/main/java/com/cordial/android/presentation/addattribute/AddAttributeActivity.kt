package com.cordial.android.presentation.addattribute

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityAddAttributeBinding
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.model.attribute.ProfileAttributeType
import com.cordial.android.util.Const
import com.cordial.android.util.DateTimeUtils
import com.cordial.util.ScreenUtils
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import java.util.*
import javax.inject.Inject

class AddAttributeActivity : AppCompatActivity(), AddAttributeView,
    TimePickerDialog.OnTimeSetListener, DatePickerDialog.OnDateSetListener {

    private lateinit var binding: ActivityAddAttributeBinding

    private val attrTypes = arrayOf(
        ProfileAttributeType.STRING.type,
        ProfileAttributeType.NUMERIC.type,
        ProfileAttributeType.BOOLEAN.type,
        ProfileAttributeType.ARRAY.type,
        ProfileAttributeType.DATE.type,
        ProfileAttributeType.GEO.type
    )

    private val attrCountries = arrayOf(
        "",
        "United States of America",
        "United Kingdom (Great Britain)",
        "France",
        "Italy"
    )
    private val attrTimezones = arrayOf(
        "",
        "America/Los_Angeles",
        "Europe/London",
        "Europe/Paris",
        "Europe/Rome"
    )
    private var attrType = ProfileAttributeType.STRING
    private var attrGeoCountry = ""
    private var attrGeoTimezone = ""
    private var editProfileAttribute: ProfileAttribute? = null

    @Inject
    protected lateinit var presenter: AddAttributePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        initAttribute()
        binding = ActivityAddAttributeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        setListeners()
        fillViews()
    }

    private fun inject() {
        App.injector?.profileAttributeComponent?.inject(this)
    }

    private fun initAttribute() {
        intent?.extras?.let { extras ->
            if (extras.containsKey(Const.PROFILE_ATTRIBUTE)) {
                editProfileAttribute = extras.getSerializable(Const.PROFILE_ATTRIBUTE) as ProfileAttribute
            }
        }
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title =
            if (editProfileAttribute == null) resources.getString(R.string.add_attribute) else resources.getString(R.string.edit_attribute)
    }

    private fun setListeners() {
        with(binding) {
            configureDateInputs()
            btnAddAttr.setOnClickListener {
                ScreenUtils.hideKeyboard(btnAddAttr, this@AddAttributeActivity)
                addAttribute()
            }
            initSpinner(spinnerAttrType, attrTypes) { position ->
                attrType = ProfileAttributeType.findType(position)
                configureAttrInput()
            }
            initSpinner(spinnerAttrCountry, attrCountries) { position ->
                attrGeoCountry = attrCountries[position]
            }
            initSpinner(spinnerAttrTimezone, attrTimezones) { position ->
                attrGeoTimezone = attrTimezones[position]
            }
        }
    }

    private fun initSpinner(
        spinner: Spinner,
        objects: Array<String>,
        onItemSelectedListener: ((position: Int) -> Unit)?
    ) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, objects)
        spinner.adapter = arrayAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onItemSelectedListener?.invoke(position)
            }
        }
    }

    private fun configureDateInputs() {
        with(binding) {
            etAttrDate.inputType = InputType.TYPE_NULL
            etAttrDate.setOnClickListener {
                showDatePickerDialog(etAttrDate.text.toString())
            }
            etAttrDate.setOnFocusChangeListener { _, isFocused ->
                if (isFocused) {
                    showDatePickerDialog(etAttrDate.text.toString())
                }
            }
            etAttrTime.inputType = InputType.TYPE_NULL
            etAttrTime.setOnClickListener {
                showTimePickerDialog(etAttrTime.text.toString())
            }
            etAttrTime.setOnFocusChangeListener { _, isFocused ->
                if (isFocused) {
                    showTimePickerDialog(etAttrTime.text.toString())
                }
            }
        }
    }

    private fun showDatePickerDialog(dateTime: String) {
        val now: Calendar = Calendar.getInstance()
        if (dateTime.isNotEmpty()) {
            val date = DateTimeUtils.parseDateNumbers(dateTime)
            now.set(date.first, date.second - 1, date.third)
        }
        val dpd = DatePickerDialog.newInstance(
            this,
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        )

        dpd.accentColor = ContextCompat.getColor(this, R.color.colorOrange)
        dpd.setOkColor(ContextCompat.getColor(this, R.color.colorOrange))
        dpd.setCancelColor(ContextCompat.getColor(this, R.color.colorOrange))
        dpd.show(supportFragmentManager, "DatePickerDialog")
    }

    private fun showTimePickerDialog(dateTime: String) {
        val now: Calendar = Calendar.getInstance()
        if (dateTime.isNotEmpty()) {
            val time = DateTimeUtils.parseTimeNumbers(dateTime)
            now.set(0, 0, 0, time.first, time.second, time.third)
        }
        val tpd =
            TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                now.get(Calendar.SECOND),
                true
            )
        tpd.enableSeconds(true)
        tpd.accentColor = ContextCompat.getColor(this, R.color.colorOrange)
        tpd.setOkColor(ContextCompat.getColor(this, R.color.colorOrange))
        tpd.setCancelColor(ContextCompat.getColor(this, R.color.colorOrange))
        tpd.show(supportFragmentManager, "TimePickerDialog")
    }

    private fun fillViews() {
        editProfileAttribute?.let { attribute ->
            with(binding) {
                etAttrKey.setText(attribute.key)
                when (attribute.type) {
                    ProfileAttributeType.BOOLEAN -> {
                        cbAttr.isChecked = attribute.value == "true"
                    }
                    ProfileAttributeType.GEO -> {
                        etAttrStreetAddress.setText(attribute.streetAddress)
                        etAttrStreetAddress2.setText(attribute.streetAddress2)
                        etAttrPostalCode.setText(attribute.postalCode)
                        spinnerAttrCountry.setSelection(attrCountries.indexOf(attribute.country))
                        etAttrState.setText(attribute.state)
                        etAttrCity.setText(attribute.city)
                        spinnerAttrTimezone.setSelection(attrTimezones.indexOf(attribute.timezone))
                    }
                    ProfileAttributeType.DATE -> {
                        attribute.value?.let { value ->
                            val date = DateTimeUtils.parseDate(value)
                            etAttrDate.setText(date)
                            val time = DateTimeUtils.parseTime(value)
                            etAttrTime.setText(time)
                        }
                    }
                    else -> {
                        etAttrValue.setText(attribute.value)
                    }
                }
                spinnerAttrType.setSelection(attribute.type.typePosition)
                btnAddAttr.text = resources.getString(R.string.edit_attribute)
            }
        }
    }

    private fun validateAttrKey(): Boolean {
        with(binding) {
            val key = etAttrKey.text.toString().trim()
            return if (key.length in 1..Const.EMAIL_MAX_SYMBOLS) {
                tilAttrKey.isErrorEnabled = false
                true
            } else {
                tilAttrKey.error = resources.getString(R.string.attr_key_error)
                tilAttrKey.isErrorEnabled = true
                false
            }
        }
    }

    private fun validateAttrValue(): Boolean {
        return when (attrType) {
            ProfileAttributeType.NUMERIC -> validateAttrNumericValue()
            ProfileAttributeType.DATE -> validateAttrDateTimeFields()
            else -> true
        }
    }

    private fun validateAttrNumericValue(): Boolean {
        with(binding) {
            val value = etAttrValue.text.toString().trim()
            return if (value.length in 0..Const.EMAIL_MAX_SYMBOLS && value.matches(Regex(Const.NUMBER_REGEX))) {
                tilAttrValue.isErrorEnabled = false
                true
            } else {
                tilAttrValue.error =
                    resources.getString(R.string.attr_numeric_value_error)
                tilAttrValue.isErrorEnabled = true
                false
            }
        }
    }

    private fun validateAttrDateTimeFields(): Boolean {
        with(binding) {
            val dateValue = etAttrDate.text.toString().trim()
            val isDateValid = dateValue.isNotEmpty()
            val timeValue = etAttrTime.text.toString().trim()
            val isTimeValid = timeValue.isNotEmpty()
            if (!isDateValid and !isTimeValid) return true
            if (isDateValid and isTimeValid) return true
            if (isDateValid and !isTimeValid) {
                tilAttrDate.isErrorEnabled = false
            } else {
                tilAttrDate.error = resources.getString(R.string.attr_date_validation_error)
                tilAttrDate.isErrorEnabled = true
            }
            if (isTimeValid and !isDateValid) {
                tilAttrTime.isErrorEnabled = false
            } else {
                tilAttrTime.error = resources.getString(R.string.attr_time_validation_error)
                tilAttrTime.isErrorEnabled = true
            }
            return isDateValid and isTimeValid
        }
    }

    private fun configureAttrInput() {
        with(binding) {
            when (attrType) {
                ProfileAttributeType.STRING -> {
                    etAttrValue.inputType = InputType.TYPE_CLASS_TEXT
                    tilAttrValue.hint = getString(R.string.attribute_value)
                    showAttrValueFields(true)
                }
                ProfileAttributeType.ARRAY -> {
                    etAttrValue.inputType = InputType.TYPE_CLASS_TEXT
                    tilAttrValue.hint = getString(R.string.attribute_value_array)
                    showAttrValueFields(true)
                }
                ProfileAttributeType.NUMERIC -> {
                    val value = etAttrValue.text.toString()
                    if (value.toDoubleOrNull() == null) {
                        etAttrValue.setText("")
                    }
                    etAttrValue.inputType =
                        InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
                    tilAttrValue.hint = getString(R.string.attribute_value)
                    showAttrValueFields(true)
                }
                ProfileAttributeType.BOOLEAN -> {
                    showAttrValueFields(false)
                }
                ProfileAttributeType.DATE -> {
                    showDateTimeFields()
                }
                ProfileAttributeType.GEO -> {
                    showGeoFields()
                }
            }
            hideAttrValueError()
        }
    }

    private fun hideAttrValueError() {
        binding.tilAttrValue.isErrorEnabled = false
    }

    private fun showAttrValueFields(isTextInput: Boolean) {
        with(binding) {
            tilAttrValue.visibility = if (isTextInput) View.VISIBLE else View.INVISIBLE
            cbAttr.visibility = if (isTextInput) View.INVISIBLE else View.VISIBLE
            vgAttrDateTime.visibility = View.GONE
            vgAttrGeo.visibility = View.GONE
        }
    }

    private fun showDateTimeFields() {
        with(binding) {
            vgAttrDateTime.visibility = View.VISIBLE
            tilAttrValue.visibility = View.GONE
            cbAttr.visibility = View.GONE
            vgAttrGeo.visibility = View.GONE
        }
    }

    private fun showGeoFields() {
        with(binding) {
            vgAttrGeo.visibility = View.VISIBLE
            tilAttrValue.visibility = View.GONE
            cbAttr.visibility = View.GONE
            vgAttrDateTime.visibility = View.GONE
        }
    }

    private fun addAttribute() {
        with(binding) {
            if (validateAttrKey() && validateAttrValue()) {
                val attrKey = etAttrKey.text.toString().trim()
                val attrValue = when (attrType) {
                    ProfileAttributeType.BOOLEAN -> {
                        if (cbAttr.isChecked) "true" else "false"
                    }
                    ProfileAttributeType.DATE -> {
                        when {
                            etAttrDate.text == null || etAttrTime.text == null -> null
                            etAttrDate.text.toString().isEmpty() && etAttrTime.text.toString().isEmpty() -> null
                            else -> {
                                val dateTime = "${etAttrDate.text} ${etAttrTime.text}"
                                dateTime
                            }
                        }
                    }
                    ProfileAttributeType.GEO -> {
                        ""
                    }
                    else -> {
                        if (etAttrValue.text.toString().isEmpty())
                            null
                        else etAttrValue.text.toString().trim()
                    }
                }
                val attribute = ProfileAttribute(attrKey, attrType, attrValue)
                if (attrType == ProfileAttributeType.GEO) {
                    attribute.streetAddress = etAttrStreetAddress.text.toString()
                    attribute.streetAddress2 = etAttrStreetAddress2.text.toString()
                    attribute.postalCode = etAttrPostalCode.text.toString()
                    attribute.country = attrGeoCountry
                    attribute.state = etAttrState.text.toString()
                    attribute.city = etAttrCity.text.toString()
                    attribute.timezone = attrGeoTimezone
                }
                if (editProfileAttribute != null && editProfileAttribute?.key != attrKey) {
                    editProfileAttribute?.let {
                        presenter.deleteAttribute(it, attribute)
                    }
                } else {
                    saveEditAttribute(attribute, isEditAttribute = true)
                }
            }
        }
    }

    private fun saveEditAttribute(attribute: ProfileAttribute, isEditAttribute: Boolean) {
        if (editProfileAttribute != null && isEditAttribute) {
            presenter.updateAttribute(attribute)
        } else {
            presenter.saveAttribute(attribute)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val month = monthOfYear + 1
        val date = DateTimeUtils.formatDate(year, month, dayOfMonth)
        binding.etAttrDate.setText(date)
        binding.tilAttrDate.isErrorEnabled = false
    }

    override fun onTimeSet(view: TimePickerDialog?, hourOfDay: Int, minute: Int, second: Int) {
        val time = DateTimeUtils.formatTime(hourOfDay, minute, second)
        binding.etAttrTime.setText(time)
        binding.tilAttrTime.isErrorEnabled = false
    }

    override fun attributeIsDeleted(deletedAttribute: ProfileAttribute, newAttribute: ProfileAttribute) {
        saveEditAttribute(newAttribute, isEditAttribute = false)
    }

    override fun attributeIsSaved(attribute: ProfileAttribute) {
        val intent = Intent()
        intent.putExtra(Const.PROFILE_ATTRIBUTE, attribute)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        ScreenUtils.hideKeyboard(binding.btnAddAttr, this)
        super.onBackPressed()
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }
}