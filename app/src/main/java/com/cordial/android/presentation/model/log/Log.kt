package com.cordial.android.presentation.model.log

data class Log(val tag: String, val message: String, val timestamp: String)