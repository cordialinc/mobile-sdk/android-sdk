package com.cordial.android.presentation.customeventjson

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.cordial.android.R
import com.cordial.android.databinding.ActivityCustomEventJsonBinding
import com.cordial.android.presentation.model.JsonType
import com.cordial.android.presentation.model.property.CustomEvent
import com.cordial.android.util.Const
import com.cordial.android.util.JsonUtils
import com.cordial.feature.sendevent.model.EventRequestHelper
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class CustomEventJSONActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCustomEventJsonBinding

    private var customEvent: CustomEvent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getCustomEvent()
        binding = ActivityCustomEventJsonBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        setClickListeners()
        setCustomEvent()
    }

    private fun getCustomEvent() {
        intent.extras?.let { extras ->
            if (extras.containsKey(Const.CUSTOM_EVENT))
                customEvent = extras.getSerializable(Const.CUSTOM_EVENT) as CustomEvent?
        }
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.json)
    }

    private fun setClickListeners() {
        binding.btnImportJson.setOnClickListener {
            validateJson()
        }
    }

    private fun setCustomEvent() {
        customEvent?.let { event ->
            val properties = event.properties?.mapValues {
                PropertyValue.StringProperty(it.value)
            }
            val customEventJson = EventRequestHelper().getCustomEventJson(event.eventName, properties)
            val jsonArray = JSONArray(customEventJson)
            val formattedJson = jsonArray.toString(4)
            binding.etJson.setText(formattedJson)
        }
    }

    private fun validateJson() {
        val json = binding.etJson.text.toString().trim()
        try {
            when (JsonUtils.getJsonType(json)) {
                JsonType.ARRAY -> {
                    processJsonArray(JSONArray(json))
                }
                JsonType.OBJECT -> {
                    processJsonObject(JSONObject(json))
                }
                JsonType.NONE -> {
                    showErrorDialog()
                }
            }
        } catch (ex: JSONException) {
            showErrorDialog()
        }
    }

    private fun processJsonArray(jsonArray: JSONArray) {
        val eventNames = mutableListOf<String>()
        for (i in 0 until jsonArray.length()) {
            val jsonObject = jsonArray.getJSONObject(i)
            val eventName = jsonObject.getString("event")
            eventNames.add(eventName)
        }
        if (eventNames.size > 1) {
            showPickEventDialog(jsonArray, eventNames.toTypedArray())
        } else {
            processJsonObject(jsonArray.getJSONObject(0))
        }
    }

    private fun showPickEventDialog(jsonArray: JSONArray, eventNames: Array<String>) {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.choose_event))
            .setItems(eventNames) { dialog, which ->
                val jsonObject = jsonArray.getJSONObject(which)
                processJsonObject(jsonObject)
                dialog.dismiss()
            }
            .show()
    }

    private fun processJsonObject(jsonObject: JSONObject) {
        val eventName = jsonObject.getString("event")
        val properties = JsonUtils.getMapFromJson(jsonObject.optString("properties"))
        val customEvent = CustomEvent(eventName, properties)
        importEvent(customEvent)
    }

    private fun importEvent(customEvent: CustomEvent) {
        val intent = Intent()
        intent.putExtra(Const.CUSTOM_EVENT, customEvent)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun showErrorDialog() {
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.parse_error))
            .setMessage(R.string.json_invalid_error)
            .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }
}