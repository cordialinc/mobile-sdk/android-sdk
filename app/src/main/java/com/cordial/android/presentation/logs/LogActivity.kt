package com.cordial.android.presentation.logs

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityLogBinding
import com.cordial.android.presentation.model.log.Log
import com.cordial.android.util.JsonUtils
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.log.CordialLoggerListener
import javax.inject.Inject


class LogActivity : AppCompatActivity(), LogView {
    private lateinit var binding: ActivityLogBinding
    private lateinit var logAdapter: LogAdapter
    private var logs: ArrayList<Log> = arrayListOf()
    private var logListener: LogActivityListener? = null
    @Inject
    lateinit var presenter: LogPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        binding = ActivityLogBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        initLogRecyclerView()
        setLogListener()
    }

    private fun inject() {
        App.injector?.logComponent?.inject(this)
    }
    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.logs)
    }

    private fun initLogRecyclerView() {
        loadLogsFromPreferences()
        logAdapter = LogAdapter(logs, onClearAboveListener =  { position ->
          clearLogsAbovePosition(position)
        })
        with(binding) {
            rvLogs.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = logAdapter
            }
        }
    }

    private fun loadLogsFromPreferences() {
        val preferences = PreferenceHelper(this)
        val logsJson = preferences.getString(AppPreferenceKeys.LOGS, "")
        val logs = ArrayList(JsonUtils.getLogsFromJson(logsJson))
        presenter.storeLogs(logs)
    }

    private fun setLogListener() {
        // Clear previous Log Listeners and set LogActivityListener
        logListener = LogActivityListener()
        logListener?.let { listener ->
            CordialApiConfiguration.getInstance().setLoggers(listOf(listener))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_log, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.item_clear -> {
                clearLogs()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun clearLogs() {
        // Clear preferences
        val preferences = PreferenceHelper(this)
        preferences.remove(AppPreferenceKeys.LOGS)
        // Clear db
        presenter.clearLogs()
        // Clear adapter
        clearAdapter()
    }

    private fun clearAdapter() {
        val size = logs.size
        logs.clear()
        logAdapter.notifyItemRangeRemoved(0, size)
    }

    private fun clearLogsAbovePosition(position: Int) {
        for (i in 0 until position) {
            logs.remove(logs[0])
            presenter.deleteFirstLog()
        }
        logAdapter.notifyDataSetChanged()
        binding.rvLogs.scrollToPosition(0)
    }

    override fun logStored(log: Log) {
        logs.add(log)
        logAdapter.notifyItemInserted(logs.size - 1)
        binding.rvLogs.scrollToPosition(logs.size - 1)
    }

    override fun logsStored() {
        // Clear preferences
        val preferences = PreferenceHelper(this)
        preferences.remove(AppPreferenceKeys.LOGS)
        presenter.getAllLogs()
    }

    override fun showLogs(logs: List<Log>) {
        val logsPrevSize = logs.size
        logs.forEach {  log ->
            this.logs.add(log)
        }
        logAdapter.notifyItemRangeInserted(logsPrevSize, logs.size)
        binding.rvLogs.scrollToPosition(logs.size - 1)
    }

    override fun logDeleted() {
    }
    
    override fun logsDeleted() {
    }

    override fun showError(error: String?) {
        error?.let {
            android.util.Log.e(C.LOG_TAG, error)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        // Clear previous Log Listeners and set CordialDemoLogger
        val cordialDemoLogger = CordialDemoLogger(applicationContext)
        CordialApiConfiguration.getInstance().setLoggers(listOf(cordialDemoLogger))
    }

    inner class LogActivityListener: CordialLoggerListener {
        override fun info(message: String, timestamp: String, tag: String) {
            addLog(message, timestamp, tag)
        }

        override fun debug(message: String, timestamp: String, tag: String) {
            addLog(message, timestamp, tag)
        }

        override fun error(message: String, timestamp: String, tag: String) {
            addLog(message, timestamp, tag)
        }

        override fun warning(message: String, timestamp: String, tag: String) {
            addLog(message, timestamp, tag)
        }

        private fun addLog(message: String, timestamp: String, tag: String) {
            presenter.storeLog(Log(tag, message, timestamp))
        }
    }
}