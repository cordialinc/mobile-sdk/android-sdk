package com.cordial.android.presentation.cart

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.product.CartItemPresentationModel

interface CartView : BaseView {
    fun showCartProducts(cartItems: List<CartItemPresentationModel>, upsertContact: Boolean)
    fun productDeletedFromCart()
    fun cartIsCleared()
}