package com.cordial.android.presentation.updateattributes

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityUpdateAttributesBinding
import com.cordial.android.presentation.addattribute.AddAttributeActivity
import com.cordial.android.presentation.cart.SwipeToDeleteCallback
import com.cordial.android.presentation.custom.BetterActivityResult
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.model.attribute.ProfileAttributeType
import com.cordial.android.util.Const
import com.cordial.android.util.DateTimeUtils
import com.cordial.android.util.extension.splitIgnoreEmpty
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.CordialApi
import com.cordial.feature.upsertcontact.model.attributes.*
import javax.inject.Inject

class UpdateAttributesActivity : AppCompatActivity(), UpdateAttributesView {

    private lateinit var binding: ActivityUpdateAttributesBinding

    private lateinit var attrAdapter: AttributesAdapter

    @Inject
    protected lateinit var presenter: UpdateAttributesPresenter

    private val activityLauncher = BetterActivityResult.registerActivityForResult(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateAttributesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        fillViews()
        setClickListeners()
        initRecyclerView()
        getAllAttributes()
    }

    private fun inject() {
        App.injector?.profileAttributeComponent?.inject(this)
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.update_attributes)
    }

    private fun fillViews() {
        val preferenceHelper = PreferenceHelper(this)
        val primaryKey = preferenceHelper.getString(AppPreferenceKeys.PRIMARY_KEY)
        binding.tvPrimaryKey.text = primaryKey
    }

    private fun setClickListeners() {
        binding.ivAddAttr.setOnClickListener {
            addAttribute(null)
        }
        binding.btnUpdateProfile.setOnClickListener {
            updateAttributes()
        }
    }

    private fun addAttribute(attribute: ProfileAttribute?) {
        val intent = Intent(this, AddAttributeActivity::class.java)
        attribute?.let {
            intent.putExtra(Const.PROFILE_ATTRIBUTE, it)
        }
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
            override fun onActivityResult(result: ActivityResult) {
                handleActivityResult(result, requestCode = Const.ADD_EDIT_ATTRIBUTE)
            }
        })
    }

    private fun initRecyclerView() {
        attrAdapter = AttributesAdapter(null) { position ->
            editAttribute(position)
        }
        binding.rvAttrs.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = attrAdapter
            itemAnimator?.changeDuration = 0
            addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
        }
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(this) { position ->
            onDeleteItem(position)
        })
        itemTouchHelper.attachToRecyclerView(binding.rvAttrs)
    }

    private fun editAttribute(position: Int) {
        val attribute = attrAdapter.getItem(position)
        addAttribute(attribute)
    }

    private fun getAllAttributes() {
        presenter.getAllAttributes()
    }

    private fun updateAttributes() {
        val attrs = HashMap<String, AttributeValue>()
        attrAdapter.itemsList?.forEach { attr ->
            val value = when (attr.type) {
                ProfileAttributeType.STRING -> StringValue(attr.value)
                ProfileAttributeType.NUMERIC -> {
                    val number = attr.value?.toDoubleOrNull()
                    NumericValue(number)
                }
                ProfileAttributeType.BOOLEAN -> {
                    val boolean = attr.value.toBoolean()
                    BooleanValue(boolean)
                }
                ProfileAttributeType.ARRAY -> {
                    val array = attr.value?.splitIgnoreEmpty(",")?.map { it.trim() }?.toTypedArray()
                    if (array == null) StringValue(array) else ArrayValue(array)
                }
                ProfileAttributeType.DATE -> {
                    val value = attr.value
                    val date = if (value != null)  DateTimeUtils.getDate(value) else null
                    DateValue(date)
                }
                ProfileAttributeType.GEO -> {
                    val geoValue = GeoValue()
                    geoValue.streetAddress = attr.streetAddress
                    geoValue.streetAddress2 = attr.streetAddress2
                    geoValue.postalCode = attr.postalCode
                    geoValue.country = attr.country
                    geoValue.state = attr.state
                    geoValue.city = attr.city
                    geoValue.timezone = attr.timezone
                    geoValue
                }
            }
            attrs[attr.key] = value
        }
        CordialApi().upsertContact(attrs)
        Toast.makeText(this, R.string.update_attributes_success, Toast.LENGTH_SHORT).show()
    }

    private fun onDeleteItem(position: Int) {
        presenter.deleteAttribute(attrAdapter.getItem(position))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun showAllAttributes(attributes: List<ProfileAttribute>) {
        attrAdapter.updateItems(attributes.toMutableList(), true)
    }

    override fun attributeIsDeleted(attribute: ProfileAttribute) {
        val itemPosition = attrAdapter.getItemPosition(attribute)
        attrAdapter.deleteItem(itemPosition)
    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun handleActivityResult(result: ActivityResult?, requestCode: Int) {
        if (result?.resultCode == Activity.RESULT_OK) {
            if (requestCode == Const.ADD_EDIT_ATTRIBUTE) {
                presenter.getAllAttributes()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.injector?.recycleProfileAttributeComponent()
    }
}