package com.cordial.android.presentation.cart

import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.presentation.model.PresentationMapper
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class CartPresenter(
    private val cartUseCase: CartUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<CartView>() {

    private val disposable = CompositeDisposable()

    fun getAllCartProducts(upsertContact: Boolean) {
        cartUseCase.getAllCartProducts().observeOn(observeOnSchedulers)
            .map {
                PresentationMapper.cartItemsToDomainModel(it)
            }
            .subscribe({
                view?.showCartProducts(it, upsertContact)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }


    fun deleteProductFromCart(cartItem: CartItemPresentationModel) {
        cartUseCase.deleteProductFromCart(cartItem)
            .observeOn(observeOnSchedulers)
            .subscribe({
                view?.productDeletedFromCart()
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    fun clearCart() {
        cartUseCase.clearCart()
            .observeOn(observeOnSchedulers)
            .subscribe({
                view?.cartIsCleared()
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    override fun detachView(view: CartView) {
        super.detachView(view)
        disposable.clear()
    }
}