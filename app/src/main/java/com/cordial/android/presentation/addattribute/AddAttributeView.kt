package com.cordial.android.presentation.addattribute

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.attribute.ProfileAttribute

interface AddAttributeView : BaseView {
    fun attributeIsSaved(attribute: ProfileAttribute)
    fun attributeIsDeleted(deletedAttribute: ProfileAttribute, newAttribute: ProfileAttribute)
}