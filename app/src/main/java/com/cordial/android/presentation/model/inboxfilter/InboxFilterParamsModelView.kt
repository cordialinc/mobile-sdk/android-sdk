package com.cordial.android.presentation.model.inboxfilter

import java.io.Serializable
import java.util.*

class InboxFilterParamsModelView(
    val isRead: Boolean? = null,
    val beforeDate: Date? = null,
    val afterDate: Date? = null,
) : Serializable