package com.cordial.android.presentation.auth

import io.reactivex.Completable

interface AuthUseCase {
    fun clearUserCart(): Completable
    fun clearProfileAttributes(): Completable
}