package com.cordial.android.presentation.main

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.cordial.android.R
import com.cordial.android.databinding.ActivityMainBinding
import com.cordial.android.presentation.auth.AuthActivity
import com.cordial.android.presentation.cart.CartActivity
import com.cordial.android.presentation.catalogcategory.ProductActivity
import com.cordial.android.presentation.inbox.InboxActivity
import com.cordial.android.presentation.logs.LogActivity
import com.cordial.android.presentation.model.product.ProductPresentationModel
import com.cordial.android.presentation.sendcustomevent.SendCustomEventActivity
import com.cordial.android.presentation.updateattributes.UpdateAttributesActivity
import com.cordial.android.util.Const
import com.cordial.android.util.ProductUtil
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.CordialApi
import com.cordial.api.NotificationSettings
import com.cordial.feature.notification.model.NotificationPermissionEducationalUISettings
import com.cordial.feature.notification.model.NotificationPermissionEducationalUiColor
import com.cordial.feature.sendevent.model.property.PropertyValue

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var catalogAdapter: CatalogAdapter
    private var productList = mutableListOf<ProductPresentationModel>()

    private var path: Uri? = null
    private var fallBackPath: Uri? = null
    private var isLoggedIn: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        fillData()
        initCatalogList()
        handleIntent()
    }

    private fun handleIntent() {
        if (intent.hasExtra(Const.PRODUCT_PATH)) {
            path = Uri.parse(intent.getStringExtra(Const.PRODUCT_PATH))
            fallBackPath = Uri.parse(intent.getStringExtra(Const.FALLBACK_PRODUCT_PATH) ?: "")
            var isAnyPathProcessed = false
            productList.forEach { product ->
                if (product.path.contains(path?.lastPathSegment)) {
                    openProduct(product)
                    overridePendingTransition(0, 0)
                    isAnyPathProcessed = true
                    return
                }
            }
            productList.forEach { product ->
                if (product.path.contains(fallBackPath?.lastPathSegment)) {
                    openProduct(product)
                    overridePendingTransition(0, 0)
                    isAnyPathProcessed = true
                    return@forEach
                }

            }
            if (!isAnyPathProcessed) {
                openDeepLinkUrlInBrowser()
            }
        }
        if (intent.hasExtra(Const.IS_LOGGED_IN)) {
            isLoggedIn = intent.getBooleanExtra(Const.IS_LOGGED_IN, false)
            if (isLoggedIn) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    val educationalUiLayoutId = null
                    val educationalUiSettings = NotificationPermissionEducationalUISettings(
                        educationalUiLayoutId,
                        NotificationPermissionEducationalUiColor(
                            R.color.colorPrimary,
                            R.color.colorOrange,
                            R.color.colorGrey
                        ),
                        showUntilAllowed = true
                    )
                    CordialApi().requestNotificationPermission(educationalUiSettings)
                }
            }
        }
    }

    private fun openDeepLinkUrlInBrowser() {
        path?.let { path ->
            val intent = Intent(Intent.ACTION_VIEW, path)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
            intent.data = if (path.host == Const.CORDIAL_DEMO_DEEP_LINK_HOST)
                Uri.parse("${path.scheme}://${path.host}")
            else path
            try {
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                openFallBackDeepLinkUrlInBrowser(intent)
            }
        }
    }

    private fun openFallBackDeepLinkUrlInBrowser(intent: Intent) {
        fallBackPath?.let { fallbackPath ->
            intent.data = if (fallbackPath.host == Const.CORDIAL_DEMO_DEEP_LINK_HOST)
                Uri.parse("${fallbackPath.scheme}://${fallbackPath.host}")
            else Uri.parse("$fallbackPath")
            try {
                startActivity(intent)
            } catch (ex: ActivityNotFoundException) {
                ex.printStackTrace()
            }
        }
    }

    private fun configureActionBar() {
        supportActionBar?.title = resources.getString(R.string.mens)
    }

    private fun fillData() {
        val products = ProductUtil.getCatalogProducts()
        productList.addAll(products)
    }

    private fun initCatalogList() {
        with(binding) {
            catalogAdapter = CatalogAdapter(productList) { position ->
                onProductClicked(position)
            }
            rvCatalog.layoutManager = GridLayoutManager(this@MainActivity, 2)
            rvCatalog.adapter = catalogAdapter
            rvCatalog.setHasFixedSize(true)
        }
    }

    override fun onStart() {
        super.onStart()
        if (path != null) {
            path = null
            return
        }
        sendEventBrowseCategory()
    }

    private fun sendEventBrowseCategory() {
        val eventName = "browse_category"
        val properties = mapOf("catalogName" to PropertyValue.StringProperty("Mens"))
        CordialApi().sendEvent(eventName, properties)
    }

    private fun onProductClicked(position: Int) {
        openProduct(productList[position])
    }

    private fun openProduct(product: ProductPresentationModel) {
        val intent = Intent(this@MainActivity, ProductActivity::class.java)
        intent.putExtra(Const.PRODUCT, product)
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_cart -> {
                openCartActivity()
                true
            }
            R.id.item_logout -> {
                logout()
                true
            }
            R.id.item_login -> {
                openAuthScreen()
                true
            }
            R.id.item_update_profile -> {
                openUpdateProfileScreen()
                true
            }
            R.id.item_send_custom_event -> {
                openSendCustomEventScreen()
                true
            }
            R.id.inbox -> {
                openInboxActivity()
                true
            }
            R.id.notification_settings -> {
                NotificationSettings().openNotificationSettings()
                true
            }
            R.id.logs -> {
                openLogActivity()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openCartActivity() {
        val intent = Intent(this@MainActivity, CartActivity::class.java)
        startActivity(intent)
    }

    private fun logout() {
        CordialApi().unsetContact()
        PreferenceHelper(this).put(AppPreferenceKeys.IS_LOGGED_IN, false)
        openAuthScreen()
    }

    private fun openAuthScreen() {
        val intent = Intent(this@MainActivity, AuthActivity::class.java)
        intent.putExtra(Const.CHECK_ON_AUTH_STATE, false)
        startActivity(intent)
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit)
        finish()
    }

    private fun openUpdateProfileScreen() {
        val intent = Intent(this@MainActivity, UpdateAttributesActivity::class.java)
        startActivity(intent)
    }

    private fun openSendCustomEventScreen() {
        val intent = Intent(this@MainActivity, SendCustomEventActivity::class.java)
        startActivity(intent)
    }

    private fun openInboxActivity() {
        val intent = Intent(this@MainActivity, InboxActivity::class.java)
        startActivity(intent)
    }

    private fun openLogActivity() {
        val intent = Intent(this@MainActivity, LogActivity::class.java)
        startActivity(intent)
    }
}

