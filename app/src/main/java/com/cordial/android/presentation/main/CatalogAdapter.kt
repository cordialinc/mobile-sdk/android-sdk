package com.cordial.android.presentation.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.databinding.ItemCatalogBinding
import com.cordial.android.presentation.model.product.ProductPresentationModel

class CatalogAdapter(
    private val items: List<ProductPresentationModel>,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<CatalogAdapter.CatalogViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogViewHolder {
        val binding = ItemCatalogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CatalogViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: CatalogViewHolder, position: Int) {
        holder.bind(items[position])
    }

    inner class CatalogViewHolder(private val binding: ItemCatalogBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(product: ProductPresentationModel) {
            itemView.setOnClickListener {
                clickListener.invoke(bindingAdapterPosition)
            }

            with(binding) {
                ivCatalogItemImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, product.img))
                //ivCatalogItemImage.setBackgroundResource(product.img)
                tvCatalogItemName.text = product.name
                tvCatalogItemBrand.text = product.brand
                val productPrice = "$ ${product.price}"
                tvCatalogItemPrice.text = productPrice
            }
        }
    }
}