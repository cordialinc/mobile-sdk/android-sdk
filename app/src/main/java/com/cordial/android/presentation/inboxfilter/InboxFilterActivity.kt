package com.cordial.android.presentation.inboxfilter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.cordial.android.R
import com.cordial.android.databinding.ActivityInboxFilterBinding
import com.cordial.android.presentation.model.inboxfilter.InboxFilterParamsModelView
import com.cordial.android.util.Const
import com.cordial.android.util.DateTimeUtils
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import java.util.*

class InboxFilterActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInboxFilterBinding

    private var inboxFilterParamsModelView: InboxFilterParamsModelView? = null
    private var isRead: Boolean? = null
    private var beforeDate: Date? = null
    private var afterDate: Date? = null
    private val readStatuses = arrayOf(
        "",
        "true",
        "false"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initInboxFilterParamsModelView()
        binding = ActivityInboxFilterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        setClickListeners()
        fillViews()
        configureDateInputs()
    }

    private fun initInboxFilterParamsModelView() {
        inboxFilterParamsModelView =
            intent.getSerializableExtra(Const.INBOX_FILTER_PARAMS_MODEL_VIEW) as InboxFilterParamsModelView?
    }

    private fun configureActionBar() {
        supportActionBar?.title = resources.getString(R.string.inbox_filter)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun fillViews() {
        inboxFilterParamsModelView?.let { params ->
            params.isRead?.let { isRead ->
                this.isRead = isRead
                setSpinnerSelection()
            }
            params.beforeDate?.let { beforeDate ->
                val timestamp = DateTimeUtils.getTimestamp(beforeDate)
                binding.etDateBefore.setText(timestamp)
                this.beforeDate = DateTimeUtils.getDate(timestamp)
            }
            params.afterDate?.let { afterDate ->
                val timestamp = DateTimeUtils.getTimestamp(afterDate)
                binding.etDateAfter.setText(timestamp)
                this.afterDate = DateTimeUtils.getDate(timestamp)
            }
        }
    }

    private fun setClickListeners() {
        binding.btnApply.setOnClickListener {
            onApplyFilter()
        }
        initSpinner(binding.spinnerRead, readStatuses) { position ->
            selectReadStatus(position)
        }
    }

    private fun initSpinner(
        spinner: Spinner,
        objects: Array<String>,
        onItemSelectedListener: ((position: Int) -> Unit)?
    ) {
        val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, objects)
        spinner.adapter = arrayAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                onItemSelectedListener?.invoke(position)
            }
        }
    }

    private fun selectReadStatus(position: Int) {
        when (position) {
            0 -> {
                isRead = null
            }
            1 -> {
                isRead = true
            }
            2 -> {
                isRead = false
            }
        }
    }

    private fun setSpinnerSelection() {
        with(binding) {
            when (isRead) {
                null -> {
                    spinnerRead.setSelection(0)
                }
                true -> {
                    spinnerRead.setSelection(1)
                }
                false -> {
                    spinnerRead.setSelection(2)
                }
            }
        }
    }

    private fun configureDateInputs() {
        with(binding) {
            etDateBefore.inputType = InputType.TYPE_NULL
            etDateBefore.setOnClickListener {
                showBeforeDatePickerDialog()
            }
            etDateBefore.setOnFocusChangeListener { _, isFocused ->
                if (isFocused) showBeforeDatePickerDialog()
            }
            etDateAfter.inputType = InputType.TYPE_NULL
            etDateAfter.setOnClickListener {
                showAfterDatePickerDialog()
            }
            etDateAfter.setOnFocusChangeListener { _, isFocused ->
                if (isFocused) showAfterDatePickerDialog()
            }
        }
    }

    private fun showBeforeDatePickerDialog() {
        with(binding) {
            showDateTimePickerDialog(etDateBefore.text.toString()) { dateTime ->
                etDateBefore.setText(dateTime)
                beforeDate = DateTimeUtils.getDate(etDateBefore.text.toString())
            }

        }
    }

    private fun showAfterDatePickerDialog() {
        with(binding) {
            showDateTimePickerDialog(etDateAfter.text.toString()) { dateTime ->
                etDateAfter.setText(dateTime)
                afterDate = DateTimeUtils.getDate(etDateAfter.text.toString())
            }
        }
    }

    private fun showDateTimePickerDialog(dateTime: String, onDatePickListener: (dateTime: String?) -> Unit) {
        val now: Calendar = Calendar.getInstance()
        if (dateTime.isNotEmpty()) {
            val date = DateTimeUtils.parseDateNumbers(dateTime)
            now.set(date.first, date.second - 1, date.third)
        }
        val dpd = DatePickerDialog.newInstance(
            { _, year, monthOfYear, dayOfMonth ->
                val month = monthOfYear + 1
                val date = DateTimeUtils.formatDate(year, month, dayOfMonth)
                showTimePickerDialog(dateTime) { time ->
                    onDatePickListener.invoke("$date $time")
                }
            },
            now.get(Calendar.YEAR),
            now.get(Calendar.MONTH),
            now.get(Calendar.DAY_OF_MONTH)
        )

        dpd.accentColor = ContextCompat.getColor(this, R.color.colorOrange)
        dpd.setOkColor(ContextCompat.getColor(this, R.color.colorOrange))
        dpd.setCancelColor(ContextCompat.getColor(this, R.color.colorOrange))
        dpd.show(supportFragmentManager, "DatePickerDialog")
    }

    private fun showTimePickerDialog(dateTime: String, onTimePickListener: (dateTime: String?) -> Unit) {
        val now: Calendar = Calendar.getInstance()
        if (dateTime.isNotEmpty()) {
            val parsedTime = DateTimeUtils.parseTime(dateTime)
            val time = DateTimeUtils.parseTimeNumbers(parsedTime)
            now.set(0, 0, 0, time.first, time.second, time.third)
        }
        val tpd = TimePickerDialog.newInstance(
            { _, hourOfDay, minute, second ->
                val time = DateTimeUtils.formatTime(hourOfDay, minute, second)
                onTimePickListener.invoke(time)
            },
            now.get(Calendar.HOUR_OF_DAY),
            now.get(Calendar.MINUTE),
            now.get(Calendar.SECOND),
            true
        )
        tpd.enableSeconds(true)
        tpd.accentColor = ContextCompat.getColor(this, R.color.colorOrange)
        tpd.setOkColor(ContextCompat.getColor(this, R.color.colorOrange))
        tpd.setCancelColor(ContextCompat.getColor(this, R.color.colorOrange))
        tpd.show(supportFragmentManager, "TimePickerDialog")
    }

    private fun onApplyFilter() {
        inboxFilterParamsModelView = InboxFilterParamsModelView(isRead, beforeDate, afterDate)
        val intent = Intent()
        intent.putExtra(Const.INBOX_FILTER_PARAMS_MODEL_VIEW, inboxFilterParamsModelView)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_inbox_filter, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.menu_filter_clear -> {
                clearFilter()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun clearFilter() {
        with(binding) {
            isRead = null
            beforeDate = null
            afterDate = null
            spinnerRead.setSelection(0)
            etDateBefore.setText("")
            etDateAfter.setText("")
        }
    }
}