package com.cordial.android.presentation.cart

import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.R
import com.cordial.android.databinding.FooterCartBinding
import com.cordial.android.presentation.model.product.CartItemPresentationModel

class CartFooter(private val binding: FooterCartBinding, private val onCheckoutClickListener: () -> Unit) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(cartItems: MutableList<CartItemPresentationModel>?) {
        var totalQuantity = 0
        var totalPrice = 0.0
        cartItems?.forEach {
            totalQuantity += it.quantity
            totalPrice += (it.quantity * it.product.price)
        }
        val totalQuantityText = itemView.resources.getString(R.string.total_quantity) + " : $totalQuantity"
        with(binding) {
            tvTotalQuantity.text = totalQuantityText
            val totalPriceText = itemView.resources.getString(R.string.total_price) + " : $totalPrice"
            tvTotalPrice.text = totalPriceText
            btnCheckout.setOnClickListener {
                onCheckoutClickListener.invoke()
            }
        }
    }
}