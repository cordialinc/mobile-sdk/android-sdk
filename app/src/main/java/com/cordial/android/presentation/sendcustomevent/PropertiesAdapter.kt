package com.cordial.android.presentation.sendcustomevent

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.databinding.ItemPropertyBinding
import com.cordial.android.presentation.custom.HeaderFooterAdapter
import com.cordial.android.presentation.model.property.CustomEventProperty

class PropertiesAdapter(items: List<CustomEventProperty>?) :
    HeaderFooterAdapter<RecyclerView.ViewHolder, Any?, CustomEventProperty, Any?>(
        null,
        items,
        null
    ) {

    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemPropertyBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PropertyHolder(binding)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as PropertyHolder).bind(getItem(position))
    }

    inner class PropertyHolder(private val binding: ItemPropertyBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(property: CustomEventProperty) {
            binding.tvPropertyKey.text = property.key
            binding.tvPropertyValue.text = property.value
        }
    }
}