package com.cordial.android.presentation.inbox

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.R
import com.cordial.android.databinding.ActivityInboxBinding
import com.cordial.android.presentation.cart.SwipeToDeleteCallback
import com.cordial.android.presentation.custom.BetterActivityResult
import com.cordial.android.presentation.custom.RVScrollLoadHelper
import com.cordial.android.presentation.inboxfilter.InboxFilterActivity
import com.cordial.android.presentation.inboxmessage.InboxMessageActivity
import com.cordial.android.presentation.model.JsonType
import com.cordial.android.presentation.model.inboxfilter.InboxFilterParamsModelView
import com.cordial.android.util.Const
import com.cordial.android.util.JsonUtils
import com.cordial.api.C
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.api.CordialInboxMessageApi
import com.cordial.feature.inboxmessage.CordialInboxMessageListener
import com.cordial.feature.inboxmessage.InboxMessage
import com.cordial.feature.inboxmessage.getinboxmessages.model.InboxFilterParams
import com.cordial.network.request.Page
import com.cordial.network.request.PageRequest
import org.json.JSONObject

class InboxActivity : AppCompatActivity(), RVScrollLoadHelper.RVCloseToEndListener {

    private lateinit var binding: ActivityInboxBinding

    private lateinit var inboxMessageAdapter: InboxMessageAdapter
    private lateinit var scrollHelper: RVScrollLoadHelper

    private var cordialInboxMessageApi: CordialInboxMessageApi? = null
        get() {
            if (field == null) {
                field = CordialInboxMessageApi()
            }
            return field
        }

    private var dataCurrentPage = 1
    private var dataLastPage = 2
    private var perPage = 10
    private lateinit var pageRequest: PageRequest
    private var inboxFilterParamsModelView: InboxFilterParamsModelView? = null
    private var inboxView = InboxView.LIST
    private var toast: Toast? = null

    private val activityLauncher = BetterActivityResult.registerActivityForResult(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInboxBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        setClickListeners()
        initInboxMessagesList()
        initPageRequest()
        fetchAllInboxMessages(pageRequest)
        initInboxMessagesListener()
    }

    private fun configureActionBar() {
        supportActionBar?.title = resources.getString(R.string.inbox)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setClickListeners() {
        binding.btnRefreshInbox.setOnClickListener {
            initPageRequest()
            fetchAllInboxMessages(pageRequest)
        }
    }

    private fun initInboxMessagesList() {
        inboxMessageAdapter = InboxMessageAdapter(null, inboxView, onItemClickListener = { position ->
            onInboxMessageClick(position)
        }, onDeleteItemListener = { inboxMessage ->
            onDeleteInboxMessage(inboxMessage)
        })
        with(binding) {
            rvInboxMessages.apply {
                layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = inboxMessageAdapter
            }
            scrollHelper = RVScrollLoadHelper(this@InboxActivity, visibleThreshold = 4)
            scrollHelper.addRecyclerViews(rvInboxMessages)
            val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(this@InboxActivity) { position ->
                inboxMessageAdapter.deleteInboxMessage(position)
            })
            itemTouchHelper.attachToRecyclerView(rvInboxMessages)
        }
    }

    private fun initPageRequest() {
        dataCurrentPage = 1
        dataLastPage = 2
        pageRequest = PageRequest(dataCurrentPage, perPage)
    }

    private fun fetchAllInboxMessages(pageRequest: PageRequest) {
        if (pageRequest.page == 1) inboxMessageAdapter.updateItems(null, true)
        setDataLoading(true, dataCurrentPage < dataLastPage)
        val inboxFilterParams = InboxFilterParams(
            inboxFilterParamsModelView?.isRead,
            inboxFilterParamsModelView?.beforeDate,
            inboxFilterParamsModelView?.afterDate
        )
        cordialInboxMessageApi?.fetchInboxMessages(
            pageRequest,
            inboxFilterParams,
            onSuccess = { inboxMessages ->
                onFetchInboxMessages(inboxMessages)
            },
            onFailure = { error ->
                onFetchInboxMessagesError(error)
            })
    }

    private fun initInboxMessagesListener() {
        CordialApiConfiguration.getInstance().inboxMessageListener = object : CordialInboxMessageListener {
            override fun newInboxMessageDelivered(mcID: String) {
                cordialInboxMessageApi?.fetchInboxMessage(mcID, onSuccess = { inboxMessage ->
                    val inboxMessageModelView = InboxMessageMapper.inboxMessageToModelView(inboxMessage)
                    inboxMessageAdapter.updateItems(
                        mutableListOf(inboxMessageModelView),
                        removeOld = false,
                        isHead = true
                    )
                    binding.rvInboxMessages.smoothScrollToPosition(0)
                }, onFailure = {
                    Log.d(C.LOG_TAG, "Getting inbox message with $mcID failed on: $it")
                })
            }
        }
    }

    private fun onFetchInboxMessages(inboxMessages: Page<InboxMessage>) {
        dataCurrentPage = inboxMessages.current
        dataLastPage = inboxMessages.last
        setDataLoading(false, dataCurrentPage < dataLastPage)
        val inboxMessageList = InboxMessageMapper.inboxMessagesToModelView(inboxMessages.content)
        inboxMessageAdapter.updateItems(inboxMessageList.toMutableList(), dataCurrentPage == 1)
        if (inboxMessageList.isEmpty()) {
            Log.d(C.LOG_TAG, "Inbox is empty")
            return
        }
    }

    private fun onFetchInboxMessagesError(error: String) {
        if (error != "The jwt token has expired") {
            setDataLoading(false, dataCurrentPage < dataLastPage)
        }
        Log.d(C.LOG_TAG, "Get inbox messages error: $error")
        Toast.makeText(this, "Error: $error", Toast.LENGTH_SHORT).show()
    }

    private fun onInboxMessageClick(position: Int) {
        if (inboxView == InboxView.LIST) {
            openInboxMessageActivity(position)
        } else {
            val inboxMessageModelView = inboxMessageAdapter.getItem(position)
            inboxMessageModelView.previewData?.let { metadata ->
                if (JsonUtils.getJsonType(metadata) == JsonType.OBJECT) {
                    val jsonObject = JSONObject(metadata)
                    val deepLink = jsonObject.optString(Const.INBOX_MESSAGE_METADATA_DEEP_LINK)
                    if (deepLink.isEmpty()) {
                        showDeepLinkError()
                        return
                    }
                    CordialApi().openDeepLink(deepLink)
                } else {
                    showDeepLinkError()
                }
            }
        }
    }

    private fun showDeepLinkError() {
        toast?.cancel()
        toast = Toast.makeText(this, Const.INBOX_MESSAGE_METADATA_DEEP_LINK_ERROR, Toast.LENGTH_SHORT)
        toast?.show()
    }

    private fun openInboxMessageActivity(position: Int) {
        val intent = Intent(this@InboxActivity, InboxMessageActivity::class.java)
        val inboxMessageModelView = inboxMessageAdapter.getItem(position)
        intent.putExtra(Const.INBOX_MESSAGE_MODEL_VIEW, inboxMessageModelView)
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
            override fun onActivityResult(result: ActivityResult) {
                handleActivityResult(result, requestCode = Const.INBOX_MESSAGE_DETAILS)
            }
        })
    }

    private fun onDeleteInboxMessage(inboxMessage: InboxMessageModelView?) {
        inboxMessage?.let { message ->
            cordialInboxMessageApi?.deleteInboxMessage(message.mcID)
        }
    }

    private fun setDataLoading(isLoading: Boolean, hasMoreData: Boolean) {
        scrollHelper.setIsLoadingState(binding.rvInboxMessages, isLoading)
        scrollHelper.setHasMoreDataState(binding.rvInboxMessages, hasMoreData)
        inboxMessageAdapter.configureFooter(isLoading)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_inbox, menu)
        menu?.let {
            val menuFilter = menu.findItem(R.id.menu_filter)
            menuFilter.actionView?.setOnClickListener {
                openInboxFilterActivity()
            }
        }
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        val inboxViewIcon = if (inboxView == InboxView.LIST)
            ContextCompat.getDrawable(this, R.drawable.ic_card_view)
        else
            ContextCompat.getDrawable(this, R.drawable.ic_list_view)
        menu?.getItem(0)?.icon = inboxViewIcon
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.menu_filter -> {
                openInboxFilterActivity()
                true
            }
            R.id.menu_inbox_view -> {
                changeInboxView()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openInboxFilterActivity() {
        val intent = Intent(this, InboxFilterActivity::class.java)
        inboxFilterParamsModelView?.let { params ->
            intent.putExtra(Const.INBOX_FILTER_PARAMS_MODEL_VIEW, params)
        }
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
            override fun onActivityResult(result: ActivityResult) {
                handleActivityResult(result, requestCode = Const.INBOX_FILTER)
            }
        })
    }

    private fun changeInboxView() {
        inboxView = if (inboxView == InboxView.LIST) InboxView.CARD else InboxView.LIST
        invalidateOptionsMenu()
        inboxMessageAdapter.changeInboxView(inboxView)
        inboxMessageAdapter.notifyDataSetChanged()
    }

    override fun onRecyclerViewScrollCloseToEnd(recyclerView: RecyclerView) {
        fetchAllInboxMessages(pageRequest.next())
    }

    private fun handleActivityResult(result: ActivityResult?, requestCode: Int) {
        if (result?.resultCode == Activity.RESULT_OK) {
            if (requestCode == Const.INBOX_MESSAGE_DETAILS) {
                result.data?.extras?.let { extras ->
                    try {
                        val inboxMessage =
                            extras.getSerializable(Const.INBOX_MESSAGE_MODEL_VIEW) as InboxMessageModelView?
                        inboxMessage?.let {
                            try {
                                val inboxMessagePosition = findInboxMessagePositionByMCID(inboxMessage.mcID)
                                inboxMessageAdapter.getItem(inboxMessagePosition).isRead = inboxMessage.isRead
                                inboxMessageAdapter.notifyItemChanged(inboxMessagePosition)
                            } catch (e: java.lang.IndexOutOfBoundsException) {
                                e.printStackTrace()
                            }
                        }
                    } catch (e: IndexOutOfBoundsException) {
                        e.printStackTrace()
                    }
                }
            } else if (requestCode == Const.INBOX_FILTER) {
                result.data?.extras?.let { extras ->
                    inboxFilterParamsModelView =
                        extras.getSerializable(Const.INBOX_FILTER_PARAMS_MODEL_VIEW) as InboxFilterParamsModelView?
                    initPageRequest()
                    fetchAllInboxMessages(pageRequest)
                }
            }
        }
    }

    private fun findInboxMessagePositionByMCID(mcID: String): Int {
        inboxMessageAdapter.itemsList?.forEachIndexed { index, inboxMessage ->
            if (inboxMessage.mcID == mcID) {
                return index
            }
        }
        return -1
    }

    override fun onDestroy() {
        removeInboxMessageListener()
        super.onDestroy()
    }

    private fun removeInboxMessageListener() {
        CordialApiConfiguration.getInstance().inboxMessageListener = null
    }
}