package com.cordial.android.presentation.model.product

import java.util.*

class CartItemPresentationModel(
    val product: ProductPresentationModel,
    val productSku: String,
    val quantity: Int,
    val timestamp: Date?
)