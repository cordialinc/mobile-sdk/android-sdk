package com.cordial.android.presentation.sendcustomevent

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.result.ActivityResult
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.R
import com.cordial.android.databinding.ActivitySendCustomEventBinding
import com.cordial.android.presentation.addproperty.AddPropertyActivity
import com.cordial.android.presentation.cart.SwipeToDeleteCallback
import com.cordial.android.presentation.custom.BetterActivityResult
import com.cordial.android.presentation.customeventjson.CustomEventJSONActivity
import com.cordial.android.presentation.model.property.CustomEvent
import com.cordial.android.presentation.model.property.CustomEventProperty
import com.cordial.android.util.Const
import com.cordial.api.CordialApi
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.google.android.material.dialog.MaterialAlertDialogBuilder


class SendCustomEventActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySendCustomEventBinding

    private lateinit var propertiesAdapter: PropertiesAdapter

    private val activityLauncher = BetterActivityResult.registerActivityForResult(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySendCustomEventBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configureActionBar()
        setClickListeners()
        initRecyclerView()
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.send_custom_event)
    }

    private fun setClickListeners() {
        binding.ivAddProperty.setOnClickListener {
            addProperty()
        }

        binding.btnSendCustomEvent.setOnClickListener {
            sendCustomEvent()
        }
    }

    private fun addProperty() {
        val intent = Intent(this@SendCustomEventActivity, AddPropertyActivity::class.java)
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
            override fun onActivityResult(result: ActivityResult) {
                handleActivityResult(result, requestCode = Const.ADD_CUSTOM_EVENT_PROPERTY)
            }
        })
    }

    private fun handleActivityResult(result: ActivityResult?, requestCode: Int) {
        if (result?.resultCode == Activity.RESULT_OK) {
            if (requestCode == Const.ADD_CUSTOM_EVENT_PROPERTY) {
                result.data?.extras?.let { extras ->
                    val property = extras.getSerializable(Const.CUSTOM_EVENT_PROPERTY) as CustomEventProperty
                    checkOnExistingProperty(property)
                    propertiesAdapter.updateItems(mutableListOf(property), removeOld = false)
                }
            } else if (requestCode == Const.CUSTOM_EVENT_JSON) {
                result.data?.extras?.let { extras ->
                    val customEvent = extras.getSerializable(Const.CUSTOM_EVENT) as CustomEvent
                    setCustomEvent(customEvent)
                }
            }
        }
    }

    private fun initRecyclerView() {
        propertiesAdapter = PropertiesAdapter(null)
        binding.rvProperties.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = propertiesAdapter
            itemAnimator?.changeDuration = 0
        }
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(this) { position ->
            onDeleteItem(position)
        })
        itemTouchHelper.attachToRecyclerView(binding.rvProperties)
    }

    private fun onDeleteItem(position: Int) {
        propertiesAdapter.deleteItem(position)
    }

    private fun sendCustomEvent() {
        if (validateEventName()) {
            val eventName = binding.etEventName.text.toString().trim()
            val properties = getProperties()
            val cordialApi = CordialApi()
            cordialApi.sendEvent(eventName, properties)
            if (binding.cbFlushEvents.isChecked) {
                cordialApi.flushEvents()
            }
            showDialog()
        }
    }

    private fun showDialog() {
        val message = if (binding.cbFlushEvents.isChecked) resources.getString(R.string.custom_event_sent_and_flushed)
        else resources.getString(R.string.custom_event_sent)
        MaterialAlertDialogBuilder(this)
            .setTitle(resources.getString(R.string.custom_event))
            .setMessage(message)
            .setPositiveButton(resources.getString(R.string.ok)) { dialog, _ ->
                dialog.dismiss()
            }
            .show()
    }

    private fun validateEventName(): Boolean {
        with(binding) {
            val key = etEventName.text.toString().trim()
            return if (key.length in 1..Const.EMAIL_MAX_SYMBOLS) {
                tilEventName.isErrorEnabled = false
                true
            } else {
                tilEventName.error = resources.getString(R.string.event_name_error)
                tilEventName.isErrorEnabled = true
                false
            }
        }
    }

    private fun getProperties(): Map<String, PropertyValue>? {
        val properties = mutableMapOf<String, PropertyValue>()
        propertiesAdapter.itemsList?.forEach { property ->
            properties[property.key] = PropertyValue.StringProperty(property.value)
        }
        return if (properties.isEmpty()) null else properties
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_send_custom_event, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            R.id.item_json -> {
                openJsonScreen()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun openJsonScreen() {
        val intent = Intent(this, CustomEventJSONActivity::class.java)
        val customEvent = getCustomEvent()
        if (customEvent.eventName.isNotEmpty() || !customEvent.properties.isNullOrEmpty()) {
            intent.putExtra(Const.CUSTOM_EVENT, customEvent)
        }
        activityLauncher.launch(intent, object : BetterActivityResult.OnActivityResult<ActivityResult> {
            override fun onActivityResult(result: ActivityResult) {
                handleActivityResult(result, requestCode = Const.CUSTOM_EVENT_JSON)
            }
        })
    }

    private fun getCustomEvent(): CustomEvent {
        val eventName = binding.etEventName.text.toString().trim()
        val properties = mutableMapOf<String, String>()
        propertiesAdapter.itemsList?.let { items ->
            items.forEach { item ->
                properties[item.key] = item.value
            }
        }
        return CustomEvent(eventName, properties)
    }

    private fun checkOnExistingProperty(property: CustomEventProperty) {
        var position = -1
        propertiesAdapter.itemsList?.let { items ->
            items.forEach {
                if (it.key == property.key) {
                    position = propertiesAdapter.getItemPosition(it)
                    return@forEach
                }
            }
        }
        if (position != -1) propertiesAdapter.deleteItem(position)
    }

    private fun setCustomEvent(customEvent: CustomEvent) {
        binding.etEventName.setText(customEvent.eventName)
        binding.etEventName.setSelection(customEvent.eventName.length)
        customEvent.properties?.let { properties ->
            val propertyList = mutableListOf<CustomEventProperty>()
            properties.forEach { property ->
                propertyList.add(CustomEventProperty(property.key, property.value))
            }
            propertiesAdapter.updateItems(propertyList, true)
        }
    }
}