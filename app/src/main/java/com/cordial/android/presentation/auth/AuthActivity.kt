package com.cordial.android.presentation.auth

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityAuthBinding
import com.cordial.android.presentation.cart.CartActivity
import com.cordial.android.presentation.location.LocationService
import com.cordial.android.presentation.main.MainActivity
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.settings.SettingsActivity
import com.cordial.android.util.Const
import com.cordial.android.util.extension.afterTextChanged
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.CordialApi
import javax.inject.Inject

class AuthActivity : AppCompatActivity(), AuthView {
    private lateinit var binding: ActivityAuthBinding

    private lateinit var preferenceHelper: PreferenceHelper

    private var path: String? = null
    private var fallbackPath: String? = null
    private var isAuthorized: Boolean = false
    private var isCheckOnAuth: Boolean = true

    @Inject
    protected lateinit var presenter: AuthPresenter

    private var locationManager: LocationService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
        handleIntent()
        initPreferences()
        initLocationService()
        configureActionBar()
        fillViews()
        setClickListeners()
    }

    private fun initLocationService() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                Const.REQUEST_LOCATION_PERMISSIONS
            )
        } else {
            locationManager = LocationService.getLocationManager()
            if (isAuthorized) {
                finish()
            }
        }
    }

    private fun inject() {
        App.injector?.authComponent?.inject(this)
    }

    private fun handleIntent() {
        if (intent.hasExtra(Const.PRODUCT_PATH)) {
            path = intent.getStringExtra(Const.PRODUCT_PATH)
        }
        if (intent.hasExtra(Const.FALLBACK_PRODUCT_PATH)) {
            fallbackPath = intent.getStringExtra(Const.FALLBACK_PRODUCT_PATH)
        }
        if (intent.hasExtra(Const.CHECK_ON_AUTH_STATE)) {
            isCheckOnAuth = intent.getBooleanExtra(Const.CHECK_ON_AUTH_STATE, true)
        }
        if (intent.hasExtra(Const.SKIP_AUTH_CHECK)) {
            if (intent.getBooleanExtra(Const.SKIP_AUTH_CHECK, false)) {
                openMainActivity()
            }
        }
    }

    private fun initPreferences() {
        preferenceHelper = PreferenceHelper(this)
        if (isCheckOnAuth) {
            if (preferenceHelper.contains(AppPreferenceKeys.IS_LOGGED_IN)) {
                if (preferenceHelper.getBoolean(AppPreferenceKeys.IS_LOGGED_IN, false)) {
                    openMainActivity()
                }
            }
        }
    }

    private fun configureActionBar() {
        supportActionBar?.title = resources.getString(R.string.login)
    }

    private fun fillViews() {
        with(binding) {
            val lastEmail = preferenceHelper.getString(AppPreferenceKeys.PRIMARY_KEY, "email:example@ex.com")
            etPrimaryKeyInput.setText(lastEmail)
            etPrimaryKeyInput.text?.let {
                etPrimaryKeyInput.setSelection(it.length)
            }
        }
    }

    private fun setClickListeners() {
        with(binding) {
            btnLogin.setOnClickListener {
                setTextWatchers()
                if (validateEmailField()) {
                    closeKeyboard()
                    login(etPrimaryKeyInput.text.toString())
                }
            }
            btnGuestLogin.setOnClickListener {
                CordialApi().setContact(null)
                openMainActivity(isLoggedIn = true)
                finish()
            }
        }
    }

    private fun closeKeyboard() {
        currentFocus?.let { view ->
            val imm = getSystemService(INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun setTextWatchers() {
        binding.etPrimaryKeyInput.afterTextChanged {
            validateEmailField()
        }
    }

    private fun validateEmailField(): Boolean {
        with(binding) {
            val email = etPrimaryKeyInput.text.toString()
            return if (email.trim().length in 1..Const.EMAIL_MAX_SYMBOLS) {
                tilPrimaryKey.isErrorEnabled = false
                true
            } else {
                tilPrimaryKey.error = resources.getString(R.string.invalid_primary_key)
                tilPrimaryKey.isErrorEnabled = true
                false
            }
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_auth, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.item_settings -> {
                openSettingsScreen()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    private fun openSettingsScreen() {
        val intent = Intent(this@AuthActivity, SettingsActivity::class.java)
        startActivity(intent)
    }

    private fun login(primaryKey: String) {
        if (primaryKey != preferenceHelper.getString(AppPreferenceKeys.PRIMARY_KEY)) {
            presenter.clearUserCart()
        }
        CordialApi().setContact(primaryKey)
        preferenceHelper.put(AppPreferenceKeys.IS_LOGGED_IN, true)
        preferenceHelper.put(AppPreferenceKeys.PRIMARY_KEY, primaryKey)
        openMainActivity(isLoggedIn = true)
        finish()
    }

    private fun openMainActivity(isLoggedIn: Boolean = false) {
        val intent = Intent(this@AuthActivity, MainActivity::class.java)
        path?.let {
            intent.putExtra(Const.PRODUCT_PATH, it)
            overridePendingTransition(0, 0)
        }
        fallbackPath?.let {
            intent.putExtra(Const.FALLBACK_PRODUCT_PATH, it)
            overridePendingTransition(0, 0)
        }
        intent.putExtra(Const.IS_LOGGED_IN, isLoggedIn)
        startActivity(intent)
        isAuthorized = true
    }

    override fun showCartProducts(items: List<CartItemPresentationModel>) {
        CordialApi().upsertContactCart(CartActivity.getCartItems(items))
    }

    override fun cartIsCleared() {
        presenter.clearProfileAttributes()
    }

    override fun profileAttributesClear() {
    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            Const.REQUEST_LOCATION_PERMISSIONS -> if (grantResults.size > 1) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    initLocationService()
                    return
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
        if (isAuthorized) {
            finish()
        }
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.injector?.recycleAuthComponent()
    }
}