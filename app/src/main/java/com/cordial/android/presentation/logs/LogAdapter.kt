package com.cordial.android.presentation.logs

import android.app.AlertDialog
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.databinding.ItemLogBinding
import com.cordial.android.presentation.model.log.Log
import com.cordial.android.util.Const


class LogAdapter(
    private val logs: List<Log>,
    private val onClearAboveListener: (position: Int) -> Unit
) : RecyclerView.Adapter<LogAdapter.LogViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LogViewHolder {
        val binding = ItemLogBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LogViewHolder(binding, parent.context, onClearAboveListener)
    }

    override fun getItemCount(): Int {
        return logs.size
    }

    override fun onBindViewHolder(holder: LogViewHolder, position: Int) {
        holder.bind(logs[position])
    }

    inner class LogViewHolder(
        private val binding: ItemLogBinding,
        private val context: Context,
        private val onClearAboveListener: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(log: Log) {
            val logText = "[${log.timestamp}] ${log.tag}: ${log.message}"
            binding.tvLog.text = logText
            setOnCopyListener(log)
        }

        private fun setOnCopyListener(log: Log) {
            binding.tvLog.setOnClickListener {
                showDialog(log)
            }
        }

        private fun showDialog(log: Log) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            val items = if (bindingAdapterPosition == 0)
                arrayOf(Const.COPY) else arrayOf(Const.COPY, Const.CLEAR_ALL_ABOVE)
            builder.setItems(items) { _, which ->
                when (which) {
                    0 -> {
                        val clipboard: ClipboardManager? =
                            context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager?
                        val clip = ClipData.newPlainText("label", log.message)
                        clipboard?.setPrimaryClip(clip)
                    }

                    1 -> {
                        onClearAboveListener.invoke(bindingAdapterPosition)
                    }
                }
            }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }
}