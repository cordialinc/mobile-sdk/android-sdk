package com.cordial.android.presentation.updateattributes

import com.cordial.android.domain.model.attribute.ProfileAttributeDomainModel
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import io.reactivex.Completable
import io.reactivex.Single

interface ProfileAttributeUseCase {
    fun saveAttribute(attribute: ProfileAttribute): Completable
    fun updateAttribute(attribute: ProfileAttribute): Completable
    fun getAllAttributes(): Single<List<ProfileAttributeDomainModel>>
    fun deleteAttribute(attributeKey: String): Completable
    fun deleteAllAttributes(): Completable
}