package com.cordial.android.presentation.logs

import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.presentation.model.PresentationMapper
import com.cordial.android.presentation.model.log.Log
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class LogPresenter(
    private val logUseCase: LogUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<LogView>() {

    private val disposable = CompositeDisposable()
    fun storeLog(log: Log) {
        logUseCase.storeLog(log).observeOn(observeOnSchedulers)
            .subscribe({
                view?.logStored(log)
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    fun storeLogs(logs: List<Log>) {
        logUseCase.storeLogs(logs).observeOn(observeOnSchedulers)
            .subscribe({
                view?.logsStored()
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    fun getAllLogs() {
        return logUseCase.getAllLogs().observeOn(observeOnSchedulers)
            .map { logs ->
                PresentationMapper.logsDomainModelToLog(logs)
            }
            .subscribe({ logs ->
                view?.showLogs(logs)
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    fun deleteFirstLog() {
        logUseCase.deleteFirstLog().observeOn(observeOnSchedulers)
            .subscribe({
                view?.logDeleted()
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    fun clearLogs() {
        logUseCase.deleteAllLogs().observeOn(observeOnSchedulers)
            .subscribe({
                view?.logsDeleted()
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    override fun detachView(view: LogView) {
        super.detachView(view)
        disposable.clear()
    }
}