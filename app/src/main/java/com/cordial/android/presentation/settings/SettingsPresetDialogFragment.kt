package com.cordial.android.presentation.settings

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cordial.android.databinding.DialogPresetsBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class SettingsPresetDialogFragment(
    private val isCopyMode: Boolean = false,
    private val onItemClickListener: ((presetType: PresetType) -> Unit)?
) : BottomSheetDialogFragment() {

    private lateinit var binding: DialogPresetsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DialogPresetsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillViews()
        setClickListeners()
        configureCopyMode()
    }

    private fun fillViews() {
        with(binding) {
            tvPresetQc.text = PresetType.QC.presetName
            tvPresetStaging.text = PresetType.STAGING.presetName
            tvPresetProduction.text = PresetType.PRODUCTION.presetName
            tvPresetCustom.text = PresetType.CUSTOM.presetName
        }
    }

    private fun setClickListeners() {
        with(binding) {
            vgPresetQc.setOnClickListener {
                onItemClickListener?.invoke(PresetType.QC)
                dismiss()
            }
            vgPresetStaging.setOnClickListener {
                onItemClickListener?.invoke(PresetType.STAGING)
                dismiss()
            }
            vgPresetProduction.setOnClickListener {
                onItemClickListener?.invoke(PresetType.PRODUCTION)
                dismiss()
            }
            vgPresetCustom.setOnClickListener {
                onItemClickListener?.invoke(PresetType.CUSTOM)
                dismiss()
            }
        }
    }

    private fun configureCopyMode() {
        binding.vgPresetCustom.visibility = if (isCopyMode) View.GONE else View.VISIBLE
    }
}