package com.cordial.android.presentation.logs

import com.cordial.android.domain.model.log.LogDomainModel
import com.cordial.android.presentation.model.log.Log
import io.reactivex.Completable
import io.reactivex.Single

interface LogUseCase {
    fun storeLog(log: Log): Completable
    fun storeLogs(logs: List<Log>): Completable
    fun getAllLogs(): Single<List<LogDomainModel>>
    fun deleteFirstLog(): Completable
    fun deleteAllLogs(): Completable
}