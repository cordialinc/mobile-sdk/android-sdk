package com.cordial.android.presentation.updateattributes

import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.presentation.model.PresentationMapper
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class UpdateAttributesPresenter(
    private val attributeUseCase: ProfileAttributeUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<UpdateAttributesView>() {

    private val disposable = CompositeDisposable()

    fun getAllAttributes() {
        attributeUseCase.getAllAttributes().observeOn(observeOnSchedulers).map {
            PresentationMapper.attributesToPresentationModel(it)
        }.subscribe({
            view?.showAllAttributes(it)
        }, {
            view?.showError(it.localizedMessage)
        }).disposeBy(disposable)
    }

    fun deleteAttribute(attribute: ProfileAttribute) {
        attributeUseCase.deleteAttribute(attribute.key).observeOn(observeOnSchedulers)
            .subscribe({
                view?.attributeIsDeleted(attribute)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    override fun detachView(view: UpdateAttributesView) {
        disposable.clear()
        super.detachView(view)
    }
}