package com.cordial.android.presentation.catalogcategory

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.product.CartItemPresentationModel

interface ProductView : BaseView {
    fun productCartAdded()
    fun showCartProducts(items: List<CartItemPresentationModel>)
}