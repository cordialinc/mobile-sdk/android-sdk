package com.cordial.android.presentation.inbox

enum class InboxView(val view: String, val viewType: Int) {
    LIST("LIST", 0),
    CARD("CARD", 1);

    companion object {
        fun findViewType(viewType: Int): InboxView {
            return values().firstOrNull { it.viewType == viewType } ?: LIST
        }
    }
}