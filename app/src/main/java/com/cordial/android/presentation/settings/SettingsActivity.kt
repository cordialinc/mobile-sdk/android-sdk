package com.cordial.android.presentation.settings

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.TooltipCompat
import com.cordial.android.R
import com.cordial.android.databinding.ActivitySettingsBinding
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.notification.permission.model.EducationalUiModeEnum

class SettingsActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySettingsBinding

    private lateinit var preferenceHelper: PreferenceHelper
    private lateinit var presetType: PresetType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initPreferences()
        initPresetType()
        configureActionBar()
        configureByPreset(presetType)
        setClickListeners()
    }

    private fun initPreferences() {
        preferenceHelper = PreferenceHelper(this)
    }

    private fun initPresetType() {
        presetType = PresetType.findPresetType(preferenceHelper.getString(AppPreferenceKeys.PRESET, ""))
    }

    private fun configureActionBar() {
        supportActionBar?.title = resources.getString(R.string.settings)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun configureByPreset(presetType: PresetType) {
        fillByPreset(presetType)
        with(binding) {
            etHost.isEnabled = presetType == PresetType.CUSTOM
            cbMessageHub.visibility =
                if (presetType == PresetType.CUSTOM) View.VISIBLE else View.GONE
            cbMessageHub.isEnabled = presetType == PresetType.CUSTOM
            etMessageHubHost.isEnabled = presetType == PresetType.CUSTOM && cbMessageHub.isChecked
            etAccountKey.isEnabled = presetType == PresetType.CUSTOM
            etChannelKey.isEnabled = presetType == PresetType.CUSTOM
            etQty.isEnabled = presetType == PresetType.CUSTOM
            etBulkSize.isEnabled = presetType == PresetType.CUSTOM
            etBulkUploadInterval.isEnabled = presetType == PresetType.CUSTOM
            etMaxInboxMessageContentCacheSize.isEnabled = presetType == PresetType.CUSTOM
            etMaxCacheableInboxMessageContentSize.isEnabled = presetType == PresetType.CUSTOM
            ivCopyFromPreset.visibility = if (presetType == PresetType.CUSTOM) View.VISIBLE else View.GONE
            TooltipCompat.setTooltipText(ivCopyFromPreset, getString(R.string.copy_from_preset))
            if (presetType != PresetType.CUSTOM) {
                etHost.clearFocus()
                etAccountKey.clearFocus()
                etChannelKey.clearFocus()
                etQty.clearFocus()
                etBulkSize.clearFocus()
                etBulkUploadInterval.clearFocus()
                etMaxInboxMessageContentCacheSize.clearFocus()
                etMaxCacheableInboxMessageContentSize.clearFocus()
            }
        }
    }

    private fun fillByPreset(presetType: PresetType) {
        if (presetType != PresetType.CUSTOM) {
            with(binding) {
                etHost.setText(presetType.eventsStreamUrl)
                etMessageHubHost.setText(presetType.messageHubUrl)
                cbMessageHub.isChecked = false
                etAccountKey.setText(presetType.accountKey)
                etChannelKey.setText(presetType.channelKey)
                etQty.setText(presetType.qtyCached.toString())
                etBulkSize.setText(presetType.eventsBulkSize.toString())
                etBulkUploadInterval.setText(presetType.eventsBulkUploadInterval.toString())
                etMaxInboxMessageContentCacheSize.setText(presetType.maxInboxMessageContentCacheSize.toString())
                etMaxCacheableInboxMessageContentSize.setText(presetType.maxCacheableInboxMessageContentSize.toString())
            }
        } else {
            fillViews()
        }
        fillDisplayEducationalUiCb()
    }

    private fun fillViews() {
        with(binding) {
            if (preferenceHelper.contains(AppPreferenceKeys.HOST))
                etHost.setText(preferenceHelper.getString(AppPreferenceKeys.HOST))
            else
                etHost.setText(PresetType.CUSTOM.eventsStreamUrl)
            etHost.text?.let {
                etHost.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.MESSAGE_HUB_HOST))
                etMessageHubHost.setText(preferenceHelper.getString(AppPreferenceKeys.MESSAGE_HUB_HOST))
            else
                etMessageHubHost.setText(PresetType.CUSTOM.messageHubUrl)
            etMessageHubHost.text?.let {
                etMessageHubHost.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.MESSAGE_HUB_HOST_ENABLED))
                cbMessageHub.isChecked = preferenceHelper.getBoolean(AppPreferenceKeys.MESSAGE_HUB_HOST_ENABLED, false)
            if (preferenceHelper.contains(AppPreferenceKeys.ACCOUNT_KEY))
                etAccountKey.setText(preferenceHelper.getString(AppPreferenceKeys.ACCOUNT_KEY))
            else
                etAccountKey.setText(PresetType.CUSTOM.accountKey)
            etAccountKey.text?.let {
                etAccountKey.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.CHANNEL_KEY))
                etChannelKey.setText(preferenceHelper.getString(AppPreferenceKeys.CHANNEL_KEY))
            else
                etChannelKey.setText(PresetType.CUSTOM.channelKey)
            etChannelKey.text?.let {
                etChannelKey.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.QTY))
                etQty.setText(preferenceHelper.getInt(AppPreferenceKeys.QTY).toString())
            else
                etQty.setText(PresetType.CUSTOM.qtyCached.toString())
            etQty.text?.let {
                etQty.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.EVENTS_BULK_SIZE))
                etBulkSize.setText(preferenceHelper.getInt(AppPreferenceKeys.EVENTS_BULK_SIZE).toString())
            else
                etBulkSize.setText(PresetType.CUSTOM.eventsBulkSize.toString())
            etBulkSize.text?.let {
                etBulkSize.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.EVENTS_BULK_UPLOAD_INTERVAL))
                etBulkUploadInterval.setText(
                    preferenceHelper.getInt(AppPreferenceKeys.EVENTS_BULK_UPLOAD_INTERVAL).toString()
                )
            else
                etBulkUploadInterval.setText(PresetType.CUSTOM.eventsBulkUploadInterval.toString())
            etBulkUploadInterval.text?.let {
                etBulkUploadInterval.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE))
                etMaxInboxMessageContentCacheSize.setText(
                    preferenceHelper.getInt(AppPreferenceKeys.MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE).toString()
                )
            else
                etMaxInboxMessageContentCacheSize.setText(PresetType.CUSTOM.maxInboxMessageContentCacheSize.toString())
            etMaxInboxMessageContentCacheSize.text?.let {
                etMaxInboxMessageContentCacheSize.setSelection(it.length)
            }
            if (preferenceHelper.contains(AppPreferenceKeys.MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE))
                etMaxCacheableInboxMessageContentSize.setText(
                    preferenceHelper.getInt(AppPreferenceKeys.MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE).toString()
                )
            else
                etMaxCacheableInboxMessageContentSize.setText(PresetType.CUSTOM.maxCacheableInboxMessageContentSize.toString())
            etMaxCacheableInboxMessageContentSize.text?.let {
                etMaxCacheableInboxMessageContentSize.setSelection(it.length)
            }
        }
    }

    private fun fillDisplayEducationalUiCb() {
        if (preferenceHelper.contains(AppPreferenceKeys.SHOW_CATEGORIES_EDUCATIONAL_UI)) {
            binding.cbEducationalUiDisplay.isChecked =
                preferenceHelper.getBoolean(AppPreferenceKeys.SHOW_CATEGORIES_EDUCATIONAL_UI)
        }
    }

    private fun setClickListeners() {
        with(binding) {
            btnChoosePreset.setOnClickListener {
                val settingsPresetDialogFragment = SettingsPresetDialogFragment {
                    setPresetType(it)
                }
                settingsPresetDialogFragment.show(supportFragmentManager, "settings_preset_dialog_fragment")
            }
            cbMessageHub.setOnCheckedChangeListener { _, isChecked ->
                etMessageHubHost.isEnabled = isChecked
            }
            ivCopyFromPreset.setOnClickListener {
                val settingsPresetDialogFragment = SettingsPresetDialogFragment(isCopyMode = true) {
                    fillByPreset(it)
                }
                settingsPresetDialogFragment.show(supportFragmentManager, "settings_preset_dialog_fragment")
            }
        }
    }

    private fun setPresetType(presetType: PresetType) {
        saveSettings()
        this.presetType = presetType
        configureByPreset(presetType)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        reinitializeSdk()
    }

    private fun reinitializeSdk() {
        with(binding) {
            val host = etHost.text.toString()
            val messageHubHostText = etMessageHubHost.text.toString()
            val messageHubHost =
                if (messageHubHostText.isEmpty() || !cbMessageHub.isChecked) null else messageHubHostText
            val accountKey = etAccountKey.text.toString()
            val channelKey = etChannelKey.text.toString()
            val qty = etQty.text.toString().toInt()
            val bulkSize = etBulkSize.text.toString().toInt()
            val bulkUploadInterval = etBulkUploadInterval.text.toString().toInt()
            val maxInboxMessageContentCacheSize = etMaxInboxMessageContentCacheSize.text.toString().toInt()
            val maxCacheableInboxMessageContentSize = etMaxCacheableInboxMessageContentSize.text.toString().toInt()
            val showCategoriesNotificationPermissionEducationalUi = cbEducationalUiDisplay.isChecked
            val config = CordialApiConfiguration.getInstance()
            config.initialize(applicationContext, accountKey, channelKey, host, messageHubHost)
            config.setQty(qty)
            config.eventsBulkSize = bulkSize
            config.eventsBulkUploadInterval = if (bulkUploadInterval != 0) bulkUploadInterval else null
            config.inboxMessageCache.maxCacheSize = maxInboxMessageContentCacheSize
            config.inboxMessageCache.maxCacheableMessageSize = maxCacheableInboxMessageContentSize
            setEducationalUiMode(showCategoriesNotificationPermissionEducationalUi)

            preferenceHelper.put(AppPreferenceKeys.PRESET, presetType.presetName)
            saveSettings()
        }
    }

    private fun saveSettings() {
        with(binding) {
            val host = etHost.text.toString()
            val messageHubHost = etMessageHubHost.text.toString()
            val accountKey = etAccountKey.text.toString()
            val channelKey = etChannelKey.text.toString()
            val qty = etQty.text.toString().toInt()
            val bulkSize = etBulkSize.text.toString().toInt()
            val bulkUploadInterval = etBulkUploadInterval.text.toString().toInt()
            val maxInboxMessageContentCacheSize = etMaxInboxMessageContentCacheSize.text.toString().toInt()
            val maxCacheableInboxMessageContentSize = etMaxCacheableInboxMessageContentSize.text.toString().toInt()
            val showCategoriesEducationalUi = cbEducationalUiDisplay.isChecked
            preferenceHelper.put(AppPreferenceKeys.HOST, host)
            if (presetType == PresetType.CUSTOM && messageHubHost.isNotEmpty())
                preferenceHelper.put(AppPreferenceKeys.MESSAGE_HUB_HOST, messageHubHost)
            preferenceHelper.put(AppPreferenceKeys.MESSAGE_HUB_HOST_ENABLED, cbMessageHub.isChecked)
            preferenceHelper.put(AppPreferenceKeys.ACCOUNT_KEY, accountKey)
            preferenceHelper.put(AppPreferenceKeys.CHANNEL_KEY, channelKey)
            preferenceHelper.put(AppPreferenceKeys.QTY, qty)
            preferenceHelper.put(AppPreferenceKeys.EVENTS_BULK_SIZE, bulkSize)
            preferenceHelper.put(AppPreferenceKeys.EVENTS_BULK_UPLOAD_INTERVAL, bulkUploadInterval)
            preferenceHelper.put(
                AppPreferenceKeys.MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE,
                maxInboxMessageContentCacheSize
            )
            preferenceHelper.put(
                AppPreferenceKeys.MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE,
                maxCacheableInboxMessageContentSize
            )
            preferenceHelper.put(AppPreferenceKeys.SHOW_CATEGORIES_EDUCATIONAL_UI, showCategoriesEducationalUi)
        }
    }
    private fun setEducationalUiMode(showCategoriesEducationalUi: Boolean) {
        CordialApiConfiguration.getInstance().educationalUiMode =
            if (showCategoriesEducationalUi) EducationalUiModeEnum.NOTIFICATION_CATEGORIES else EducationalUiModeEnum.NONE
    }
}