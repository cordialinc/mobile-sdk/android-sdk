package com.cordial.android.presentation.deeplink

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import com.cordial.android.presentation.auth.AuthActivity
import com.cordial.android.util.Const
import com.cordial.android.util.extension.decode
import com.cordial.api.C
import com.cordial.feature.deeplink.model.CordialDeepLink
import com.cordial.feature.deeplink.model.CordialDeepLinkOpenListener
import com.cordial.feature.deeplink.model.DeepLinkAction

class DemoDeepLinkListener : CordialDeepLinkOpenListener {

    override fun appOpenViaDeepLink(
        context: Context?,
        cordialDeepLink: CordialDeepLink,
        fallBackUri: Uri?,
        onComplete: ((action: DeepLinkAction) -> Unit)?
    ) {
        val uri = cordialDeepLink.uri
        if (checkOnValidLink(uri, onComplete)) return
        if (checkOnNotificationSettings(uri, context)) return
        val intent = Intent(context, AuthActivity::class.java)
        intent.putExtra(Const.SKIP_AUTH_CHECK, true)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        uri?.let {
            val path = getDecodedUrl(uri)
            intent.putExtra(Const.PRODUCT_PATH, path)
        }
        fallBackUri?.let {
            val fallBackPath = getDecodedUrl(fallBackUri)
            intent.putExtra(Const.FALLBACK_PRODUCT_PATH, fallBackPath)
        }
        logUrls(cordialDeepLink, fallBackUri)
        context?.startActivity(intent)
    }

    private fun checkOnNotificationSettings(uri: Uri?, context: Context?): Boolean {
        uri?.let {
            val decodedUrl = getDecodedUrl(uri)
            if (decodedUrl == Const.APP_NOTIFICATION_SETTINGS_URL) {
                openAppNotificationSettings(context)
                return true
            }
        }
        return false
    }

    private fun checkOnValidLink(uri: Uri?, onComplete: ((action: DeepLinkAction) -> Unit)?): Boolean {
        uri?.let {
            val decodedUrl = getDecodedUrl(uri)
            val fragment = Uri.parse(decodedUrl).fragment ?: ""
            if (fragment.contains(Const.INVALID_DEEP_LINK_FRAGMENT)) {
                onComplete?.invoke(DeepLinkAction.OPEN_IN_BROWSER)
                return true
            }
        }
        return false
    }

    private fun getDecodedUrl(uri: Uri): String {
        return uri.decode()
    }

    private fun openAppNotificationSettings(context: Context?) {
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.action = Settings.ACTION_APP_NOTIFICATION_SETTINGS
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, context?.packageName)
        } else {
            intent.action = "android.settings.APP_NOTIFICATION_SETTINGS"
            intent.putExtra("app_package", context?.packageName)
            intent.putExtra("app_uid", context?.applicationInfo?.uid)
        }
        context?.startActivity(intent)
    }

    private fun logUrls(cordialDeepLink: CordialDeepLink, fallBackUri: Uri?) {
        Log.i(C.LOG_TAG, "DeepLink Listener has been called")
        cordialDeepLink.uri?.let { Log.i(C.LOG_TAG, "url = ${cordialDeepLink.uri}") }
        cordialDeepLink.vanityUri?.let { Log.i(C.LOG_TAG, "vanityUrl = ${cordialDeepLink.vanityUri}") }
        fallBackUri?.let { Log.i(C.LOG_TAG, "fallbackUrl = $fallBackUri") }
    }
}