package com.cordial.android.presentation.custom

interface BaseView {
    fun showError(error: String?)
}