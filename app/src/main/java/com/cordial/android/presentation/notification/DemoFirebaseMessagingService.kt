package com.cordial.android.presentation.notification

import com.cordial.feature.notification.CordialNotificationProcessService
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class DemoFirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        val cordialNotificationProcessService = CordialNotificationProcessService()
        if (cordialNotificationProcessService.isCordialMessage(remoteMessage))
            cordialNotificationProcessService.processMessage(this, remoteMessage)
    }

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        CordialNotificationProcessService().processNewToken(this, token)
    }
}