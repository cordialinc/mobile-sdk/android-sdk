package com.cordial.android.presentation.inappmessage

import android.util.Log
import com.cordial.api.C
import com.cordial.feature.inappmessage.model.InAppMessageInputsListener
import com.cordial.feature.sendevent.model.property.PropertyValue

class DemoInAppMessageInputsListener : InAppMessageInputsListener {
    override fun inputsCaptured(eventName: String, properties: Map<String, PropertyValue>?) {
        Log.d(C.LOG_TAG, "Event: $eventName captured inputs: $properties")
    }
}