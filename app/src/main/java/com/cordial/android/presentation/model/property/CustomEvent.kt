package com.cordial.android.presentation.model.property

import java.io.Serializable

class CustomEvent(
    val eventName: String,
    val properties: Map<String, String>?
) : Serializable