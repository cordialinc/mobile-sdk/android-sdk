package com.cordial.android.presentation.logs

import com.cordial.android.presentation.custom.BaseView
import com.cordial.android.presentation.model.log.Log


interface LogView: BaseView {
    fun logStored(log: Log)
    fun logsStored()
    fun showLogs(logs: List<Log>)
    fun logDeleted()
    fun logsDeleted()
}