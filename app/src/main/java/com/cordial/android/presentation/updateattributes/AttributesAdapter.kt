package com.cordial.android.presentation.updateattributes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.databinding.ItemProfileAttributeBinding
import com.cordial.android.presentation.custom.HeaderFooterAdapter
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.model.attribute.ProfileAttributeType
import com.cordial.android.util.DateTimeUtils

class AttributesAdapter(
    items: List<ProfileAttribute>?,
    val onEditListener: ((position: Int) -> Unit)?
) :
    HeaderFooterAdapter<RecyclerView.ViewHolder, Any?, ProfileAttribute, Any?>(null, items, null) {

    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemProfileAttributeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AttributeHolder(binding)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as AttributeHolder).bind(getItem(position))
    }

    inner class AttributeHolder(private val binding: ItemProfileAttributeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.ivEdit.setOnClickListener {
                onEditListener?.invoke(bindingAdapterPosition)
            }
            itemView.setOnClickListener {
                onEditListener?.invoke(bindingAdapterPosition)
            }
        }

        fun bind(item: ProfileAttribute) {
            with(binding) {
                tvAttrValue.text = ""
                tvAttrType.text = item.type.type
                tvAttrKey.text = item.key
                when (item.type) {
                    ProfileAttributeType.GEO -> {
                        tvAttrValueName.visibility = View.GONE
                        tvAttrValue.visibility = View.GONE
                        vgAttrValueGeo.visibility = View.VISIBLE
                        tvAttrGeoStreetAddress.text = item.streetAddress
                        tvAttrGeoStreetAddress2.text = item.streetAddress2
                        tvAttrGeoPostalCode.text = item.postalCode
                        tvAttrGeoCountry.text = item.country
                        tvAttrGeoState.text = item.state
                        tvAttrGeoCity.text = item.city
                        tvAttrGeoTimezone.text = item.timezone
                    }
                    else -> {
                        if (item.type == ProfileAttributeType.DATE) {
                            item.value?.let { value ->
                                val dateTime = DateTimeUtils.formatDateTime(value)
                                tvAttrValue.text = dateTime
                            }
                        } else {
                            tvAttrValue.text = item.value
                        }
                        tvAttrValueName.visibility = View.VISIBLE
                        tvAttrValue.visibility = View.VISIBLE
                        vgAttrValueGeo.visibility = View.GONE
                    }
                }
            }
        }
    }
}