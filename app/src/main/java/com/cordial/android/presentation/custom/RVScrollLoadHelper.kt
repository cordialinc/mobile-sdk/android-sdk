package com.cordial.android.presentation.custom

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

class RVScrollLoadHelper(
    private val listener: RVCloseToEndListener,
    private var visibleThreshold: Int
) {
    private var scrollListener: RecyclerView.OnScrollListener? = null
    private val rvHolderSet = HashSet<RvHolder>(1)

    //Constructor with default visibleThreshold
    fun RVScrollLoadHelper(listener: RVCloseToEndListener) {
        RVScrollLoadHelper(listener, 0)
    }

    //Implement this interface in your activity/fragment to listen
    interface RVCloseToEndListener {
        fun onRecyclerViewScrollCloseToEnd(recyclerView: RecyclerView)
    }

    //Create new RecyclerView.OnScrollListener instance
    private fun createListener(): RecyclerView.OnScrollListener {
        return object : RecyclerView.OnScrollListener() {
            fun isNeedToLoad(holder: RvHolder?): Boolean {
                if (holder == null) return false
                val visibleItemCount = holder.mRecyclerView.childCount
                val llManager = holder.mRecyclerView.layoutManager as LinearLayoutManager
                val totalItemCount = llManager.itemCount
                val firstVisibleItem = llManager.findFirstVisibleItemPosition()
                val isCloseToTheEnd =
                    totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold
                return !holder.isLoading && isCloseToTheEnd && holder.hasMoreData
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (isNeedToLoad(getRvHolder(recyclerView))) {
                    listener.onRecyclerViewScrollCloseToEnd(recyclerView)
                }
            }
        }
    }

    fun setVisibleThreshold(visibleThreshold: Int) {
        if (visibleThreshold > 0)
            this.visibleThreshold = visibleThreshold
    }

    //Get RecyclerView.OnScrollListener instance
    private fun getScrollListener(): RecyclerView.OnScrollListener {
        if (scrollListener == null) {
            scrollListener = createListener()
        }
        return scrollListener as RecyclerView.OnScrollListener
    }

    //Add RecyclerView instance to listen scroll-close-to-end events
    fun addRecyclerViews(vararg recyclerViews: RecyclerView) {
        for (rv in recyclerViews)
            if (rvHolderSet.add(RvHolder(rv)))
                rv.addOnScrollListener(getScrollListener())
    }

    //Remove RecyclerView instance from helper and remove its scroll listener
    fun removeRecyclerView(recyclerView: RecyclerView) {
        if (rvHolderSet.remove(RvHolder(recyclerView)))
            recyclerView.removeOnScrollListener(getScrollListener())
    }

    fun isRecyclerViewAdded(recyclerView: RecyclerView): Boolean {
        return rvHolderSet.contains(RvHolder(recyclerView))
    }

/*    //Set new "hasMoreData" and "isLoading" states for RecyclerView
    public void setStates(RecyclerView recyclerView, boolean newHasMoreDataState, boolean newIsLoadingState) {
        RvHolder rvHolder = getRvHolder(recyclerView);
        if(rvHolder != null) {
            handleRVListener(rvHolder, newHasMoreDataState);
            rvHolder.hasMoreData = newHasMoreDataState;
            rvHolder.isLoading = newIsLoadingState;
        }
    }*/

    //Set new "hasMoreData" state for RecyclerView
    //"true" if there is more data we can load next time for RecyclerView
    fun setHasMoreDataState(recyclerView: RecyclerView, newState: Boolean) {
        val rvHolder = getRvHolder(recyclerView)
        if (rvHolder != null) {
            handleRVListener(rvHolder, newState)
            rvHolder.hasMoreData = newState
        }
    }

    fun getHasMoreDataState(recyclerView: RecyclerView): Boolean {
        val rvHolder = getRvHolder(recyclerView)
        return rvHolder != null && rvHolder.hasMoreData
    }

    //Set new "isLoading" state for RecyclerView
    //"true" if data loading is in progress, "false" when loading is finished
    fun setIsLoadingState(recyclerView: RecyclerView, newState: Boolean) {
        val rvHolder = getRvHolder(recyclerView)
        if (rvHolder != null) rvHolder.isLoading = newState
    }

    //Optional method for optimizing speed
    //Remove or add ScrollListener to RecyclerView relying on newHasMoreDataState
    private fun handleRVListener(holder: RvHolder, newHasMoreDataState: Boolean) {
        //Remove scroll listener, if there is no more data
        if (!newHasMoreDataState)
            holder.mRecyclerView.removeOnScrollListener(getScrollListener())
        else if (!holder.hasMoreData)
            holder.mRecyclerView.addOnScrollListener(getScrollListener())//Add again scroll listener, if fore some reasons new data can be loaded
    }

    private fun getRvHolder(recyclerView: RecyclerView): RvHolder? {
        for (rvHolder in rvHolderSet) {
            if (rvHolder.mRecyclerView === recyclerView) {
                return rvHolder
            }
        }
        return null
    }

    companion object {
        fun smoothScrollToTop(rv: RecyclerView, maxSmoothLines: Int) {
            rv.adapter?.let { adapter ->
                if (rv.canScrollVertically(-1)
                    && adapter.itemCount > 0
                ) {
                    val itemsCount = adapter.itemCount
                    if (rv.layoutManager is GridLayoutManager) {
                        val glm = rv.layoutManager as GridLayoutManager
                        val maxSmoothGrid = maxSmoothLines * glm.spanCount
                        if (itemsCount > maxSmoothGrid) {
                            val firstVisible = glm.findFirstVisibleItemPosition()
                            if (firstVisible > maxSmoothGrid - 1)
                                rv.scrollToPosition(maxSmoothGrid - 1)
                        }
                    } else if (rv.layoutManager is LinearLayoutManager && itemsCount > maxSmoothLines) {
                        val llm = rv.layoutManager as LinearLayoutManager
                        val firstVisible = llm.findFirstVisibleItemPosition()
                        if (firstVisible > maxSmoothLines - 1)
                            rv.scrollToPosition(maxSmoothLines - 1)
                    }
                    rv.post { rv.smoothScrollToPosition(0) }
                }
            }
        }

        fun smoothScrollToTop(rv: RecyclerView) {
            smoothScrollToTop(rv, 8)
        }

        //Fix for ScrollHelper and SwipeRefreshLayout starts loading data at the same time
        fun checkCanStartLoadingVertical(rv: RecyclerView): Boolean {
            rv.layoutManager.let { layoutManager ->
                return (rv.canScrollVertically(-1) || (!rv.canScrollVertically(1)
                        && layoutManager != null
                        && layoutManager.childCount > 1))
            }
        }
    }

    private inner class RvHolder constructor(var mRecyclerView: RecyclerView) {
        var hasMoreData = true
        var isLoading = false

        override fun hashCode(): Int {
            return mRecyclerView.hashCode()
        }

        override fun equals(other: Any?): Boolean {
            return this === other || other?.javaClass == RvHolder::class.java && mRecyclerView == (other as RvHolder).mRecyclerView
        }
    }
}
