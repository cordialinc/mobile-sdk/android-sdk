package com.cordial.android.presentation

import android.content.Context
import com.cordial.android.di.component.*
import com.cordial.android.di.module.AppModule
import com.cordial.android.di.module.LocalStorageModule

class Injector {
    var appComponent: AppComponent? = null
        private set

    fun createAppComponent(context: Context) {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(context))
            .localStorageModule(LocalStorageModule(context))
            .build()
    }

    var authComponent: AuthComponent? = null
        private set
        get() {
            if (field == null) {
                field = DaggerAuthComponent.builder().appComponent(appComponent).build()
            }
            return field
        }

    var cartComponent: CartComponent? = null
        private set
        get() {
            if (field == null) {
                field = DaggerCartComponent.builder()
                    .appComponent(appComponent)
                    .build()
            }
            return field
        }

    var profileAttributeComponent: ProfileAttributeComponent? = null
        private set
        get() {
            if (field == null) {
                field = DaggerProfileAttributeComponent.builder()
                    .appComponent(appComponent)
                    .build()
            }
            return field
        }

    var logComponent: LogComponent? = null
        private set
        get() {
            if (field == null) {
                field = DaggerLogComponent.builder()
                    .appComponent(appComponent)
                    .build()
            }
            return field
        }

    fun recycleAuthComponent() {
        authComponent = null
    }

    fun recycleCartComponent() {
        cartComponent = null
    }

    fun recycleProfileAttributeComponent() {
        profileAttributeComponent = null
    }

    fun recycleLogComponent() {
        logComponent = null
    }
}