package com.cordial.android.presentation.inbox

import java.io.Serializable

class InboxMessageModelView(
    val mcID: String,
    var isRead: Boolean,
    val sentAt: String,
    val previewData: String?
) : Serializable