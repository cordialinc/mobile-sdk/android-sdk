package com.cordial.android.presentation.custom

import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.databinding.ItemLoadingProgressBinding

class LoadingViewHolder(binding: ItemLoadingProgressBinding) : RecyclerView.ViewHolder(binding.root)