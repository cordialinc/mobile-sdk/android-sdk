package com.cordial.android.presentation.model.property

import java.io.Serializable

class CustomEventProperty(
    val key: String,
    val value: String
) : Serializable