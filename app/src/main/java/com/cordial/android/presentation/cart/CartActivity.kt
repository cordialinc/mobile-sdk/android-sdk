package com.cordial.android.presentation.cart

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.cordial.android.App
import com.cordial.android.R
import com.cordial.android.databinding.ActivityCartBinding
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.api.CordialApi
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.inappmessage.model.InAppMessageDelayShowType
import com.cordial.feature.sendcontactorder.model.Address
import com.cordial.feature.sendcontactorder.model.Order
import com.cordial.feature.sendevent.model.property.PropertyValue
import com.cordial.feature.upsertcontactcart.model.CartItem
import org.json.JSONArray
import java.util.*
import javax.inject.Inject

class CartActivity : AppCompatActivity(), CartView {

    private lateinit var binding: ActivityCartBinding
    private lateinit var cartAdapter: CartAdapter

    @Inject
    protected lateinit var presenter: CartPresenter

    private val cordialApi = CordialApi()
    private val inAppMessageDelayMode = CordialApiConfiguration.getInstance().inAppMessageDelayMode

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        configureActionBar()
        binding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initCatalogList()
        getCartProducts(false)
    }

    private fun inject() {
        App.injector?.cartComponent?.inject(this)
    }

    private fun configureActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = resources.getString(R.string.cart)
    }


    private fun initCatalogList() {
        cartAdapter = CartAdapter(null, onCheckoutListener = {
            checkout()
        }, onDeleteItemListener = {
            presenter.deleteProductFromCart(it)
            updateFooter()
        })
        with(binding) {
            rvCatalog.layoutManager = LinearLayoutManager(this@CartActivity)
            rvCatalog.adapter = cartAdapter
            val itemTouchHelper = ItemTouchHelper(SwipeToDeleteCallback(this@CartActivity) { position ->
                onDeleteItem(position)
            })
            itemTouchHelper.attachToRecyclerView(rvCatalog)
        }
    }

    private fun getCartProducts(upsertContact: Boolean) {
        presenter.getAllCartProducts(upsertContact)
    }

    private fun upsertContactCart() {
        cordialApi.upsertContactCart(getCartItems(cartAdapter.itemsList))
    }

    private fun checkout() {
        val shippingAddress = Address(
            "shippingAddressName",
            "shippingAddress",
            "shippingAddressCity",
            "shippingAddressState",
            "shippingAddressPostalCode",
            "shippingAddressCountry"
        )
        val billingAddress = Address(
            "billingAddressName",
            "billingAddress",
            "billingAddressCity",
            "billingAddressState",
            "billingAddressPostalCode",
            "billingAddressCountry"
        )
        val orderID = UUID.randomUUID().toString()
        val order = Order(
            orderID,
            "orderStatus",
            "storeID",
            "customerID",
            shippingAddress,
            billingAddress,
            getCartItems(cartAdapter.itemsList)
        )
            .withTax(21.2)
            .withShippingAndHandling(130.5)
            .withProperties(
                mapOf(
                    "email" to PropertyValue.StringProperty("example@ex.com"),
                    "subscribeToEmail" to PropertyValue.BooleanProperty(true),
                    "discount" to PropertyValue.NumericProperty(15),
//                    "delivery" to PropertyValue.JSONObjectProperty(
//                        JSONObject().put("isHome", true).put("cost", 20)
//                    ),
                    "phoneNumbers" to PropertyValue.JSONArrayProperty(JSONArray("[\"444-55-111\", \"123-212-12\"]"))
                )
            )
        cordialApi.sendContactOrder(order)
        presenter.clearCart()
        finish()
    }

    private fun onDeleteItem(position: Int) {
        cartAdapter.deleteProduct(position)
    }

    override fun onStart() {
        super.onStart()
        presenter.attachView(this)
    }

    override fun onResume() {
        super.onResume()
        inAppMessageDelayMode.delayedShow()
    }

    override fun showCartProducts(cartItems: List<CartItemPresentationModel>, upsertContact: Boolean) {
        cartAdapter.updateItems(cartItems.toMutableList(), true)
        updateFooter()

        if (upsertContact) {
            upsertContactCart()
        }
    }

    private fun updateFooter() {
        val holder = binding.rvCatalog.findViewHolderForAdapterPosition(cartAdapter.itemCount - 1)
        if (holder != null && holder is CartFooter)
            holder.bind(
                cartAdapter.itemsList
            )
    }

    override fun productDeletedFromCart() {
        getCartProducts(true)
        Toast.makeText(this, "Product was deleted", Toast.LENGTH_SHORT).show()
    }

    override fun cartIsCleared() {
        getCartProducts(true)
    }

    override fun showError(error: String?) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        inAppMessageDelayMode.show(InAppMessageDelayShowType.IMMEDIATELY)
    }

    override fun onStop() {
        super.onStop()
        presenter.detachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        App.injector?.recycleCartComponent()
    }

    companion object {
        fun getCartItems(itemList: List<CartItemPresentationModel>?): List<CartItem> {
            val categoryName = "Mens"
            var cartItems = listOf<CartItem>()
            itemList?.let { items ->
                cartItems = items.map {
                    val cartItem = CartItem(
                        it.product.id.toString(),
                        it.product.name,
                        it.product.sku,
                        categoryName,
                        it.quantity
                    ).withItemPrice(
                        it.product.price
                    ).withSalePrice(
                        it.product.price
                    ).withAttr(
                        mapOf("catalogID" to "1011")
                    ).withImages(
                        listOf(it.product.imgUrl)
                    ).withProperties(
                        mapOf(
                            "catalogName" to PropertyValue.StringProperty("Men"),
                            "isWishList" to PropertyValue.BooleanProperty(true),
                            "numberOfViews" to PropertyValue.NumericProperty(10),
//                            "size" to PropertyValue.JSONObjectProperty(
//                                JSONObject("{\"height\": 180, \"width\": 100}")
//                            ),
                            "selectedColors" to PropertyValue.JSONArrayProperty(JSONArray("[\"blue\", \"black\"]"))
                        )
                    )
                    it.timestamp?.let { date ->
                        cartItem.setTimestamp(date)
                    }
                    cartItem
                }
            }
            return cartItems
        }
    }
}