package com.cordial.android.presentation.cart

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.cordial.android.R
import com.cordial.android.databinding.FooterCartBinding
import com.cordial.android.databinding.ItemCartBinding
import com.cordial.android.presentation.custom.HeaderFooterAdapter
import com.cordial.android.presentation.model.product.CartItemPresentationModel

class CartAdapter(
    items: List<CartItemPresentationModel>?,
    private val onCheckoutListener: () -> Unit,
    private val onDeleteItemListener: (product: CartItemPresentationModel) -> Unit
) : HeaderFooterAdapter<RecyclerView.ViewHolder, Any?, CartItemPresentationModel, Any>(
    null,
    items,
    Any()
) {

    fun deleteProduct(position: Int) {
        val product = getItem(position)
        deleteItem(position)
        onDeleteItemListener.invoke(product)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemCartBinding = ItemCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartItemVH(itemCartBinding)
    }

    override fun onCreateFooterViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val footerCartBinding = FooterCartBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CartFooter(footerCartBinding, onCheckoutListener)
    }

    override fun onBindItemViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CartItemVH).bind(getItem(position))
    }

    override fun onBindFooterViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CartFooter).bind(itemsList)
    }

    inner class CartItemVH(private val itemCartBinding: ItemCartBinding) :
        RecyclerView.ViewHolder(itemCartBinding.root) {

        fun bind(cartItem: CartItemPresentationModel) {
            val product = cartItem.product

            with(itemCartBinding) {
                ivCartItemImage.setImageDrawable(ContextCompat.getDrawable(itemView.context, product.img))
                tvCartItemName.text = product.name
                tvCartItemBrand.text = product.brand
                val productPrice = "$ ${product.price}"
                tvCartItemPrice.text = productPrice
                val quantityText = itemView.resources.getString(R.string.quantity) + " : ${cartItem.quantity}"
                tvCartItemQuantity.text = quantityText
            }
        }
    }
}