package com.cordial.android.presentation.model

enum class JsonType {
    ARRAY,
    OBJECT,
    NONE;
}