package com.cordial.android.presentation.logs

import android.content.Context
import com.cordial.android.util.Const
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.feature.log.CordialLoggerListener
import org.json.JSONArray
import org.json.JSONObject

class CordialDemoLogger(private val context: Context): CordialLoggerListener {
    override fun info(message: String, timestamp: String, tag: String) {
        storeLog(message, timestamp, tag)
    }

    override fun debug(message: String, timestamp: String, tag: String) {
        storeLog(message, timestamp, tag)
    }

    override fun error(message: String, timestamp: String, tag: String) {
        storeLog(message, timestamp, tag)
    }

    override fun warning(message: String, timestamp: String, tag: String) {
        storeLog(message, timestamp, tag)
    }

    private fun storeLog(message: String, timestamp: String, tag: String) {
        val preferences = PreferenceHelper(context)
        val logsJson = preferences.getString(AppPreferenceKeys.LOGS, "")
        val jsonArray = if (logsJson.isEmpty()) JSONArray() else JSONArray(logsJson)
        val jsonObject = JSONObject()
        jsonObject.put(Const.TAG, tag)
        jsonObject.put(Const.MESSAGE, message)
        jsonObject.put(Const.TIMESTAMP, timestamp)
        val result = jsonArray.put(jsonObject)
        preferences.put(AppPreferenceKeys.LOGS, result)
    }
}