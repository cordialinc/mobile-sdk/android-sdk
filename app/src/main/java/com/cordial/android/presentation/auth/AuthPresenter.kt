package com.cordial.android.presentation.auth

import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class AuthPresenter(
    private val authUseCase: AuthUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<AuthView>() {

    private val disposable = CompositeDisposable()

    fun clearUserCart() {
        authUseCase.clearUserCart().observeOn(observeOnSchedulers).subscribe({
            view?.cartIsCleared()
        }, {
            view?.showError(it.localizedMessage)
        })
            .disposeBy(disposable)
    }

    fun clearProfileAttributes() {
        authUseCase.clearProfileAttributes().observeOn(observeOnSchedulers).subscribe({
            view?.profileAttributesClear()
        }, {
            view?.showError(it.localizedMessage)
        })
            .disposeBy(disposable)
    }

    override fun detachView(view: AuthView) {
        super.detachView(view)
        disposable.clear()
    }
}