package com.cordial.android.presentation.custom

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class HeaderFooterAdapter<VH : RecyclerView.ViewHolder, H, T, F>(
    var header: H,
    items: Collection<T>? = null,
    var footer: F
) : RecyclerView.Adapter<VH>() {
    private var items: MutableList<T>? = null
    private var showFooter = true

    val itemsList: MutableList<T>?
        get() = items

    init {
        this.items = (items ?: mutableListOf()) as MutableList<T>?
    }

    @Synchronized
    fun updateItems(newItems: MutableList<T>?, removeOld: Boolean) {
        updateItems(newItems, removeOld, isHead = false)
    }

    @Synchronized
    fun updateItems(newItems: MutableList<T>?, removeOld: Boolean, isHead: Boolean) {
        items?.let { items ->
            val headerShift = if (hasHeader()) 1 else 0
            if (newItems == null) {
                if (items.isNotEmpty()) {
                    //Log.d(LOG_TAG, "updateItems(): 1");
                    val size = items.size
                    items.clear()
                    notifyItemRangeRemoved(headerShift, size)
                }
            } else if (removeOld) {
                if (items.isNotEmpty()) {
                    //Log.d(LOG_TAG, "updateItems(): 4");
                    val prevSize = items.size
                    val newSize = newItems.size
                    this.items = newItems
                    when {
                        prevSize < newSize -> {
                            notifyItemRangeChanged(headerShift, prevSize)
                            notifyItemRangeInserted(prevSize + headerShift, newSize - prevSize)
                        }
                        prevSize > newSize -> {
                            notifyItemRangeChanged(headerShift, prevSize)
                            notifyItemRangeRemoved(prevSize + headerShift, prevSize - newSize)
                        }
                        else -> notifyItemRangeChanged(headerShift, prevSize)
                    }
                } else {
                    //Log.d(LOG_TAG, "updateItems(): 5");
                    this.items = newItems
                    notifyItemRangeInserted(headerShift, newItems.size)
                }
            } else {
                //Log.d(LOG_TAG, "updateItems(): 6");
                if (isHead) {
                    items.addAll(headerShift, newItems)
                    notifyItemRangeInserted(headerShift, newItems.size)
                } else {
                    val insertPositionStart = items.size + headerShift
                    items.addAll(newItems)
                    notifyItemRangeInserted(insertPositionStart, newItems.size)
                }
            }
        }
    }

    @Synchronized
    fun updateItemsHard(newItems: MutableList<T>?, removeOld: Boolean) {
        items?.let {
            when {
                newItems == null -> {
                    it.clear()
                    notifyDataSetChanged()
                }
                removeOld -> {
                    items = newItems
                    notifyDataSetChanged()
                }
                else -> {
                    it.addAll(newItems)
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun deleteItem(position: Int) {
        var pos = position
        if (hasHeader()) pos++
        itemsList?.remove(getItem(pos))
        notifyItemRemoved(pos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val viewHolder: VH? = when {
            isHeaderType(viewType) -> {
                onCreateHeaderViewHolder(parent, viewType)
            }
            isFooterType(viewType) -> {
                onCreateFooterViewHolder(parent, viewType)
            }
            else -> {
                onCreateItemViewHolder(parent, viewType)
            }
        }
        return viewHolder!!
    }

    protected open fun onCreateHeaderViewHolder(parent: ViewGroup, viewType: Int): VH? {
        return null
    }

    protected abstract fun onCreateItemViewHolder(parent: ViewGroup, viewType: Int): VH

    protected open fun onCreateFooterViewHolder(parent: ViewGroup, viewType: Int): VH? {
        return null
    }

    override fun getItemCount(): Int {
        var size = 0
        items?.let {
            size = it.size

            if (hasHeader()) {
                size++
            }
            if (hasFooter()) {
                size++
            }
        }
        return size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        when {
            isHeaderPosition(position) -> {
                onBindHeaderViewHolder(holder, position)
            }
            isFooterPosition(position) -> {
                onBindFooterViewHolder(holder, position)
            }
            else -> {
                onBindItemViewHolder(holder, position)
            }
        }
    }

    override fun onBindViewHolder(holder: VH, position: Int, payloads: List<Any>) {
        when {
            isHeaderPosition(position) -> {
                onBindHeaderViewHolder(holder, position, payloads)
            }
            isFooterPosition(position) -> {
                onBindFooterViewHolder(holder, position, payloads)
            }
            else -> {
                onBindItemViewHolder(holder, position, payloads)
            }
        }
    }

    protected open fun onBindHeaderViewHolder(holder: VH, position: Int) {}

    protected open fun onBindFooterViewHolder(holder: VH, position: Int) {}

    protected abstract fun onBindItemViewHolder(holder: VH, position: Int)


    protected open fun onBindHeaderViewHolder(holder: VH, position: Int, payloads: List<Any>) {
        onBindHeaderViewHolder(holder, position)
    }

    protected fun onBindItemViewHolder(holder: VH, position: Int, payloads: List<Any>) {
        onBindItemViewHolder(holder, position)
    }

    protected open fun onBindFooterViewHolder(holder: VH, position: Int, payloads: List<Any>) {
        onBindFooterViewHolder(holder, position)
    }

    override fun getItemViewType(position: Int): Int {
        var viewType =
            TYPE_ITEM
        if (isHeaderPosition(position)) {
            viewType =
                TYPE_HEADER
        } else if (isFooterPosition(position)) {
            viewType =
                TYPE_FOOTER
        }
        return viewType
    }

    fun getItem(position: Int): T {
        var pos = position
        if (hasHeader() && hasItems()) {
            --pos
        }
        return items!![pos]
    }

    fun showFooter() {
        if (footer != null) {
            val headerShift = if (hasHeader()) 1 else 0
            if (!showFooter) {
                this.showFooter = true
                notifyItemInserted(items?.size ?: 0 + headerShift)
            } else {
                notifyItemChanged(items?.size ?: 0 + headerShift)
            }
        }
    }

    fun hideFooter() {
        if (hasFooter()) {
            this.showFooter = false
            val shift = if (hasHeader()) 1 else 0
            notifyItemRemoved(items?.size ?: 0 + shift)
        } else {
            this.showFooter = false
        }
    }

    fun isHeaderPosition(position: Int): Boolean {
        return hasHeader() && position == 0
    }

    fun isFooterPosition(position: Int): Boolean {
        val lastPosition = itemCount - 1
        return hasFooter() && position == lastPosition
    }

    protected fun isHeaderType(viewType: Int): Boolean {
        return viewType == TYPE_HEADER
    }

    protected fun isFooterType(viewType: Int): Boolean {
        return viewType == TYPE_FOOTER
    }


    fun hasHeader(): Boolean {
        return header != null
    }

    fun hasFooter(): Boolean {
        return footer != null && showFooter
    }

    private fun hasItems(): Boolean {
        return items?.size ?: 0 > 0
    }

    fun getItemPosition(item: T): Int {
        items?.let {
            for (i in it.indices) {
                if (it[i] == item) {
                    return i
                }
            }
        }
        return -1
    }

    companion object {

        const val TYPE_HEADER = -2
        const val TYPE_ITEM = -1
        const val TYPE_FOOTER = -3
    }
}