package com.cordial.android.presentation.inboxmessage

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import com.cordial.android.R
import com.cordial.android.databinding.ActivityInboxMessageBinding
import com.cordial.android.presentation.inbox.InboxMessageModelView
import com.cordial.android.util.Const
import com.cordial.api.CordialApi
import com.cordial.api.CordialInboxMessageApi
import com.cordial.util.ScreenUtils
import org.json.JSONObject

class InboxMessageActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInboxMessageBinding

    private var inboxMessageModelView: InboxMessageModelView? = null
    private val cordialInboxMessageApi = CordialInboxMessageApi()
    private val cordialApi = CordialApi()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityInboxMessageBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initInboxMessageModelView()
        configureActionBar()
        initWebView()
        saveMcID()
        sendInboxMessageReadEvent()
        fetchInboxMessageContent()
        setOnBackPressListener()
    }

    private fun initInboxMessageModelView() {
        inboxMessageModelView = intent.getSerializableExtra(Const.INBOX_MESSAGE_MODEL_VIEW) as InboxMessageModelView?
    }

    private fun configureActionBar() {
        val title = resources.getString(R.string.inbox_message)
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    @SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
    private fun initWebView() {
        with(binding) {
            wvInboxMessageContent.isHapticFeedbackEnabled = false
            wvInboxMessageContent.settings.loadWithOverviewMode = true
            wvInboxMessageContent.settings.useWideViewPort = false
            wvInboxMessageContent.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
            wvInboxMessageContent.settings.javaScriptEnabled = true
            overrideUrlLoading()
            setWebViewScale()
        }
    }

    private fun overrideUrlLoading() {
        binding.wvInboxMessageContent.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = request?.url.toString()
                cordialApi.openDeepLink(deepLinkUrl = url)
                return true
            }
        }
    }

    private fun setWebViewScale() {
        binding.wvInboxMessageContent.setInitialScale(
            ScreenUtils.getInitialScaleByWidth(this, ScreenUtils.getScreenWidth(this))
        )
    }

    private fun loadHtmlIntoWebView(html: String) {
        binding.wvInboxMessageContent.loadDataWithBaseURL(
            null,
            html,
            "text/html; charset=utf-8",
            "UTF-8",
            null
        )
    }

    private fun saveMcID() {
        inboxMessageModelView?.let { inboxMessage ->
            cordialApi.setMcID(inboxMessage.mcID)
        }
    }

    private fun sendInboxMessageReadEvent() {
        inboxMessageModelView?.let { inboxMessage ->
            cordialInboxMessageApi.sendInboxMessageReadEvent(inboxMessage.mcID)
            if (!inboxMessage.isRead) {
                cordialInboxMessageApi.markInboxMessagesRead(listOf(inboxMessage.mcID))
                inboxMessage.isRead = true
                invalidateOptionsMenu()
            }
        }
    }

    private fun fetchInboxMessageContent() {
        with(binding) {
            pbContentLoading.visibility = View.VISIBLE
            inboxMessageModelView?.let { inboxMessageModelView ->
                cordialInboxMessageApi.fetchInboxMessageContent(
                    inboxMessageModelView.mcID,
                    onSuccess = { content ->
                        pbContentLoading.visibility = View.GONE
                        val inboxContent = getInboxContent(content)
                        loadHtmlIntoWebView(inboxContent)
                    },
                    onFailure = { error ->
                        pbContentLoading.visibility = View.GONE
                        Toast.makeText(this@InboxMessageActivity, error, Toast.LENGTH_SHORT).show()
                    })
            }
        }
    }

    private fun getInboxContent(json: String): String {
        val jsonObject = JSONObject(json)
        return jsonObject.optString("html")
    }

    private fun setOnBackPressListener() {
        onBackPressedDispatcher.addCallback(object: OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                inboxMessageModelView?.let { inboxMessage ->
                    val intent = Intent()
                    intent.putExtra(Const.INBOX_MESSAGE_MODEL_VIEW, inboxMessage)
                    setResult(Activity.RESULT_OK, intent)
                }
                isEnabled = false
                onBackPressedDispatcher.onBackPressed()
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_inbox_message, menu)
        inboxMessageModelView?.let { inboxMessage ->
            menu?.findItem(R.id.menu_unread)?.isVisible = inboxMessage.isRead
            menu?.findItem(R.id.menu_read)?.isVisible = !inboxMessage.isRead
        }
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.menu_read -> {
                updateInboxMessageReadStatus(true)
                true
            }
            R.id.menu_unread -> {
                updateInboxMessageReadStatus(false)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun updateInboxMessageReadStatus(isRead: Boolean) {
        inboxMessageModelView?.let { inboxMessage ->
            if (isRead) {
                cordialInboxMessageApi.markInboxMessagesRead(listOf(inboxMessage.mcID))
                inboxMessage.isRead = true
            } else {
                cordialInboxMessageApi.markInboxMessagesUnread(listOf(inboxMessage.mcID))
                inboxMessage.isRead = false
            }
            invalidateOptionsMenu()
        }
    }
}