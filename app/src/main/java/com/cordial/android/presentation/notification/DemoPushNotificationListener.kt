package com.cordial.android.presentation.notification

import android.content.Context
import android.content.Intent
import com.cordial.android.presentation.auth.AuthActivity
import com.cordial.feature.notification.CordialPushNotificationListener
import com.google.firebase.messaging.RemoteMessage

class DemoPushNotificationListener(private val context: Context?) :
    CordialPushNotificationListener {
    override fun onNotificationReceived(remoteMessage: RemoteMessage, isReceivedInForeground: Boolean) {
//        Log.d("CordialSdkLog", "demo app message received ${remoteMessage?.data?.get("mcID")}")
    }

    override fun onNotificationClicked(remoteMessage: RemoteMessage) {
//        Log.d("CordialSdkLog", "demo app message clicked ${remoteMessage?.data?.get("mcID")}")
        val intent = Intent(context, AuthActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context?.startActivity(intent)
    }

    override fun onNotificationDismissed(remoteMessage: RemoteMessage) {
//        Log.d("CordialSdkLog", "demo app message dismissed ${remoteMessage?.data?.get("mcID")}")
    }

    override fun onFcmTokenReceived(token: String) {
    }
}