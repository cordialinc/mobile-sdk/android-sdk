package com.cordial.android.presentation.addattribute

import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.presentation.model.attribute.ProfileAttribute
import com.cordial.android.presentation.updateattributes.ProfileAttributeUseCase
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class AddAttributePresenter(
    private val profileAttributeUseCase: ProfileAttributeUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<AddAttributeView>() {

    private val disposable = CompositeDisposable()

    fun saveAttribute(attribute: ProfileAttribute) {
        profileAttributeUseCase.saveAttribute(attribute).observeOn(observeOnSchedulers)
            .subscribe({
                view?.attributeIsSaved(attribute)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    fun updateAttribute(attribute: ProfileAttribute) {
        profileAttributeUseCase.updateAttribute(attribute).observeOn(observeOnSchedulers)
            .subscribe({
                view?.attributeIsSaved(attribute)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    fun deleteAttribute(deleteAttribute: ProfileAttribute, newAttribute: ProfileAttribute) {
        profileAttributeUseCase.deleteAttribute(deleteAttribute.key).observeOn(observeOnSchedulers)
            .subscribe({
                view?.attributeIsDeleted(deleteAttribute, newAttribute)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }

    override fun detachView(view: AddAttributeView) {
        disposable.clear()
        super.detachView(view)
    }
}