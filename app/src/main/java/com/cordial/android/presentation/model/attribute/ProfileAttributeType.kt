package com.cordial.android.presentation.model.attribute

import java.io.Serializable

enum class ProfileAttributeType(val type: String, val typePosition: Int) : Serializable {
    STRING("String", 0),
    NUMERIC("Numeric", 1),
    BOOLEAN("Boolean", 2),
    ARRAY("Array", 3),
    DATE("Date", 4),
    GEO("Geo", 5);

    companion object {
        fun findType(typePosition: Int): ProfileAttributeType {
            return values().firstOrNull { it.typePosition == typePosition } ?: STRING
        }
    }
}