package com.cordial.android.presentation.cart

import com.cordial.android.domain.model.product.CartItemDomainModel
import com.cordial.android.presentation.model.product.CartItemPresentationModel
import com.cordial.android.presentation.model.product.ProductPresentationModel
import io.reactivex.Completable
import io.reactivex.Single
import java.util.*

interface CartUseCase {
    fun addProductToCart(productPresentationModel: ProductPresentationModel, timestamp: Date?): Completable
    fun getAllCartProducts(): Single<List<CartItemDomainModel>>
    fun deleteProductFromCart(cartItemPresentationModel: CartItemPresentationModel): Completable
    fun clearCart(): Completable
}