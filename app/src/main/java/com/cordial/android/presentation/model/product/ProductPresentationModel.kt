package com.cordial.android.presentation.model.product

import java.io.Serializable

class ProductPresentationModel(
    val id: Int,
    val img: Int,
    val brand: String,
    val name: String,
    val price: Double,
    val sku: String,
    val shortDesc: String,
    val path: List<String>,
    val imgUrl: String
) : Serializable