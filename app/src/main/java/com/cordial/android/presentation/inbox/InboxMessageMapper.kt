package com.cordial.android.presentation.inbox

import com.cordial.feature.inboxmessage.InboxMessage

object InboxMessageMapper {

    fun inboxMessagesToModelView(inboxMessages: List<InboxMessage>): List<InboxMessageModelView> {
        return inboxMessages.map { inboxMessage ->
            inboxMessageToModelView(inboxMessage)
        }
    }

    fun inboxMessageToModelView(inboxMessage: InboxMessage) = InboxMessageModelView(
        inboxMessage.mcID,
        inboxMessage.isRead,
        inboxMessage.sentAt,
        inboxMessage.previewData
    )
}