package com.cordial.android.presentation.catalogcategory

import com.cordial.android.presentation.cart.CartUseCase
import com.cordial.android.presentation.custom.BasePresenter
import com.cordial.android.presentation.model.PresentationMapper
import com.cordial.android.presentation.model.product.ProductPresentationModel
import com.cordial.android.util.extension.disposeBy
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import java.util.*

class ProductPresenter(
    private val cartUseCase: CartUseCase,
    private val observeOnSchedulers: Scheduler
) : BasePresenter<ProductView>() {

    private val disposable = CompositeDisposable()

    fun addProductToCart(product: ProductPresentationModel, timestamp: Date?) {
        cartUseCase.addProductToCart(product, timestamp)
            .observeOn(observeOnSchedulers)
            .subscribe({
                view?.productCartAdded()
            }, {
                view?.showError(it.localizedMessage)
            }).disposeBy(disposable)
    }

    fun getAllCartProducts() {
        cartUseCase.getAllCartProducts().observeOn(observeOnSchedulers)
            .map {
                PresentationMapper.cartItemsToDomainModel(it)
            }
            .subscribe({
                view?.showCartProducts(it)
            }, {
                view?.showError(it.localizedMessage)
            })
            .disposeBy(disposable)
    }


    override fun detachView(view: ProductView) {
        super.detachView(view)
        disposable.clear()
    }
}