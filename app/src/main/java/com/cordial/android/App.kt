package com.cordial.android

import android.content.Context
import android.content.IntentFilter
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.cordial.android.presentation.Injector
import com.cordial.android.presentation.deeplink.DemoDeepLinkListener
import com.cordial.android.presentation.inappmessage.DemoInAppMessageInputsListener
import com.cordial.android.presentation.logs.CordialDemoLogger
import com.cordial.android.presentation.notification.DemoPushNotificationListener
import com.cordial.android.presentation.receiver.EventsReceiver
import com.cordial.android.presentation.settings.PresetType
import com.cordial.android.util.Const
import com.cordial.android.util.preference.AppPreferenceKeys
import com.cordial.android.util.preference.PreferenceHelper
import com.cordial.api.C
import com.cordial.api.CordialApiConfiguration
import com.cordial.feature.log.CordialLogLevel
import com.cordial.feature.notification.PushesConfiguration
import com.cordial.feature.notification.model.NotificationCategory
import com.cordial.feature.notification.permission.model.EducationalUiModeEnum
import com.cordial.feature.sendevent.model.property.PropertyValue

class App : MultiDexApplication() {

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(base)
    }

    override fun onCreate() {
        super.onCreate()
        initSdk()

        createInjector()
        injector?.createAppComponent(applicationContext)
    }

    private fun initSdk() {
        val preferenceHelper = PreferenceHelper(this)
        val accountKey = if (preferenceHelper.contains(AppPreferenceKeys.ACCOUNT_KEY))
            preferenceHelper.getString(AppPreferenceKeys.ACCOUNT_KEY) else PresetType.STAGING.accountKey
        val channelKey = if (preferenceHelper.contains(AppPreferenceKeys.CHANNEL_KEY))
            preferenceHelper.getString(AppPreferenceKeys.CHANNEL_KEY) else PresetType.STAGING.channelKey
        val eventsStreamUrl = if (preferenceHelper.contains(AppPreferenceKeys.HOST))
            preferenceHelper.getString(AppPreferenceKeys.HOST) else PresetType.STAGING.eventsStreamUrl
        val messageHubUrl = if (preferenceHelper.contains(AppPreferenceKeys.MESSAGE_HUB_HOST))
            preferenceHelper.getString(AppPreferenceKeys.MESSAGE_HUB_HOST) else null
        val qty = if (preferenceHelper.contains(AppPreferenceKeys.QTY))
            preferenceHelper.getInt(AppPreferenceKeys.QTY) else PresetType.STAGING.qtyCached
        val config = CordialApiConfiguration.getInstance()
        val pushNotificationListener = DemoPushNotificationListener(applicationContext)
        val deepLinkListener = DemoDeepLinkListener()
        val inAppMessageInputsListener = DemoInAppMessageInputsListener()
        setupBulkAndInboxCacheSettings(config, PreferenceHelper(this))
        config.pushesConfiguration = PushesConfiguration.SDK
        config.pushNotificationListener = pushNotificationListener
        config.pushNotificationIconSilhouette = R.drawable.ic_cordial_silhouette
        config.deepLinkListener = deepLinkListener
        config.vanityDomains = listOf(
            "e.a45.clients.cordialdev.com",
            "s.cordial.com",
            "s.a1105.clients.cordialdev.com",
            "s.a1003.clients.cordialdev.com",
            "events-handling-svc.stg.cordialdev.com",
            "e.a1003.clients.cordialdev.com"
        )
        config.inAppMessageInputsListener = inAppMessageInputsListener
        config.deepLinkWebOnlyFragments = listOf("webonly")
        config.initialize(applicationContext, accountKey, channelKey, eventsStreamUrl, messageHubUrl)
        config.setQty(qty)
        config.setSystemEventProperties(
            mapOf(
                "deepLinkUrl" to PropertyValue.StringProperty(Const.CORDIAL_WEBSITE),
                "fallbackUrl" to PropertyValue.StringProperty(Const.CORDIAL_SUPPORT_WEBSITE)
            )
        )
        setNotificationCategories()
        config.inAppMessages.showOnPushDismiss = false
        config.inAppMessages.displayDelayInSeconds = 2.5

        val intentFilter = IntentFilter(C.FAILED_EVENTS)
        LocalBroadcastManager.getInstance(this).registerReceiver(EventsReceiver(), intentFilter)

        // Initialize Cordial Demo Logger
        val cordialDemoLogger = CordialDemoLogger(applicationContext)
        config.setLogger(cordialDemoLogger)
        config.logLevel = CordialLogLevel.ALL
    }

    private fun setupBulkAndInboxCacheSettings(config: CordialApiConfiguration, preferenceHelper: PreferenceHelper) {
        config.eventsBulkSize = if (PresetType.findPresetType(
                preferenceHelper.getString(
                    AppPreferenceKeys.PRESET,
                    ""
                )
            ) == PresetType.CUSTOM && preferenceHelper.contains(AppPreferenceKeys.EVENTS_BULK_SIZE)
        )
            preferenceHelper.getInt(AppPreferenceKeys.EVENTS_BULK_SIZE)
        else PresetType.STAGING.eventsBulkSize
        config.eventsBulkUploadInterval = if (PresetType.findPresetType(
                preferenceHelper.getString(
                    AppPreferenceKeys.PRESET,
                    ""
                )
            ) == PresetType.CUSTOM && preferenceHelper.contains(AppPreferenceKeys.EVENTS_BULK_UPLOAD_INTERVAL)
        )
            preferenceHelper.getInt(AppPreferenceKeys.EVENTS_BULK_UPLOAD_INTERVAL)
        else PresetType.STAGING.eventsBulkUploadInterval

        if (preferenceHelper.contains(AppPreferenceKeys.MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE))
            config.inboxMessageCache.maxCacheSize = preferenceHelper.getInt(
                AppPreferenceKeys.MAX_INBOX_MESSAGE_CONTENT_CACHE_SIZE
            )
        if (preferenceHelper.contains(AppPreferenceKeys.MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE))
            config.inboxMessageCache.maxCacheableMessageSize = preferenceHelper.getInt(
                AppPreferenceKeys.MAX_CACHEABLE_INBOX_MESSAGE_CONTENT_SIZE
            )
    }

    private fun setNotificationCategories() {
        CordialApiConfiguration.getInstance().setNotificationCategories(
            listOf(
                NotificationCategory(id = "discounts", name = "Discounts", state = false, description = "Discounts"),
                NotificationCategory(
                    id = "new-arrivals",
                    name = "New Arrivals",
                    state = true,
                    description = "New Arrivals"
                ),
                NotificationCategory(
                    id = "top-products",
                    name = "Top Products",
                    state = true,
                    description = "Top Products"
                )
            )
        )
        CordialApiConfiguration.getInstance().educationalUiMode = EducationalUiModeEnum.NOTIFICATION_CATEGORIES
    }

    companion object {
        var injector: Injector? = null
            private set

        fun createInjector() {
            injector = Injector()
        }
    }
}