plugins {
    alias(libs.plugins.android.application)
    alias(libs.plugins.kotlin.android)
    alias(libs.plugins.gms)
    alias(libs.plugins.crashlytics)
    alias(libs.plugins.ksp)
}

android {
    namespace = "com.cordial.android"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.cordialdemo.android"

        minSdk = 21
        targetSdk = 34

        versionCode = 140
        versionName = "6.1.5"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        multiDexEnabled = true
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(projects.cordialsdk)

    // Android libs
    implementation(libs.android.support.multidex)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.recyclerview)
    implementation(libs.android.material)
    implementation(libs.android.stdlib.jdk7)


    // Firebase
    implementation(platform(libs.firebase.bom))
    implementation(libs.firebase.core)
    implementation(libs.firebase.messaging.ktx)
    implementation(libs.firebase.gcm)

    // Room
    implementation(libs.androidx.room.runtime)
    implementation(libs.androidx.room.rxjava)
    implementation(libs.androidx.room.testing)
    ksp(libs.androidx.room.compiler)

    // ReactiveX
    implementation(libs.rxjava.core)
    implementation(libs.rxjava.android)

    // Dependency Injection
    implementation(libs.dagger.core)
    ksp(libs.dagger.compiler)

    implementation(libs.materialdatetimepicker)
    implementation(libs.glide)

    testImplementation(libs.test.junit)
    androidTestImplementation(libs.androidx.test.runner)
    androidTestImplementation(libs.androidx.espresso)
}