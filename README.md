# Android Cordial SDK Documentation
##  Contents
* [Install SDK](#install-sdk)
* [Initialize SDK](#initialize-sdk)
* [Contacts](#contacts)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Setting a Contact](#setting-a-contact)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Unsetting a Contact](#unsetting-a-contact)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Updating Attributes and Lists Memeberships](#updating-attributes-and-lists-memeberships)<br>
* [Events](#events)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Tracking System Events](#tracking-system-events)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Sending an Event](#sending-an-event)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Event Caching](#event-caching)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Events Bulking](#events-bulking)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Events Flushing](#events-flushing)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Configuring Location Tracking Updates](#configuring-location-tracking-updates)<br>
* [Push Notifications](#push-notifications)
&nbsp;&nbsp;&nbsp;&nbsp;[Push Notification Categories](#push-notification-categories)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Push Notification Permission Educational UI](#push-notification-permission-educational-ui)<br>
* [In-App Messages](#in-app-messages)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[In-App Messages Link Actions](#in-app-messages-link-actions)<br>
&nbsp;&nbsp;&nbsp;&nbsp;[Delaying In-App Messages](#delaying-in-app-messages)<br>
* [Inbox Messages](#inbox-messages)
* [Deep Linking](#deep-linking)
* [Message Attribution](#message-attribution)
* [Revenue Attribution for Web View Applications](#revenue-attribution-for-web-view-applications)
* [Setting a Contact Cart](#setting-a-contact-cart)
* [Sending an Order](#sending-an-order)
* [Logging](#logging)
&nbsp;&nbsp;&nbsp;&nbsp;[Log Level](#log-level)<br>
* [Using Cordial SDK From Flutter](#using-cordial-sdk-from-flutter)
* [Updating major SDK versions](#updating-major-sdk-versions)

## Install SDK
Follow these steps to install the SDK:

1. Declare the Maven Central repository in your `Top-level` `build.gradle` file:
```
repositories {
   mavenCentral()
}
```
2. Add kotlin plugin to your `Top-level` `build.gradle` file:
 ```
 dependencies {
     ...
     classpath 'org.jetbrains.kotlin:kotlin-gradle-plugin:1.5.30'
 }
 ```
3. Add the `cordialsdk` dependency to your `app` module level `build.gradle` file:
```
implementation 'com.cordial.cordialsdk:android:3.0.0'
```

## Initialize SDK

Please contact your Customer Success Manager (CSM) at Cordial to obtain the URLs for the Message Hub Service and the Event Stream Service.

Before initializing the SDK make sure that you added `google-services.json` file from the Firebase project to your Android application as described here: https://firebase.google.com/docs/android/setup

In order to initialize the Android SDK, specify `pushesConfiguration` value and pass needed parameters to `CordialApiConfiguration.initialize` method and call it from the `onCreate()` method of your Application class:

```kotlin
CordialApiConfiguration.getInstance().pushesConfiguration = PushesConfiguration.SDK
CordialApiConfiguration.getInstance().initialize(applicationContext, accountKey = "your_account_key", channelKey = "your_channel_key", eventsStreamUrl= "your_events_stream_url", messageHubUrl = "your_message_hub_url")
```

_For details on `pushesConfiguration` see **Push Notifications** section below. In short, `PushesConfiguration.SDK` value tells the SDK that it should auto configure push notifications without any additional code from application._

After fully initialized, the SDK will automatically start tracking system events as they occur in the application. Those events include:

* App opens and closes
* App installs
* App opens via notification tap
* Push notification dismissals
* Receipts of push notification while the app is in the foreground
* Receipt of push notification while the app is in the background
* App opens via deep link/Android app link
* In-app message is shown
* In-app message is manually dismissed
* In-app message is automatically dismissed

In addition to automatically tracked events, the SDK allows developers to make Cordial specific calls. Those calls include:

* Updating a contact
* Sending a custom event
* Sending a contact cart
* Sending an order

The access point for the above calls is the `CordialApi` class. You can either have a global reference to the object of the class or create an object for every action - that choice is left up to your application.
```kotlin
val cordialApi = CordialApi()
```

## Contacts
### Setting a Contact

Every app is assumed to be operating on behalf of a specific contact. Contact is a user of the client application. For example, if Amazon is the client, each user of Amazon who logs in is a contact. Every contact must have a primary key. Naturally, when the app is installed, there is no contact associated with the app as the user might not have logged in yet. In this case, identifying a contact is done via device ID, which is a unique identifier of the Android device the app is running on.

Every piece of information (system or custom events, updating a contact, etc.) that is passed by SDK to Cordial backend, has a device ID automatically associated with it. Later, when the user logs into the app and their primary key becomes known, the client application must pass that primary key to the backend by calling the setContact method. When the backend receives a contact update with the primary key, it associates the device ID with the primary key of that contact. That association is crucial for effectively using Cordial.

There are two states in the SDK: `Logged in` and `Logged out`. The difference is that the `Logged in` state sends requests and receives push messages, while the `Logged out` state caches requests and does not receive push messages. The `Logged out` state described below in "Unsetting a contact". The `Logged in` state can be with and without a primary key. By default, the SDK is automatically set to `Logged in` state without the primary key.

setContact with primary key usage:
```kotlin
 cordialApi.setContact(primaryKey = "foo@example.com")
```

setContact with secondary key usage:
```kotlin
 cordialApi.setContact(primaryKey = "email:foo@example.com")
```

setContact without key usage:
```kotlin
 cordialApi.setContact(null)
```

In case that the application configures push notifications itself without relying on SDK for that, its push notification token should be passed in to the SDK. Otherwise the SDK will not track any user behaviour in the app. For details see Push Notifications section.

### Unsetting a Contact

Whenever a contact is disassociated with the application, typically due to a logout event, the Cordial SDK should be notified so that contact generated events are no longer associated with their profile. This is done by calling the `unsetContact` method.

```kotlin
cordialApi.unsetContact()
```

### Updating Attributes and Lists Memeberships

In order to udpate a contact's attributes, call the upsertContact method passing it new attributes values, for example:
```kotlin
val attributes = mapOf(
    "name" to StringValue("Jon Doe"),
    "employed" to BooleanValue(true),
    "age" to NumericValue(32),
    "children" to ArrayValue(arrayOf("Sofia", "Jack"))
)
cordialApi.upsertContact(attributes)
```
Adding a contact to a list is done via passing an attribute update with list name as a key and boolean as a value. The boolean means if the contact is added to or removed from the list. The following code makes sure the contact is added to `list1` and removed from `list2`:
```kotlin
val attributes = mapOf("list1" to BooleanValue(true), "list2" to BooleanValue(false))
cordialApi.upsertContact(attributes)
```
The StringValue, NumericValue and DateValue parameters can be set to null
```kotlin
val attributes = mapOf(
    "name" to StringValue(null),
    "age" to NumericValue(null),
    "date" to DateValue(null)
)
cordialApi.upsertContact(attributes)
```

## Events

### Tracking System Events

To add custom properties to system events, call the `setSystemEventProperties` method after initializing the SDK.
```kotlin
CordialApiConfiguration.getInstance().initialize(applicationContext, accountKey: String, channelKey: String)
CordialApiConfiguration.getInstance().setSystemEventProperties(properties: Map<String, PropertyValue>?)
```
`PropertyValue` contains the following implementations:
- StringProperty
- NumericProperty
- BooleanProperty
- JSONObjectProperty
- JSONArrayProperty

### Sending an Event

Besides system events, the SDK allows sending of custom events specific to each app. Those may be, for example, user logged in, discount applied or app preferences updated to name a few. To send an event, use the `CordialApi.sendEvent` method:

```kotlin
cordialApi.sendEvent(eventName: String, properties: Map<String, PropertyValue>?)
```

### Event Caching

Cordial SDK limits the number of events that may be cached. When the limit of cached events is reached, the oldest events are removed and replaced by the incoming events, and will not be resent. By default, the cache limit is set to 1,000 events. To change the limit, pass qty via the `CordialApiConfiguration.setQty` method.

```kotlin
CordialApiConfiguration.getInstance().setQty(qty: Int)
```

### Events Bulking

In order to optimize devices' resource usage, Cordial SDK groups events into bulks and upload them in one request. Each event happened on a device will be added to bulk. The SDK sends a bulk of events in 3 cases:

1. Events limit in bulk is reached. The bulk size is configured via `eventsBulkSize`. Set to 5 by default.
2. The bulk has not been sent for specified time interval. Even if a bulk is not fully populated with events it will be sent every `eventsBulkUploadInterval` in seconds. Bulk upload interval is configured via `eventsBulkUploadInterval`. Set to 30 seconds by default.
3. The application is closed.

```kotlin
CordialApiConfiguration.getInstance().eventsBulkSize = 3
CordialApiConfiguration.getInstance().eventsBulkUploadInterval = 20
```

To disable sending events bulk on timer:

```kotlin
CordialApiConfiguration.getInstance().eventsBulkUploadInterval = null
```

### Events Flushing

The SDK allows to send all cached events immediately. This is done by calling the `flushEvents` method on `CordialApi`.

```kotlin
cordialApi.flushEvents()
```

### Configuring Location Tracking Updates
You can expand custom events data by setting geo locations to be sent with each custom event. To enable the delivery of location-related events to your app, call corresponding method in the `CordialApi` class
```kotlin
cordialApi.setLongLat(longitude: Double, latitude: Double)
```

## Push Notifications

Cordial SDK allows to configure receiving push notifications in two ways:

**Option 1.** Cordial SDK automatically configures push notifications, displays and tracks them. This option is best if the only push notification source for your application is Cordial.

To allow Cordial SDK handling push notifications automatically set `pushesConfiguration` to SDK. This can be done in your Application class, before initializing the Cordial SDK.

```kotlin
CordialApiConfiguration.getInstance().pushesConfiguration = PushesConfiguration.SDK
CordialApiConfiguration.getInstance().initialize(...)
```

**Option 2.** Cordial SDK lets your application configure itself for push notifications and displays and tracks push notifications from Cordial when they are manually passed in by the app. In this case the app should send the token to the SDK once it's received and pass push notifications sent by Cordial to the SDK. This option should be used when you have multiple push notifications sources, for example, if your backend is sending push notifications itself. Note, it is really important to pass the token otherwise the SDK will not be tracking any user behaviour on the device.

To allow the app to configure push notifications itself, set `pushesConfiguration` to APP:

```kotlin
CordialApiConfiguration.getInstance().pushesConfiguration = PushesConfiguration.APP
CordialApiConfiguration.getInstance().initialize(...)
```
To pass a push notification token to the Cordial SDK:

```kotlin
override fun onNewToken(token: String?) {
     ...
     CordialNotificationProcessService().processNewToken(context = this, token)
}
```

The app should pass push notifications sent by Cordial to `CordialNotificationProcessService` class so that the SDK can display and track these notifications' events:

```kotlin
override fun onMessageReceived(remoteMessage: RemoteMessage?) {
     ...
     val cordialNotificationProcessService = CordialNotificationProcessService()
     if (cordialNotificationProcessService.isCordialMessage(remoteMessage))
         cordialNotificationProcessService.processMessage(context = this, remoteMessage)
}
```

**For both options**, you can implement an optional `CordialPushNotificationListener` interface and have its methods called when the following events occur:
* Notification is received (in foreground or in background)
* Notification is tapped
* Notification is dismissed
* Firebase token is received

You can provide Cordial SDK with an instance of the `CordialPushNotificationListener` interface. This can be done in your Application class, where you initialize Cordial SDK:

```kotlin
val pushNotificationListener = YourImplementationOfCordialPushNotificationListener()
CordialApiConfiguration.getInstance().pushNotificationListener = pushNotificationListener
```

###  Push Notification Icon

Android 5.0 and higher uses a silhouette (alpha-only) of your icon for push notifications. See [Android 5.0 changes](https://developer.android.com/about/versions/android-5.0-changes.html#BehaviorNotifications) for details.

To set silhouette icon just pass the resource id of a drawable to `CordialApiConfiguration` class:
```kotlin
CordialApiConfiguration.getInstance().pushNotificationIconSilhouette = R.drawable.ic_cordial_silhouette
```

###  Push Notification Custom Sound

To add custom sound for push notifications, save the audio file in the `/res/raw` folder. After it send the name of the sound file via Sound (Android) custom key from the admin panel.
Note: only push notifications that are displayed by the SDK will have the custom sound played.

### Requesting push notifications permission since Android 13

Since Android 13 (API level 33) a separate permission is required for receiving push notifications: POST_NOTIFICATIONS. To request a permission call `requestNotificationPermission()` on `CordialApi`:
```kotlin
cordialApi.requestNotificationPermission()
```

It displays a permission prompt to allow application to receive push notifications.

#### Upgrading to Android 13 or higher
To minimize disruptions associated with the notification permission, the system automatically pre-grants the permission to the application when a user upgrades their device to Android 13 or higher. In other words, these apps continue to send notifications to users without the permission prompt.

### Push Notification Categories

Push notification categories allow application users to control which types of push notifications they receive. For example, users might choose to receive 'Discounts' notifications but not 'New Arrivals'. Presenting users with a choice of which push notification categories they wish to receive, and communicating this upfront before requesting push notification permission, may drastically improve opt-in rates.
To start using this feature, pass the available push notification categories to the SDK:

```kotlin
CordialApiConfiguration.getInstance().setNotificationCategories(
   listOf(
      NotificationCategory(id = "discounts", name = "Discounts", state = false, description = "Get discounts on your favorite items"),
      NotificationCategory(id = "new-arrivals", name = "New Arrivals", state = true, description = "Don't miss out on new arrivals"),
      NotificationCategory(id = "top-products", name = "Top Products", state = true, description = "Top products of the month")
   )
)
```

### Push Notification Permission Educational UI

Before displaying the system notification permission prompt, you can show an educational UI to explain to users why they should allow the application to send them notifications. You can configure educational UI display using educational UI modes.

There are three types of educational UI modes:

```kotlin
EducationalUiModeEnum.CUSTOM
EducationalUiModeEnum.NOTIFICATION_CATEGORIES
EducationalUiModeEnum.NONE
```

To set the educational UI mode, change this variable in `CordialApiConfiguration`:
```kotlin
CordialApiConfiguration.getInstance().educationalUiMode = EducationalUiModeEnum.CUSTOM
```

#### EducationalUiModeEnum.CUSTOM

With this mode, you can set a custom layout for the educational UI. This can be configured via the `layoutId` parameter in `NotificationPermissionEducationalUISettings`:

```kotlin
CordialApiConfiguration.getInstance().educationalUiMode = EducationalUiModeEnum.CUSTOM

val customEducationalUiLayoutId = R.layout.activity_custom_educational_ui
val educationalUiSettings = NotificationPermissionEducationalUISettings(
   layoutId = customEducationalUiLayoutId,
   color = null,
   showUntilAllowed = true
)
CordialApi().requestNotificationPermission(educationalUiSettings)
```

`NotificationPermissionEducationalUISettings` includes a `showUntilAllowed` parameter, which controls whether to display the educational UI until the user grants permission.

_Note: To configure the educational UI with a custom layout, you must include two button views with these IDs: `R.id.btn_skip` for the skip button, and `R.id.btn_ok` for the OK button.._

#### EducationalUiModeEnum.NOTIFICATION_CATEGORIES

With this mode, the SDK will display the default educational UI, allowing users to enable or disable the notification categories they wish to receive. You can customize the color theme of the default educational UI. This is configured via `color` parameter in `NotificationPermissionEducationalUISettings`:

```kotlin
CordialApiConfiguration.getInstance().educationalUiMode = EducationalUiModeEnum.NOTIFICATION_CATEGORIES

val educationalUiSettings = NotificationPermissionEducationalUISettings(
   layoutId = null,
   color = NotificationPermissionEducationalUiColor(
      primary = R.color.colorPrimary,
      accent = R.color.colorOrange,
      primaryLight = R.color.colorGrey
   ),
   showUntilAllowed = true
)
CordialApi().requestNotificationPermission(educationalUiSettings)
```

#### Educational UI Localization

The SDK allows your application to localize the text in the default educational UI. To do that, change the texts for these SDK's strings:

```
<string name="crdl_notification_permission_educational_ui_title">Get Notified!</string>
<string name="crdl_notification_permission_educational_ui_desc">Do not miss our best offers or personalized suggestions\nSelect categories of notifications that you want to receive</string>
<string name="crdl_notification_permission_educational_ui_skip_btn_text">No Thanks</string>
<string name="crdl_notification_permission_educational_ui_ok_btn_text">I\'m in</string>
```

#### EducationalUiModeEnum.NONE

With this mode, the educational UI will not be displayed before the system notification permission prompt. Also, you do not need to pass the `NotificationPermissionEducationalUISettings` parameter.

```kotlin
CordialApiConfiguration.getInstance().educationalUiMode = EducationalUiModeEnum.NONE

CordialApi().requestNotificationPermission()
```

### Control the display of push notifications when the application is in the foreground

To control the display of push notifications when the application is in the foreground set `showPushInForeground` property in `CordialApiConfiguration`. By default `showPushInForeground` is set to true.

```kotlin
// Display push notifications when the application in the foreground 
CordialApiConfiguration.getInstance().showPushInForeground = true

// Do not display push notifications when the application in the foreground 
CordialApiConfiguration.getInstance().showPushInForeground = false
```

## In-App Messages
### In-App Messages Link Actions
Cordial SDK can handle actions from a designated HTML object.

Using `crdlAction` function you can create buttons that deep link to specific content within your app or send custom events such as cart, browse, discount_applied, and dismissed. To be able to handle deep links from an in-app message, see [Deep Linking](#deep-linking) section.

Using `crdlCaptureAllInputs` function you can capture inputs from input and select html elements and send them as properties of specified custom event. You can provide Cordial SDK with an instance of the `InAppMessageInputsListener` interface to have a callback called signalizing that inputs were captured. The callback is called with inputs values captured and the event name that was generated. This can be done in your Application class, where you initialize Cordial SDK:

```kotlin
val inAppMessageInputsListener= YourImplementationOfInAppMessageInputsListener()
CordialApiConfiguration.getInstance().inAppMessageInputsListener= inAppMessageInputsListener
```

For more information, see Cordial Knowledge Base: https://support.cordial.com/hc/en-us/articles/360046096752#linkActions.

### Delaying In-App Messages
Cordial SDK allows application developers to delay displaying of in-app messages. There are 3 delay modes in the SDK to control in-app messages display:

1. Show. In-app messages are displayed without delay, which is the default behavior.
2. Delayed Show. Displaying in-app messages is delayed until Delayed Show mode is turned off.
3. Disallowed Activities. Displaying in-app messages is not allowed on certain screens, which are determined by the application developer.

Switching to the Show mode is achieved by calling the `show()` method, which optionally takes a parameter identifying when to show the next in-app message. To display an in-app right away, pass the value of `InAppMessageDelayShowType.IMMEDIATELY`, to display an in-app on next app open, pass the value of `InAppMessageDelayShowType.NEXT_APP_OPEN` or just call `show()` without parameter, which is the default behaviour.

To switch between modes, call corresponding methods in the `CordialApiConfiguration` class:
```kotlin
CordialApiConfiguration.getInstance().inAppMessageDelayMode.show()
CordialApiConfiguration.getInstance().inAppMessageDelayMode.show(InAppMessageDelayShowType.IMMEDIATELY)
CordialApiConfiguration.getInstance().inAppMessageDelayMode.show(InAppMessageDelayShowType.NEXT_APP_OPEN)
CordialApiConfiguration.getInstance().inAppMessageDelayMode.delayedShow()
CordialApiConfiguration.getInstance().inAppMessageDelayMode.disallowedActivities(disallowedActivities: List<Class<out Activity>>)
```

### Hiding In-App Messages on Push Notification Dismiss
To control in-app messages display when push notification is dismissed set corresponding property in the `CordialApiConfiguration` class:

```kotlin
CordialApiConfiguration.getInstance().inAppMessages.showOnPushDismiss = false
```

### Configuring In-App Messages Display Delay
To configure in-app messages display delay in seconds set corresponding property in the `CordialApiConfiguration` class:

```kotlin
CordialApiConfiguration.getInstance().inAppMessages.displayDelayInSeconds = 0.5
```

## Inbox Messages

To work with inbox messages you will have to use the `CordialInboxMessageApi` and `CordialInboxMessageListener` classes. They are the entry points to all inbox messages related functionality. The api supports the following operations:

### Fetch all inbox messages for currently logged in contact

```kotlin
cordialInboxMessageApi.fetchInboxMessages(
    pageRequest: PageRequest,
    onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
    onFailure: (error: String) -> Unit
)
```

`InboxMessage` represents one inbox message, containing its mcID, metadata, if the message is read and when it was sent. The metadata is a special field which is populated in the admin panel when creating message content. It is specific to each message and should contain the data to be used when a page of inbox messages is loaded. For example, it may contain image thumbnail, title and subtitle to generate a preview without loading inbox message content.

To filter inbox messages, pass an `InboxFilterParams` instance to the `fetchInboxMessages` method:
```kotlin
cordialInboxMessageApi.fetchInboxMessages(
    pageRequest: PageRequest,
    inboxFilterParams: InboxFilterParams,
    onSuccess: (inboxMessages: Page<InboxMessage>) -> Unit,
    onFailure: (error: String) -> Unit
)
```
InboxFilterParams contains the following filter parameters:

- If the inbox message is read
- If the inbox message was sent before the specified date
- If the inbox message was sent after the specified date

### Fetch one specific inbox message for currently logged in contact

To fetch one specific inbox message, call

```kotlin
cordialInboxMessageApi.fetchInboxMessage(
    mcID: String,
    onSuccess: (inboxMessage: InboxMessage) -> Unit,
    onFailure: (error: String) -> Unit
)
```

### Fetch inbox message content:

To get inbox message content, call

```kotlin
cordialInboxMessageApi.fetchInboxMessageContent(
    mcID: String,
    onSuccess: (content: String) -> Unit,
    onFailure: (error: String) -> Unit
)
```

### Send up an inbox message is read event.

```kotlin
cordialInboxMessageApi.sendInboxMessageReadEvent(mcID: String)
```

This is the method to be called to signal a message is read by the user and should be triggered every time a contact reads (or opens) a message.

### Mark a message as read/unread

This operations actually marks a message as read or unread which toggles the `read` flag on the corresponding `InboxMessage` object.

To mark messages as read:

```kotlin
cordialInboxMessageApi.markInboxMessagesRead(mcIDs: List<String>)
```

To mark messages as unread:

```kotlin
cordialInboxMessageApi.markInboxMessagesUnread(mcIDs: List<String>)
```

### To delete an inbox message:

To remove an inbox message from user's inbox, call

```kotlin
cordialInboxMessageApi.deleteInboxMessage(mcID: String)
```

### Notifications about new incoming inbox message

The SDK can notify when a new inbox message has been delivered to the device. In order to be notified set a **CordialInboxMessageListener**:

```kotlin
val inboxMessageListener = YourCordialInboxMessageListenerImplementation()
CordialApiConfiguration.getInstance().inboxMessageListener = inboxMessageListener

```
### Inbox messages cache
The SDK caches inbox messages in order to limit the number of request the SDK makes. To control the size of the cache so that it doesn't grow unlimited the SDK configures the cache with two values:

- max size of each inbox message in bytes. Messages bigger than max size will not be cached. Default value is 200 kB.
- total cache size in bytes. As soon as the cache reaches it max size, the SDK will replace the least used inbox messages with the new ones. Default value is 10 MB.

To override default values, set them via:
```kotlin
CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheableMessageSize = 200 * 1024
CordialApiConfiguration.getInstance().inboxMessageCache.maxCacheSize = 10 * 1024 * 1024
```

## Deep Linking

Cordial SDK is capable of tracking deep links and app link opens. There are two ways to let Cordial SDK do that:

1. Let Cordial SDK handle deep link opens and then route that fact to your code via the listener your set:

    - Register `DeepLinkHandlerActivity` in your `AndroidManifest` file. Add intent filters for all deep link domains inside this activity declaration. For example:

            <activity
                android:name="com.cordialdemo.cordialsdk.deeplink.DeepLinkHandlerActivity">
                <intent-filter>
                    <action android:name="android.intent.action.VIEW" />
                    <category android:name="android.intent.category.DEFAULT" />
                    <category android:name="android.intent.category.BROWSABLE" />
                    <data android:host="tjs.cordialdev.com" />
                    <data android:scheme="https" />
                </intent-filter>
                <intent-filter>
                    <action android:name="android.intent.action.VIEW" />
                    <category android:name="android.intent.category.DEFAULT" />
                    <category android:name="android.intent.category.BROWSABLE" />
                    <data android:host="your-vanity-domain1.com" />
                    <data android:scheme="https" />
                </intent-filter>
            </activity>

    - Provide Cordial SDK with an instance of the `CordialDeepLinkOpenListener` interface. This should be done in the Application class where you initialize Cordial SDK:
       ```kotlin
            val deepLinkListener = YourImplementationOfCordialDeepLinkOpenListener()
            CordialApiConfiguration.getInstance().deepLinkListener = deepLinkListener
      ```
    - The `CordialDeepLinkOpenListener` interface provides a method with a `context`, `uri` and `fallbackUri` as parameters. In this method, you can start the desired activities and pass data from URI fields.
        ```kotlin
             fun appOpenViaDeepLink(context: Context?, cordialDeepLink : CordialDeepLink, fallBackUri: Uri?, onComplete: ((action: DeepLinkAction) -> Unit)?)
        ```
2. Let your application handle deep link opens and route that information to Cordial SDK. In this case, you can add intent filters at any point in time, and you should call the appropriate `CordialApi` method:
```kotlin
cordialApi.openDeepLink(deepLinkUrl: String, fallbackUrl: String?)
```

### Configure vanity domains for link tracking
In order for SDK to support opening deep links with tracking on the SDK should be configured with links vanity domain. Vanity domain to be provided by Cordial.

In order to configure the SDK with a vanity domain:

1. Configure your vanity domain as deep link. See [Deep Linking](#deep-linking) section.
2. The domain should be added to SDK as a vanity domain:
```kotlin
CordialApiConfiguration.getInstance().vanityDomains = listOf("your-vanity-domain1.com", "your-vanity-domain2.com")
```

### Have the SDK opening deep links unknown to the application

In case the SDK calls `appOpenViaDeepLink` with a deep link url that the application doesn't know how to handle, the app should ask the SDK to open the deep link in a web browser. The application can open the deep link in a web browser itself but in this case revenue attribution flow may be lost. In order to tell the SDK to open an unknown deep link, call `onComplete` callback in your `appOpenViaDeepLink` method, passing it `OPEN_IN_BROWSER` option:

```kotlin
onComplete?.invoke(DeepLinkAction.OPEN_IN_BROWSER)
```

### Deep Link Web Only Fragments

The SDK can skip opening deep links with specified url fragments. In this case deep links will be opened directly in a web browser. To specify web only fragments use `deepLinkWebOnlyFragments` field from `CordialApiConfiguration` class:

```kotlin
CordialApiConfiguration.getInstance().deepLinkWebOnlyFragments =  listOf("webonly")
```

To open links that do not contain any fragment in a web browser, add an empty string to `deepLinkWebOnlyFragments`. This is a way to tell the SDK that links with an empty fragment (or no fragment) is a web only link.
```kotlin
CordialApiConfiguration.getInstance().deepLinkWebOnlyFragments =  listOf("webonly", "")
```

## Message attribution

To attribute future events the SDK sends to a message, a client app should explicitly set mcID of the message. Note, this typically should be done for inbox messages only as in-app messages and push notifications set mcID automatically when a user interacts with the message:

```kotlin
cordialApi.setMcID(mcID: String)
```

## Revenue Attribution for Web View Applications

If your application is built on a WebView that views a mobile friendly version of your website which is running the Cordial JavaScript Listener, you will need to change how you process deep links to fix attribution. This is because when a message is clicked, the Cordial SDK will store the `mcID`, but the website will not know about this `mcID`. To fix this in your implementation of the `CordialDeepLinksListener`, the app should pass the `vanityUrl` version of the deep link to the WebView which allows the JavaScript Listener to correctly store the `mcID` for event and order attribution.

```kotlin
override fun appOpenViaDeepLink(
        context: Context?,
        cordialDeepLink: CordialDeepLink,
        fallBackUri: Uri?,
        onComplete: ((action: DeepLinkAction) -> Unit)?
    ) {
        val url = (cordialDeepLink.vanityUri ?: cordialDeepLink.uri).toString()
        yourWebView.loadUrl(url)
}
```

## Setting a Contact Cart

The SDK allows track cart changes. First create a list of `CartItem` objects:
```kotlin
CartItem(productId: String, name: String, sku: String, category: String, qty: Int)
```
You can set additional parameters for the  `CartItem` object:
```kotlin
cartItem.withUrl(url: String?)
  .withItemDescription(itemDescription: String?)
  .withItemPrice(itemPrice: Double?)
  .withSalePrice(salePrice: Double?)
  .withAttr(attr: Map<String, String>?)
  .withImages(images: List<String>?)
  .withProperties(properties: Map<String, PropertyValue>?)
  .withTimestamp(date: Date)
```

Note, if the timestamp is not initialized, it will be initialized when the `CartItem` object is created.

After `CartItem` list is instantiated, pass it to the `CordialApi` :
```kotlin
cordialApi.upsertContactCart(cartItems: List<CartItem>)
```

## Sending an Order

To send an order create an instance of `Order` object:
```kotlin
Order(orderId: String, status: String, storeId: String, customerId: String,
      shippingAddress: Address, billingAddress: Address, items: List<CartItem>)

Address(name: String, address: String, city: String, state: String, postalCode: String, country: String)
```
You can set additional parameters for the  `Order` object:
```kotlin
order.withTax(tax: Double?)
  .withShippingAndHandling(shippingAndHandling: Double?)
  .withProperties(properties: Map<String, PropertyValue>?)
  .withPurchaseDate(date: Date)
```
Note, if the purchase date is not initialized, it will be initialized when the `Order` object is created.

After `Order` object is instantiated, pass it to the `CordialApi` :
```kotlin
cordialApi.sendContactOrder(order: Order)
```

## Logging

SDK allows to receive SDK logs in your application.

To do so, create a logger object implementing the `CordialLoggerListener` interface and set it to the SDK using the following call:

```kotlin
val logger = YourImplementationOfCordialLoggerListener()
CordialApiConfiguration.getInstance().setLogger(logger)
```

### Log Level

The log level feature allows you to control which log messages are displayed. The available log levels are:
- `ALL`: Logs everything.
- `INFO`: Logs info, debug, warnings and errors.
- `DEBUG`: Logs debug, warnings and errors.
- `WARNING`: Logs warnings and errors.
- `ERROR`: Logs errors only.
- `NONE`: No logs.

Set the log level using:

```kotlin
CordialApiConfiguration.getInstance().logLevel = CordialLogLevel.ALL
```

## Using Cordial SDK From Flutter

Cordial SDK can be used in a Flutter project but a Platform Channel must be set up first in your Flutter project. This article desribes the process: https://flutter.dev/docs/development/platform-integration/platform-channels
1. For example, let `cordialSDK` be the name of our channel:
```dart
static const platform = const MethodChannel('cordialSDK')
```
2. Next, invoke a method on the method channel. For example, we must tell SDK that we are logged in so let's use `"userLoggedIn"` method name. The call might fail, for example, if the platform does not support the platform API (such as when running in a simulator), so wrap the `invokeMethod` call in a try-catch statement.
```dart
try {
        final <T> result = await platform.invokeMethod('userLoggedIn',
            <String, dynamic>{
              "primaryKey" : "user123@example.com"
            });
      } on PlatformException catch (e) {
}
```
Where `"userLoggedIn"` is the name of the method, and the `"primaryKey"` and `"user123@example.com"` are the arguments that we can pass through the method channel. For our purpose, we need to pass the user identifier - `primary key`.

3. Next, go to Android application module. Cordial SDK must have been initialized at this point. You can initialize it in your Activity class, where
you use the SDK, or in your Application class, which is the recommended approach.

In order to initialize the Android SDK, pass your account key and channel key to `CordialApiConfiguration.initialize` method and call it from the `onCreate()` method of your Application class (must be a child of `FlutterApplication` class):

```kotlin
CordialApiConfiguration.getInstance()
     .initialize(applicationContext, accountKey = "<your_account_key>", channelKey = "<your_channel_key>")
```

4. In our MainActivity `onCreate()` method lets create a `MethodChannel` and call `setMethodCallHandler()`. Make sure to use the same channel name as was used on the Flutter client side.
```kotlin
private val CHANNEL = "cordialSDK"
MethodChannel(flutterView, CHANNEL).setMethodCallHandler { call, result ->
      if (call.method == "userLoggedIn") {
        val primaryKey = call.argument<String>("primaryKey")
        primaryKey?.let{ pk ->
           cordialApi.setContact(pk)
          }
        }
      } else {
        result.notImplemented()
      }
}
```

## Updating major SDK versions

### From v3.x to v4.x

Update the `uri: Uri?` param of `CordialDeepLinkOpenListener.appOpenViaDeepLink` to `cordialDeepLink: CordialDeepLink`. Instead of the v3.x `uri` param, use `cordialDeepLink.uri`.


[Top](#contents)
